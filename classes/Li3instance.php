<?php

  class Li3instance {

    /** @var string */
    public $UrlPath;
    /** @var string */
    public $StaticBasePath;
    /** @var string */  
    public $BasePath;
    /** @var string */ 
    protected $UserId;
    /** @var string */ 
    protected $UserName;
    /** @var string */
    protected $Token;
        
    /**
    * Default constructor
    * 
    * @param string  $urlPath
    * @param string  $userId
    * 
    * @return Li3page
    */
    public function __construct( $urlPath, $userId, $basePath, $staticBasePath ) {
      $this->UrlPath = $urlPath;
      $this->UserId = $userId;
      $this->BasePath = $basePath;
      $this->StaticBasePath = $staticBasePath;
      $this->UserName = $UserName;
      $this->Token = "499a3c3d198a41795ced7e2d7691648b375803c2d13c83a4f7d5af78e5ec1899";	  
    }
    
    public function Redirect( $path ) {
  
      header( "HTTP/1.1 301 Moved Permanently" );
      header( "Location: ".$path );
      die;
  
    }
    
    public function Reload( $path ) {
  
      header( "HTTP/1.1 301 Moved Permanently" );
      header( "Location: ".$this->UrlPath );
      die; 
  
    }

    // USER ID ===============================================
    public function GetUserId( ) {
      if( is_null( $this->UserId ) ) {
        $this->UserId = $_SESSION["userId"];
      }
      if( $this->UserId == "" ) {
        $this->UserId = null;
      }
      return $this->UserId;
    }
    public function EchoUserId( ) {
      if( is_null( $this->UserId ) ) {
        $this->UserId = $_SESSION["userId"];
      }
      if( $this->UserId == "" ) {
        $this->UserId = null;
      }
      echo $this->UserId;
    }    
    
    public function SetUserId( $userId ) {
      $this->UserId = $_SESSION["userId"] = $userId;
    }
    // =======================================================

	  // PROFILE NAME ==========================================
    public function GetUserName( ) {
      if( is_null( $this->UserName ) ) {
        $this->UserName = $_SESSION["UserName"];
      }
      if( $this->UserName == "" ) {
        $this->UserName = null;
      }
      return $this->UserName;
    }
    
    public function EchoUserName( ) {
      if( is_null( $this->UserName ) ) {
        $this->UserName = $_SESSION["UserName"];
      }
      if( $this->UserName == "" ) {
        $this->UserName = null;
      }
      echo $this->UserName;
    }
  
    public function SetUserName( $UserName ) {
      $this->UserName = $_SESSION["UserName"] = $UserName;
    }
    // =======================================================

    // USER ADMIN ============================================
    public function GetUserAdmin( ) {
      if( is_null( $this->UserAdmin ) ) {
        $this->UserAdmin = $_SESSION["UserAdmin"];
      }
      if( $this->UserAdmin == "" ) {
        $this->UserAdmin = null;
      }
      // return $this->UserAdmin;
      return $this->UserAdmin;
    }
    public function SetUserAdmin( $userId ) {
      $this->UserAdmin = $_SESSION["UserAdmin"] = $userId;
    }
    
    public function IsUserAdmin( $userId ) {
      
      $token = $this->Token;
      $url = "http://dash.reshape.net/api/ajax/entity/user/pull/typeCode?token=".$token."&id=".$userId."&typeCode=20";

      $AdminCheck = $this->GetAjaxReponse($url, $postData);  
      $AdminFlag = $AdminCheck['hasTypeCode'];
      
      return $AdminFlag;   
      
    }
    
    // =======================================================

    // PROFILE URL ===========================================
    public function GetProfileURL(){
      return "http://beta.reshape.net/Profile/";
    }
    // =======================================================

	  // PROFILE PIC ===========================================
    public function GetProfilePictureURL( ) {
      if( is_null( $this->ProfilePictureURL ) ) {
        $this->ProfilePictureURL = $_SESSION["ProfilePictureURL"];
      }
      if( $this->ProfilePictureURL == "" ) {
        $this->ProfilePictureURL = null;
      }
      return $this->ProfilePictureURL;
    }
    
    public function EchoProfilePictureURL( ) {
      if( is_null( $this->ProfilePictureURL ) ) {
        $this->ProfilePictureURL = $_SESSION["ProfilePictureURL"];
      }
      if( $this->ProfilePictureURL == "" ) {
        $this->ProfilePictureURL = null;
      }
      echo $this->ProfilePictureURL;
    }
  
    public function SetProfilePictureURL( $ProfilePictureURL ) {
      $this->ProfilePictureURL = $_SESSION["ProfilePictureURL"] = $ProfilePictureURL;
    }
    // ========================================================
  
    public function GetToken(){
      return $this->Token;
    }
    
    public function GetStaticPath( $subPath = null ) {
      return $this->StaticBasePath.$subPath;    
    }
    
    public function EchoStaticPath( $subPath = null ) {
      echo $this->GetStaticPath( $subPath );
    }
    
    public function EchoCurrentPath( ) {
      echo $this->UrlPath;
    }
    
    public function GetPath( $subPath ) {
      return $this->BasePath.$subPath;    
    }
    
    public function EchoPath( $subPath ) {
      echo $this->GetPath( $subPath );   
    }   

    public function TitleEchoStaticPath( $subPath = null ) {
      return $this->UrlPath;
    }

    public function TitleEchoStaticPathDisplay( $subPath = null ) {
      echo $this->UrlPath;
    }

    public function TitleEchoCompanyName() {
      return "ReShape.net";
    }
    
    public function EchoCompanyName() {
      echo "ReSphape.net";   
    }
	
    public function GetDomainNameURL(){
      return "http://dash.reshape.net/";
    }

    public function EchoDomainNameURL(){
      echo "http://dash.reshape.net/";
    }
    
    // Build the HTML format for options (for select HTML objects).
    // $array must be composed by at least 2 dimensions ("id" & "name" are mandatory).
    // $selectedOptionsArray is not mandatory ; this array contains all options that must be selected.
    // To use $selectedOptionsArray properly, the array must contain an "id" as dimension name.
    public function	SetOptions( $array, $selectedOptionsArray = null ){
      // For each option to add
      foreach( $array as $element ){
        $selectedTag = "";
        $actualId = $element['id'];
        // Check if the current option to add must be selected
        if($selectedOptionsArray){
          foreach($selectedOptionsArray as $selectOptionArray){
            foreach($selectOptionArray as $optionId){				
              // Mark the option as "selected"
              if($actualId == $optionId["id"]){
                $selectedTag = " selected";
                break;
              }
            }			
          }
        }
        // Add the HTML code
        echo "<option value=". $element['id'] . $selectedTag . ">" . $element['name'] . "</option>";
        // If the current element contains child attributes, call the same function recursively   
        if(sizeof($element["attributes"]) > 0){
          $this->SetOptions($element["attributes"], $selectedOptionsArray);
        }
      }
    }
		
    public function SetProfileBackgroundColor($profileBackgroundColor, $selectOptions){
      var_dump($profileBackgroundColor);
      foreach($selectOptions as $option){
        $optionColor = $option[0];
        $optionText  = $option[1];
        $selectedText = $optionColor == $profileBackgroundColor ? " selected" : ""; 
        echo "<option data-color='" . $optionColor ."' value='" . $optionColor . "' ". $selectedText .">". $optionText ."</option>";
      }		
    }

    public function GetAjaxReponse( $url, $postData ){
      $ch = curl_init($url);		
      curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array( 'Content-Type: application/json' ),
        CURLOPT_POSTFIELDS => $postData
      ));
      
      // Send the request
      $response = curl_exec($ch);
  
      // Check for errors
      if($response === FALSE){
        die(curl_error($ch));
      }
  
      // Decode the response
      $responseData = json_decode($response, TRUE);
  
      return $responseData;		
    }
    
  }
  
?>