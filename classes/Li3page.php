<?php

  class Li3page {
    /** @var string */
    public $FilePath;
    
    /** @var bpolean */
    public $ShowLayout;
    
    /**
    * Default constructor
    * 
    * @param string  $filePath
    * @param boolean $showNavi
    * 
    * @return Li3page
    */
    public function __construct( $filePath, $showLayout ) {
      $this->FilePath   = $filePath;
      $this->ShowLayout = $showLayout;
    }     

  }

?>