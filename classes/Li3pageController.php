<?php

  require_once $_SERVER['DOCUMENT_ROOT']."/classes/Li3page.php";

  class Li3pageController {

    /** @var string */
    protected $UrlPath;
    /** @var string */
    protected $Page404;
    /** @var boolean */
    protected $IsExecuted;
    /** @var Array */
    protected $PageMap; //Li3page[string]
    /** @var Array */
    protected $NaviTopSections;
    /** @var Array */
    protected $NaviBottomSections;
    /** @var Array */
    protected $RedirectMap;

    /**
    * Default constructor
    *
    * @param string $urlPath
    * @return Li3pageController
    */
    public function __construct( /** @var string */ $urlPath ) {
      $this->UrlPath            = $urlPath;
      $this->IsExecuted         = false;
      $this->PageMap            = new ArrayObject( );
      $this->NaviTopSections    = new ArrayObject( );
      $this->NaviBottomSections = new ArrayObject( );
      $this->RedirectMap        = new ArrayObject( );
    }

    /**
    * Method that maps url paths to individual pages
    *
    * @param string  $urlPath
    * @param string  $filePath
    * @param boolean $showNavigation
    *
    * @returns Li3page
    */
    public function DefinePage( $urlPath, $filePath, $showLayout = true ) {
      /** @var Li3page */
      $page = new Li3page( $filePath, $showLayout );

      $this->PageMap[$urlPath] = $page;

      return $page;
    }

    /**
    * Specify 301 redirect
    *
    * @param string $path
    * @param string $newPath
    *
    * @returns void
    */
    public function SetRedirect( $path, $newPath ) {
      if( !is_null( $path ) ) {
        $this->RedirectMap[$path] = $newPath;
      }
    }

    /**
    * Adds a filepath that will be loaded before a page
    *
    * @param mixed $filePath
    * @returns void
    */
    public function AddTopLayoutSection( $filePath ) {
      if( !is_null( $filePath ) ) {
        $this->NaviTopSections[] = $filePath;
      }
    }

    /**
    * Adds a filepath that will be loaded after a page
    *
    * @param mixed $filePath
    * @returns void
    */
    public function AddBottomLayoutSection( $filePath ) {
      if( !is_null( $filePath ) ) {
        $this->NaviBottomSections[] = $filePath;
      }
    }

    /**
    * Method that defines which
    *
    * @param string  $filePath
    * @param boolean $showNavigation
    *
    * @returns void
    */
    public function Define404Page( $filePath, $showLayout = false ) {
      $this->Page404 = $filePath;
    }

    /**
    * Method that loads active page
    * @return void
    */
    public function Execute( ) {

      if( !$this->IsExecuted ) {
        if( $this->RedirectMap->offsetExists( $this->UrlPath ) ) {
          /** @var string */
          $newPath = $this->RedirectMap[$this->UrlPath];

          header( "HTTP/1.1 301 Moved Permanently" );
          header( "Location: ".$newPath );

          $this->IsExecuted = true;

        } else if( $this->PageMap->offsetExists( $this->UrlPath ) ) {

          /** @var Li3page */
          $page = $this->PageMap[$this->UrlPath];

          if( !is_null( $page->FilePath ) ) {
            if( $page->ShowLayout ) {
              foreach( $this->NaviTopSections as $filePath ) {
                $this->LoadFile( $filePath );
              }
            }

            $this->LoadFile( $page->FilePath );

            if( $page->ShowLayout ) {
              foreach( $this->NaviBottomSections as $filePath ) {
                $this->LoadFile( $filePath );
              }
            }

            $this->IsExecuted = true;
          } else {
              $this->Return500( "Error - Undefined page path" );
          }
        } else {
          $this->Return404( );
        }
      }

      die( );

    }

    /**
    * Returns 404 http status to web client
    * @returns void
    */
    public function Return404( ) {
      if( !$this->IsExecuted ) {
        header( "HTTP/1.0 404 Not Found" );
        if( !is_null( $this->Page404 ) ) {
          $this->LoadFile( $this->Page404 );
        }
        $this->IsExecuted = true;
      }
    }

    /**
    * Returns 500 http status to web client
    * @returns void
    */
    public function Return500( $message = "Internal Error" ) {
      if( !$this->IsExecuted ) {
        header( "HTTP/1.0 500 ".$message );
        $this->IsExecuted = true;
      }
    }

    /**
    * Loads local file
    * @returns void
    */
    protected function LoadFile( $filePath ) {
      include_once $filePath;
    }
  }
?>