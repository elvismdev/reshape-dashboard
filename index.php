<?php

  ini_set('display_errors', 0);
  

  require_once "classes/Li3instance.php";
  require_once "classes/Li3pageController.php";

  /** @var Li3instance */
  global $Li3;

  session_start( );

  $urlPath = $_GET["HT_urlPath"];

  /** @var Li3instance */
  $Li3     = new Li3instance( $urlPath, $userId, "", "static/Li3/" );

  /** @var Li3pageController */
  $ctrl    = new Li3pageController( $urlPath );

  /** @var string */
  $userId  = $Li3->GetUserId( );

  /** @var string for admin */
  // $IsAdmin = $Li3->IsUserAdmin( $userId );

  if( !is_null( $Li3->GetUserId( ) ) ) {

    // Authenticated user flow

    // Order matters for layout sections
    $ctrl->AddTopLayoutSection( "layout/auth/head/Li3metaStartSection.php" );
    $ctrl->AddTopLayoutSection( "layout/auth/head/Li3metaSection.php" );
    $ctrl->AddTopLayoutSection( "layout/auth/head/Li3bodyStartSection.php" );
    $ctrl->AddTopLayoutSection( "layout/auth/side/Li3sidebarSection.php" );
    $ctrl->AddTopLayoutSection( "layout/auth/head/Li3headerSection.php" );

    $ctrl->AddBottomLayoutSection( "layout/auth/foot/Li3footerSection.php" );
    $ctrl->AddBottomLayoutSection( "layout/auth/foot/Li3bodyEndSection.php" );
    $ctrl->AddBottomLayoutSection( "layout/auth/foot/Li3metaEndSection.php" );

    $ctrl->Define404Page( "pages/auth/auth404.php" );

    // Define Individial pages | Last true/false determines whether layout will be included
    $ctrl->DefinePage( "Home", "pages/auth/home.php", true );
    $ctrl->DefinePage( "Profile", "pages/auth/profile.php", true );
    $ctrl->DefinePage( "Services", "pages/auth/services.php", true );
    $ctrl->DefinePage( "Sessions", "pages/auth/sessions.php", true );
    $ctrl->DefinePage( "Network", "pages/auth/network.php", true );
    $ctrl->DefinePage( "Messaging", "pages/auth/messaging.php", true );
    $ctrl->DefinePage( "Multimedia", "pages/auth/multimedia.php", true );
    $ctrl->DefinePage( "Multimedia2", "pages/auth/multimedia2.php", true );
    $ctrl->DefinePage( "AlbumMedia", "pages/auth/multimedia-album.php", true );
    $ctrl->DefinePage( "AlbumMedia2", "pages/auth/multimedia-album2.php", true );

    // If User is Admin
    if ( $IsAdmin == true ) {
      $ctrl->DefinePage( "Accounts", "pages/auth/accounts.php", true );
      $ctrl->DefinePage( "Attributes", "pages/auth/attributes.php", true );
      $ctrl->DefinePage( "loginas",  "pages/auth/loginas.php",  false );
      $ctrl->DefinePage( "logoutadmin",  "pages/auth/logoutadmin.php",  false );
    }

    $ctrl->DefinePage( "Profile-Client",  "pages/auth/profile-client.php",  true );

    $ctrl->DefinePage( "Logout", "pages/auth/logout.php", false );

    $ctrl->DefinePage( "test",  "pages/auth/test.php",  false );



    // Define redirects
    $ctrl->SetRedirect( "", "Home" );

  } else {

    // Unathenticated visitor
    $ctrl->Define404Page( "pages/public/public404.php" );

    // Define Individial pages | Last true/false determines whether layout will be included
    $ctrl->DefinePage( "Login",  "pages/public/login.php",  false );
    $ctrl->DefinePage( "LoginProcess",  "pages/public/login_process.php",  false );
    $ctrl->DefinePage( "SignUp", "pages/public/signup.php", false );
    $ctrl->DefinePage( "Session", "static/session-execution/index.php", false );

    // Define redirects
    $ctrl->SetRedirect( "", "Login" );

  }
  $ctrl->Execute( );

?>