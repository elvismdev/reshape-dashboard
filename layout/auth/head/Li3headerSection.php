<?php

  /** @var Li3instance */
  global $Li3;

  $token = $Li3->GetToken();
  $userId = $Li3->GetUserId();
  $userName = $Li3->GetUserName( );
  
  // Professional info
  $proResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/entity/user/pull/userInfo?token=". $token. "&id=". $userId, null); 

  $ind_firstName = $proResponseData["user"]["firstName"];
  $ind_lastName = $proResponseData["user"]["lastName"];
  $ind_email = $proResponseData["user"]["email"];
  $ind_dob = date("m/d/Y", strtotime($proResponseData["user"]["dob"]));
  $ind_phone = $proResponseData["user"]["phone"];
  
?>

<div id="main-container">
<header class="navbar-fixed-top navbar navbar-default" style="min-height: 66px;">
				<div class="sidebar-brand pull-left"> <a href="/"><img src="/static/Li3/img/ReShape-Dashboard-Logo.png"></a> </div>
  <ul class="nav navbar-nav-custom hidden-lg hidden-md">
    <li> <a title="" href="javascript:void(0)" data-toggle="button" onclick="App.sidebar('toggle-sidebar');"> <i class=" fa fa-bars fa-fw"></i> </a> </li>
  </ul>
  <ul class="nav navbar-nav-custom pull-right">
    <li class="dropdown">
      <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
<!--        <li> <a href="#"> <i class="fa fa-clock-o fa-fw pull-right"></i> <span class="badge pull-right">3</span> Sessions pending </a> <a href="#"> <i class="fa fa-envelope-o fa-fw pull-right"></i> <span class="badge pull-right">5</span> Messages </a> <a href="#"> <i class="fa fa-user fa-fw pull-right"></i> Profile </a> </li>
        <li class="divider"></li>
-->        <li> <a href="#account-info" data-toggle="modal"> <i class="fa fa-cog fa-fw pull-right"></i> Account Settings </a> </li>
        <li class="divider"></li>
        <li> <a href="#account-logout" data-toggle="modal"><i class="fa fa-ban fa-fw pull-right"></i> Logout </a> </li>
      </ul>
      <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <img src="<?php	$Li3->EchoProfilePictureURL( ); ?>" alt="" /> <i class=" fa fa-angle-down white-text"></i> </a> </li>
  </ul>
</header>
				
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="account-info" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title">Account Settings </h2>
      </div>
      <div class="modal-body">
      
        <form class="form-horizontal form-bordered modal-padding" id="ptsid_41_11-form" action="/profile/info" method="post" enctype="multipart/form-data" novalidate>
          <div class="block-full"></div>
          <fieldset>
            <legend>Personal Information </legend>
            <div id="error_profile" style="text-align:center; padding-top:10px; color:#d22130; display:none;">123</div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="ind_firstName">First Name <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="text" class="form-control" id="ind_firstName" placeholder="First Name" name="ind_firstName" value="<?php echo $ind_firstName; ?>" onKeyDown="hide_profile_error();">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="ind_lastName">Last Name <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="text" class="form-control" id="ind_lastName" placeholder="Last Name" name="ind_lastName" value="<?php echo $ind_lastName; ?>" onKeyDown="hide_profile_error();">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="ind_dob">Date of Birth <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="text" id="ind_dob" name="ind_dob" class="form-control input-datepicker-close" data-date-format="mm/dd/yyyy" placeholder="mm/dd/yyyy" value="<?php echo $ind_dob; ?>" onKeyDown="hide_profile_error();">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="ind_phone">Phone </label>
              <div class=" col-md-8 ">
                <input type="text" class="form-control" id="ind_phone" placeholder="(999) 999-9999" name="ind_phone" value="<?php echo $ind_phone; ?>" onKeyDown="hide_profile_error();">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="ind_email">Email <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="text" class="form-control" id="ind_email" placeholder="name@domain.tld" name="ind_email" value="<?php echo $ind_email; ?>" onKeyDown="hide_profile_error();"`>
              </div>
            </div>
            <legend>Login Information </legend>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="username1">Username <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="text" class="form-control" id="username1" placeholder="admin" name="username1" autocomplete="off" value="<?php echo $userName; ?>">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="password0">Old Password <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="password" class="form-control" id="password0" placeholder="Enter your old password" name="password0" autocomplete="off">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="password1">New Password <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="password" class="form-control" id="password1" placeholder="Please choose a complex one.." name="password1" autocomplete="off">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="password1reconfirm">Confirm New Password <span class="text-danger">* </span> </label>
              <div class=" col-md-8 ">
                <input type="password" class="form-control" id="password1reconfirm" placeholder="..and confirm it!" name="password1reconfirm" autocomplete="off">
              </div>
            </div>
          </fieldset>
          <div class="form-group">
            <div class="col-xs-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default hidden-sm">Cancel </button>
              <button type="button" class="btn btn-sm btn-primary hidden-sm" onClick="update_account();">Update </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

			
				
				
			

<script type="text/javascript" language="javascript" src="/static/Li3/js/jquery.maskedinput.min.js"></script> 
    
<script>

  jQuery(function($){
    $("#ind_dob").mask("99/99/9999");
    $("#ind_phone").mask("(999) 999-9999");
  });

  function update_account() {
    var username_f               = $("#username1");
    var password_f               = $("#password0");
    var password_new_f           = $("#password1");
    var password_reconfirm_new_f = $("#password1reconfirm");
    var actual_username = "<?php echo $userName; ?>";
    
    if ( $('#ind_firstName').val() == "" ) {
      // $('#ind_firstName').addClass('has-error');
      $('#error_profile').html('First Name is a required field');
      $('#ind_firstName').focus();
      show_profile_error();
      return false;
    } else if ( $('#ind_lastName').val() == "" ) {
      // $('#ind_firstName').addClass('has-error');
      $('#error_profile').html('Last Name is a required field');
      $('#ind_lastName').focus();
      show_profile_error();
      return false;
    } else if ( $('#ind_lastName').val() == "" ) {
      // $('#ind_firstName').addClass('has-error');
      $('#error_profile').html('Last Name is a required field');
      $('#ind_lastName').focus();
      show_profile_error();
      return false;
    } else if ( $('#ind_lastName').val() == "" ) {
      // $('#ind_firstName').addClass('has-error');
      $('#error_profile').html('Last Name is a required field');
      $('#ind_lastName').focus();
      show_profile_error();
      return false;
    }
    
    var post_data = {	
      token: '1234',
      firstName: $('#ind_firstName').val(),
      lastName: $('#ind_lastName').val(),        
      email: $('#ind_email').val(),        
      phone: $('#ind_phone').val(),        
      dob: $('#ind_dob').val()      
    };
    var ActionURL = "http://dash.reshape.net/api/ajax/entity/user/push/userInfo"; 

    var request = $.ajax({
      type: "POST",
      url: ActionURL,
      dataType: "json",
      xhrFields: { withCredentials: true },
      crossDomain: true,
      data: post_data
    }).done(function(msg) {
      
      // var jsonString = JSON.stringify(msg);
      // alert((jsonString));

      // UPDATE LOGIN INFORMATION
      $('#account-info').modal('hide');
      
      if ( ( $('#ind_firstName').val() + ' ' + $('#ind_lastName').val() ) != '<?php echo $ind_firstName." ".$ind_lastName; ?>' ) {        
        location.reload();
        
        
        // Push login update if : username has changed OR new password is defined
        /*if( ( username_f.val() != actual_username ) ){
          console.log("user name" + username_f.val() );
          var post_data = {	
            token: '1234',
            username: username_f.val(),            
          };
          var ActionURL = "http://dash.reshape.net/api/ajax/entity/user/push/loginInfo";
          
          var request = $.ajax({
            type: "POST",
            url: ActionURL,
            dataType: "json",
            xhrFields: { withCredentials: true },
            crossDomain: true,
            data: post_data
          }).done( function( response ) {
            if( response.error == "false" ){
              console.log( response );
            } else { 
              console.log( "ERROR " + response );
            }
          
          });
          
        }*/
        
      }
    });
  }
  
  function show_profile_error() {
    $('#error_profile').show();  
  }
  function hide_profile_error() {
    $('#error_profile').hide();  
  }
  
</script>