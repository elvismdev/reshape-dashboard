<?php

  /** @var Li3instance */ 
  global $Li3;

  $page_title = $Li3->TitleEchoStaticPath();

  if ( $page_title == "Profile" ) { $page_title = "Professional Profile"; }
  if ( $page_title == "Profile-Client" ) { $page_title = "Client Profile"; }
  if ( $page_title == "Services-Offered" ) { $page_title = "Services Offered"; }
  if ( $page_title == "AlbumMedia" ) { $page_title = "Album :: Multimedia"; }


  $company_name = $Li3->TitleEchoCompanyName();
  $this_page = $page_title." :: ".$company_name;

  // DEFINE BREADCRUMBS PATH
  $breadcrumb_this = $page_title; 

?>

<!-- __PTS_HEAD__ -->
<head>
<!-- __PTS_META__ -->
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
<!-- __PTS_META__ -->
<title><?php echo $this_page; ?></title>
<!-- __PTS_CSS__ -->
<link href="<?php $Li3->EchoStaticPath( "back/css/bootstrap.min.css" ); ?>" type="text/css" rel="stylesheet" />
<link href="<?php $Li3->EchoStaticPath( "back/css/plugins.css" ); ?>" type="text/css" rel="stylesheet" />
<link href="<?php $Li3->EchoStaticPath( "back/css/main.css" ); ?>" type="text/css" rel="stylesheet" />
<link href="<?php $Li3->EchoStaticPath( "back/css/themes.css" ); ?>" type="text/css" rel="stylesheet" />
<link href="<?php $Li3->EchoStaticPath( "back/css/themes/night.css" ); ?>" type="text/css" rel="stylesheet" />
<link href="<?php $Li3->EchoStaticPath( "back/css/sweetalert/sweet-alert.css" ); ?>" type="text/css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,700,500' rel='stylesheet' type='text/css'>
<link rel="icon" type="image/x-icon" href="/static/img/favicon.ico" />
<!-- __PTS_CSS__ -->
<!-- __PTS_JS__ -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<script src="<?php $Li3->EchoStaticPath( "back/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js" ); ?>" type="text/javascript"></script>
<script src="<?php $Li3->EchoStaticPath( "back/js/vendor/bootstrap.min.js" ); ?>" type="text/javascript"></script>
<script src="<?php $Li3->EchoStaticPath( "back/js/plugins.js" ); ?>" type="text/javascript"></script>
<script src="<?php $Li3->EchoStaticPath( "back/js/app.js" ); ?>" type="text/javascript"></script>
<script src="<?php $Li3->EchoStaticPath( "back/js/sweetalert/sweet-alert.min.js" ); ?>" type="text/javascript"></script>
<!-- __PTS_JS__ -->
</head>
<!-- __PTS_HEAD__ -->