<?php

  /** @var Li3instance */
  global	$Li3;

  // DEFINE ACTIVE PAGE FOR NAVBAR
  if ( $Li3->TitleEchoStaticPath() == "Home" ) { $home_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Profile" ) { $profile_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Profile-Client" ) { $client_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Services" ) { $services_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Sessions" ) { $sessions_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Network" ) { $network_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Messaging" ) { $messaging_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Multimedia" ) { $multimedia_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "AlbumMedia" ) { $multimedia_active = "active"; }
  if ( $Li3->TitleEchoStaticPath() == "Accounts" ) { $accounts_active = "active"; $admin_open = "open"; $display_admin= "style=\"display:block\""; }
  if ( $Li3->TitleEchoStaticPath() == "Attributes" ) { $attributes_active = "active"; $admin_open = "open"; $display_admin= "style=\"display:block\""; }

  // Professional info
  $token = $Li3->GetToken();
  $userId = $Li3->GetUserId();
  $url = "http://dash.reshape.net/api/ajax/profile/info?token=".$token."&id=".$userId;    
  $ProcessProfileBasic = $Li3->GetAjaxReponse($url, $postData);  
  $ProfileName = $ProcessProfileBasic['name'];
  $Li3->SetUserName( $ProfileName );

  $IsAdmin = $Li3->IsUserAdmin( $userId );

  if ( $IsAdmin == 'true' ) {
    $AdminOptions = "
      <li> <a href=\"#\" class=\"sidebar-nav-menu ".$admin_open."\"> <i class=\"sidebar-nav-icon gi gi-settings\"></i> <i class=\"sidebar-nav-indicator fa fa-angle-left\"></i>Administration </a>
        <ul ".$display_admin.">
          <li> </li>
          <li> <a href=\"/Accounts\" class=\"".$accounts_active."\">System Accounts </a> </li>
          <li> </li>
          <li> <a href=\"/Attributes\" class=\"".$attributes_active."\">Dynamic Attributes </a> </li>
        </ul>
      </li>  
    ";
  }

  $UserAdmin = $Li3->GetUserAdmin();
  if ( $UserAdmin != "" ) {
    $logout_text = "Logout Admin";
    $logout_path = "/logoutadmin?id=".$UserAdmin;
  } else {
    $logout_text = "Logout";
    $logout_path = "/Logout";
  }

?>
  
  <div id="sidebar-alt">
    <div class="sidebar-scroll">
      <div class="sidebar-content"> <a href="#" class="sidebar-title"> <i class="pull-right fa fa-heart"></i> <strong></strong> </a>
        <div></div>
      </div>
    </div>
  </div>
  <div id="sidebar" style="position:fixed;">
    <div class="sidebar-scroll">
      <div class="sidebar-content">
        <div class="sidebar-brand"> <a href="/"><img src="/static/Li3/img/ReShape-Dashboard-Logo.png" /></a> </div>
        <div class="sidebar-section sidebar-user clearfix" style="height:100%;">
          <div style="display:inline-block; vertical-align:middle; margin-top:8px;">
            <div class="sidebar-user-avatar"> <img src="<?php	$Li3->EchoProfilePictureURL( ); ?>" alt="<?php	$Li3->EchoUserName( ); ?>" /> </div>
            <div class="sidebar-user-name" style="margin-top:0px;"><?php	$Li3->EchoUserName( ); ?></div>
            <div class="sidebar-user-links"> <a href="#" data-toggle="tooltip" title="Live Profile" data-placement="bottom"> <i class=" gi gi-user"></i> </a> 
              <a title="Messaging" href="/Messaging" data-placement="bottom" data-toggle="tooltip"> <i class=" gi gi-envelope"></i> </a> 
              <a href="#account-logout" title="Log out" data-toggle="modal"> <i class=" gi gi-exit"></i> </a> </div>
          </div>
        </div>
        <ul class="sidebar-nav">
          <li> <a href="<?php	$Li3->EchoPath("home");	?>" class="<?php echo $home_active; ?>"> <i class="sidebar-nav-icon fa fa-home"></i>Home </a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Profile-Client");	?>" class="<?php echo $client_active; ?>"> <i class="sidebar-nav-icon gi gi-user"></i>Client Profile </a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Profile");	?>" class="<?php echo $profile_active; ?>"> <i class="sidebar-nav-icon gi gi-user"></i>Professional Profile </a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Services");	?>" class="<?php echo $services_active; ?>"> <i class="sidebar-nav-icon fa fa-certificate"></i>Services Offered</a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Sessions");	?>" class="<?php echo $sessions_active; ?>"> <i class="sidebar-nav-icon gi gi-stopwatch"></i>Sessions </a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Network");	?>" class="<?php echo $network_active; ?>"> <i class="sidebar-nav-icon gi gi-parents"></i>Network </a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Messaging");	?>" class="<?php echo $messaging_active; ?>"> <i class="sidebar-nav-icon gi gi-envelope"></i>Messaging </a> </li>
          <li> <a href="<?php	$Li3->EchoPath("Multimedia");	?>" class="<?php echo $multimedia_active; ?>"> <i class="sidebar-nav-icon fa fa-image"></i>Multimedia </a> </li>
          <?php echo $AdminOptions; ?>
        </ul>
        <div class="sidebar-header"></div>
      </div>
								
								 <div class="row bottom-footer">
              <div class="col-md-12" style="margin-left: 17px;">
                <img class="ricon" src="http://beta.reshape.net//bundles/reshape/assets/img/icon-64.png">
                <span style="color:#adadad; font-size:12px; line-height:17px; font-family: 'Open Sans', sans-serif;">&copy; 2014 Reshape, Inc. <br>All Rights Reserved</span>
              </div>
            
            </div>
      
    </div>
						
  </div>
  
  <!-- MODAL FORMS START -->
  <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="ptsid_41_11">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center">
          <div class="form-group form-actions"></div>
          <h2 class="modal-title">Account Information </h2>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-bordered" id="ptsid_41_11-form" action="/home" method="post" enctype="multipart/form-data">
            <div class="block-full"></div>
            <script></script>
            <input type="hidden" name="ptshiddenfield" value="ptsid_41_11-form" />
            <fieldset>
              <legend>Personal Info </legend>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="firstName">First Name <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="firstName" placeholder="First Name" name="field1" value="Paul" />
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="lastName">Last Name <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="lastName" placeholder="Last Name" name="field2" value="Tesar" />
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="dateOfBirth1">Date of Birth <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="dateOfBirth1" placeholder="dd/mm/yyyy" name="dateOfBirth1" value="9/13/1987" />
                </div>
                <script>$(document).ready(function () {
                                    $('#dateOfBirth1').mask('99/99/9999');
                                });</script> 
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="masked_phone">Phone </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="masked_phone" placeholder="(999) 999-9999" name="field4" value="(731) 487-6766" />
                </div>
                <script>$('#masked_phone').mask('(999) 999-9999');
                              </script> 
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="email1">Email <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="email1" placeholder="name@domain.tld" name="email1" value="paultesar@gmail.com" />
                </div>
              </div>
            </fieldset>
            <div class="form-group form-actions">
              <div class="col-xs-12 text-right">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Cancel </button>
                <button type="submit" class="btn btn-sm btn-primary  hidden-sm">Save </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script></script> 
  </div>
  <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="ptsid_98_12">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center">
          <div class="form-group form-actions"></div>
          <h2 class="modal-title">Login Information </h2>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-bordered" id="ptsid_98_12-form" action="/home" method="post" enctype="multipart/form-data">
            <div class="block-full"></div>
            <script>var FormsValidation = function () {
                            return {
                                init: function () {
                                    $('#ptsid_98_12-form').validate({
                                        errorClass: 'help-block animation-slideDown',
                                        errorElement: 'div',
                                        errorPlacement: function (error, e) {
                                            e.parents('.form-group > div').append(error);
                                        },
                                        highlight: function (e) {
                                            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                                            $(e).closest('.help-block').remove();
                                        },
                                        success: function (e) {
                                            e.closest('.form-group').removeClass('has-success has-error');
                                            e.closest('.help-block').remove();
                                        },
                                        rules: {username1: {required: true, minlength: 5}, password0: {required: true, minlength: 5}, password1: {required: true, minlength: 5}, password1reconfirm: {required: true, equalTo: '#password1', }, },
                                        messages: {username1: {required: 'Please enter a username', minlength: 'Your username must consist of at least 5 characters'}, password0: {required: 'Please provide a password', minlength: 'Your password must be at least 5 characters long'}, password1: {required: 'Please provide a password', minlength: 'Your password must be at least 5 characters long'}, password1reconfirm: {required: 'Please provide a password', equalTo: 'Please enter the same password as above'}, }
                                    });
                                }};
                        }();
                        $(function () {
                            FormsValidation.init();
                        });</script>
            <input type="hidden" name="ptshiddenfield" value="ptsid_98_12-form" />
            <fieldset>
              <legend>Login Update </legend>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="username1">Username <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="username1" placeholder="admin" name="username1" autocomplete="off" value="paultesar" />
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="password0">Old Password <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="password" class="form-control" id="password0" placeholder="Enter your old password" name="password0" autocomplete="off" />
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="password1">New Password <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="password" class="form-control" id="password1" placeholder="Please choose a complex one.." name="password1" autocomplete="off" />
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="password1reconfirm">Confirm New Password <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="password" class="form-control" id="password1reconfirm" placeholder="..and confirm it!" name="password1reconfirm" autocomplete="off" />
                </div>
              </div>
            </fieldset>
            <div class="form-group form-actions">
              <div class="col-xs-12 text-right">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
                <button type="submit" class="btn btn-sm btn-primary  hidden-sm">Save Changes </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
				
  <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="account-logout" style="display: none;">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header text-center modal-bg">
          <h4 class="modal-title"><img src="<?php	$Li3->EchoProfilePictureURL( ); ?>" alt="<?php	$Li3->EchoUserName( ); ?>" style="border-radius:100px; width:150px;" /></h4>
        </div>
        <div class="modal-body">
          <div class="block-full"></div>
          <fieldset>
            <h4 class="text-center col-xs-12">Are you sure you want to log out?</h4>
          </fieldset>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-12 text-center">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default hidden-sm">Cancel </button>
                <button type="button" class="btn btn-sm btn-primary hidden-sm" onClick="window.location='<?php echo $logout_path; ?>';"><?php echo $logout_text; ?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    
  	
  <!--<script type="text/javascript">
    (function ( ) {
      var ele = window.document.getElementById("ptsid_212_18");
      var fc = function (ele, ev) {
        swal({title: 'Are you sure?', text: 'You are about to sign out.', type: 'warning', showCancelButton: true, }, function ( ) {
          window.location.href = ele.href;
        });
      };
      var evl = null;
      var cb = function (ev) {
        ev.preventDefault( );
        fc(ele, ev);
      };
      evl = ele.addEventListener("click", cb, true);
    })( );
  </script> -->
  <!-- MODAL FORMS END -->