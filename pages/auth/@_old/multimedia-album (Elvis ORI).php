<?php
/** @var Li3instance */
global	$Li3;
$page_title = "Multimedia";
$page_description = "Edit your Albums, Photos and Videos";
$page_icon = "sidebar-nav-icon fa fa-image";
include($_SERVER['DOCUMENT_ROOT']."/static/inc/page_content_beg.php");

  // API RELATED FIELDS ===============
$token = $Li3->GetToken();
$userId = $Li3->GetUserId();

  // MAKE API CALL & OUTPUT JSON TO PHP ARRAY
$proResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/profile/media?token=". $token. "&id=". $userId ."&isActive=null&itemSquareThumb=true", null);
$proResponseDataJson = json_encode($proResponseData['albums']);

?>

<!-- <pre style="background-color:#FFFFFF;">
<?php //print_r($proResponseData['albums']); exit(); ?>
</pre> -->

<div class="block">

  <!-- PAGE SPECIFIC CONTENT BEG -->
  <div class="block-title network-filter">

    <div class="col-md-4">
      <button class="btn btn-primary newservicebtn" data-toggle="modal" data-target="#modal-new-image-album" onClick="addImageAlbum();">ADD NEW IMAGE</button>
    </div>

    <div class="col-md-4 text-center" id="albumTitle">
      <?php if ($proResponseData['albums']) { ?>
      <h2>All Albums</h2>
      <?php } ?>
    </div>

    <div class="col-md-4" style="margin-top: 3px; float: right;">
      <?php if ($proResponseData['albums']) { ?>
      <div class="btn-group pull-right" style="margin-right:-10px;">
        <a href="javascript:void(0)" data-toggle="dropdown" class="btn info-btn dropdown-toggle">SWITCH ALBUM<span class="caret"></span></a>
        <ul class="dropdown-menu text-left" style="margin-left: -58px;">
          <li>
            <a class="selectItem" href="#">All Albums</a>
          </li>
          <?php foreach ($proResponseData['albums'] as $album) { ?>
          <li>
            <a class="selectItem" href="#"><?php echo $album['name']; ?></a>
          </li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
    </div>
  </div>

  <div class="row" id="galleryDivParent" style="margin-bottom: 7px;">

    <div id="galleryDiv" style="width:100%;"></div>

    <!-- grid data list -->
    <ul id="playlist" style="display: none;">
      <!-- skin -->
      <ul data-skin="">
        <li data-preloader-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/rotite-30-29.png"></li>
        <li data-show-more-thumbnails-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/showMoreThumbsNormalState.png"></li>
        <li data-show-more-thumbnails-button-selectsed-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/showMoreThumbsSelectedState.png"></li>
        <li data-image-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/photoIcon.png"></li>
        <li data-video-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/videoIcon.png"></li>
        <li data-link-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/linkIcon.png"></li>
        <li data-iframe-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/iframeIcon.png"></li>
        <li data-hand-move-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/handnmove.cur"></li>
        <li data-hand-drag-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/handgrab.cur"></li>
        <li data-combobox-down-arrow-icon-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/combobox-down-arrow.png"></li>
        <li data-combobox-down-arrow-icon-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/combobox-down-arrow-rollover.png"></li>
        <li data-lightbox-slideshow-preloader-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/slideShowPreloader.png"></li>
        <li data-lightbox-close-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/galleryCloseButtonNormalState.png"></li>
        <li data-lightbox-close-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/galleryCloseButtonSelectedState.png"></li>
        <li data-lightbox-next-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/nextIconNormalState.png"></li>
        <li data-lightbox-next-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/nextIconSelectedState.png"></li>
        <li data-lightbox-prev-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/prevIconNormalState.png"></li>
        <li data-lightbox-prev-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/prevIconSelectedState.png"></li>
        <li data-lightbox-play-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/playButtonNormalState.png"></li>
        <li data-lightbox-play-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/playButtonSelectedState.png"></li>
        <li data-lightbox-pause-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/pauseButtonNormalState.png"></li>
        <li data-lightbox-pause-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/pauseButtonSelectedState.png"></li>
        <li data-lightbox-maximize-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/maximizeButtonNormalState.png"></li>
        <li data-lightbox-maximize-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/maximizeButtonSelectedState.png"></li>
        <li data-lightbox-minimize-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/minimizeButtonNormalState.png"></li>
        <li data-lightbox-minimize-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/minimizeButtonSelectedState.png"></li>
        <li data-lightbox-info-button-open-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonOpenNormalState.png"></li>
        <li data-lightbox-info-button-open-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonOpenSelectedState.png"></li>
        <li data-lightbox-info-button-close-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonCloseNormalPath.png"></li>
        <li data-lightbox-info-button-close-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonCloseSelectedPath.png"></li>
      </ul>

      <!-- image playlist category  -->
      <?php if ($proResponseData['albums']) { ?>
      <?php foreach ($proResponseData['albums'] as $album) { ?>
      <ul data-cat="<?php echo $album['name']; ?>">
        <?php foreach ($album['albumItems'] as $image) { ?>
        <ul>
          <li data-type="link" data-url="javascript:editImage(<?php echo "{$image['id']}, {$album['id']}, '{$image['videoId']}'"; ?>);" data-target="_self"></li>
          <li data-thumbnail-path="<?php echo $image['thumbUrl'] ?>"></li>
          <li data-thumbnail-text>
            <p class="largeLabel">
              <i class="fa fa-pencil-square-o fa-2x"></i>
            </p>
          </li>
        </ul>
        <?php } ?>
      </ul>
      <?php } ?>
      <?php } else { ?>
      <ul data-cat="NO ALBUM"></ul>
      <?php } ?>

    </ul>

      <!-- Multimedia Content Here
      <pre style="background-color:#FFFFFF;">
      <?php //print_r($proResponseData); ?>
    </pre> -->
  </div>

  <!-- PAGE SPECIFIC CONTENT END -->

</div>
</div>

<div id="modal-new-image-album" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-actions"></div>
        <h2 id="AddEditModal" class="modal-title">Add New Image or Album</h2>
      </div>
      <div class="modal-body">
        <form id="AddEditServiceForm" class="form-horizontal form-bordered modal-padding" id="modal-edit-service-form" action="" method="post" enctype="multipart/form-data" novalidate>
          <div class="block-full"></div>
          <input type="hidden" id="currentAction">
          <fieldset>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="selectedAlbum">Album </label>
              <div class=" col-md-8 ">
                <select class="form-control" id="selectedAlbum" name="selectedAlbum" style="height:34px;" size="1">
                  <?php foreach ($proResponseData['albums'] as $album) { ?>
                  <option value="<?php echo $album['id']; ?>"><?php echo $album['name']; ?></option>
                  <?php } ?>
                </select>
                <a href="#" id="newAlbumTgl" style="margin-top:6px; float:left; margin-left:3px;">Add new album</a>
                <div class="input-group" id="albumNameInput" style="display: none; float:left;">
                  <input type="text" id="albumName" class="form-control" placeholder="New album name">
                  <span class="input-group-addon" id="addAlbum" style="cursor: pointer;">
                    <i class="fa fa-check"></i>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="youtubeVideo">Video </label>
              <div class=" col-md-8 ">
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-youtube fa-lg"></i></span>
                  <input type="text" id="youtubeVideo" name="youtubeVideo" class="form-control" placeholder="Youtube Video Url or ID"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="main-title">Picture</label>
                <div class="col-md-8">
                  <!--<input id="profilePic" class="form-control ui-wizard-content" type="file" style="height: auto;">-->
                  <div id="service-image-editor">
                    <!-- .cropit-image-preview-container is needed for background image to work -->
                    <div class="cropit-image-preview-container">
                      <input id="chose-service-pic" type="file" class="cropit-image-input">
                      <div data-name="pic" class="cropit-image-preview"></div>
                      <br/>
                      <div class="col-md-3">
                        <input data-name="pic" type="range" class="cropit-image-zoom-input" style="width:118px;">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
            <input type="hidden" name="formId" value="modal-new-service">
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button id="deleteImg" type="button" data-dismiss="modal" class="btn btn-sm btn-danger hidden-sm pull-left">Delete Image </button>
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default hidden-sm">Cancel </button>
                <button id="AddEditButton" type="button" class="btn btn-sm btn-primary hidden-sm" onClick="saveImg($('#currentAction').val());">Save </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- JS CODE GOES IN HERE -->

  <script type="text/javascript" src="static/Li3/js/responsive-gridfolio/FWDGrid.js"></script>
  <script type="text/javascript" src="static/Li3/js/imagecrop/jquery.cropit.min.js"></script>
  <script type="text/javascript">
  //FWDConsole.setPrototype();
      //var logger =  new FWDConsole()

      var grid;
      var albums_json = $.parseJSON('<?php echo $proResponseDataJson; ?>');

      FWDUtils.onReady(function(){
        create();
      })


      $('.selectItem').click(function () {
        destroy($(this).html());
      })

      function destroy(name){
        grid = null;
        $("#galleryDiv").children().remove();
        category(name);
        create();
      }

      function create(){

        grid = new FWDGrid({
        //main settings
        gridHolderId:"galleryDiv",
        gridPlayListAndSkinId:"playlist",
        showContextMenu:"no",
        //grid settings
        thumbnailOverlayType:"text",
        addMargins:"yes",
        loadMoreThumbsButtonOffest:0,
        thumbnailBaseWidth:200,
        thumbnailBaseHeight:200,
        // nrOfThumbsToShowOnSet:12,
        horizontalSpaceBetweenThumbnails:0,
        verticalSpaceBetweenThumbnails:0,
        thumbnailBorderSize:0,
        thumbnailBorderRadius:0,
        thumbnailOverlayOpacity:.85,
        backgroundColor:"transparent",
        thumbnailOverlayColor:"#000000",
        thumbnailBackgroundColor:"transparent",
        thumbnailBorderNormalColor:"#FFFFFF",
        thumbnailBorderSelectedColor:"#FFFFFF",
        //combobox settings
        startAtCategory:1,
        selectLabel:"SELECT ALBUM",
        allCategoriesLabel:"All Albums",
        showAllCategories:"yes",
        comboBoxPosition:"topleft",
        selctorBackgroundNormalColor:"#FFFFFF",
        selctorBackgroundSelectedColor:"#000000",
        selctorTextNormalColor:"#000000",
        selctorTextSelectedColor:"#FFFFFF",
        buttonBackgroundNormalColor:"#FFFFFF",
        buttonBackgroundSelectedColor:"#000000",
        buttonTextNormalColor:"#000000",
        buttonTextSelectedColor:"#FFFFFF",
        comboBoxShadowColor:"#000000",
        comboBoxHorizontalMargins:2,
        comboBoxVerticalMargins:-30,
        comboBoxCornerRadius:0,
        //ligtbox settings
        addLightBoxKeyboardSupport:"yes",
        showLightBoxNextAndPrevButtons:"yes",
        showLightBoxZoomButton:"yes",
        showLightBoxInfoButton:"yes",
        showLighBoxSlideShowButton:"yes",
        showLightBoxInfoWindowByDefault:"no",
        slideShowAutoPlay:"no",
        lightBoxVideoAutoPlay:"yes",
        lighBoxBackgroundColor:"#000000",
        lightBoxInfoWindowBackgroundColor:"#FFFFFF",
        lightBoxItemBorderColor:"#FFFFFF",
        lightBoxItemBackgroundColor:"#222222",
        lightBoxMainBackgroundOpacity:.8,
        lightBoxInfoWindowBackgroundOpacity:.9,
        lightBoxBorderSize:1,
        lightBoxBorderRadius:0,
        lightBoxSlideShowDelay:4
      });
}

function category(name) {
  var html = '<ul id="playlist" style="display: none;">' +
  '<ul data-skin="">' +
  '<li data-preloader-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/rotite-30-29.png"></li>' +
  '<li data-show-more-thumbnails-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/showMoreThumbsNormalState.png"></li>' +
  '<li data-show-more-thumbnails-button-selectsed-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/showMoreThumbsSelectedState.png"></li>' +
  '<li data-image-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/photoIcon.png"></li>' +
  '<li data-video-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/videoIcon.png"></li>' +
  '<li data-link-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/linkIcon.png"></li>' +
  '<li data-iframe-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/iframeIcon.png"></li>' +
  '<li data-hand-move-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/handnmove.cur"></li>' +
  '<li data-hand-drag-icon-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/handgrab.cur"></li>' +
  '<li data-combobox-down-arrow-icon-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/combobox-down-arrow.png"></li>' +
  '<li data-combobox-down-arrow-icon-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/combobox-down-arrow-rollover.png"></li>' +
  '<li data-lightbox-slideshow-preloader-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/slideShowPreloader.png"></li>' +
  '<li data-lightbox-close-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/galleryCloseButtonNormalState.png"></li>' +
  '<li data-lightbox-close-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/galleryCloseButtonSelectedState.png"></li>' +
  '<li data-lightbox-next-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/nextIconNormalState.png"></li>' +
  '<li data-lightbox-next-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/nextIconSelectedState.png"></li>' +
  '<li data-lightbox-prev-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/prevIconNormalState.png"></li>' +
  '<li data-lightbox-prev-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/prevIconSelectedState.png"></li>' +
  '<li data-lightbox-play-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/playButtonNormalState.png"></li>' +
  '<li data-lightbox-play-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/playButtonSelectedState.png"></li>' +
  '<li data-lightbox-pause-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/pauseButtonNormalState.png"></li>' +
  '<li data-lightbox-pause-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/pauseButtonSelectedState.png"></li>' +
  '<li data-lightbox-maximize-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/maximizeButtonNormalState.png"></li>' +
  '<li data-lightbox-maximize-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/maximizeButtonSelectedState.png"></li>' +
  '<li data-lightbox-minimize-button-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/minimizeButtonNormalState.png"></li>' +
  '<li data-lightbox-minimize-button-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/minimizeButtonSelectedState.png"></li>' +
  '<li data-lightbox-info-button-open-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonOpenNormalState.png"></li>' +
  '<li data-lightbox-info-button-open-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonOpenSelectedState.png"></li>' +
  '<li data-lightbox-info-button-close-normal-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonCloseNormalPath.png"></li>' +
  '<li data-lightbox-info-button-close-selected-path="static/Li3/img/assets/responsive-gridfolio/skin_minimal_dark_square/infoButtonCloseSelectedPath.png"></li>' +
  '</ul>';

  for (a in albums_json) {
    if (name != 'All Albums' && albums_json[a].name == name) {
      html += '<ul data-cat="' + albums_json[a].name + '">';
      // albums_json[a].albumItems = shuffle(albums_json[a].albumItems);
      for (i in albums_json[a].albumItems) {
        html += '<ul>' +
        '<li data-type="link" ';

        // if (albums_json[a].albumItems[i].videoId) {
        //   html += 'data-width="1024" data-height="768" ';
        // }

        // if (albums_json[a].albumItems[i].videoId) {
        //   html += 'data-url="http://www.youtube.com/watch?v='+albums_json[a].albumItems[i].videoId+'"></li>';
        // } else {
          html += 'data-target="_self" data-url="javascript:editImage('+albums_json[a].albumItems[i].id+', '+albums_json[a].id+', \''+albums_json[a].albumItems[i].videoId+'\');"></li>';
        // }

        html += '<li data-thumbnail-path="' + albums_json[a].albumItems[i].thumbUrl + '"></li>' +
        '<li data-thumbnail-text>' +
        '<p class="largeLabel">';

        html += '<i class="fa fa-pencil-square-o fa-2x"></i>';

        html += '</p>' +
        '</li>' +
        '</ul>';
      }
      html +='</ul>';
      break;
    }

    if (name == 'All Albums') {
      html += '<ul data-cat="' + albums_json[a].name + '">';
      // albums_json[a].albumItems = shuffle(albums_json[a].albumItems);
      for (i in albums_json[a].albumItems) {
        html += '<ul>' +
        '<li data-type="link" ';

        // if (albums_json[a].albumItems[i].videoId) {
        //   html += 'data-width="1024" data-height="768" ';
        // }

        // if (albums_json[a].albumItems[i].videoId) {
        //   html += 'data-url="http://www.youtube.com/watch?v='+albums_json[a].albumItems[i].videoId+'"></li>';
        // } else {
          html += 'data-target="_self" data-url="javascript:editImage('+albums_json[a].albumItems[i].id+', '+albums_json[a].id+', \''+albums_json[a].albumItems[i].videoId+'\');"></li>';
        // }

        html += '<li data-thumbnail-path="' + albums_json[a].albumItems[i].thumbUrl + '"></li>' +
        '<li data-thumbnail-text>' +
        '<p class="largeLabel">';

        html += '<i class="fa fa-pencil-square-o fa-2x"></i>';

        html += '</p>' +
        '</li>' +
        '</ul>';
      }
      html +='</ul>';
    }
  }

  html += '</ul>';

  $("#galleryDivParent").parent().append(html);
  $("#albumTitle").children().remove();
  $("#albumTitle").append('<h2>'+name+'</h2>');
}

function shuffle(array) {
  var counter = array.length, temp, index;

      // While there are elements in the array
      while (counter > 0) {
          // Pick a random index
          index = Math.floor(Math.random() * counter);

          // Decrease counter by 1
          counter--;

          // And swap the last element with it
          temp = array[counter];
          array[counter] = array[index];
          array[index] = temp;
        }

        return array;
      }


// IMAGES AND ALBUMS CRUD

var baseUrl = "<?php  $Li3->EchoDomainNameURL( ); ?>";
var globalImgId;
var globalAlbumId;

$( document ).ready(function() {
  $('#service-image-editor').cropit({
    exportZoom: 1.25,
    imageBackground: false,
    imageBackgroundBorderWidth: 20
  });
});

var delImg = function(imgId, albumId) {
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this picture.",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it",
    closeOnConfirm: false
  },
  function () {
    var token = 1234;
    var post_data = {};
    post_data["token"] = token;
    post_data["delete"] = true;
    post_data["albumId"] = albumId;
    post_data["picId"] = imgId;

    var request = $.ajax({
      type: "POST",
      url: baseUrl + "api/ajax/auth/user/profile/albums/push/editPic",
      dataType: "json",
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      data: post_data
    });

  // Request successful!
  request.done(function (response) {
    if (response.error == "false") {
      swal("Deleted", "The picture has been deleted.", "success");
      location.reload();
    }
    else if (response.error == "true") {
      swal("Error", "There was an unexpected error.", "error");
    }
  });

  // if the request fails
  request.fail(function (jqXHR, textStatus, error) {
    console.log("search failed")
    console.log(jqXHR.statusText);
    console.log(textStatus);
    console.log(error);
  });

});
}

var imgUpdate = function(imgId, albumId) {
  var token = 1234;
  var post_data = {};
  post_data["token"] = token;
  post_data["delete"] = true;
  post_data["albumId"] = albumId;
  post_data["picId"] = imgId;

  var request = $.ajax({
    type: "POST",
    url: baseUrl + "api/ajax/auth/user/profile/albums/push/editPic",
    dataType: "json",
    xhrFields: {
      withCredentials: true
    },
    crossDomain: true,
    data: post_data
  });

  // Request successful!
  request.done(function (response) {
    if (response.error == "false") {
      // swal("Updated", "The picture has been updated.", "success");
      // location.reload();
    }
    else if (response.error == "true") {
      swal("Error", "There was an unexpected error.", "error");
    }
  });

  // if the request fails
  request.fail(function (jqXHR, textStatus, error) {
    console.log("search failed")
    console.log(jqXHR.statusText);
    console.log(textStatus);
    console.log(error);
  });

}

function addImageAlbum() {
  $('#AddEditModal').html('Add New Image or Album');
  $('#AddEditButton').html('Add Image');
  $('#deleteImg').css('display','none');
  $('#currentAction').val('add');
  $('#service-image-editor').find(".cropit-image-preview.cropit-image-loaded").css("background-image" , "");
}

function editImage(imgId, albumId, videoId) {
  $('#AddEditModal').html('Edit Image');
  $('#AddEditButton').html('Update');
  $('#currentAction').val('edit');
  // $('#DeleteServiceButton').css('display','block');
  // Set the current album selected in the dropdown
  $('#selectedAlbum').val(albumId);
  // Set the video ID if exist
  $('#youtubeVideo').val(videoId);
  // Set the current picture in the container
  $('#service-image-editor').cropit('imageSrc', 'http://pics.reshape.net/p/id_' + imgId + '__b_0__ext_jpeg__w_1920/blob');
  // Trigger and show the modal

  $('#deleteImg').on("click", function(){
    delImg(imgId, albumId);
  });

  $('#modal-new-image-album').modal('show');

  globalImgId = imgId;
  globalAlbumId = albumId;
}

function saveImg(currentAction) {

  var formData = new FormData();
  formData.append('token', '1234');
  formData.append('videoId', getVideoId());

  if ( currentAction == 'add' ) {
    formData.append('albumId', $('#selectedAlbum').val());
    formData.append('picFile', imageBlob());
    ActionURL = baseUrl + 'api/ajax/auth/user/profile/albums/push/upload';
  } else {
    if($('#chose-service-pic').val() != ""){
      imgUpdate(globalImgId, globalAlbumId);
      formData.append('albumId', $('#selectedAlbum').val());
      formData.append('picFile', imageBlob());
      ActionURL = baseUrl + 'api/ajax/auth/user/profile/albums/push/upload';
    } else {
      formData.append('picId', globalImgId);
      formData.append('albumId', globalAlbumId);
      if ($('#selectedAlbum').val() != globalAlbumId) {
        formData.append('newAlbumId', $('#selectedAlbum').val());
      }
      ActionURL = baseUrl + 'api/ajax/auth/user/profile/albums/push/editPic';
    }
  }

  $.ajax({
    type: 'POST',
    processData: false,
    contentType: false,
    data: formData,
    url: ActionURL,
    dataType: 'json',
    xhrFields: {
      withCredentials: true
    },
    crossDomain: true,
    success: function(data){
      $('#modal-new-service').modal('hide');
      location.reload();
      // console.log(data);
    }
  });

}

function getVideoId() {
  var yVideo = $('#youtubeVideo').val();
  if (yVideo) {
    if (yVideo.indexOf('youtube.com') != -1) {
     return yVideo.match(/v=(.{11})/)[1];
   } else {
    return yVideo;
  }
} else {
  return '';
}
}

function imageBlob() {
  var img = $('#service-image-editor').cropit('export', {
    type: 'image/jpeg',
    quality: .9,
    originalSize: true
  });

  if(img){

    var imgData = img;
    var postFieldName = "picFile";

      var binary = atob(imgData.split(',')[1]); // Create 8-bit unsigned array
      var array  = [];

      for (var i = 0; i < binary.length; i++)
      {
        array.push(binary.charCodeAt(i));
      }

      // Return our Blob object
      var blob = new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
      return blob;
    }
  }

  $( '#newAlbumTgl' ).click(function() {
    $( '#albumNameInput' ).toggle( 'show' );
  });


  $( '#addAlbum' ).click(function() {
    if ($('#albumName').val() != '') {
      var albumData = new FormData();
      albumData.append('token', '1234');
      albumData.append('name', $('#albumName').val());

      ActionURL = baseUrl + 'api/ajax/auth/user/profile/albums/push/add';

      $.ajax({
        type: 'POST',
        processData: false,
        contentType: false,
        data: albumData,
        url: ActionURL,
        dataType: 'json',
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        success: function(data){
          $('#albumNameInput').toggle( 'show' );
          $('#albumName').val('');
          $('#selectedAlbum').append('<option value="'+data.newAlbum.id+'">'+data.newAlbum.name+'</option>');
          $('#selectedAlbum>option[value="' + data.newAlbum.id + '"]').prop('selected', true);
          // console.log(data);
        }
      });
    } else {
      swal("Error", "You must give an album name.", "error");
    }

  });
</script>