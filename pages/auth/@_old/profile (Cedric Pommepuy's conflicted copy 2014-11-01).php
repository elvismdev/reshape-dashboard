<?php
  /** @var Li3instance */
  global	$Li3;
  $page_title = "Professional Profile";
  $page_description = "Edit your ReShape Profile";
  $page_icon = "sidebar-nav-icon gi gi-user";
  //include($_SERVER['DOCUMENT_ROOT']."/static/inc/page_content_beg.php");
  // Fields values
  $publicAlias = "";
?>
  <link href="<?php	$Li3->EchoStaticPath("css/showcase/skin_modern_silver.css");	?>" type="text/css" rel="stylesheet" />
  <link href="<?php	$Li3->EchoStaticPath("css/showcase/html_content.css");	?>" type="text/css" rel="stylesheet" /> 
  <div id="page-content">
  <div class="content-header">
    <ul class="breadcrumb breadcrumb-top">
      <li class="bcfirst-li"><a href="/"><i class="sidebar-nav-icon fa fa-home"></i></a> <a href="/">Home</a></li>
      <li><?php echo $page_title; ?></li>
    </ul>
    <div class="header-section">
      <h1> <i class="<?php echo $page_icon; ?>" style="margin-right: 23px;"></i> <?php echo $page_title; ?> <small style="margin-top: -14px;"><?php echo $page_description; ?></small> </h1>
    </div>
  </div>
  <div class="block" style="background-color:#000000;">
    <div class="row" style="padding-bottom: 17px;">
      <div class="col-md-5" style="margin-top: 82px; color: #fff;">
        <div class="editprof-intro">
          <h1 class="sub-header text-center prof-welcome">WELCOME</h1>
          <p style="text-align:center; color:#DFDFDF;">Here is where you will start creating your Reshape profile. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <h4 style="font-weight: 700; font-size: 14px;">Content Input Progress</h4>
          <ul class="prof-progress">
            <li><span style="color:#aad178"><i class="fa fa-check"></i></span> General Info</li>
            <li><span style="color:#eaeaea;"><i class="fa fa-check"></i></span> Primary Location</li>
            <li><span style="color:#eaeaea;"><i class="fa fa-check"></i></span> Education/Awards</li>
            <li><span style="color:#eaeaea;"><i class="fa fa-check"></i></span> Classification</li>
            <li><span style="color:#eaeaea;"><i class="fa fa-check"></i></span> Social Media</li>
            <li><span style="color:#eaeaea;"><i class="fa fa-check"></i></span> Profile Customization</li>
          </ul>
          </br>
          <p><a href="#" class="btn btn-lg btn-primary btn-block">View Live Reshape Profile</a></p>
        </div>
      </div>
      <div class="col-md-7 profile-steps col-md-offset-1"> 
        <!-- Clickable Wizard Content -->
		<?php
			
			$postData = array( 'userId' => "4", 'token' => $Li3->GetToken());		
			$token = $Li3->GetToken();
			//$userId = $Li3->GetUserId();
			$userId = "4";
			
			// Professional info
			$proResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/auth/user/profile/page?token=". $token. "&userId=". $userId, null); 
			$publicName = $proResponseData["pageInfo"]["pageData"]["publicAlias"];
			$tagLine    = $proResponseData["pageInfo"]["pageData"]["tagLine"];
			$title 		= $proResponseData["pageInfo"]["pageData"]["title"];
			$bio 		= $proResponseData["pageInfo"]["pageData"]["bio"];
			$city 		= $proResponseData["pageInfo"]["pageData"]["city"];
			$region 	= $proResponseData["pageInfo"]["pageData"]["region"];
			$mainDegree = $proResponseData["pageInfo"]["pageData"]["mainDegree"];
			$mainSchool = $proResponseData["pageInfo"]["pageData"]["mainSchool"];
			$backColor  = $proResponseData["pageInfo"]["pageData"]["backColor"];
			$website 	= $proResponseData["pageInfo"]["pageData"]["website"];
			
			// Awards and Accomplishments
			$lineItems = $proResponseData["pageInfo"]["pageData"]["lineItems"];
			$lineItemsAward = array();
			$lineItemsAccomplishment = array();
			foreach( $lineItems as $lineItem ){
				if($lineItem["type"] == "award"){
					array_push($lineItemsAward,$lineItem["name"]);
				}
				elseif($lineItem["type"] == "accomplishment"){
					array_push($lineItemsAccomplishment,$lineItem["name"]);
				}					
			}
			
			// Social medias			
			$socialMediaResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/profile/social?token=". $token. "&id=". $userId, null); 
			foreach($socialMediaResponseData["socialMedia"]["accounts"] as $account ){
				switch ($account["label"]){
					case "Facebook"  : $fbAcct 		   = $account["url"]; break;
					case "Google+"   : $googlePlusAcct = $account["url"]; break;
					case "Twitter"   : $twitterAcct    = $account["url"]; break;
					case "LinkedIn"  : $linkedInAcct   = $account["url"]; break;
					case "YouTube"   : $youtubeUrl 	   = $account["url"]; break;
					case "Instagram" : $instagramAcct  = $account["url"]; break; 
					case "Pinterest" : $pinterestAcct  = $account["url"]; break;
					case "Vimeo" 	 : $vimeoAcct 	   = $account["url"]; break;
					case "Spotify"   : $spotifyAcct    = $account["url"]; break;
				}			
			}
			
			// Profile classifications
			$profileGenderId = array();
			$profileDepartments = array();	
			$profileFocuses = array();
			$profileSpecialties = array();
			$profileActivities = array();
			$profileCertifications = array();
			$profileClassificationsResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/profile/classif?token=". $token. "&id=". $userId, null); 
			foreach($profileClassificationsResponseData["attrGroups"] as $group ){
				switch ($group["name"]){
					case "Gender" 		  : array_push($profileGenderId 	  , $group["attributes"]); break;
					case "Certifications" : array_push($profileCertifications , $group["attributes"]) ; break;
					case "Department" 	  : array_push($profileDepartments	  , $group["attributes"]) ; break;
					case "Activities" 	  : array_push($profileActivities	  , $group["attributes"]) ; break;
					case "Specialities"   : array_push($profileSpecialties    , $group["attributes"]) ; break;
					case "Focuses"   	  : array_push($profileFocuses    	  , $group["attributes"]) ; break;
				}
			}			  
			
			// Classifications
			$departmentClassif = array();
			$specialtiesClassif = array();
			$focusesClassif = array();
			$activitiesClassif = array();
			$certificationsClassif = array();
			$genders = array();
			
			$classificationsResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/attributes/groups?isClassifier=null&token=". $token, null); 
			foreach($classificationsResponseData["attrGroups"] as $group  ){
				switch ($group["name"]){
					case "Department"     : array_push($departmentClassif     , $group["attributes"]) ; break;
					case "Specialities"   : array_push($specialtiesClassif    , $group["attributes"]) ; break;
					case "Focuses" 		  : array_push($focusesClassif        , $group["attributes"]) ; break;
					case "Activities" 	  : array_push($activitiesClassif  	  , $group["attributes"]) ; break;
					case "Certifications" : array_push($certificationsClassif , $group["attributes"]) ; break;
					case "Gender" 		  : array_push($genders				  , $group["attributes"]) ; break;
				}			
			}			
		?>
        <form id="clickable-wizard" action="page_forms_wizard.php" method="post" class="form-horizontal form-bordered ui-formwizard">
          <!-- First Step -->
          <div id="clickable-first" class="step ui-formwizard-content" style="display: block;"> 
            <!-- Step Info -->
            <div class="form-group">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified clickable-steps">
                  <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>General Info</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>Primary Location</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Education/Awards</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Classification</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Social Media</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Profile Customization</strong></a></li>
                </ul>
              </div>
            </div>
            <!-- END Step Info -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="public-name">Public Name</label>
              <div class="col-md-5">
                <input type="text" id="public-name" name="public-name" class="form-control ui-wizard-content" placeholder="Enter you public name" value = "<?php echo $publicName; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="gender">Gender</label>
              <div class="col-md-5">
                <select id="example-select" name="example-select" class="form-control" size="1">
					<option value="0">Please select</option>
					<?php $Li3->SetOptions($genders[0], $profileGenderId); ?>                  
                  <!--<option value="1">Female</option>
                  <option value="2">Male</option>-->
                </select>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="main-title">Main Title</label>
              <div class="col-md-5">
                <input type="text" id="main-title" name="main-title" class="form-control ui-wizard-content" placeholder="Enter your main title" value = "<?php echo $title; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="tagline">Tagline</label>
              <div class="col-md-5">
                <input type="text" id="tagline" name="tagline" class="form-control ui-wizard-content" placeholder="Add a tagline" value = "<?php echo $tagLine; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="main-title">Profile Picture</label>
              <div class="col-md-5">
                <input id="profilePic" class="form-control" type="file" style="height: auto;">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="main-title">Personal/company logo</label>
              <div class="col-md-5">
                <input id="personalLogo" class="form-control" type="file" style="height: auto;">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="bio">Bio</label>
              <div class="col-md-5">
                <textarea id="bio" name="bio" rows="6" class="form-control ui-wizard-content bio" placeholder="Write a short bio of yourself" disabled="disabled"><?php echo $bio; ?></textarea>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="personal-website">Personal Website</label>
              <div class="col-md-5">
                <input id="personal-website" name="personal-website" class="form-control ui-wizard-content" placeholder="Enter the domain of  your personal website" value = "<?php echo $website; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
          </div>
          <!-- END First Step --> 
          <!-- Second Step -->
          <div id="clickable-second" class="step ui-formwizard-content" style="display: none;"> 
            <!-- Step Info -->
            <div class="form-group">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified clickable-steps">
                  <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>General Info</strong></a></li>
                  <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>Primary Location</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Education/Awards</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Classification</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Social Media</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Profile Customization</strong></a></li>
                </ul>
              </div>
            </div>
            <!-- END Step Info -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="city">City</label>
              <div class="col-md-5">
                <input type="text" id="city" name="city" class="form-control ui-wizard-content" placeholder="Enter the city of your primary location" value = "<?php echo $city; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="region">Region</label>
              <div class="col-md-5">
                <input type="text" id="region" name="region" class="form-control ui-wizard-content" placeholder="Enter the region of your primary location" value = "<?php echo $region; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="state-region">State</label>
              <div class="col-md-5">
                <select id="state-region" name="state-region" class="form-control" size="1">
                  <option value="" selected="selected">Select a State</option>
                  <option value="AL">Alabama</option>
                  <option value="AK">Alaska</option>
                  <option value="AZ">Arizona</option>
                  <option value="AR">Arkansas</option>
                  <option value="CA">California</option>
                  <option value="CO">Colorado</option>
                  <option value="CT">Connecticut</option>
                  <option value="DE">Delaware</option>
                  <option value="DC">District Of Columbia</option>
                  <option value="FL">Florida</option>
                  <option value="GA">Georgia</option>
                  <option value="HI">Hawaii</option>
                  <option value="ID">Idaho</option>
                  <option value="IL">Illinois</option>
                  <option value="IN">Indiana</option>
                  <option value="IA">Iowa</option>
                  <option value="KS">Kansas</option>
                  <option value="KY">Kentucky</option>
                  <option value="LA">Louisiana</option>
                  <option value="ME">Maine</option>
                  <option value="MD">Maryland</option>
                  <option value="MA">Massachusetts</option>
                  <option value="MI">Michigan</option>
                  <option value="MN">Minnesota</option>
                  <option value="MS">Mississippi</option>
                  <option value="MO">Missouri</option>
                  <option value="MT">Montana</option>
                  <option value="NE">Nebraska</option>
                  <option value="NV">Nevada</option>
                  <option value="NH">New Hampshire</option>
                  <option value="NJ">New Jersey</option>
                  <option value="NM">New Mexico</option>
                  <option value="NY">New York</option>
                  <option value="NC">North Carolina</option>
                  <option value="ND">North Dakota</option>
                  <option value="OH">Ohio</option>
                  <option value="OK">Oklahoma</option>
                  <option value="OR">Oregon</option>
                  <option value="PA">Pennsylvania</option>
                  <option value="RI">Rhode Island</option>
                  <option value="SC">South Carolina</option>
                  <option value="SD">South Dakota</option>
                  <option value="TN">Tennessee</option>
                  <option value="TX">Texas</option>
                  <option value="UT">Utah</option>
                  <option value="VT">Vermont</option>
                  <option value="VA">Virginia</option>
                  <option value="WA">Washington</option>
                  <option value="WV">West Virginia</option>
                  <option value="WI">Wisconsin</option>
                  <option value="WY">Wyoming</option>
                </select>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
          </div>
          <!-- END Second Step --> 
          <!-- Third Step -->
          <div id="clickable-third" class="step ui-formwizard-content" style="display: none;"> 
            <!-- Step Info -->
            <div class="form-group">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified clickable-steps">
                  <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>General Info</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>Primary Location</strong></a></li>
                  <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Education/Awards</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Classification</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Social Media</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Profile Customization</strong></a></li>
                </ul>
              </div>
            </div>
            <!-- END Step Info -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="degree">Education Degree</label>
              <div class="col-md-5">
                <input type="text" id="degree" name="degree" class="form-control ui-wizard-content" placeholder="Enter your highest degree earned" value = "<?php echo $mainDegree; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="institution">Institution</label>
              <div class="col-md-5">
                <input type="text" id="institution" name="institution" class="form-control ui-wizard-content" placeholder="Enter the institution where you earned said degree" value = "<?php echo $mainSchool; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="awards">Awards</label>
                <div class="col-md-5">
                  <button data-duplicate-add="Award" type="button" class="btn btn-alt btn-default">+ Add award</button>
                </div>
                <div class="col-md-3" style="margin-left: -8px;"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
              <div class="row input-space">
				<?php
					foreach($lineItemsAward as $award){
					?>
						<div data-duplicate="Award">
						  <div class="col-md-4"></div>
						  <div class="col-md-5">
							<input type="input" class="form-control" name="Award" placeholder="Enter award" value="<?php echo $award; ?>">
						  </div>
						  <div class="col-md-3" style="margin-left: -8px;">
							<button data-duplicate-remove="Award" type="button" class=" btn btn-sm btn-danger">-</button>
						  </div>
						</div>					
					<?php					
					}
				?>
                <div data-duplicate="Award">
                  <div class="col-md-4"></div>
                  <div class="col-md-5">
                    <input type="input" class="form-control" name="Award" placeholder="Enter award">
                  </div>
                  <div class="col-md-3" style="margin-left: -8px;">
                    <button data-duplicate-remove="Award" type="button" class=" btn btn-sm btn-danger">-</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="accomplishment">Accomplishment</label>
                <div class="col-md-5">
                  <button data-duplicate-add="Accomplishment" type="button" class=" btn btn-alt btn-default">+ Add accomplishment</button>
                </div>
                <div class="col-md-3" style="margin-left: -8px;"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
              <div class="row input-space">
				<?php
					foreach($lineItemsAccomplishment as $accomplishment){
					?>
						<div data-duplicate="Accomplishment">
						  <div class="col-md-4"></div>
						  <div class="col-md-5">
							<input type="input" class="form-control" name="Accomplishment" placeholder="Enter Accomplishment" value="<?php echo $accomplishment; ?>">
						  </div>
						  <div class="col-md-3" style="margin-left: -8px;">
							<button data-duplicate-remove="Accomplishment" type="button" class=" btn btn-sm btn-danger">-</button>
						  </div>
						</div>					
					<?php					
					}
				?>
                <div data-duplicate="Accomplishment">
                  <div class="col-md-4"></div>
                  <div class="col-md-5">
                    <input type="input" class="form-control" name="Accomplishment" placeholder="Enter accomplishment">
                  </div>
                  <div class="col-md-3" style="margin-left: -8px;">
                    <button data-duplicate-remove="Accomplishment" type="button" class=" btn btn-sm btn-danger">-</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END Third Step --> 
          <!-- Fourth Step -->
          <div id="clickable-fourth" class="step ui-formwizard-content" style="display: none;"> 
            <!-- Step Info -->
            <div class="form-group">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified clickable-steps">
                  <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>General Info</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>Primary Location</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Education/Awards</strong></a></li>
                  <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Classification</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Social Media</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Profile Customization</strong></a></li>
                </ul>
              </div>
            </div>
            <!-- END Step Info -->
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="department">Department</label>
                <div class="col-md-5">
				  <select class="select-chosen" id="id0" name="id0" size="1" data-placeholder="Enter one or more options." multiple="" style="display: none;">
				  <?php					
					// Display options
					$Li3->SetOptions($departmentClassif[0], $profileDepartments);					
				  ?>
                  </select>
                </div>
                <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="speacialties">Specialties</label>
                <div class="col-md-5">
                  <select class="select-chosen" id="id1" name="id1" size="1" data-placeholder="Enter one or more options." multiple="">
					<?php $Li3->SetOptions($specialtiesClassif[0], $profileSpecialties); ?>                    
                  </select>
                </div>
                <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="focuses">Focuses</label>
                <div class="col-md-5">					
                  <select class="select-chosen" id="id2" name="id2" size="1" data-placeholder="Enter one or more options." multiple="">
					<?php $Li3->SetOptions($focusesClassif[0], $profileFocuses); ?>
                  </select>
                </div>
                <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="activities">Activities</label>
                <div class="col-md-5">
                  <select class="select-chosen" id="id3" name="id3" size="1" data-placeholder="Enter one or more options." multiple="">
					<?php $Li3->SetOptions($activitiesClassif[0], $profileActivities); ?>              
                  </select>
                </div>
                <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <label class="col-md-4 control-label" for="certifications">Certifications</label>
                <div class="col-md-5">
                  <select class="select-chosen" id="id4" name="id4" size="1" data-placeholder="Enter one or more options." multiple="">
					<?php $Li3->SetOptions($certificationsClassif[0], $profileCertifications); ?>
                    <!--<option value="1041">AAAI</option>
                    <option value="1062">AASDN</option>
                    <option value="1010">ACE</option>
                    <option value="1013">ACSM</option>
                    <option value="1019">AFAA</option>
                    <option value="1037">AFPA</option>
                    <option value="1092">Agama Yoga</option>
                    <option value="1049">American Academy of Health and Fitness</option>
                    <option value="1059">Animal Flow</option>
                    <option value="1030">ASFA</option>
                    <option value="1098">Ashtanga Yoga</option>
                    <option value="1080">ATP</option>
                    <option value="1081">AVP</option>
                    <option value="1043">BASI</option>
                    <option value="1045">Bender Training Academy</option>
                    <option value="1093">Bikram Yoga</option>
                    <option value="1063">Bodymind Institute</option>
                    <option value="1051">CAN FIT PRO</option>
                    <option value="1064">CBNS</option>
                    <option value="1065">CDR Pediatric</option>
                    <option value="1073">CNCB</option>
                    <option value="1034">CrossCore</option>
                    <option value="1052">CrossFit</option>
                    <option value="1058">CSCS</option>
                    <option value="1060">Energii</option>
                    <option value="1048">FIT4MOM</option>
                    <option value="1044">FITour</option>
                    <option value="1066">FNS</option>
                    <option value="1067">IAACN</option>
                    <option value="1033">IDEA</option>
                    <option value="1020">IFPA</option>
                    <option value="1094">IKYTA</option>
                    <option value="1068">Institute for Integrative Nutrition</option>
                    <option value="1074">iPEC</option>
                    <option value="1031">ISFTA</option>
                    <option value="1023">ISSA</option>
                    <option value="1113">ISSA</option>
                    <option value="1069">ISSN</option>
                    <option value="1095">Iyengar Yoga</option>
                    <option value="1032">KettleBell Concepts</option>
                    <option value="1096">Kripalu Yoga</option>
                    <option value="1097">Kriya Yoga</option>
                    <option value="1061">MAT</option>
                    <option value="1071">McKenzie Institute</option>
                    <option value="1082">MLB</option>
                    <option value="1083">MLS</option>
                    <option value="1084">MMA</option>
                    <option value="1038">NAFC</option>
                    <option value="1011">NASM</option>
                    <option value="1029">National Personal Training Institute</option>
                    <option value="1085">NBA</option>
                    <option value="1072">NBNSC</option>
                    <option value="1012">NCCPT</option>
                    <option value="1039">NCEP</option>
                    <option value="1017">NCSF</option>
                    <option value="1018">NESTA</option>
                    <option value="1016">NETA</option>
                    <option value="1086">NFL</option>
                    <option value="1014">NFPT</option>
                    <option value="1087">NHL</option>
                    <option value="1027">NPI</option>
                    <option value="1130">NPTI</option>
                    <option value="1022">NSCA</option>
                    <option value="1075">NSCA-CPT</option>
                    <option value="1076">NSCA-CSPS</option>
                    <option value="1077">NSCA-TSAC-F</option>
                    <option value="1024">NSPA</option>
                    <option value="1088">Olympic Athlete</option>
                    <option value="1089">PBA</option>
                    <option value="1047">Peak Pilates</option>
                    <option value="1046">Performing Arts Physical Therapy</option>
                    <option value="1057">PFIT</option>
                    <option value="1090">PGA</option>
                    <option value="1040">PMA</option>
                    <option value="1078">Precision Nutrition</option>
                    <option value="1025">PTA Global</option>
                    <option value="1079">RDN</option>
                    <option value="1070">Registered Dietitian</option>
                    <option value="1050">Silver Sneakers</option>
                    <option value="1099">Sivananda Yoga</option>
                    <option value="1100">SRF</option>
                    <option value="1026">The Biomechanics Method</option>
                    <option value="1036">The Cooper Institute</option>
                    <option value="1021">TRX</option>
                    <option value="1054">Turbo Kick</option>
                    <option value="1091">UFC</option>
                    <option value="1053">USAKL</option>
                    <option value="1105">VIPR</option>
                    <option value="1028">WITS</option>
                    <option value="1101">YA-RYT 200</option>
                    <option value="1102">YA-RYT 500</option>
                    <option value="1055">Yoga Alliance</option>
                    <option value="1056">Yoga Works</option>
                    <option value="1042">YogaFit</option>
                    <option value="1035">ZUMBA</option>-->
                  </select>
                </div>
                <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
              </div>
            </div>
          </div>
          
          <!-- END Fourth Step --> 
          <!-- Fifth Step -->
          <div id="clickable-fifth" class="step ui-formwizard-content" style="display: none;"> 
            <!-- Step Info -->
            <div class="form-group">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified clickable-steps">
                  <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>General Info</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>Primary Location</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Education/Awards</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Classification</strong></a></li>
                  <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Social Media</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Profile Customization</strong></a></li>
                </ul>
              </div>
            </div>
            <!-- END Step Info -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="facebook"><img class="socialicon" src="/static/img/socialicons/fb.png"></label>
              <div class="col-md-5">
                <input type="text" id="facebook" name="facebook" class="form-control ui-wizard-content" placeholder="Enter your Facebook page" value="<?php echo $fbAcct; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="twitter"><img class="socialicon" src="/static/img/socialicons/twit.png"></label>
              <div class="col-md-5">
                <input type="text" id="twitter" name="twitter" class="form-control ui-wizard-content" placeholder="Enter your Twitter handle"  value="<?php echo $twitterAcct; ?>" disabled>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="instagram"><img class="socialicon" src="/static/img/socialicons/insta.png"></label>
              <div class="col-md-5">
                <input type="text" id="instagram" name="instagram" class="form-control ui-wizard-content" placeholder="Enter your Instagram username"  value="<?php echo $instagramAcct; ?>" disabled>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="spotify"><img class="socialicon" src="/static/img/socialicons/spotify.png"></label>
              <div class="col-md-5">
                <input type="text" id="spotify" name="spotify" class="form-control ui-wizard-content" placeholder="Enter the link for your Spotify Playlist"  value="<?php echo $spotifyAcct; ?>" disabled>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="youtube"><img class="socialicon" src="/static/img/socialicons/yt.png"></label>
              <div class="col-md-5">
                <input type="text" id="youtube" name="youtube" class="form-control ui-wizard-content" placeholder="Enter your Youtube username"  value="<?php echo $youtubeUrl; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="googleplus"><img class="socialicon" src="/static/img/socialicons/googleplus.png"></label>
              <div class="col-md-5">
                <input type="text" id="googleplus" name="googleplus" class="form-control ui-wizard-content" placeholder="Enter your Google+ username"  value="<?php echo $googlePlusAcct; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="linkedin"><img class="socialicon" src="/static/img/socialicons/linkedin.png"></label>
              <div class="col-md-5">
                <input type="text" id="linkedin" name="linkedin" class="form-control ui-wizard-content" placeholder="Enter your Linkedin profile URL"  value="<?php echo $linkedInAcct; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="pinterest"><img class="socialicon" src="/static/img/socialicons/pinterest.png"></label>
              <div class="col-md-5">
                <input type="text" id="pinterest" name="pinterest" class="form-control ui-wizard-content" placeholder="Enter your Pinterest username"  value="<?php echo $pinterestAcct; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="vimeo"><img class="socialicon" src="/static/img/socialicons/vimeo.png"></label>
              <div class="col-md-5">
                <input type="text" id="vimeo" name="vimeo" class="form-control ui-wizard-content" placeholder="Enter your Vimeo username"  value="<?php echo $vimeoAcct; ?>">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i></a> </div>
            </div>
          </div>
            <!-- END Fifth Step --> 
          <!-- Sixth Step -->
          <div id="clickable-sixth" class="step ui-formwizard-content" style="display: none;"> 
            <!-- Step Info -->
            <div class="form-group">
              <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified clickable-steps">
                  <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>General Info</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>Primary Location</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Education/Awards</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Classification</strong></a></li>
                  <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Social Media</strong></a></li>
                  <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Profile Customization</strong></a></li>
                </ul>
              </div>
            </div>
            <!-- END Step Info -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="custom-url">Custom URL</label>
              <div class="col-md-5">
                <input type="text" id="custom-url" name="custom-url" class="form-control ui-wizard-content" placeholder="Enter your custom url">
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="background-color">Background Color</label>
              <div class="col-md-5">
                <select class="" id="background-color" name="background-color" size="1">
                  <option selected value="11" data-color="#353536">Dark Gray</option>
                  <option value="22" data-color="#2699E5">Blue</option>
                  <option value="33" data-color="#B6B6B6 ">Light Gray</option>
                  <option value="44" data-color="#FC839B">Pink</option>
                  <option value="55" data-color="#774298 ">Purple</option>
                  <option value="66" data-color="#81cfc1">Teal</option>
                  <option value="77" data-color="#F0bb32">Gold Orange</option>
                  <option value="88" data-color="#d41a1a">Red</option>
                  <option value="99" data-color="#531f36">Eggplant</option>
                  <option value="00" data-color="#000000">Black</option>
                </select>
              </div>
              <div class="col-md-3"> <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a> </div>
            </div>
          </div>
          <!-- END Sixth Step --> 
          <!-- Form Buttons -->
          <center>
            <div class="form-group form-actions profile-btns">
              <div class="col-md-12">
                <button type="reset" class="btn btn-sm btn-gray ui-wizard-content ui-formwizard-button" id="back4" value="Back" disabled="disabled">Previous</button>
                <button type="submit" class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button" id="next4" value="Next">Next</button>
              </div>
            </div>
          </center>
          <!-- END Form Buttons -->
        </form>
        <!-- END Clickable Wizard Content --> 
      </div>
    </div>
  </div>
  <!-- Create a slideshow modal form -->
  <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="createSlide">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <div class="form-group form-actions"></div>
                <h2 class="modal-title">
                    <i class=" fa fa-pencil"></i> Add new slide
                </h2>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-bordered" id="ptsid_250_13-form" action="" method="post" enctype="multipart/form-data">
                    <div class="block-full"></div>
                    <script></script>
                    <input type="hidden" name="ptshiddenfield" value="ptsid_250_13-form" />
                    <div class=" form-group">
                        <label class="col-md-4 control-label" for="title">
                            Heading
                        </label>
                        <div class=" col-md-8 ">
                            <input type="text" class="form-control" id="title" placeholder="Title for your image or video" name="field1" />
                        </div>
                    </div>
                    <div class=" form-group">
                        <label class="col-md-4 control-label" for="picFile">
                            Upload new image
                        </label>
                        <div class=" col-md-8 ">
                            <div class="block full">
                                <div class="block-title">
                                    <div class="block-options pull-right"></div>
                                    <h2>
                                        <small>Click to upload files</small>
                                    </h2>
                                </div>
                                <input type="file" class="form-control" id="picFile" style="height: auto;" name="formInput" />
                            </div>
                        </div>
                    </div>
                    <div class=" form-group">
                        <label class="col-md-4 control-label" for="videoId">
                            Youtube Video/Image URL
                        </label>
                        <div class=" col-md-8 ">
                            <input type="text" class="form-control" id="videoId" placeholder="http://youtu.be/yourvideo" name="field3" />
                            <label class="help-block">
                                To add a Youtube video to any media slide, simply click the “Share” button on Youtube beneath your video, copy the URL thats generated and paste in the Youtube video field for that slide. Your video must be public in order to play correctly.
                            </label>
                        </div>
                    </div>
                    <div class=" form-group">
                        <label class="col-md-4 control-label" for="ptsid_288_27">
                            Description
                        </label>
                        <div class=" col-md-8 ">
                            <textarea class="form-control" name="field4" placeholder="Description for your image or video" rows="4"></textarea>
                        </div>
                    </div>
                    <div class=" form-group">
                        <label class="col-md-4 control-label" for="active">
                            Status
                        </label>
                        <div class=" col-md-8 ">
                            <div class="radio-inline">
                                <input type="radio" value="1" id="active" checked name="field5" />
                                <label for="active">Visible</label>
                            </div>
                            <div class="radio-inline">
                                <input type="radio" value="0" id="inactive" name="field5" />
                                <label for="inactive">Invisible</label>
                            </div>
                        </div>
                    </div>
                    <div class=" form-group">
                        <label class="col-md-4 control-label" for="sortOrder">
                            Slide Priority
                        </label>
                        <div class=" col-md-8 ">
                            <input type="text" class="form-control" id="sortOrder" placeholder="0" name="field6" value="9" />
                            <label class="help-block">
                                You can sort the order of the showcase slides by changing the priority number of each. You can change the order to customize the initial look of the showcase media.
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">
                                Close
                            </button>
                            <button id="saveNewSlide" type="submit" class="btn btn-sm btn-primary  hidden-sm">
                                Save Changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
  <!-- end of Create a new slideshow modal form -->
  <div class="block">
    <div class="block-title">
      <h2>Your <strong>Profile Carousel</strong></h2>
    </div>
    <script>
$(document).ready(function () {
    var coverflow = new FWDRoyal3DCoverflow({
        coverflowHolderDivId: 'show1',
        coverflowDataListDivId: 'coverflowData',
        displayType: 'responsive',
        autoScale: 'yes',
        coverflowWidth: 1800,
        coverflowHeight: 590,
        skinPath: 'http://static.reshape.net/LI3/css/showcase/skin_modern_silver',
        backgroundColor: 'none',
        backgroundRepeat: 'repeat-x',
        showDisplay2DAlways: 'no',
        coverflowStartPosition: 'center',
        numberOfThumbnailsToDisplayLeftAndRight: 4,
        slideshowDelay: 5000,
        autoplay: 'no',
        showPrevButton: 'no',
        showNextButton: 'no',
        showSlideshowButton: 'no',
        disableNextAndPrevButtonsOnMobile: 'no',
        controlsMaxWidth: 940,
        slideshowTimerColor: '#777777',
        showContextMenu: 'no',
        addKeyboardSupport: 'yes',
        thumbnailWidth: 650,
        thumbnailHeight: 366,
        thumbnailXOffset3D: 60,
        thumbnailXSpace3D: 60,
        thumbnailZOffset3D: 142,
        thumbnailZSpace3D: 28,
        thumbnailYAngle3D: 20,
        thumbnailXOffset2D: 20,
        thumbnailXSpace2D: 30,
        thumbnailBorderSize: 0,
        thumbnailBackgroundColor: '#666666',
        thumbnailBorderColor1: '#fcfdfd',
        thumbnailBorderColor2: '#e4e4e4',
        transparentImages: 'no',
        maxNumberOfThumbnailsOnMobile: 10,
        showThumbnailsGradient: 'yes',
        showThumbnailsHtmlContent: 'yes',
        textBackgroundColor: '#333333',
        thumbnailBorderColor1: '#fcfdfd',
        thumbnailBorderColor2: '#e4e4e4',
        textBackgroundOpacity: .7,
        showText: 'yes',
        showTextBackgroundImage: 'no',
        showThumbnailBoxShadow: 'yes',
        thumbnailBoxShadowCss: '0px 2px 2px #555555',
        showReflection: 'no',
        reflectionHeight: 60,
        reflectionDistance: 0,
        reflectionOpacity: .2,
        showScrollbar: 'no',
        disableScrollbarOnMobile: 'yes',
        enableMouseWheelScroll: 'yes',
        scrollbarHandlerWidth: 300,
        scrollbarTextColorNormal: '#777777',
        scrollbarTextColorSelected: '#000000',
        showComboBox: 'no',
        startAtCategory: 1,
        selectLabel: 'SELECT CATEGORIES',
        allCategoriesLabel: 'All Categories',
        showAllCategories: 'yes',
        comboBoxPosition: 'topright',
        selectorBackgroundNormalColor1: '#fcfdfd',
        selectorBackgroundNormalColor2: '#e4e4e4',
        selectorBackgroundSelectedColor1: '#a7a7a7',
        selectorBackgroundSelectedColor2: '#8e8e8e',
        selectorTextNormalColor: '#8b8b8b',
        selectorTextSelectedColor: '#FFFFFF',
        buttonBackgroundNormalColor1: '#e7e7e7',
        buttonBackgroundNormalColor2: '#e7e7e7',
        buttonBackgroundSelectedColor1: '#a7a7a7',
        buttonBackgroundSelectedColor2: '#8e8e8e',
        buttonTextNormalColor: '#000000',
        buttonTextSelectedColor: '#FFFFFF',
        comboBoxShadowColor: '#000000',
        comboBoxHorizontalMargins: 12,
        comboBoxVerticalMargins: 12,
        comboBoxCornerRadius: 0,
        addLightBoxKeyboardSupport: 'yes',
        showLightBoxNextAndPrevButtons: 'yes',
        showLightBoxZoomButton: 'yes',
        showLightBoxInfoButton: 'yes',
        showLighBoxSlideShowButton: 'yes',
        showLightBoxInfoWindowByDefault: 'no',
        slideShowAutoPlay: 'no',
        lightBoxVideoAutoPlay: 'no',
        lightBoxVideoWidth: 640,
        lightBoxVideoHeight: 480,
        lightBoxIframeWidth: 800,
        lightBoxIframeHeight: 600,
        lightBoxBackgroundColor: '#000000',
        lightBoxInfoWindowBackgroundColor: '#FFFFFF',
        lightBoxItemBorderColor1: '#fcfdfd',
        lightBoxItemBorderColor2: '#e4FFe4',
        lightBoxItemBackgroundColor: '#333333',
        lightBoxMainBackgroundOpacity: .8,
        lightBoxInfoWindowBackgroundOpacity: .9,
        lightBoxBorderSize: 5,
        lightBoxBorderRadius: 0,
        lightBoxSlideShowDelay: 4000
    });
})
        </script>
    <div class="form-horizontal">
      <div class="col-md-12"> <span></span> </div>
      <center>
        <div class="form-group">
          <div id="show1"></div>
          <div id="coverflowData" style="display: none;">
            <ul data-cat="Category 1">
            </ul>
          </div>
        </div>
        <div class="form-group form-actions">
          <div class="col-md-12">
            <button type="submit" class="btn btn-sm btn-primary  hidden-sm" data-toggle="modal" data-target="#createSlide"> <i class=" fa fa-angle-right fa-fw"></i> Add a new slide </button>
          </div>
        </div>
      </center>
    </div> 
  </div>
</div>
<script>
    $(function () {
        FormsWizard.init();
    });
</script> 
<script>
    $(function () {
        $('#background-color').colorselector();
    });
</script> 
<script src="<?php	$Li3->EchoStaticPath("js/showcase/FWDRoyal3DCoverflow.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/pages/profile-formsWizard.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/pages/profile-jquery.duplicate.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/ajax/ajaxCalls_ProfessionalProfile.js");	?>" type="text/javascript"></script> 
<script src="http://bootstrap-colorselector.flaute.com/lib/bootstrap-colorselector-0.2.0/js/bootstrap-colorselector.js" type="text/javascript"></script> 
