<?php
/** @var Li3instance */
global	$Li3;
$page_title	=	"Services Offered";
$page_description	=	"Create &amp; edit the services you will offer";
$page_icon	=	"sidebar-nav-icon fa fa-certificate";
include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg.php");
?>

<div class="block">
  <div class="block-title">
    <div class="service-filter">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">
            <button class="btn btn-primary newservicebtn" data-toggle="modal" data-target="#modal-new-service">ADD NEW SERVICE</button>
          </div>
          <div class="col-md-10 push-right">
            <div class="filterright">
              <h4 class="filtertitle">FILTER BY:</h4>
              <ul class="filtericons">
                <li><a href="#" class="service_filter_all_top" data-toggle="tooltip" title="Show all"></a></li>
                <li><a href="#" class="service_filter_one" data-toggle="tooltip" title="One to One"></a></li>
                <li><a href="#" class="service_filter_class" data-toggle="tooltip" title="Class"></a></li>
                <li><a href="#" class="service_filter_group" data-toggle="tooltip" title="Group"></a></li>
                <li><a href="#" class="service_filter_broadcast" data-toggle="tooltip" title="Broadcast"></a></li>
                <li><a href="#" class="service_filter_person" data-toggle="tooltip" title="In Person"></a></li>
                <li><a href="#" class="service_filter_private" data-toggle="tooltip" title="Private"></a></li>
              </ul>
            </div>
            <div class="viewright">
              <h4 class="viewtitle">VIEW AS:</h4>
              <ul class="filtericons">
                <li><a href="/su/ref/L2FkbWluL2FjY3Qv/4/services/home/grid" data-original-title="Grid" data-toggle="tooltip" class="gridicon"> <i class=" fa fa-2x fa-th gridicon"></i></a></li>
                <li style="margin-left: -8px;"><a href="/su/ref/L2FkbWluL2FjY3Qv/4/services/home/list" data-original-title="List" data-toggle="tooltip" class="listicon"> <i class=" fa fa-2x fa-th-list listicon"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class=" row">
    <div class=" col-md-4 col-lg-3 ">
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://pictures.paultesar.com/p/id_1637__b_0__ext_jpeg__w_640__hf_1/blob') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"> </div>
        </div>
        <h2 class="servicename text-center">My service</h2>
        <p class="text-center serviceinfo"> <strong>Other: </strong>0 mins/$0.00 </p>
      </div>
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://pictures.paultesar.com/p/id_4030__b_0__ext_jpeg__w_640__hf_1/blob') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">Core Explore</h2>
        <p class="text-center serviceinfo"> <strong>1-on-group: </strong>45 mins/$20.00 </p>
      </div>
    </div>
    <div class=" col-md-4 col-lg-3 ">
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://pictures.paultesar.com/p/id_1615__b_0__ext_jpeg__w_640__hf_1/blob') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">Bicep training</h2>
        <p class="text-center serviceinfo"> <strong>1-on-1: </strong>30 mins/$60.00 </p>
      </div>
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://static.reshape.net/LI3/img/assets/serviceSample.png') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">Core Attack</h2>
        <p class="text-center serviceinfo"> <strong>Other: </strong>30 mins/$30.00 </p>
      </div>
    </div>
    <div class=" col-md-4 col-lg-3 ">
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://pictures.paultesar.com/p/id_4031__b_0__ext_jpeg__w_640__hf_1/blob') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">Making Healthy Choices</h2>
        <p class="text-center serviceinfo"> <strong>Broadcast: </strong>60 mins/$10.00 </p>
      </div>
      <div class="block servicebox">
        <div class=" block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://static.reshape.net/LI3/img/assets/serviceSample.png') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">Test 093014</h2>
        <p class="text-center serviceinfo"> <strong>1-on-1: </strong>60 mins/$150.00 </p>
      </div>
    </div>
    <div class=" col-md-4 col-lg-3 ">
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://pictures.paultesar.com/p/id_3781__b_0__ext_jpeg__w_640__hf_1/blob') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">Personal Training</h2>
        <p class="text-center serviceinfo"> <strong>1-on-1: </strong>60 mins/$200.00 </p>
      </div>
      <div class="block servicebox">
        <div class="block-title block-title-light">
          <div class="pull-right servicebtn">
            <div class="btn-group"> <a class="btn btn-alt btn-sm btn-danger"><i class="fa fa-pencil"></i></a> </div>
          </div>
          <div style="background:url('http://static.reshape.net/LI3/img/assets/serviceSample.png') no-repeat; background-size: cover; min-height: 180px; background-position: center center;"></div>
        </div>
        <h2 class="servicename text-center">test</h2>
        <p class="text-center serviceinfo"> <strong>Broadcast: </strong>60 mins/$6.00 </p>
      </div>
    </div>
  </div>
</div>
<div id="modal-new-service" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title"> Create new service </h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered" id="modal-edit-service-form" action="/su/ref/L2FkbWluL2FjY3Qv/4/services/home/grid/" method="post" enctype="multipart/form-data" novalidate>
          <div class="block-full"></div>
          <input type="hidden" name="ptshiddenfield" value="modal-edit-service-form">
          <fieldset>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="serviceType">Service Type <span class="text-danger">* </span> </label>
              <div class=" col-md-7 ">
                <select class="form-control" id="serviceType" name="serviceType" style="height:34px;" size="1">
                  <option value="10">1-on-1</option>
                  <option value="20">1-on-group</option>
                  <option value="40">1-on-class</option>
                  <option value="50">Broadcast</option>
                  <option value="60">Other</option>
                </select>
              </div>
														<div class="col-md-1" style="margin-top: 3px; margin-left: -9px;">
																<a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a>
														</div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="serviceTitle">Service Title </label>
              <div class=" col-md-8 ">
                <input type="text" class="form-control" id="serviceTitle" placeholder="Enter Service Title" name="field2">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="main-title">Service Picture</label>
              <div class="col-md-8">
                <input id="profilePic" class="form-control ui-wizard-content" type="file" style="height: auto;">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="serviceTitle">Service Description </label>
              <div class=" col-md-8 ">
                <textarea id="servicedescription" name="servicedescription" rows="4" class="form-control" placeholder="Description of your service"></textarea>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="servicePrice">Service Price </label>
              <div class=" col-md-8 ">
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                  <input type="text" id="example-input3-group1" name="example-input3-group1" class="form-control" placeholder="Enter hourly price">
                  <span class="input-group-addon">.00</span> </div>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="serviceDuration">Service Duration </label>
              <div class=" col-md-8 ">
                <div class="input-group bootstrap-timepicker">
                  <input type="text" id="serviceduration" name="duration" class="form-control input-timepicker">
                  <span class="input-group-btn"> <a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a> </span> </div>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="visibility">Visibility </label>
              <div class=" col-md-8" style="margin-top: 6px;">
                <label for="visibility">
                  <input type="checkbox" id="hideservice" name="hideservice" value="option1">
                  &nbsp; Hide this service </label>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="formId" value="modal-new-service">
          <div class="form-group form-actions">
            <div class="col-xs-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
              <button type="submit" class="btn btn-sm btn-primary  hidden-sm">Save </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#serviceduration').timepicker({
      minuteStep: 5,
      showSeconds: false,
      showMeridian: false,
      disableFocus: true
  });
</script>
<?php
include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_end.php");
?>
