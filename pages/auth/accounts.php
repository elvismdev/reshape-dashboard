<?php
  /** @var Li3instance */
  global	$Li3;
  $page_title	=	"System Accounts";
  $page_description	=	"Manage ReShape Users";
  $page_icon	=	"sidebar-nav-icon gi gi-settings";
  include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg_admin.php");
?>

  <div class="block">
    <div class="block-title">
      <div class="service-filter">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-2">
              <button class="btn btn-primary newservicebtn" onClick="add_account();">ADD NEW ACCOUNT</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="services_list" style="padding:10px;">

      <?php
        $token = $Li3->GetToken();
        $userId = $Li3->GetUserId();

        $profile_url = $Li3->GetProfileURL();

        // Professional info
        $proResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/search/activeUsers?token=". $token. "&userTypeCode=0", null); 
      
        $userList = $proResponseData["users"];
        
//        print_r($userList);
  
        foreach( $userList as $userInfo ) {
          
          $user_id = $userInfo["id"];
          $user_name = $userInfo["name"];
          $user_email = $userInfo["email"];
          $user_phone = $userInfo["phone"];
          $user_type = $userInfo["typeCodes"];
          
          if ( $user_type == "10" ) { $user_type = "Professional"; }
          if ( $user_type == "11" ) { $user_type = "Subscriber"; }
          if ( $user_type == "12" ) { $user_type = "Corporate"; }
          if ( $user_type == "13" ) { $user_type = "Facility"; }
          if ( $user_type == "14" ) { $user_type = "Personal"; }
          if ( $user_type == "15" ) { $user_type = "Associate"; }
          if ( $user_type == "16" ) { $user_type = "Gym"; }
          if ( $user_type == "20" ) { $user_type = "Admin"; }
          
          $user_picture = $userInfo["picture_url"];
            
          $list_td .= "
          <tr data=\"".$ind_typeCode."\">
            <td align=\"center\"><img src=\"".$user_picture."\" style=\"max-height:64px;\" class=\"img-circle\"></td>
            <td>".$user_name."</td>
            <td>".$user_email."</td>
            <td>".$user_phone."</td>
            <td>".$user_type."</td>
            <td align='center'>
              <div class=\"btn-group btn-group-xs\">
                <a data-toggle=\"tooltip\" href=\"/loginas?id=".$user_id."\" title=\"\" class=\"btn btn-default\" data-original-title=\"Enter Dashboard\"><i class=\"fa fa-desktop\"></i></a>
                <a data-toggle=\"tooltip\" href=\"".$profile_url.$user_id."\" title=\"\" class=\"btn btn-default\" data-original-title=\"View Profile\" target=\"profile\"><i class=\"fa fa-user fa-fw\"></i></a>
              </div>
            </td>
          </tr>";
          
        }
        
      ?>


      <table id="services_list_table" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Type</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Type</th>
            <th>Actions</th>
          </tr>
        </tfoot>
        <tbody>
          <?php echo $list_td; ?>
        </tbody>
      </table>
    </div>
  </div>

  <div id="modal-new-account" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center modal-bg">
          <div class="form-actions"></div>
          <h2 id="AddEditModal" class="modal-title">Create new service</h2><h4 style="text-transform: uppercase; font-size: 11px;"><span style="color:#d22130;">*</span> All fields are required</h4></span>
        </div>
        <div class="modal-body">
          <form id="AddEditServiceForm" class="form-horizontal form-bordered modal-padding" id="modal-edit-service-form" action="" method="post" enctype="multipart/form-data" novalidate>
            <div class="block-full"></div>
            <input type="hidden" id="CurrentAction">
            <input type="hidden" id="serviceId">
            <fieldset>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="ServiceType">Service Type </label>
                <div class=" col-md-7 ">
                  <select class="form-control" id="ServiceType" name="ServiceType" style="height:34px;" size="1">
                    <option value="10">1-on-1</option>
                    <option value="20">1-on-group</option>
                    <option value="40">1-on-class</option>
                    <option value="50">Broadcast</option>
                    <option value="60">Other</option>
                  </select>
                </div>
                <div class="col-md-1" style="margin-top: 3px; margin-left: -9px;">
                  <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="ServiceTitle">Service Title </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="ServiceTitle" placeholder="Enter Service Title" name="field2">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="main-title">Service Picture</label>
                <div class="col-md-8">
                  <!--<input id="profilePic" class="form-control ui-wizard-content" type="file" style="height: auto;">-->
					  <div id="service-image-editor"> 					
						<!-- .cropit-image-preview-container is needed for background image to work -->
						<div class="cropit-image-preview-container">
							<input id="chose-service-pic" type="file" class="cropit-image-input">
							<div data-name="pic" class="cropit-image-preview"></div>
							<br/>
							<div class="col-md-3">
								<input data-name="pic" type="range" class="cropit-image-zoom-input" style="width:118px;">
							</div>
						</div>									
					</div>
                </div>
              </div>
              <div class="form-group" style="display:block;">
                <label class="col-md-4 control-label" for="ServiceDescription">Service Description </label>
                <div class=" col-md-8 ">
                  <textarea id="ServiceDescription" name="ServiceDescription" rows="6" class="form-control" placeholder="Description of your service" style="resize:none;"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="servicePrice">Service Price </label>
                <div class=" col-md-8 ">
                  <div class="input-group"> <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                    <input type="text" id="ServicePrice" name="ServicePrice" class="form-control key-numeric" placeholder="Enter total price"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="serviceDuration">Service Duration </label>
                <div class=" col-md-8 ">
                  <div class="input-group"> <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input type="text" id="ServiceDuration" name="ServiceDuration" class="form-control key-numeric" placeholder="Enter total minutes">
                    <span class="input-group-addon">minutes</span> </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="visibility">Visibility </label>
                <div class=" col-md-8" style="margin-top: 6px;">
                  <label for="visibility">
                  <input type="checkbox" id="ServiceHidden" name="ServiceHidden" value="option1">
                  &nbsp; Hide this service </label>
                </div>
              </div>
            </fieldset>
            <input type="hidden" name="formId" value="modal-new-service">
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button id="DeleteServiceButton" type="button" data-dismiss="modal" class="btn btn-sm btn-danger hidden-sm pull-left">Delete Service </button>
				<button type="button" data-dismiss="modal" class="btn btn-sm btn-default hidden-sm">Cancel </button>
                <button id="AddEditButton" type="button" class="btn btn-sm btn-primary hidden-sm" onClick="update_service($('#serviceId').val());">Save </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.3/css/jquery.dataTables.css">
  <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>      
  <script type="text/javascript" language="javascript" src="/static/Li3/js/pages/equal_height_rows.js"></script>  <script>
    $(document).ready(function() {
      $('#services_list_table').DataTable({
        "order": [[ 1, "asc" ]],   
        "columnDefs": [ { "targets": 0, "orderable": false },{ "targets": 5, "orderable": false }  ]
      });	  
    });
  </script>  

  <?php include($_SERVER['DOCUMENT_ROOT']	."/static/inc/page_content_end.php"); ?>