<?php
  /** @var Li3instance */
  global	$Li3;
  $page_title	=	"Dynamic Attributes";
  $page_description	=	"Manage Dynamic Attributes";
  $page_icon	=	"sidebar-nav-icon gi gi-settings";
  include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg_admin.php");
?>

  <div class="block">
    <div class="block-title">
      <div class="service-filter">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-2">
              <button class="btn btn-primary newservicebtn" onClick="">ADD NEW ATTRIBUTE</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="services_list" style="padding:10px;">

      <?php
      
        $token = $Li3->GetToken();
        $userId = $Li3->GetUserId();

        $IsAdmin = $Li3->IsUserAdmin( $userId );
        echo "@".$IsAdmin."@";
      
      
      ?>
      
    </div>
  </div>

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.3/css/jquery.dataTables.css">
  <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>      
  <script type="text/javascript" language="javascript" src="/static/Li3/js/pages/equal_height_rows.js"></script>  <script>
  </script>  

  <?php include($_SERVER['DOCUMENT_ROOT']	."/static/inc/page_content_end.php"); ?>