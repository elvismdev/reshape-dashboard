<?php
/** @var Li3instance */
global $Li3;
$ProfileName = $Li3->GetUserName();
$page_title = "Welcome " . $ProfileName;
$page_description = "Create &amp; edit the services you will offer";
$page_icon = "sidebar-nav-icon fa fa-home";

$UserAdmin = $Li3->GetUserAdmin();

if ($UserAdmin != "") {
    $first_item = "
    <ul class=\"breadcrumb breadcrumb-top\">
      <li class=\"bcfirst-li\"><button type=\"button\" class=\"btn btn-xs btn-warning\" onclick=\"window.location='/logoutadmin?id=" . $UserAdmin . "'\">LOGOUT ADMIN</button></li>
    </ul>    
    ";
}

include($_SERVER['DOCUMENT_ROOT'] . "/static/inc/page_content_home.php");
?>

<div class="block">
    <div class="progress progress-striped active">
        <div id="progress-bar-wizard" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="progresssteps text-center ">
                <li style="padding-left:0px;"><center><div class="circle-icon"><i class="fa fa-check"></i></div>Carousel</center></li>
                <li><center><div class="circle-icon"><i class="fa fa-check"></i></div>General Info</center></li>
                <li><center><div class="circle-icon"><i class="fa fa-check"></i></div>Primary Location</i></center></li>
                <li><center><div class="circle-icon"><i class="fa fa-check"></i></div>Education/Awards</i></center></li>
                <li><center><div class="circle-icon"><i class="fa fa-check"></i></div>Classification</i></center></li>
                <li><center><div class="circle-icong"><i class="fa fa-minus"></i></div>Social Media</i></center></li>
                <li><center><div class="circle-icong"><i class="fa fa-minus"></i></div>Customization</i></center></li>
            </ul>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-6">
        <div class="event-calendar clearfix">
            <div class="col-lg-7 calendar-block">
                <div class="cal1 ">
                </div>
            </div>
            <div class="col-lg-5 event-list-block">
                <div class="cal-day">
                    <span>Today</span>
                    Friday
                </div>
                <ul class="event-list">
                    <li><small>Group</small><br>Personal Training @ 3:30pm <a href="#" class="session-quick"><i class="fa fa-angle-double-right"></i></a></li>
                    <li><small>One to One</small><br>Vinyasa Yoga @ 4:30pm <a href="#" class="session-quick"><i class="fa fa-angle-double-right"></i></a></li>
                    <li><small>Class</small><br>Zumba @ 5:45pm <a href="#" class="session-quick"><i class="fa fa-angle-double-right"></i></a></li>
                    <li><small>Class</small><br>Breathing Exercises @ 7:00pm <a href="#" class="session-quick"><i class="fa fa-angle-double-right"></i></a></li>
                    <li><small>Class</small><br>Bootcamp @ 9:30pm <a href="#" class="session-quick"><i class="fa fa-angle-double-right"></i></a></li>
                </ul>
                <a href="#"><span class="pull-right" style="color: #8E8E8E; margin-top: -10px; padding-right: 5px;">NEXT DAY <i class="fa fa-arrow-right"></i></span></a>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="col-sm-6 col-md-6 col-lg-6 ">
            <!-- Widget -->
            <a href="page_ready_inbox.php" class="widget widget-hover-effect1">
                <div class="widget-simple graywidget" style="min-height: 138px;">

                    <h3 style="font-size:33px; margin-top: 24px;" class="widget-content text-center animation-pullDown">
                        <strong style="font-weight: 400;">$</strong><span>1k</span>
                        <small>Revenue to date</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 ">
            <!-- Widget -->
            <a href="page_ready_inbox.php" class="widget widget-hover-effect1">
                <div class="widget-simple dgraywidget" style="min-height: 138px;">

                    <h3 style="font-size:33px; margin-top: 24px;" class="widget-content text-center animation-pullDown">
                        <strong style="font-weight: 400;">+</strong><span>3</span>
                        <small>Pending Session Requests</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 ">
            <!-- Widget -->
            <a href="page_ready_inbox.php" class="widget widget-hover-effect1">
                <div class="widget-simple dgraywidget" style="min-height: 138px;">

                    <h3 style="font-size:33px; margin-top: 24px;" class="widget-content text-center animation-pullDown">
                        <span>200</span>
                        <small>Total Clients</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6 ">
            <!-- Widget -->
            <a href="page_ready_inbox.php" class="widget widget-hover-effect1">
                <div class="widget-simple graywidget" style="min-height: 138px;">

                    <h3 style="font-size:33px; margin-top: 24px;" class="widget-content text-center animation-pullDown">
                        <span>3k</span>
                        <small>Visits, in the past month</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>


        <div class="col-md-12 ">
            <a href="#" class="widget widget-hover-effect1" style="background-color:#d3202c;">

                <div class="widget-simple"  style="min-height: 138px;">
                    <div class="row">
                        <div class="col-md-2 info-bg">
                            <div class="animation-fadeIn text-center">
                                <i class="fa fa-info infoicon"></i>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <h3 class="widget-content text-left animation-pullDown" style="color:#f2f2f2; MARGIN-top: 20px;">
                                <small style="color:#E2E2E2;">30 OCT</small>
                                <p>New Dashboard features have been added.<small style="font-size: 12px; color:#E2E2E2;">LEARN MORE</small></p>
                            </h3>
                        </div>
                        <div class="col-md-2">
                            <span class="pull-right"><i class="fa fa-chevron-right"></i></span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="block">
            <div class="block-title">
                    <h2>Newsfeed</h2>
                </div>

            <ul class="media-list media-feed media-feed-hover">



                <li class="media">
                    <a href="page_ready_user_profile.php" class="pull-left">
                        <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="Avatar" class="newsavatar img-circle">
                    </a>
                    <div class="media-body">
                        <p class="push-bit">
                            <span class="text-muted pull-right">
                                <small>45 min now</small>
                                <span class="text-danger" data-toggle="tooltip" title="From Web"><i class="fa fa-globe"></i></span>
                            </span>
                            <strong><a href="page_ready_user_profile.php">Diego</a> updated his status</strong>
                        </p>

                        <p>Check out the new sessions available</p>
                        <p>
                            <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-thumbs-o-up"></i> Like</a>
                            <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-share-square-o"></i> Share</a>
                        </p>


                        <ul class="media-list push">

                            <li class="media">
                                <a href="page_ready_user_profile.php" class="pull-left">
                                    <img src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_200/1df849d.jpg" alt="Avatar" class="newsavatar img-circle">
                                </a>
                                <div class="media-body">
                                    <a href="page_ready_user_profile.php"><strong>User</strong></a>
                                    <span class="text-muted"><small><em>18 min ago</em></small></span>
                                    <p>In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend</p>
                                </div>
                            </li>
                            <li class="media">
                                <a href="page_ready_user_profile.php" class="pull-left">
                                    <img src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_200/1df849d.jpg" alt="Avatar" class="newsavatar img-circle">
                                </a>
                                <div class="media-body">
                                    <form action="page_ready_user_profile.php" method="post" onsubmit="return false;">
                                        <textarea id="profile-newsfeed-comment1" name="profile-newsfeed-comment1" class="form-control" rows="2" placeholder="Your comment.."></textarea>
                                        <button type="submit" class="btn btn-xs pull-right btn-primary"><i class="fa fa-pencil"></i> Post Comment</button>
                                    </form>
                                </div>
                            </li>
                        </ul>

                    </div>
                </li>

                <li class="media">
                    <a href="page_ready_user_profile.php" class="pull-left">
                        <img src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_200/1df849d.jpg" alt="Avatar" class="newsavatar img-circle">
                    </a>
                    <div class="media-body">
                        <p class="push-bit">
                            <span class="text-muted pull-right">
                                <small>5 hours ago</small>
                                <span class="text-info" data-toggle="tooltip" title="From Custom App"><i class="fa fa-wrench"></i></span>
                            </span>
                            <strong><a href="page_ready_user_profile.php">User</a> updated status.</strong>
                        </p>
                        <p>Hey there! First post from the new application!</p>
                        <p>
                            <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-thumbs-o-up"></i> Like</a>
                            <a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-share-square-o"></i> Share</a>
                        </p>

                        <ul class="media-list push">
                            <li class="media">
                                <a href="page_ready_user_profile.php" class="pull-left">
                                    <img src="http://pics.reshape.net/p/id_10146__b_0__ext_jpeg__w_200/blob" alt="Avatar" class="newsavatar img-circle">
                                </a>
                                <div class="media-body">
                                    <a href="page_ready_user_profile.php"><strong>User</strong></a>
                                    <span class="text-muted"><small><em>1 hour ago</em></small></span>
                                    <p>Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum.</p>
                                </div>
                            </li>
                            <li class="media">
                                <a href="page_ready_user_profile.php" class="pull-left">
                                    <img src="http://pics.reshape.net/p/id_10146__b_0__ext_jpeg__w_200/blob" alt="Avatar" class="newsavatar img-circle">
                                </a>
                                <div class="media-body">
                                    <form action="page_ready_user_profile.php" method="post" onsubmit="return false;">
                                        <textarea id="profile-newsfeed-comment" name="profile-newsfeed-comment" class="form-control" rows="2" placeholder="Your comment.."></textarea>
                                        <button type="submit" class="btn btn-xs pull-right btn-primary"><i class="fa fa-pencil"></i> Post Comment</button>
                                    </form>
                                </div>
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="media text-center">
                    <a href="javascript:void(0)" class="btn btn-xs btn-default push">View more..</a>
                </li>

            </ul>
        </div>



    </div>

    <div class="col-md-6">
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
                    <h2>Network <small> <a href="javascript:void(0)"> 450</a></small></h2>
                </div>
                <div class="row text-center">
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="image" class="netavatar img-circle" data-toggle="tooltip" title="" data-original-title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="image" class="netavatar img-circle" data-toggle="tooltip" title="" data-original-title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="image" class="netavatar img-circle" data-toggle="tooltip" title="" data-original-title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="image" class="netavatar img-circle" data-toggle="tooltip" title="" data-original-title="Username">
                        </a>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="image" class="netavatar img-circle" data-toggle="tooltip" title="" data-original-title="Username">
                        </a>
                    </div><div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                        <a href="javascript:void(0)">
                            <img src="http://pics.reshape.net/p/id_1926__b_0__ext_jpeg__w_200__sc_1/profile_picture.jpg" alt="image" class="netavatar img-circle" data-toggle="tooltip" title="" data-original-title="Username">
                        </a>
                    </div>

                    <div class="text-center">
                        <a href="javascript:void(0)" class="btn btn-xs btn-default push">View all</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="block">
                       <div class="block-title">
                           <h2 style="line-height:2;"><small><div class="circle-icon2">3</div></small> &nbsp; Messages</h2>
                </div>
                    <div id="msgdash"  data-id="4" class="list-group list-group-contacts border-bottom push-down-10">
                        <a href="#" class="list-group-item message" data-id="253" data-participants="Marc Megna &amp; Cedric Pommepuy" data-subject="Quick Discussion" data-started-on="12/01/2014 03:44 PM" data-members="2" style="background-color: rgb(247, 247, 247);">
                            <img class="pull-left" src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_200/1df849d.jpg" alt="Cedric Pommepuy">
                            <span class="contacts-title">Cedric Pommepuy</span>
                            <i class="fa fa-times pull-right leaveicon"></i>
                            <span class="msgdate pull-right">03:44 PM</span><p>Hi Marc!</p></a>
                        <a href="#" class="list-group-item message" data-id="245" data-participants="Paul Tesar &amp; Marc Megna" data-subject="test" data-started-on="11/26/2014 06:34 PM" data-members="2" style="background-color: white;">
                            <img class="pull-left" src="http://pics.reshape.net/p/id_9927__b_0__ext_jpeg__w_200/blob" alt="Marc Megna"><span class="contacts-title">Marc Megna</span>
                            <i class="fa fa-times pull-right leaveicon"></i>
                            <span class="msgdate pull-right">01:44 PM</span><p>Whoa</p></a>
                        <a href="#" class="list-group-item message" data-id="247" data-participants="Marc Megna &amp; Paul Tesar" data-subject="bbbbb" data-started-on="11/26/2014 06:40 PM" data-members="2" style="background-color: white;">
                            <img class="pull-left" src="http://pics.reshape.net/p/id_9927__b_0__ext_jpeg__w_200/blob" alt="Marc Megna">
                            <span class="contacts-title">Marc Megna</span><i class="fa fa-times pull-right leaveicon"></i>
                            <span class="msgdate pull-right">01:38 PM</span>
                            <p>Bam</p></a><a href="#" class="list-group-item message" data-id="249" data-participants="Paul Tesar &amp; Marc Megna" data-subject="vvvvvv" data-started-on="11/26/2014 06:42 PM" data-members="2" style="background-color: white;">
                                <img class="pull-left" src="http://pics.reshape.net/p/id_8846__b_0__ext_jpeg__w_200/me_grad.jpg" alt="Paul Tesar"><span class="contacts-title">Paul Tesar</span><i class="fa fa-times pull-right leaveicon"></i>
                                <span class="msgdate pull-right">01:30 PM</span><p>Whoa</p></a>
                        <a href="#" class="list-group-item message" data-id="251" data-participants="Marc Megna" data-subject="test" data-started-on="11/26/2014 07:06 PM" data-members="1" style="background-color: white;">
                            <img class="pull-left" src="http://pics.reshape.net/p/id_9927__b_0__ext_jpeg__w_200/blob" alt="Marc Megna"><span class="contacts-title">Marc Megna</span>
                            <i class="fa fa-times pull-right leaveicon"></i>
                            <span class="msgdate pull-right">07:06 PM</span>
                            <p></p></a>
                        <a href="#" class="list-group-item message" data-id="248" data-participants="Marc Megna &amp; Paul Tesar" data-subject="ccc" data-started-on="11/26/2014 06:40 PM" data-members="2" style="background-color: white;">
                            <img class="pull-left" src="http://pics.reshape.net/p/id_9927__b_0__ext_jpeg__w_200/blob" alt="Marc Megna">
                            <span class="contacts-title">Marc Megna</span>
                            <i class="fa fa-times pull-right leaveicon"></i>
                            <span class="msgdate pull-right">06:40 PM</span>
                            <p>cccccc</p></a>
                        <a href="#" class="list-group-item message" data-id="246" data-participants="Marc Megna &amp; Paul Tesar" data-subject="aaa" data-started-on="11/26/2014 06:39 PM" data-members="2" style="background-color: white;">
                            <img class="pull-left" src="http://pics.reshape.net/p/id_9927__b_0__ext_jpeg__w_200/blob" alt="Marc Megna">
                            <span class="contacts-title">Marc Megna</span>
                            <i class="fa fa-times pull-right leaveicon"></i>
                            <span class="msgdate pull-right">06:39 PM</span>
                            <p> tes</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                
                <div class="widget" style="max-height: 471px;">
<div class="widget-advanced widget-advanced-alt">
<div class="widget-header text-center" style="background-color:#d3202c;">
<h3 class="widget-content-light text-left pull-left animation-pullDown">
<strong>Sales &amp; Earnings</strong><br>
<small>2013</small>
</h3>
<div id="chart-widget2" class="chart"></div>
</div>
<div class="widget-main" style="padding: 5px !important; margin-top:-5px;">
<div class="row text-center">
<div class="col-xs-4">
<h3 class="animation-hatch"><i class="gi gi-group"></i> <br><small><strong>7.500</strong></small></h3>
</div>
<div class="col-xs-4">
<h3 class="animation-hatch"><i class="gi gi-stopwatch"></i><br><small><strong>10.970</strong></small></h3>
</div>
<div class="col-xs-4">
<h3 class="animation-hatch"><i class="gi gi-money"></i><br><small><strong>$ 31.230</strong></small></h3>
</div>
</div>
</div>
</div>
</div>
              


                
            </div>
        </div>

    </div>
</div>


<!-- Mini Calendar js-->
<script src="static/Li3/js/minical/clndr.js"></script>
<script src="static/Li3/js/minical/event.calendar.js" type="text/javascript"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script>  /*==Slim Scroll ==*/
                                        if ($.fn.slimScroll) {
                                            $('.event-list').slimscroll({
                                                height: '340px',
                                                wheelStep: 20
                                            });

                                        }</script>

<script>$(function () {
        FormsWizard.init();
    });</script>
<script>


    var FormsWizard = function () {

        return {
            init: function () {


                // Get the progress bar and change its width when a step is shown
                var progressBar = $('#progress-bar-wizard');
                progressBar
                        .css('width', '33%')
                        .attr('aria-valuenow', '33');

                $("#progress-wizard").bind('step_shown', function (event, data) {
                    if (data.currentStep === 'progress-first') {
                        progressBar
                                .css('width', '33%')
                                .attr('aria-valuenow', '33')
                                .removeClass('progress-bar-warning progress-bar-success')
                                .addClass('progress-bar-danger');
                    }
                    else if (data.currentStep === 'progress-second') {
                        progressBar
                                .css('width', '66%')
                                .attr('aria-valuenow', '66')
                                .removeClass('progress-bar-danger progress-bar-success')
                                .addClass('progress-bar-warning');
                    }
                    else if (data.currentStep === 'progress-third') {
                        progressBar
                                .css('width', '100%')
                                .attr('aria-valuenow', '100')
                                .removeClass('progress-bar-danger progress-bar-warning')
                                .addClass('progress-bar-success');
                    }
                });


            }
        };
    }();
</script>
<script src="static/Li3/js/pages/plugins.js"></script>
<script src="static/Li3/js/widgetsStats.js"></script>
 <script>$(function(){ WidgetsStats.init(); });</script>