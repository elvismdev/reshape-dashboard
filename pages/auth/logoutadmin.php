<?php

  require_once $_SERVER['DOCUMENT_ROOT']."/classes/Li3instance.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/classes/Li3pageController.php";

  global $Li3;

  // RETRIEVE VARIABLES FROM URL =========================================
  $ProfileID = $_GET['id'];
  // =====================================================================

  // DEFINE ADDITIONAL VARIABLES =========================================
  $token = $Li3->GetToken();
  $userId  = $Li3->GetUserId( );
  // =====================================================================
  
  // GET ADDITIONAL PROFILE INFORMATION ==================================    
  $url = "http://dash.reshape.net/api/ajax/profile/info?token=".$token."&id=".$ProfileID;    
  $ProcessProfileBasic = $Li3->GetAjaxReponse($url, $postData);  
  
  $ProfileName = $ProcessProfileBasic['name'];
  $ProfilePictureURL = $ProcessProfileBasic['picture_url'];

  $Li3->SetUserId( $ProfileID );
  $Li3->SetUserName( $ProfileName );
  $Li3->SetProfilePictureURL( $ProfilePictureURL );
  $Li3->SetUserAdmin( '' );
  
  $Li3->Redirect( "/Home" );
  // =====================================================================

?>