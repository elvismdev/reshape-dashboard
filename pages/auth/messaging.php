<?php
/** @var Li3instance */
global	$Li3;
$page_title	=	"Messaging";
$page_description	=	"View and send message to your network";
$page_icon	=	"sidebar-nav-icon gi gi-envelope";
$userId  =  $Li3->GetUserId();
include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg.php");
?>

<div class="block" style="padding-bottom: 20px;"> 
  
  <!-- PAGE SPECIFIC CONTENT BEG -->
  
  <div class="block-title">
    <div class="service-filter">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-6">
            <button id="openModalCreateConversation" class="btn btn-primary newservicebtn">CREATE A CONVERSATION</button>
          </div>
          <div class="col-md-6"> <a class="pull-right" style="margin-right: -11px;" href="/Network">
            <button class="btn info-btn newservicebtn">VIEW NETWORK</button>
            </a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row convlist">
    <div class="content-frame-right col-md-4" style="height: 765px; background-color: #E5E5E5; overflow:hidden;">
      <h2 class="chatui-header">
        <div class="input-group"> <span class="input-group-btn">
          <button type="button" class="btn info-btn"><i class="fa fa-search"></i></button>
          </span>
          <input type="text" id="example-input1-group2" name="example-input1-group2" class="form-control" placeholder="Search by subject, message or member name">
        </div>
      </h2>
      <div>
        <?php $userId = 273; ?>
        <div id="conversations_container" data-id="<?php echo $userId; ?>" class="list-group list-group-contacts border-bottom push-down-10" style="max-height: 680px; overflow: hidden; overflow-y: scroll;"> </div>
      </div>
    </div>
    <div class="content-frame-body content-frame-body-left pull-right col-md-8" style="height: 680px; padding:30px;">
      <div class="row msginfo">
        <div class="col-md-12"> 
          <strong id="conversation_subject"></strong> </br>
          <small id="conversation_participants"></small> </div>
        <div class="col-md-6 text-center"> 
          <!-- <button id="leaveConversationBtn" class="btn btn-warning pull-right">Leave conversation</button>--> 
        </div>
      </div>
      <div class="scrollable-messages">
        <div class="row" style="margin-left:0px; margin-right:0px;">
          <div class="col-md-12 text-center convstartdate"> <small id="conversation_started_on"></small></div>
        </div>
        <div data-conversation="" id="messages_container" class="messages messages-img"> </div>
      </div>
      <div class="panel panel-default push-up-10">
        <div class="panel-body panel-body-search">
          <form id="form_send_message" class="form-horizontal" enctype="multipart/form-data" method="post" action="/msg/inbox/read/53/posted">
            <div class="input-group">
              <div class="input-group-btn" style="display:none;">
                <button class="btn btn-default"><span class="fa fa-camera"></span></button>
              </div>
              <input id="input_message" type="text" class="form-control" placeholder="Your message (240 max characters)" maxlength="240">
              <div class="input-group-btn">
                <button id="pushNewMessage" class="btn btn-default">Send</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Send a message modal form -->
<div id="modal-new-message" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title"> Send new message</h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered modal-padding" id="form-modal-new-message" action="" method="post" enctype="multipart/form-data" novalidate>
          <div class="block-full"></div>
          <fieldset>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="recipientsListJsId">Recipients </label>
              <div class=" col-md-8 ">
                <select class="select-chosen2" id="recipientsListJsId" name="recipientsListJsId" size="1" multiple="" style="display: none;">
                </select>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newConversationSubject">Subject</label>
              <div class=" col-md-8">
                <input type="text" class="form-control" id="newConversationSubject" placeholder="Define a subject" name="newConversationSubject">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newConversationMessageText">Message</label>
              <div class=" col-md-8">
                <textarea class="form-control" id="newConversationMessageText" placeholder="Write your first message" rows="9" name="newConversationMessageText"></textarea>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="formId" value="modal-new-service">
          <div class="form-group">
            <div class="col-xs-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
              <button id="create_conversation_btn" type="submit" class="btn btn-sm btn-primary  hidden-sm">Send Message </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="<?php	$Li3->EchoStaticPath("js/ajax/ajaxCalls_Messaging.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/growl/bootstrap-growl.min.js");	?>" type="text/javascript"></script>