<?php

/** @var Li3instance */
global	$Li3;
$page_title	=	"Multimedia";
$page_description	=	"Edit your Albums, Photos and Videos";
$page_icon = "sidebar-nav-icon fa fa-image";
include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg.php");

$token = $Li3->GetToken();
$userId = $Li3->GetUserId();
$profile_url = $Li3->GetProfileURL();

// Professional info
$albumList = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/profile/media?token=". $token. "&id=".$userId, null."&isActive=null")["albums"];

?>

<div class="block">
  <div class="block-title">
    <div class="service-filter">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-2">
            <button class="btn btn-primary newservicebtn" data-toggle="modal" data-target="#modal-album" onClick="addAlbum();">ADD NEW ALBUM</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <?php foreach($albumList as $albumInfo) { ?>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
      <div class="block servicebox equalheight">
        <div class="block-title gallery-image block-title-light">
          <div class="gallery-image-options text-center" style="margin-top:20%;bottom:inherit;">
            <a href="/AlbumMedia?id=<?php echo $albumInfo['id']; ?>" class="gallery-link btn btn-sm btn-primary" style="cursor:pointer;">
              <i class="fa fa-eye"></i> View album
            </a>
          </div>
          <div style="text-align:center; padding:10px; max-height:180px;">
            <a href="/AlbumMedia?id=<?php echo $albumInfo['id']; ?>">
              <img class="img-responsive" style="max-height:160px; opacity:1; display:inherit;" src="<?php echo $albumInfo['picture_url']; ?>">
            </a>
          </div>
        </div>
        <div style="text-align:center;">
          <h4 class="album-name" style="float: none;">
            <a href="/AlbumMedia?id=<?php echo $albumInfo['id']; ?>">
              <?php echo $albumInfo['name']; ?>
            </a>
            <a class="btn btn-sm" data-toggle="modal" data-target="#modal-album" onclick="editAlbum(<?php echo "{$albumInfo['id']}, '{$albumInfo['name']}', {$albumInfo["isActive"]}"; ?>)">
              <i class="fa fa-pencil"></i>
            </a>
          </h4>
          <p style="color: #989898;margin-top: -9px; font-size:11px;">
            <?php echo count($albumInfo["albumItems"]); ?> PHOTOS
          </p>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
</div>

<div id="modal-album" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-actions"></div>
        <h2 id="AddEditModal" class="modal-title">Add New Album</h2>
      </div>
      <div class="modal-body">
        <form id="AddEditServiceForm" class="form-horizontal form-bordered modal-padding" id="modal-edit-service-form" action="" method="post" enctype="multipart/form-data" novalidate>
          <div class="block-full"></div>
          <input type="hidden" id="currentAction">
          <fieldset>
            <div class="form-group">
              <label class="col-md-4 control-label">Album name</label>
              <div class=" col-md-8 ">
                <input type="text" id="albumName" class="form-control" placeholder="New album name">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label"></label>
              <div class="col-md-8">
                <div class="input-group">
                  <input type="checkbox" id="albumPrivate"> Album is private.
                </div>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="formId" value="modal-new-service">
          <div class="form-group">
            <div class="col-xs-12 text-right">
              <button id="deleteAlbum" type="button" data-dismiss="modal" class="btn btn-sm btn-danger hidden-sm pull-left">Delete Album </button>
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default hidden-sm">Cancel </button>
              <button id="AddEditButton" type="button" class="btn btn-sm btn-primary hidden-sm" onClick="saveAlbum($('#currentAction').val());">Save </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" language="javascript" src="/static/Li3/js/pages/equal_height_rows.js"></script>
<script type="text/javascript">
    //Boxes of equal height
    jQuery(function($) {
      $('.equalheight').responsiveEqualHeightGrid();
      $('.equalheight2').responsiveEqualHeightGrid();
    });

    //ALBUMS CRUD

    var baseUrl = "<?php  $Li3->EchoDomainNameURL( ); ?>";
    var globalAlbumId;

    function addAlbum() {
      $('#AddEditModal').html('Add New Album');
      $('#AddEditButton').html('Add Album');
      $('#deleteAlbum').css('display','none');
      $('#currentAction').val('add');
    }

    function editAlbum(albumId, albumName, isActive) {
      $('#AddEditModal').html('Edit Album');
      $('#AddEditButton').html('Update Album');
      $('#deleteAlbum').css('display','block');
      $('#currentAction').val('edit');
      $('#albumName').val(albumName);

      //Check if Album is private
      if (isActive === false) {
        $('#albumPrivate').prop('checked', true);
      } else {
        $('#albumPrivate').prop('checked', false);
      }

      $('#deleteAlbum').on("click", function(){
        delAlbum(albumId);
      });

      globalAlbumId = albumId;
    }

    function saveAlbum(currentAction) {
      if ($('#albumName').val() != '') {
        var albumData = new FormData();
        albumData.append('token', '1234');
        albumData.append('name', $('#albumName').val());

        if ($('#albumPrivate').is(':checked')) {
          albumData.append('isActive', false);
        } else {
          albumData.append('isActive', true);
        }

        if (currentAction === 'add') {
          ActionURL = baseUrl + 'api/ajax/auth/user/profile/albums/push/add';
        } else {
          albumData.append('albumId', globalAlbumId);
          ActionURL = baseUrl + 'api/ajax/auth/user/profile/albums/push/edit';
        }

        $.ajax({
          type: 'POST',
          processData: false,
          contentType: false,
          data: albumData,
          url: ActionURL,
          dataType: 'json',
          xhrFields: {
            withCredentials: true
          },
          crossDomain: true,
          success: function(data){
            swal(
              "Success!",
              (currentAction === 'add' ? "You created a new album" : "Album updated"),
              "success"
              );
            location.reload();
        // console.log(data);
      }
    });
      } else {
        swal(
          "Error",
          "You must give an album name.",
          "error"
          );
      }

    }

    var delAlbum = function(albumId) {
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this album and all images inside will be also deleted.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it",
        closeOnConfirm: false
      },
      function () {
        var token = 1234;
        var post_data = {};
        post_data["token"] = token;
        post_data["albumId"] = albumId;
        post_data["delete"] = true;

        var request = $.ajax({
          type: "POST",
          url: baseUrl + "api/ajax/auth/user/profile/albums/push/delete",
          dataType: "json",
          xhrFields: {
            withCredentials: true
          },
          crossDomain: true,
          data: post_data
        });

  // Request successful!
  request.done(function (response) {
    if (response.error == "false") {
      swal("Deleted", "The album has been deleted.", "success");
      location.reload();
    }
    else if (response.error == "true") {
      swal("Error", "There was an unexpected error.", "error");
    }
  });

  // if the request fails
  request.fail(function (jqXHR, textStatus, error) {
    console.log("search failed")
    console.log(jqXHR.statusText);
    console.log(textStatus);
    console.log(error);
  });

});
}
</script>

<?php	include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_end.php");	?>
