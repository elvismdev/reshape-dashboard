<?php

/** @var Li3instance */
global $Li3;
$page_title = "Network";
$page_description = "Your network of people....";
$page_icon = "sidebar-nav-icon gi gi-parents";
include($_SERVER['DOCUMENT_ROOT'] . "/static/inc/page_content_beg.php");

// API RELATED FIELDS ===============
$token = $Li3->GetToken();
$userId = $Li3->GetUserId();

// MAKE API CALL & OUTPUT JSON TO PHP ARRAY
$proResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/auth/user/reshNetwork/list?token=". $token. "&userId=". $userId, null);
$proResponseData = $proResponseData['networkLinks'];

?>

<div class="block">
  <!-- PAGE SPECIFIC CONTENT BEG -->

  <div class="block-title network-filter">
    <div class="col-md-4">
      <button id="add_new_contact_btn" class="btn btn-primary newservicebtn">ADD NEW CONTACT</button>
    </div>
    <div class="col-md-8" style="margin-top: 3px; float: right; margin-right: -10px;">
      <div class="btn-group pull-right"> <a href="#" data-toggle="dropdown" class="btn info-btn dropdown-toggle">FILTER BY <span class="caret"></span></a>
        <ul class="dropdown-menu text-left" id="link_type" data-active="-1">
          <li><a data-filter="-1" href="#">All</a></li>
          <li><a data-filter="0"  href="#">Default</a></li>
          <li><a data-filter="1"  href="#">Client</a></li>
          <li><a data-filter="2"  href="#">Professional</a></li>
          <li><a data-filter="3"  href="#">Sponsor</a></li>
          <li><a data-filter="4"  href="#">Sponsored person</a></li>
          <li><a data-filter="5"  href="#">Followed person</a></li>
          <li><a data-filter="6"  href="#">Follower</a></li>
          <li><a data-filter="7"  href="#">Endorsed person</a></li>
          <li><a data-filter="8"  href="#">Endorser</a></li>
          <li><a data-filter="2"  href="#">Professional</a></li>
          <li><a data-filter="30" href="#">Endorsed products</a></li>
          <li><a data-filter="31" href="#">Product endorsers</a></li>
          
        </ul>
      </div>
      <div class="input-group col-md-5 pull-right" style="margin-right: 10px;"> <span class="input-group-btn">
        <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
      </span>
      <input type="text" id="search_network_by_name" name="example-input3-group2" class="form-control" placeholder="Search">
    </div>
  </div>
</div>
<div class="row" style="padding-bottom: 20px;">
  <?php $network_a = array(); ?>
  <?php foreach ($proResponseData as $profile) { ?>
  <?php 
  // Current id
  $profile_id     = $profile['profile']['id'];
  $name           = $profile['profile']['name'];
  $picture_url    = $profile['profile']['picture_url'];
  $city           = $profile['profile']['pageData']['city'];
  $region         = $profile['profile']['pageData']['region'];
  $link_type_code = $profile['linkTypeCode'];
  
  $is_found = false;
  
    if( sizeof($network_a) == 0 ){
      $network_a[] = $profile;
      
    } else {
      foreach( $network_a as $i => $network_profile ){
        $network_profile_id = $network_profile['profile']['id'];        
        
        // Same profile found
        if( $profile_id == $network_profile_id){          
          $is_found = true;
          $network_a[$i]['linkTypeCode'] .= "," . $link_type_code;          
        }
      }
      // If not found, we add it
      if( !$is_found ){
        $network_a[] = $profile;
      }
    }
   }
   
  foreach ($network_a as $profile_n) {
    $profile_id     = $profile_n['profile']['id'];
    $name           = $profile_n['profile']['name'];
    $picture_url    = $profile_n['profile']['picture_url'];
    $city           = $profile_n['profile']['pageData']['city'];
    $region         = $profile_n['profile']['pageData']['region'];
    $link_type_code = $profile_n['linkTypeCode'];
    
    $link_type_code_a = explode(",", $link_type_code);
    
  ?>
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 network_item" style="padding-bottom:15px;" data-filters="<?php echo $link_type_code; ?>">
    <div class="feed-box text-center" data-id="<?php echo $profile_id; ?>">
      <div class="panelmsg">
        <div class="panel-body equalheight">
          <div class="corner-ribon"> <a onclick="editModal(<?php echo "'".$picture_url."', '".$name."', '".$city."', '".$region."'"; ?>);" class="btn btn-alt btn-sm btn-default edit_network_user"><i class="fa fa-pencil"></i></a> </div>
          <a href="#">
            <?php if ($picture_url) { ?>
            <img id="profilePic" alt="" style="margin-top: -2px;" src="<?php echo $picture_url; ?>">
            <?php } else { ?>
            <a href="javascript:void(0)" class="widget-icon img-circle themed-background-night"><i class="gi gi-parents"></i></a>
            <?php } ?>
          </a>
          <h4 class="network-name"><?php echo $name; ?></h4>
          <h4 class="network-location" style="margin-top: -3px;font-size: 12px;padding-bottom: 6px;color: #000;"><?php echo $city, ($region ? ', '.$region : ''); ?></h4>
          
          <p style="margin-top:-7px;">
            <small>
              <?php foreach( $link_type_code_a as $link_type) { ?>
              <?php
              switch( $link_type ) {
                case 0  : $category = 'Default'           ; break ;
                case 1  : $category = 'Client'            ; break ;
                case 2  : $category = 'Professional'      ; break ;
                case 3  : $category = 'Sponsor'           ; break ;
                case 4  : $category = 'Sponsored person'  ; break ;
                case 5  : $category = 'Followed person'   ; break ;
                case 6  : $category = 'Follower'          ; break ;
                case 7  : $category = 'Endorsed person'   ; break ;
                case 8  : $category = 'Endorser'          ; break ;
                case 30 : $category = 'Endorsed products' ; break ;
                case 31 : $category = 'Product endorsers' ; break ;
              }
              ?>
              <a href="javascript:void(0)" class="btn btn-xs btn-default" data-placement="top" data-toggle="tooltip" title="" data-original-title="Category">
                <?php echo $category; ?>
              </a>
              <?php } ?>
            </small>
          </p>
          <button onclick="msjModal(<?php echo "'".$picture_url."', '".$name."', '".$city."', '".$region."'"; ?>);" class="btn btn-sm btn-danger btn-alt open_message_modal_btn" style="margin-top: -10px;"><i class="fa fa-envelope"></i> &nbsp;SEND A MESSAGE</button>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<!-- PAGE SPECIFIC CONTENT END -->

</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="false" id="modal-new-contact" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title"> Add new contact </h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered modal-padding" id="ptsid_268_13-form" action="#" method="post" enctype="multipart/form-data">
          <div class="block-full"></div>          
            <input type="hidden" name="ptshiddenfield" value="ptsid_268_13-form">
            <fieldset>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contactName">Name <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="contactName" placeholder="John Jones" name="field1">
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contactAge">Age <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="contactAge" placeholder="27" name="field2">
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contact-gender">Gender </label>
                <div class=" col-md-8 ">
                  <select class="form-control" id="contact-gender" name="contact-gender" size="1">
                    <option selected="" value="male">Male</option>
                    <option value="female">Female</option>
                    <!--<option value="other">Other</option>-->
                  </select>
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contactHeight">Height </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="contactHeight" placeholder="6'" name="field4">
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contactWeight">Weight </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="contactWeight" placeholder="250lbs" name="field5">
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contactLocation">Location </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="contactLocation" placeholder="Miami, FL" name="field6">
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contactNotes">Notes </label>
                <div class=" col-md-8 ">
                  <textarea class="form-control" id="contactNotes" placeholder="Write a note here" rows="4" name="contactNotes"></textarea>
                </div>
              </div>
            </fieldset>
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
                <button type="submit" class="btn btn-sm btn-primary  hidden-sm">Add Contact </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Add a contact within network -->
  <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="false" id="modal_new_network_contact" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title"> Add new contact in your network </h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered modal-padding" id="ptsid_268_13-form" action="#" method="post" enctype="multipart/form-data">
          <div class="block-full"></div>                      
            <fieldset>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="contact_name">Person <span class="text-danger">* </span> </label>
                <div class=" col-md-8 ">
                  <select class="form-control" id="select_contact_name" placeholder="John Jones" name="select_contact_name">
                  </select>
                </div>
              </div>              
              <div class=" form-group">
                <label class="col-md-4 control-label" for="network_link">Type of link</label>
                <div class=" col-md-8 ">
                  <select id="network_link" class="form-control" name="network_link" size="1" multiple="">
                    <option value="0">Default</option>
                    <option value="1">My Client</option>
                    <option value="2">My Professional</option>
                    <option value="3">My Sponsor</option>
                    <option value="4">My Sponsored Person</option>
                    <option value="5">Followed Person</option>
                    <option value="6">My Follower</option>
                    <option value="7">Endorsed Person</option>
                    <option value="8">My Endorsers</option>
                    <option value="30">Endorsed Product</option>
                    <option value="31">Product Endorsers</option>
                    <!--<option value="other">Other</option>-->
                  </select>
                </div>
              </div>             
            </fieldset>
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
                <button type="button" id="save_contact_btn" class="btn btn-sm btn-primary  hidden-sm">Add Contact </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Send a message modal -->
  <div id="modal_send_message" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center modal-bg">
          <div class="form-group form-actions"></div>
          <h2 class="modal-title"> Send message</h2>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-bordered modal-padding" id="modal-new-message" action="" method="post" enctype="multipart/form-data" novalidate>
            <div class="block-full"></div>
            <fieldset>
              <div class=" form-group">
                <label class="col-md-4 control-label">Recipient </label>
                <div class="col-md-8">
                  <div class="col-md-3 recipient-img-container" style="margin-left: -23px;">
                    <!--<img class="img-circle" alt="image" src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_150/1df849d.jpg" data-toggle="tooltip" title="Cedric Pommepuy" style="width: 75px;">																													-->
                  </div>
                  <div class="col-md-9">
                    <h4 id="profileName" style="padding-left: 14px;" name="clientFullName" class="form-control-static"></h4>
                    <!--<p style="padding-left: 14px; font-size: 14px; color: #5E5E5E;">Miami, FL</p>-->
                  </div>
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="message_subject">Message </label>
                <div class=" col-md-8">
                  <input type="text" class="form-control" id="message_subject" placeholder="Write a subject" name="message_subject">
                </div>
              </div>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="message_text">Message </label>
                <div class=" col-md-8">
                  <textarea class="form-control" id="message_text" placeholder="Write your message" rows="9" name="message_text"></textarea>
                </div>
              </div>
            </fieldset>
            <input type="hidden" name="formId" value="modal-new-service">
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
                <button id="send_message_btn" type="button" class="btn btn-sm btn-primary  hidden-sm">Send Message </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Edit network contact -->
  <div id="modal_edit_network_contact" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center modal-bg">
          <div class="form-group form-actions"></div>
          <h2 class="modal-title">Edit network contact</h2>
        </div>
        <div class="modal-body">
          <form class="form-horizontal form-bordered modal-padding" id="modal-new-message" action="" method="post" enctype="multipart/form-data" novalidate>
            <div class="block-full"></div>
            <fieldset>
              <div class=" form-group">
                <label class="col-md-4 control-label">Network contact</label>
                <div class="col-md-8">
                  <div class="col-md-3 recipient-img-container" id="edit_network_image" style="margin-left: -23px;">
                    <!--<img class="img-circle" alt="image" src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_150/1df849d.jpg" data-toggle="tooltip" title="Cedric Pommepuy" style="width: 75px;">-->
                  </div>
                  <div class="col-md-9">
                    <h4 id="edit_network_name" style="padding-left: 14px;" name="clientFullName" class="form-control-static"></h4>
                    <!--<p style="padding-left: 14px; font-size: 14px; color: #5E5E5E;">Miami, FL</p>-->
                  </div>
                </div>
              </div>              
              <!--<div class=" form-group">
                <label class="col-md-4 control-label" for="message_text">Link types</label>
                <div class=" col-md-8">
                  <select multiple="" id="edit_network_link_types">
                  </select>
                </div>
              </div>-->
            </fieldset>
            <input type="hidden" name="formId" value="modal-new-service">
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button id="remove_network_contact_btn" type="button" class="btn btn-sm btn-danger hidden-sm pull-left">Remove contact</button>
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
                <!--<button id="update_network_contact_btn" type="button" class="btn btn-sm btn-primary  hidden-sm">Update contact</button>-->
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" language="javascript" src="/static/Li3/js/pages/equal_height_rows.js"></script>
  <script type="text/javascript">

    //Boxes of equal height
    jQuery(function($) {
      $('.equalheight').responsiveEqualHeightGrid();
    });

    function msjModal( img, name, city, region ) {
      $('.recipient-img-container').html('<img class="img-circle" alt="image" src="'+img+'" data-toggle="tooltip" title="'+ name + '" style="width: 75px;">');
      $('#profileName').html(name+'<p style="font-size: 14px;">'+city+(region ? ', '+region : '')+'</p>');
      $('#modal_send_message').modal('show');
    }
    
    function editModal( img, name, city, region ){
      $('#modal_edit_network_contact').find('.recipient-img-container').html('<img class="img-circle" alt="image" src="'+img+'" data-toggle="tooltip" title="'+ name + '" style="width: 75px;">');
      $('#modal_edit_network_contact').find('#edit_network_name').html(name+'<p style="font-size: 14px;">'+city+(region ? ', '+region : '')+'</p>');
      $('#modal_edit_network_contact').modal('show');    
    }
  </script>
  <script src="<?php	$Li3->EchoStaticPath("js/ajax/ajaxCalls_Network.js");	?>" type="text/javascript"></script> 
  <script src="<?php	$Li3->EchoStaticPath("js/growl/bootstrap-growl.min.js");	?>" type="text/javascript"></script>