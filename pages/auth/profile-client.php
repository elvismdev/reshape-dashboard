<?php
/** @var Li3instance */
global	$Li3;
$page_title	=	"Client Profile";
$page_description	=	"View and send message to your network";
$page_icon	=	"sidebar-nav-icon gi gi-user";
$userId	=	$Li3->GetUserId();
include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg.php");
?>

<div class="block" style="padding-left: 36px;">

				<form action="" method="post" class="form-horizontal form-bordered ui-formwizard">

								<h3 class="sub-header" style="margin-left: 4px; margin-right: 21px;">General Info</h3>

								<div class="form-group">
												<label class="col-md-4 control-label" for="gender">Gender</label>
												<div class="col-md-5">
																<select data-group="<?php	echo	$profileGenderGroupId	?>" data-name="gender" data-input="li3InputClassif" id="example-select" name="example-select" class="form-control li3Input" size="1">
																				<option value="0">Please select</option>
																				<option value="1">Male</option>			
																				<option value="2">Female</option>	
																				<option value="3">Other</option>	
																</select>
												</div>

								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="city">City</label>
												<div class="col-md-5">
																<input data-name="city" data-input="li3InputProfile" type="text" id="city" name="city" class="form-control ui-wizard-content li3Input" placeholder="Enter the city of your primary location" value="" <?php	echo	$city;	?>">
												</div>

								</div>

								<div class="form-group" style="padding-bottom: 40px;">
												<label class="col-md-4 control-label" for="region">Region</label>
												<div class="col-md-5">
																<input data-name="region" data-input="li3InputProfile" type="text" id="region" name="region" class="form-control ui-wizard-content li3Input" placeholder="Enter the region of your primary location" value = "<?php	echo	$region;	?>">
												</div>

								</div>

								<div class="form-group">

												<label class="col-md-4 control-label" for="department">Health and Fitness Goals</label>
												<div class="col-md-5">
																<select  class="select-chosen li3Input" id="id0" name="id0" size="1" 
																									data-placeholder="Select one or more options" multiple="" >
																				<option></option>
																				<option value="1">Increase Energy</option>			
																				<option value="2">Improve Strength</option>			
																				<option value="2">Improve Flexibility</option>			
																				<option value="3">Reduce Stress</option>			
																				<option value="4">Improve Endurance</option>			
																				<option value="5">Improve Diet/Eating Habits</option>			
																				<option value="6">Improve Muscle Tone and Shape</option>			
																				<option value="7">Build Immune System</option>			
																				<option value="8">Lose Weight</option>			
																				<option value="9">Training for a Specific Sport or Event</option>		
																				<option value="10">Gain Weight/Muscle</option>		
																				<option value="11">Lose Body Fat</option>		
																				<option value="12">Develop Muscle Tone</option>		
																				<option value="13">Rehabilitate an injury</option>		
																				<option value="14">Nutrition Education</option>		
																				<option value="15">Start an Exercise Program</option>		
																				<option value="16">Sports Specific Training</option>	
																</select>
												</div>

												<label class="col-md-4 control-label" for="example-text-input"></label>
												<div class="col-md-5" style="padding-top:20px;">
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="Enter any other Health and/or Fitness goals you might have"></textarea>
												</div>
								</div>

								<div class="form-group">

												<label class="col-md-4 control-label" for="department">Equipment owned</label>
												<div class="col-md-5">
																<select  class="select-chosen li3Input" id="id0" name="id0" size="1" 
																									data-placeholder="Select one or more options" multiple="" >
																				<option></option>
																				<option value="1">Yoga Mat</option>			
																				<option value="2">Dumbells</option>			
																				<option value="2">Foam Roller</option>			
																				<option value="3">Resistance bands</option>			
																				<option value="4">Jump Rope</option>			
																				<option value="5">Medicine Balls</option>			
																				<option value="6">Kettlebells</option>			
																				<option value="7">TRX</option>			
																				<option value="8">Bosu Balance Trainer</option>			
																				<option value="9">Stability/Physio Ball</option>		
																				<option value="10">Balance Board</option>		
																				<option value="11">Abdominal Wheel</option>		
																				<option value="12">Ankle Weights</option>		
																				<option value="13">Body Bars</option>		
																				<option value="14">Push Up Bars</option>		
																				<option value="15">Cross Core 180</option>		
																				<option value="16">Cardio Equipment</option>	
																				<option value="17">Heart Rate Monitor</option>	
																				<option value="18">Body Fat Measuring Equipment</option>
																				<option value="19">Scale</option>
																				<option value="20">Activity Monitor</option>
																				<option value="21">Exercise Mat</option>
																</select>
												</div>

												<label class="col-md-4 control-label" for="example-text-input"></label>
												<div class="col-md-5" style="padding-top:20px;">
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="Enter any other equipment you own"></textarea>
												</div>

								</div>


								<h3 class="sub-header" style="margin-left: 4px; margin-right: 21px;">Fitness History</h3>
								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">When were you in the best shape of your life?</label>
												<div class="col-md-5" >
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="Enter answer here"></textarea>

												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="exercise">Have you been exercising consistently for the past three months?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="yesexercise">
																				<input type="radio" id="yesexercise" name="exercise" value="option1"> Yes
																</label>
																<label class="radio-inline" for="noexercise">
																				<input type="radio" id="noexercise" name="exercise" value="option2"> No
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">When did you first start thinking about getting in shape?</label>
												<div class="col-md-5" >
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="Enter answer here"></textarea>

												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">On a scale from 1-10, how would you rate your present fitness level? (1 being the worst, 10 being the best)</label>
												<div class="col-md-5" >
																<input type="text" id="example-slider-hor1" name="example-slider-hor1" class="form-control input-slider" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show">

												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">How does this medication affect your ability to exercise or achieve your fitness goal?</label>
												<div class="col-md-5" >
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="Enter answer here"></textarea>
												</div>
								</div>

								<h3 class="sub-header" style="margin-left: 4px; margin-right: 21px;">Health</h3>

								<div class="form-group">
												<label class="col-md-4 control-label" for="supervisedactivity">Has your doctor ever told you that you have a heart condition and recommended only medically supervised physical activity?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="supervisedactivityyes">
																				<input type="radio" id="supervisedactivityyes" name="supervisedactivity" value="option1"> Yes
																</label>
																<label class="radio-inline" for="supervisedactivityno">
																				<input type="radio" id="supervisedactivityno" name="esupervisedactivity" value="option2"> No
																</label>

												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="activitypain">Do you frequently have pains in your chest when you perform physical activity?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="activitypainyes">
																				<input type="radio" id="activitypainyes" name="activitypain" value="option1"> Yes
																</label>
																<label class="radio-inline" for="activitypainno">
																				<input type="radio" id="activitypainno" name="activitypain" value="option2"> No
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="pain">Have you had chest pain when you were not doing physical activity?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="painyes">
																				<input type="radio" id="painyes" name="pain" value="option1"> Yes
																</label>
																<label class="radio-inline" for="painno">
																				<input type="radio" id="painno" name="pain" value="option2"> No
																</label>
												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="balancelost">Do you lose your balance due to dizziness or do you ever lose consciousness?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="balancelostyes">
																				<input type="radio" id="balancelostyes" name="balancelost" value="option1"> Yes
																</label>
																<label class="radio-inline" for="balancelostno">
																				<input type="radio" id="balancelostno" name="balancelost" value="option2"> No
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="healthproblem">Do you have a bone, joint, or any other health problems that may cause you pain or limitations that must be addressed when developing an exercise program (i.e. diabetes, osteoporosis, high blood pressure, high cholesterol, arthritis, bulimia, anorexia, anemia, epilepsy, respiratory aliments, back problems etc...)?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="healthproblemyes">
																				<input type="radio" id="healthproblemyes" name="healthproblem" value="option1"> Yes
																</label>
																<label class="radio-inline" for="healthproblemno">
																				<input type="radio" id="healthproblemno" name="healthproblem" value="option2"> No
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="pregnant">Are you pregnant now or have given birth within the last 6 months?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="pregnantyes">
																				<input type="radio" id="pregnantyes" name="pregnant" value="option1"> Yes
																</label>
																<label class="radio-inline" for="pregnantno">
																				<input type="radio" id="pregnantno" name="pregnant" value="option2"> No
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="surgery">Have you had a recent surgery?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="surgeryyes">
																				<input type="radio" id="surgeryyes" name="surgery" value="option1"> Yes
																</label>
																<label class="radio-inline" for="surgeryno">
																				<input type="radio" id="surgeryno" name="surgery" value="option2"> No
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="medication">Do you take any medications, either prescription or non-prescription, on a regular basis?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="medicationyes">
																				<input type="radio" id="medicationyes" name="medication" value="option1"> Yes
																</label>
																<label class="radio-inline" for="medicationno">
																				<input type="radio" id="medicationno" name="medication" value="option2"> No
																</label>
												</div>
								</div>
								<h3 class="sub-header" style="margin-left: 4px; margin-right: 21px;">Nutrition</h3>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">On a scale of 1-10, how would you rate your nutrition? (1 being very poor, 10 being excellent)</label>
												<div class="col-md-5" >
																<input type="text" id="example-slider-hor1" name="example-slider-hor1" class="form-control input-slider" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="1" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show">

												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">How many times a day do you usually eat (including snacks)?</label>
												<div class="col-md-5" >
																<input data-name="snacks" data-input="li3InputProfile" type="text" id="city" name="snacks" class="form-control ui-wizard-content li3Input" placeholder="Enter your answer here">

												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="skipmeal">Do you skip meals?</label>
												<div class="col-md-5" >
																			<label class="radio-inline" for="skipmealyes">
																				<input type="radio" id="skipmealyes" name="skipmeal" value="option1"> Yes
																</label>
																<label class="radio-inline" for="skipmealno">
																				<input type="radio" id="skipmealno" name="skipmeal" value="option2"> No
																</label>
												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="breakfast">Do you eat breakfast?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="breakfastyes">
																				<input type="radio" id="breakfastyes" name="breakfast" value="option1"> Yes
																</label>
																<label class="radio-inline" for="breakfastno">
																				<input type="radio" id="breakfastno" name="breakfast" value="option2"> No
																</label>
												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">Do you eat late at night?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="eatlatesome">
																				<input type="radio" id="eatlatesome" name="eatlate" value="option1"> Sometimes
																</label>
																<label class="radio-inline" for="eatlateoften">
																				<input type="radio" id="eatlateoften" name="eatlate" value="option2"> Often
																</label>
																<label class="radio-inline" for="eatlatenever">
																				<input type="radio" id="eatlatenever" name="eatlate" value="option3">Never
																</label>
												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">What activities do you engage in while eating? (TV, reading, etc)?</label>
												<div class="col-md-5" >
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="Enter answer here"></textarea>
												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">How many glasses of water do you consume daily?</label>
												<div class="col-md-5" >
																<input data-name="water" data-input="li3InputProfile" type="text" id="city" name="water" class="form-control ui-wizard-content li3Input" placeholder="Enter your answer here">

												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="energydrop">Do you feel drops in your energy levels throughout the day?</label>
												<div class="col-md-5">
																<div class="col-md-2">
																				<label class="radio-inline" for="energydropyes" style="margin-left: -16px;">
																								<input type="radio" id="energydropyes" name="energydrop" value="option1"> Yes
																				</label>
																</div>
																<div class="col-md-10" style="padding-right: 0px; padding-left: 1px;">
																				<input data-name="energydrop" data-input="li3InputProfile" type="text" id="city" name="energydrop" class="form-control ui-wizard-content li3Input" placeholder="If yes, when?">
																</div>

												</div>
												<div class="col-md-4 control-label"></div>
												<div class="col-md-5">
																<label class="radio-inline" for="energydropno">
																				<input type="radio" id="energydropno" name="energydrop" value="option1"> No
																</label>
												</div>

								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="caleaten">Do you know how many calories you eat per day?</label>
												<div class="col-md-5">
																<div class="col-md-2">
																				<label class="radio-inline" for="caleatenyes" style="margin-left: -16px;">
																								<input type="radio" id="caleatenyes" name="caleaten" value="option1"> Yes
																				</label>
																</div>
																<div class="col-md-10" style="padding-right: 0px; padding-left: 1px;">
																				<input data-name="calories" data-input="li3InputProfile" type="text" id="city" name="calories" class="form-control ui-wizard-content li3Input" placeholder="If yes, how many?">
																</div>

												</div>
												<div class="col-md-4 control-label"></div>
												<div class="col-md-5">
																<label class="radio-inline" for="caleatenno">
																				<input type="radio" id="caleatenno" name="caleaten" value="option1"> No
																</label>
												</div>												
								</div>

								<div class="form-group">
												<div class="row">
																<label class="col-md-4 control-label" for="vitamin">Are you currently or have you ever taken a multivitamin or any other food supplements?</label>
																<div class="col-md-5">
																				<div class="col-md-2">
																								<label class="radio-inline" for="vitaminyes" style="margin-left: -16px;">
																												<input type="radio" id="vitaminyes" name="vitamin" value="option1"> Yes
																								</label>
																				</div>
																				<div class="col-md-10" style="padding-right: 0px; padding-left: 1px;">
																								<input data-name="supplements" data-input="li3InputProfile" type="text" id="city" name="supplements" class="form-control ui-wizard-content li3Input" placeholder="If yes, please list the supplements">
																				</div>

																</div>
												</div>
												<div class="row">
																<div class="col-md-4 control-label"></div>
																<div class="col-md-5">
																				<label class="radio-inline" for="vitaminno">
																								<input type="radio" id="vitaminno" name="vitamin" value="option1"> No
																				</label>
																</div>	
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="eatinghow">At work or school, you usually:</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="eatingout">
																				<input type="radio" id="eatingout" name="eatinghow" value="option1"> Eat out
																</label>
																<label class="radio-inline" for="eatingfood">
																				<input type="radio" id="eatingfood" name="eatinghow" value="option2"> Bring food
																</label>
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">How many glasses of water do you consume daily?</label>
												<div class="col-md-5" >
																<input data-name="water" data-input="li3InputProfile" type="text" id="city" name="water" class="form-control ui-wizard-content li3Input" placeholder="Enter your answer here">
												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">How many times per week do you eat out?</label>
												<div class="col-md-5" >
																<input data-name="eatouttimes" data-input="li3InputProfile" type="text" id="city" name="eatouttimes" class="form-control ui-wizard-content li3Input" placeholder="Enter your answer here">

												</div>
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="gshopping">Do you do your own grocery shopping?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="gshoppingyes">
																				<input type="radio" id="gshoppingyes" name="gshopping" value="option1"> Yes
																</label>
																<label class="radio-inline" for="gshoppingno">
																				<input type="radio" id="gshoppingno" name="gshopping" value="option2"> No
																</label>
												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="cooking">Do you do your own cooking?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="cookyes">
																				<input type="radio" id="cookyes" name="cooking" value="option1"> Yes
																</label>
																<label class="radio-inline" for="cookno">
																				<input type="radio" id="cookno" name="cooking" value="option2"> No
																</label>
												</div>
								</div>

								<h3 class="sub-header" style="margin-left: 4px; margin-right: 21px;">Lifestyle</h3>

								<div class="form-group">
												<label class="col-md-4 control-label" for="smoke">Do you smoke?</label>
												<div class="col-md-5">
																<div class="col-md-2">
																				<label class="radio-inline" for="smokeyes" style="margin-left: -16px;">
																								<input type="radio"id="smokeyes" name="smoke" value="option1"> Yes
																				</label>
																</div>
																<div class="col-md-10" style="padding-right: 0px; padding-left: 1px;">
																				<input data-name="smokehow" data-input="li3InputProfile" type="text" id="smokehow" name="smokehow" class="form-control ui-wizard-content li3Input" placeholder="If yes, how much?">
																</div>

												</div>
												<div class="col-md-4 control-label"></div>
												<div class="col-md-5">
																<label class="radio-inline" for="smokeno">
																				<input type="radio" id="smokeno" name="smoke" value="option2"> No
																</label>
												</div>												
								</div>

								<div class="form-group">
												<label class="col-md-4 control-label" for="alcohol">Do you drink alcohol?</label>
												<div class="col-md-5">
																<div class="col-md-2">
																				<label class="radio-inline" for="alcoholyes" style="margin-left: -16px;">
																								<input type="radio" id="alcoholyes" name="alcohol" value="option1"> Yes
																				</label>
																</div>
																<div class="col-md-10" style="padding-right: 0px; padding-left: 1px;">
																				<input data-name="calories" data-input="li3InputProfile" type="text" id="city" name="calories" class="form-control ui-wizard-content li3Input" placeholder="If yes, how many glasses per week">
																</div>

												</div>
												<div class="col-md-4 control-label"></div>
												<div class="col-md-5">
																<label class="radio-inline" for="alcoholno">
																				<input type="radio" id="alcoholno" name="alcohol" value="option1"> No
																</label>
												</div>												
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">How many hours of sleep do you regularly get each night?</label>
												<div class="col-md-5" >
																<input data-name="sleephours" data-input="li3InputProfile" type="text" id="city" name="sleephours" class="form-control ui-wizard-content li3Input" placeholder="Enter your answer here">

												</div>
								</div>
								<div class="form-group">
												<label class="col-md-4 control-label" for="jobdescription">Describe your job:</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="jobdseden">
																				<input type="radio" id="jobdseden" name="jobdescription" value="option1"> Sedentary
																</label>
																<label class="radio-inline" for="jobdactive">
																				<input type="radio" id="jobdactive" name="jobdescription" value="option2"> Active
																</label>
																<label class="radio-inline" for="jobdphysical">
																				<input type="radio" id="jobdphysical" name="jobdescription" value="option3"> Physically Demanding
																</label>
												</div>
								</div>		
								<div class="form-group">
												<label class="col-md-4 control-label" for="jobtravel">Does your job require travel?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="jobtravelyes">
																				<input type="radio" id="jobtravelyes" name="jobtravel" value="option1"> Yes
																</label>
																<label class="radio-inline" for="jobtravelno">
																				<input type="radio" id="jobtravelno" name="jobtravel" value="option2"> No
																</label>
												</div>
								</div>	
								<div class="form-group">
												<label class="col-md-4 control-label" for="overweightfamily">Is anyone in your family overweight?</label>
												<div class="col-md-5" >
																<label class="radio-inline" for="onone">
																				<input type="radio" id="onone" name="overweightfamily" value="option1"> None
																</label>
																<label class="radio-inline" for="omother">
																				<input type="radio" id="omother" name="overweightfamily" value="option2"> Mother
																</label>
																<label class="radio-inline" for="ofather">
																				<input type="radio" id="ofather" name="overweightfamily" value="option3"> Father
																</label>
																<label class="radio-inline" for="osibling">
																				<input type="radio" id="osibling" name="overweightfamily" value="option4"> Sibling
																</label>
																<label class="radio-inline" for="ograndparents">
																				<input type="radio" id="ograndparents" name="overweightfamily" value="option5"> Grandparents
																</label>
												</div>
								</div>		
								<div class="form-group">
												<label class="col-md-4 control-label" for="example-text-input">Were you overweight as a child?</label>
												<div class="col-md-5">
																<div class="col-md-2">
																				<label class="radio-inline" for="overweightyes" style="margin-left: -16px;">
																								<input type="radio" id="overweightyes" name="overweight" value="option1"> Yes
																				</label>
																</div>
																<div class="col-md-10" style="padding-right: 0px; padding-left: 1px;">
																				<input data-name="calories" data-input="li3InputProfile" type="text" id="city" name="calories" class="form-control ui-wizard-content li3Input" placeholder="If yes, at what age(s)?">
																</div>

												</div>
												<div class="col-md-4 control-label"></div>
												<div class="col-md-5">
																<label class="radio-inline" for="overweightno">
																				<input type="radio" id="overweightno" name="overweight" value="option1"> No
																</label>
												</div>												
								</div>

								<h3 class="sub-header" style="margin-left: 4px; margin-right: 21px;">Medical History and Present Medical Conditions</h3>
								<div class="form-group">

												<label class="col-md-4 control-label" for="department">Conditions you currently have or have had in the past five years</label>
												<div class="col-md-5">
																<select  class="select-chosen li3Input" id="id0" name="id0" size="1" 
																									data-placeholder="Select one or more options" multiple="" >
																				<option></option>
																				<option value="1">Heart Attack; Coronary Bypass</option>			
																				<option value="2">High Blood Pressure</option>			
																				<option value="2">Irregular Heart Beats</option>			
																				<option value="3">Migraine/Recurrent Headaches</option>			
																				<option value="4">Back Problems</option>			
																				<option value="5">Broken Bones</option>			
																				<option value="6">Light-Headedness or Fainting</option>			
																				<option value="7">Asthma</option>			
																				<option value="8">Arthritis</option>			
																				<option value="9">Diabetes</option>		
																				<option value="10">Low Blood Pressure</option>		
																				<option value="11">Swollen, Stiff, or Painful Joints</option>		
																				<option value="12">Shoulder Problems</option>		
																				<option value="13">Epilepsy or Seizures</option>		
																				<option value="14">Emphysema</option>		
																				<option value="15">Fatigue or Lack of Energy</option>		
																				<option value="16">Hernia</option>	
																				<option value="17">Bursitis</option>	
																				<option value="18">Food Problems</option>
																				<option value="19">Neck Problems</option>
																				<option value="20">Anemia</option>
																				<option value="21">Bronchitis</option>
																				<option value="22">Shortness of Breath</option>
																				<option value="23">Limited Range of Motion</option>
																				<option value="24">High Cholesterol</option>
																				<option value="25">Trouble Sleeping</option>
																				<option value="26">Chest Discomfort</option>
																				<option value="27">Other (mention below)</option>
																</select>
												</div>

												<label class="col-md-4 control-label" for="example-text-input"></label>
												<div class="col-md-5" style="padding-top:20px;">
																<textarea id="example-textarea-input" style="height:85px;" name="example-textarea-input" rows="9" class="form-control" placeholder="If you selected any of the above or chose other, please elaborate here:"></textarea>
												</div>

								</div>

								<div class="form-group form-actions text-center" style="margin-left: -16px;">
            <div class="col-md-12">

																<button type="submit" class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button" id="next4" value="save" data-original-title="" title="">Save</button>
            </div>
								</div>

				</form>
</div>



<script>
  $('.btn').popover();
  $('.btn').click(function () {
      $('.btn').not(this).popover('hide');
  });
</script>


<script src="<?php	$Li3->EchoStaticPath("js/ajax/ajaxCalls_ProfessionalProfile.js");	?>" type="text/javascript"></script>
<script src="<?php	$Li3->EchoStaticPath("js/growl/bootstrap-growl.min.js");	?>" type="text/javascript"></script>
<script src="<?php	$Li3->EchoStaticPath("js/imagecrop/jquery.cropit.min.js");	?>" type="text/javascript"></script>  
