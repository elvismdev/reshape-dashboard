<?php
  /** @var Li3instance */
  global  $Li3;
  $page_title  =  "Professional Profile";
  $page_description  =  "Edit your ReShape Profile";
  $page_icon  =  "sidebar-nav-icon gi gi-user";
  include($_SERVER['DOCUMENT_ROOT']."/static/inc/page_content_beg.php");  
  ?>  
<link rel="stylesheet" type="text/css" href="http://bootstrap-colorselector.flaute.com/lib/bootstrap-colorselector-0.2.0/css/bootstrap-colorselector.css">
<script src="http://oss.maxcdn.com/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
<div class="block" style="background-color:#000000;">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 profile-steps col-md-offset-1">
      <!-- Clickable Wizard Content -->
      <?php
        $postData  =  array('userId'  =>  "4",  'token'  =>  $Li3->GetToken());
        $token  =  $Li3->GetToken();
        $userId  =  $Li3->GetUserId();
        $baseUrl  =  $Li3->GetDomainNameURL();
        
        // Professional info
        $proResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/auth/user/profile/page?token="  .  $token  .  "&userId="  .  $userId,  null);
        $publicName  =  $proResponseData["pageInfo"]["pageData"]["publicAlias"];
        $tagLine  =  $proResponseData["pageInfo"]["pageData"]["tagLine"];
        $profileTitle  =  $proResponseData["pageInfo"]["pageData"]["title"];
        $bio  =  $proResponseData["pageInfo"]["pageData"]["bio"];
        $city  =  $proResponseData["pageInfo"]["pageData"]["city"];
        $region  =  $proResponseData["pageInfo"]["pageData"]["region"];
        $mainDegree  =  $proResponseData["pageInfo"]["pageData"]["mainDegree"];
        $mainSchool  =  $proResponseData["pageInfo"]["pageData"]["mainSchool"];
        $backColor  =  $proResponseData["pageInfo"]["pageData"]["backColor"];
        $website  =  $proResponseData["pageInfo"]["pageData"]["website"];
        
        // Profile image and logo image
        $proProfileImageResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/profile/info?token="  .  $token  .  "&id="  .  $userId,  null);
        $profilePicFile  =  $proProfileImageResponseData["picture_url"];
        $logoPicFile  =  $proProfileImageResponseData["logo_url"];
        
        // Awards and Accomplishments
        $lineItems  =  $proResponseData["pageInfo"]["pageData"]["lineItems"];
        $lineItemsAward  =  array();
        $lineItemsAccomplishment  =  array();
        foreach  ($lineItems  as  $lineItem)  {
          if  ($lineItem["type"]  ==  "award")  {
            array_push($lineItemsAward,  $lineItem["name"]);
          }  elseif  ($lineItem["type"]  ==  "accomplishment")  {
            array_push($lineItemsAccomplishment,  $lineItem["name"]);
          }
        }
        
        // Social medias      
        $socialMediaResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/profile/social?token="  .  $token  .  "&id="  .  $userId,  null);
        foreach  ($socialMediaResponseData["socialMedia"]["accounts"]  as  $account)  {
          switch  ($account["label"])  {
            case  "Facebook"  :  $fbAcct  =  $account["account"];
              break;
            case  "Google+"  :  $googlePlusAcct  =  $account["account"];
              break;
            case  "Twitter"  :  $twitterAcct  =  $account["account"];
              break;
            case  "LinkedIn"  :  $linkedInAcct  =  $account["account"];
              break;
            case  "YouTube"  :  $youtubeUrl  =  $account["account"];
              break;
            case  "Instagram"  :  $instagramAcct  =  $account["account"];
              break;
            case  "Pinterest"  :  $pinterestAcct  =  $account["account"];
              break;
            case  "Vimeo"  :  $vimeoAcct  =  $account["account"];
              break;
            case  "Spotify"  :  $spotifyAcct  =  $account["account"];
              break;
          }
        }
        
        // Profile classifications
        $profileGenderId  =  array();
        $profileCertifications  =  array();
        $profileDepartments  =  array();
        $profileActivities  =  array();
        $profileSpecialties  =  array();
        $profileFocuses  =  array();
        
        $profileClassificationsResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/profile/classif?token="  .  $token  .  "&id="  .  $userId,  null);
        
        foreach  ($profileClassificationsResponseData["attrGroups"]  as  $group)  {
          switch  ($group["name"])  {
            case  "Gender"  :  array_push($profileGenderId,  $group["attributes"]);
              break;
            case  "Certifications"  :  array_push($profileCertifications,  $group["attributes"]);
              break;
            case  "Department"  :  array_push($profileDepartments,  $group["attributes"]);
              break;
            case  "Activities"  :  array_push($profileActivities,  $group["attributes"]);
              break;
            case  "Specialities"  :  array_push($profileSpecialties,  $group["attributes"]);
              break;
            case  "Focuses"  :  array_push($profileFocuses,  $group["attributes"]);
              break;
          }
        }
        
        // Classifications
        $departmentClassif  =  array();
        $specialtiesClassif  =  array();
        $focusesClassif  =  array();
        $activitiesClassif  =  array();
        $certificationsClassif  =  array();
        $genders  =  array();
        $profileGenderGroupId  =  "";
        $profileCertificationsGroupId  =  "";
        $profileDepartmentsGroupId  =  "";
        $profileActivitiesGroupId  =  "";
        $profileSpecialtiesGroupId  =  "";
        $profileFocusesGroupId  =  "";
        
        $classificationsResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/attributes/groups?isClassifier=null&token="  .  $token,  null);
        foreach  ($classificationsResponseData["attrGroups"]  as  $group)  {
          switch  ($group["name"])  {
            case  "Gender"  :  array_push($genders,  $group["attributes"]);
              $profileGenderGroupId  =  $group["id"];
              break;
            case  "Certifications"  :  array_push($certificationsClassif,  $group["attributes"]);
              $profileCertificationsGroupId  =  $group["id"];
              break;
            case  "Department"  :  array_push($departmentClassif,  $group["attributes"]);
              $profileDepartmentsGroupId  =  $group["id"];
              break;
            case  "Activities"  :  array_push($activitiesClassif,  $group["attributes"]);
              $profileActivitiesGroupId  =  $group["id"];
              break;
            case  "Specialities"  :  array_push($specialtiesClassif,  $group["attributes"]);
              $profileSpecialtiesGroupId  =  $group["id"];
              break;
            case  "Focuses"  :  array_push($focusesClassif,  $group["attributes"]);
              $profileFocusesGroupId  =  $group["id"];
              break;
          }
        }
        
        // Profile alias
        $aliasName  =  "";
        $profileAliasResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/auth/user/profile/metaInfo/pull/alias?token="  .  $token  .  "&userId="  .  $userId,  null);
        $aliasName  =  $profileAliasResponseData["alias"];
        
        // Carousel slides      
        $carouselSlidesResponseData  =  $Li3->GetAjaxReponse($baseUrl  .  "api/ajax/profile/showcase?token="  .  $token  .  "&id="  .  $userId  .  "&isActive=null",  null);
        
        // Live Profile URL          
        $liveProfileResponseData = $Li3->GetAjaxReponse( $baseUrl . "api/ajax/profile/info?token=". $token. "&id=". $userId, null);
        $liveProfileUrl = $liveProfileResponseData["profile_url"];
        
        ?>
      <form id="clickable-wizard" action="" method="post" class="form-horizontal form-bordered ui-formwizard">
        <!-- First Step -->
        <div id="clickable-first" class="step ui-formwizard-content" style="display: block;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <script>
            $(document).ready(function () {
              var coverflow = new FWDRoyal3DCoverflow({
                coverflowHolderDivId: 'show1',
                coverflowDataListDivId: 'coverflowData',
                displayType: 'responsive',
                autoScale: 'yes',
                coverflowWidth: 1800,
                coverflowHeight: 590,
                skinPath: 'http://static.reshape.net/LI3/css/showcase/skin_modern_silver',
                backgroundColor: 'none',
                backgroundRepeat: 'repeat-x',
                showDisplay2DAlways: 'no',
                coverflowStartPosition: 'center',
                numberOfThumbnailsToDisplayLeftAndRight: 4,
                slideshowDelay: 5000,
                autoplay: 'no',
                showPrevButton: 'no',
                showNextButton: 'no',
                showSlideshowButton: 'no',
                disableNextAndPrevButtonsOnMobile: 'no',
                controlsMaxWidth: 940,
                slideshowTimerColor: '#777777',
                showContextMenu: 'no',
                addKeyboardSupport: 'no',
                thumbnailWidth: 650,
                thumbnailHeight: 366,
                thumbnailXOffset3D: 60,
                thumbnailXSpace3D: 60,
                thumbnailZOffset3D: 142,
                thumbnailZSpace3D: 28,
                thumbnailYAngle3D: 20,
                thumbnailXOffset2D: 20,
                thumbnailXSpace2D: 30,
                thumbnailBorderSize: 0,
                thumbnailBackgroundColor: '#666666',
                thumbnailBorderColor1: '#fcfdfd',
                thumbnailBorderColor2: '#e4e4e4',
                transparentImages: 'no',
                maxNumberOfThumbnailsOnMobile: 10,
                showThumbnailsGradient: 'yes',
                showThumbnailsHtmlContent: 'yes',
                textBackgroundColor: '#333333',
                thumbnailBorderColor1: '#fcfdfd',
                thumbnailBorderColor2: '#e4e4e4',
                textBackgroundOpacity: .7,
                showText: 'yes',
                showTextBackgroundImage: 'no',
                showThumbnailBoxShadow: 'yes',
                thumbnailBoxShadowCss: '0px 2px 2px #555555',
                showReflection: 'no',
                reflectionHeight: 60,
                reflectionDistance: 0,
                reflectionOpacity: .2,
                showScrollbar: 'no',
                disableScrollbarOnMobile: 'yes',
                enableMouseWheelScroll: 'yes',
                scrollbarHandlerWidth: 300,
                scrollbarTextColorNormal: '#777777',
                scrollbarTextColorSelected: '#000000',
                showComboBox: 'no',
                startAtCategory: 1,
                selectLabel: 'SELECT CATEGORIES',
                allCategoriesLabel: 'All Categories',
                showAllCategories: 'yes',
                comboBoxPosition: 'topright',
                selectorBackgroundNormalColor1: '#fcfdfd',
                selectorBackgroundNormalColor2: '#e4e4e4',
                selectorBackgroundSelectedColor1: '#a7a7a7',
                selectorBackgroundSelectedColor2: '#8e8e8e',
                selectorTextNormalColor: '#8b8b8b',
                selectorTextSelectedColor: '#FFFFFF',
                buttonBackgroundNormalColor1: '#e7e7e7',
                buttonBackgroundNormalColor2: '#e7e7e7',
                buttonBackgroundSelectedColor1: '#a7a7a7',
                buttonBackgroundSelectedColor2: '#8e8e8e',
                buttonTextNormalColor: '#000000',
                buttonTextSelectedColor: '#FFFFFF',
                comboBoxShadowColor: '#000000',
                comboBoxHorizontalMargins: 12,
                comboBoxVerticalMargins: 12,
                comboBoxCornerRadius: 0,
                addLightBoxKeyboardSupport: 'yes',
                showLightBoxNextAndPrevButtons: 'yes',
                showLightBoxZoomButton: 'yes',
                showLightBoxInfoButton: 'yes',
                showLighBoxSlideShowButton: 'yes',
                showLightBoxInfoWindowByDefault: 'no',
                slideShowAutoPlay: 'no',
                lightBoxVideoAutoPlay: 'no',
                lightBoxVideoWidth: 640,
                lightBoxVideoHeight: 480,
                lightBoxIframeWidth: 800,
                lightBoxIframeHeight: 600,
                lightBoxBackgroundColor: '#000000',
                lightBoxInfoWindowBackgroundColor: '#FFFFFF',
                lightBoxItemBorderColor1: '#fcfdfd',
                lightBoxItemBorderColor2: '#e4FFe4',
                lightBoxItemBackgroundColor: '#333333',
                lightBoxMainBackgroundOpacity: .8,
                lightBoxInfoWindowBackgroundOpacity: .9,
                lightBoxBorderSize: 5,
                lightBoxBorderRadius: 0,
                lightBoxSlideShowDelay: 4000
              });
            
              function register_slides_events() {
                var baseUrl = "http://dash.reshape.net/";
            
                $(".editSlide").click(function (event) {
                  $("#saveNewSlide").css("display", "none");
                  $("#editCurrentSlide").css("display", "inline");
                  $("#editCurrentSlide").removeAttr("disabled");
            
                  // Get values from attributes
                  var theTitle = $(this).attr("data-slide-title");
                  var theDescription = $(this).attr("data-slide-descr");
                  var theId = $(this).attr("data-slide-id");
                  var theVideo = $(this).attr("data-slide-video");
                  var theSort = $(this).attr("data-slide-sort");
                  var isActive = $(this).attr("data-slide-isActive");
            
                  // Modal title
                  $("#createSlide .modal-title").html("Edit current slide");
            
                  // Title
                  $('#title').val(theTitle);
                  // Description
                  $('#descr').val(theDescription);
                  // Youtube video
                  $('#videoId').val(theVideo);
                  // Status
                  //var status = $('#active');
                  // Order
                  $('#sortOrder').val(theSort);
                  // Slide Id
                  $('#slideId').val(theId);
                  // Active/inactive
                  if (isActive == "true") {
                    $("#active").attr('checked', 'checked');
                  } else if (isActive == "false") {
                    $("#inactive").attr('checked', 'checked');
                  }
            
                  $("#createSlide").modal("show");
                });
            
                $(".deleteSlide").click(function (event) {
                  var theSlideId = $(this).attr("data-remove");
                  swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this slide.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it",
                    closeOnConfirm: false
                  },
                  function () {
                    var token = 1234;
                    var post_data = {};
                    post_data["token"] = token;
                    post_data["delete"] = "true";
                    post_data["slideId"] = theSlideId;
            
                    var request = $.ajax({
                      type: "POST",
                      url: baseUrl + "api/ajax/auth/user/profile/push/deleteShowcaseSlide",
                      dataType: "json",
                      xhrFields: {
                        withCredentials: true
                      },
                      crossDomain: true,
                      data: post_data
                    });
            
                    // Request successful!
                    request.done(function (response) {
                      if (response.error == "false") {
                        swal("Deleted", "The slide has been deleted.", "success");
                        location.reload();
                      } else if (response.error == "true") {
                        swal("Error", "There was an unexpected error.", "error");
                      }
                    });
            
                    // if the request fails
                    request.fail(function (jqXHR, textStatus, error) {
                      console.log("search failed")
                      console.log(jqXHR.statusText);
                      console.log(textStatus);
                      console.log(error);
                    });
            
                  });
                });
              }
              coverflow.addListener(FWDRoyal3DCoverflow.IS_API_READY, function (e) {
                register_slides_events( );
              });
            })
          </script>
          <center>
            <div class="form-group">
              <div class="col-md-12">
                <button id="addSlide" type="button" class="btn btn-sm btn-primary hidden-sm" data-toggle="modal" data-target="#createSlide"> <i class=" fa fa-angle-right fa-fw"></i> Add a new slide </button>
                <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button>
              </div>
            </div>
            <div class="form-group">
              <div id="show1"></div>
              <div id="coverflowData" style="display: none;">
                <ul data-cat="Category 1">
                  <?php
                    //Initialize slides          
                    foreach  ($carouselSlidesResponseData["slides"] as $slide)  {
                      $title  =  $slide["title"];  // data-slide-title
                      $description  =  $slide["description"];  // data-slide-descr
                      $sortOrder  =  $slide["sortorder"];    // data-slide-sort
                      $slideId  =  $slide["slideId"];  // data-slide-id
                      $pictureUrl  =  $slide["picture_url"];  // data-slide-video
                      $videoId  =  $slide["video_id"];
                      $isActive  =  $slide["isActive"];
                      // data-slide-isactive
                      echo  '            
                    <ul>                
                    <li class="slide" data-toggle="modal" data-type="none"></li>
                    <li data-html-content="">
                      <div style="position: absolute; width: 100%; height: 100%; background: transparent; background: url( '  .  $pictureUrl  .  ' ) no-repeat scroll 50% 50% / 100%">
                        <div class="btn-group-vertical btn-group-sm pull-right" style="z-index: 2;">                        
                          <a 
                          data-slide-title = "'  .  htmlentities($title)  .  '"
                          data-slide-descr = "'  .  htmlentities($description)  .  '"
                          data-slide-sort = "'  .  htmlentities($sortOrder)  .  '"
                          data-slide-id = "'  .  htmlentities($slideId)  .  '"
                          data-slide-video = "'  .  htmlentities($videoId)  .  '"
                          data-slide-isActive = "'  .  htmlentities($isActive)  .  '"
                          class="btn btn-warning editSlide"  title="Edit">
                          <i class=" gi gi gi-edit"></i>
                          </a>
                          <a class="btn btn-danger deleteSlide" data-placement="right" title="Delete" data-remove="'  .  $slideId  .  '">
                            <i class=" gi gi gi-bin"></i>
                          </a>                                          
                        </div>';
                        if ($isActive  !=  "true")  {
                          echo  '
                            <div style="z-index: 2 display:table; margin: 0 auto; opacity: 0.85;" class="btn-group-vertical btn-group-sm">
                              <p style="background-color: black; border: 1px solid red; color: white; margin-left: 34px; margin-top: 20px; padding: 10px; border-radius: 10px;">NOT VISIBLE IN PUBLIC PROFILE</p>
                          </div>';
                        }
                        echo'
                      </div>                
                    </li>
                    <li data-thumbnail-text-offset-bottom="7" data-thumbnail-text-offset-top="10" data-thumbnail-text-title-offset="35" data-thumbnail-text="">
                      <p class="largeLabel">'  .  $title  .  '</p>
                      <p class="smallLabel">'  .  $description  .  '</p>
                    </li>
                    </ul>';
                    }
                    ?>      
                </ul>
              </div>
            </div>
          </center>
        </div>
        <!-- END First Step --> 
        <!-- Second Step -->
        <div id="clickable-second" class="step ui-formwizard-content" style="display: block;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="public-name">Public Name</label>
            <div class="col-md-5">
              <input data-name="publicAlias" data-input="li3InputProfile" type="text" id="public-name" name="public-name" class="form-control ui-wizard-content li3Input" placeholder="Enter you public name" value = "<?php  echo  $publicName;  ?>">
            </div>
            <div class="col-md-3"><button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="gender">Gender</label>
            <div class="col-md-5">
              <select data-group="<?php  echo  $profileGenderGroupId  ?>" data-name="gender" data-input="li3InputClassif" id="example-select" name="example-select" class="form-control li3Input" size="1">
                <option value="0">Please select</option>
                <?php  $Li3->SetOptions($genders[0],  $profileGenderId);  ?>
              </select>
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="main-title">Main Title</label>
            <div class="col-md-5">
              <input data-name="title" data-input="li3InputProfile" type="text" id="main-title" name="main-title" class="form-control ui-wizard-content li3Input" placeholder="Enter your main title" value = "<?php  echo  $profileTitle;  ?>" maxlength="10">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="tagline">Tagline</label>
            <div class="col-md-5">
              <input data-name="tagLine" data-input="li3InputProfile" type="text" id="tagline" name="tagline" class="form-control ui-wizard-content li3Input" placeholder="Add a tagline" value = "<?php  echo  $tagLine;  ?>" />
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="main-title">Profile Picture/<br>Personal or Company logo</label>
            <div class="col-md-5">
              <div class="row">
                <div class="col-md-8 col-lg-6">
                  <div id="profile-image-editor">
                    <div class="cropit-image-preview-container">
                      <h4 style="font-size: 14px; font-weight: 500;">Profile Picture</h4>
                      <input id="chose-profile-pic" type="file" class="cropit-image-input">
                      <div data-name="pic" class="cropit-image-preview""></div>
                      <br/>
                      <div class="col-md-3">
                        <input data-name="pic" type="range" class="cropit-image-zoom-input" style="width:123px;">
                        <button data-name="pic" class="btn btn-alt btn-xs btn-default savePic" type="button" style="margin-left: 25px; margin-top: 14px;">Save picture</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-lg-6">
                  <div id="personal-logo-editor">
                    <div class="cropit-image-preview-container">
                      <h4 style="font-size: 14px; font-weight: 500;">Personal/Company Logo</h4>
                      <input id="chose-personal-pic" type="file" class="cropit-image-input">
                      <div data-name="logo" class="cropit-image-preview"></div>
                      <br/>
                      <div class="col-md-3">
                        <input data-name="logo" type="range" class="cropit-image-zoom-input" style="width:123px;">
                        <button data-name="logo" class="btn btn-alt btn-xs btn-default savePic" type="button" style="margin-left: 25px; margin-top: 14px;">Save picture</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></a> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="bio">Bio</label>
            <div class="col-md-5">
              <!-- data-name="publicAlias" data-input="li3InputProfile" type="text" id="public-name" name="public-name" class="form-control ui-wizard-content li3Input" -->
              <textarea id="bio" data-name="bio" data-input="li3InputProfile" name="bio" rows="10" class="form-control ui-wizard-content bio li3Input" placeholder="Write a short bio of yourself" disabled="disabled"><?php  echo  $bio;  ?></textarea>
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
          <div class="form-group" style="padding-bottom: 40px;">
            <label class="col-md-4 control-label" for="personal-website">Personal Website</label>
            <div class="col-md-5">
              <input data-name="website" data-input="li3InputProfile" id="personal-website" name="personal-website" class="form-control ui-wizard-content li3Input" placeholder="Enter the domain of  your personal website" value = "<?php  echo  $website;  ?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
        </div>
        <!-- END Second Step --> 
        <!-- Third Step -->
        <div id="clickable-third" class="step ui-formwizard-content" style="display: none;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="city">City</label>
            <div class="col-md-5">
              <input data-name="city" data-input="li3InputProfile" type="text" id="city" name="city" class="form-control ui-wizard-content li3Input" placeholder="Enter the city of your primary location" value = "<?php  echo  $city;  ?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group" style="padding-bottom: 40px;">
            <label class="col-md-4 control-label" for="region">Region</label>
            <div class="col-md-5">
              <input data-name="region" data-input="li3InputProfile" type="text" id="region" name="region" class="form-control ui-wizard-content li3Input" placeholder="Enter the region of your primary location" value = "<?php  echo  $region;  ?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
        </div>
        <!-- END Second Step --> 
        <!-- Third Step -->
        <div id="clickable-fourth" class="step ui-formwizard-content" style="display: none;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="degree">Education Degree</label>
            <div class="col-md-5">
              <input data-name="mainDegree" data-input="li3InputProfile" type="text" id="degree" name="degree" class="form-control ui-wizard-content li3Input" placeholder="Enter your highest degree earned" value = "<?php	echo	$mainDegree;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="institution">Institution</label>
            <div class="col-md-5">
              <input data-name="mainSchool" data-input="li3InputProfile" type="text" id="institution" name="institution" class="form-control ui-wizard-content li3Input" placeholder="Enter the institution where you earned said degree" value = "<?php	echo	$mainSchool;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>          
          <!-- BEGIN of new awards -->
          <div id="awards" class="form-horizontal">
            <div class="form-group">
              <label class="col-md-4 control-label">Awards</label>
              <?php
                $first = true;
                if( sizeof( $lineItemsAward ) == 0 ) {
                  ?>
                   <div class="item">
                    <div class="col-md-5">
                      <input type="text" class="form-control li3Input" data-input="li3InputProfile" data-name="award" name="option[]" value="<?php	echo	$award;	?>" />
                    </div>
                    <div class="col-md-3">                      
                      <button type="button" class="btn btn-sm  addButton btn-danger"><i class="fa fa-plus"></i></button>                      
                    </div>
                  </div>
                  <?php                
                }
                foreach	($lineItemsAward	as	$award)	{
              ?>
              <div class="item">
                <label class="col-md-4"></label>
                <div class="col-md-5">
                  <input type="text" class="form-control li3Input" data-input="li3InputProfile" data-name="award" name="option[]" value="<?php	echo	$award;	?>" />
                </div>
                <div class="col-md-3">
                  <?php if( $first) {
                    $first = false;
                  ?>
                  <button type="button" class="addButton btn btn-sm btn-danger"><i class="fa fa-plus"></i></button>
                  <?php } else { ?>
                    <button type="button" data-name="award" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                  <?php } ?>
                </div>
              </div>
              <?php
                }
                ?>
              <div class="hide duplicate_container_aw" id="optionTemplate_aw">
                <div class="col-sm-offset-4 col-sm-5">
                  <input class="form-control li3Input" data-input="li3InputProfile" data-name="award" type="text" name="option[]" />
                </div>
                <div class="col-md-3">
                  <button type="button" data-name="award" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                </div>
              </div>  
                
            </div>
            
          </div>
          <!-- END of new awards -->
          <!-- BEGIN of new accomplishments -->
          <div id="accomplishments" class="form-horizontal" style="padding-bottom: 25px;">
            <div class="form-group">
              <label class="col-md-4 control-label">Accomplishments</label>
              <?php
                $first = true;
                if( sizeof( $lineItemsAccomplishment ) == 0 ) {
                ?>               
                <div class="item">
                  <label class="col-md-4"></label>
                  <div class="col-md-5">
                    <input type="text" class="form-control li3Input" data-input="li3InputProfile" data-name="accomp" name="option[]" value="<?php	echo	$accomplishment;	?>" />
                  </div>
                  <div class="col-md-3">                    
                    <button type="button" class="addButton btn btn-sm btn-danger"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
                
                
                <?php
                }
                foreach	($lineItemsAccomplishment	as	$accomplishment)	{
              ?>
              <div class="item">
                <label class="col-md-4"></label>
                <div class="col-md-5">
                  <input type="text" class="form-control li3Input" data-input="li3InputProfile" data-name="accomp" name="option[]" value="<?php	echo	$accomplishment;	?>" />
                </div>
                <div class="col-md-3">
                  <?php if( $first) {
                    $first = false;
                  ?>
                  <button type="button" class="addButton btn btn-sm btn-danger" ><i class="fa fa-plus"></i></button>
                  <?php } else { ?>
                    <button type="button" data-name="accomp" class="btn btn-default removeButton" ><i class="fa fa-minus"></i></button>
                  <?php } ?>
                </div>
              </div>
              <?php
                }
                ?>
              <div class="hide duplicate_container_acc" id="optionTemplate_acc">
                <div class="col-md-offset-4 col-md-5">
                  <input class="form-control li3Input" data-input="li3InputProfile" data-name="accomp" type="text" name="option[]" />
                </div>
                <div class="col-md-3">
                  <button type="button" data-name="accomp" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                </div>
              </div>  
            </div>            
          </div>
          <!-- END of new accomplishments -->          
        </div>
        <!-- END Third Step --> 
        <!-- Fourth Step -->
        <div id="clickable-fifth" class="step ui-formwizard-content" style="display: none;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="department">Department</label>
              <div class="col-md-5">
                <select data-group="<?php	echo	$profileDepartmentsGroupId	?>" data-input="li3InputClassif" data-name="multi" class="select-chosen li3Input" id="id0" name="id0" size="1" 
                  data-placeholder="Select one or more options" multiple="" style="display: none;">
                <?php
                  // Display options
                  $Li3->SetOptions($departmentClassif[0],	$profileDepartments);
                  ?>
                </select>
              </div>
              <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="speacialties">Specialties</label>
              <div class="col-md-5">
                <select data-group="<?php	echo	$profileSpecialtiesGroupId	?>" data-input="li3InputClassif" data-name="multi" class="select-chosen li3Input" id="id1" name="id1" size="1" data-placeholder="Select one or more options" multiple="">
                <?php	$Li3->SetOptions($specialtiesClassif[0],	$profileSpecialties);	?>                    
                </select>
              </div>
              <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="focuses">Focuses</label>
              <div class="col-md-5">					
                <select data-group="<?php	echo	$profileFocusesGroupId	?>" data-input="li3InputClassif" data-name="multi" class="select-chosen li3Input" id="id2" name="id2" size="1" data-placeholder="Select one or more options" multiple="">
                <?php	$Li3->SetOptions($focusesClassif[0],	$profileFocuses);	?>
                </select>
              </div>
              <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="activities">Activities</label>
              <div class="col-md-5">
                <select data-group="<?php	echo	$profileActivitiesGroupId	?>" data-input="li3InputClassif" data-name="multi" class="select-chosen li3Input" id="id3" name="id3" size="1" data-placeholder="Select one or more options" multiple="">
                <?php	$Li3->SetOptions($activitiesClassif[0],	$profileActivities);	?>              
                </select>
              </div>
              <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
            </div>
          </div>
          <div class="form-group" style="padding-bottom: 40px;">
            <div class="row">
              <label class="col-md-4 control-label" for="certifications">Certifications</label>
              <div class="col-md-5">
                <select data-group="<?php	echo	$profileCertificationsGroupId	?>" data-input="li3InputClassif" data-name="multi" class="select-chosen li3Input" id="id4" name="id4" size="1" data-placeholder="Select one or more options" multiple="">
                <?php	$Li3->SetOptions($certificationsClassif[0],	$profileCertifications);	?>                    
                </select>
              </div>
              <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
            </div>
          </div>
        </div>
        <!-- END Fourth Step --> 
        <!-- Fifth Step -->
        <div id="clickable-sixth" class="step ui-formwizard-content" style="display: none;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="facebook"><img class="socialicon" src="/static/img/socialicons/fb.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="fbAcct" type="text" id="facebook" name="facebook" class="form-control ui-wizard-content li3Input" placeholder="Enter your Facebook page" value="<?php	echo	$fbAcct;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="twitter"><img class="socialicon" src="/static/img/socialicons/twit.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="twitterAcct" type="text" id="twitter" name="twitter" class="form-control ui-wizard-content wizard-ignore" placeholder="Enter your Twitter handle"  value="<?php	echo	$twitterAcct;	?>" disabled="disabled">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="instagram"><img class="socialicon" src="/static/img/socialicons/insta.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="instagramAcct" type="text" id="instagram" name="instagram" class="form-control ui-wizard-content wizard-ignore" placeholder="Enter your Instagram username"  value="<?php	echo	$instagramAcct;	?>" disabled="disabled">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="spotify"><img class="socialicon" src="/static/img/socialicons/spotify.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="spotifyAcct" type="text" id="spotify" name="spotify" class="form-control wizard-ignore ui-wizard-content" placeholder="Enter the link for your Spotify Playlist"  value="<?php	echo	$spotifyAcct;	?>" disabled="disabled">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="youtube"><img class="socialicon" src="/static/img/socialicons/yt.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="youTubeUrl" type="text" id="youtube" name="youtube" class="form-control ui-wizard-content li3Input" placeholder="Enter your Youtube username"  value="<?php	echo	$youtubeUrl;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="googleplus"><img class="socialicon" src="/static/img/socialicons/googleplus.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="googlePlusAcct" type="text" id="googleplus" name="googleplus" class="form-control ui-wizard-content li3Input" placeholder="Enter your Google+ username"  value="<?php	echo	$googlePlusAcct;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="linkedin"><img class="socialicon" src="/static/img/socialicons/linkedin.png"></label>
            <div class="col-md-5">
              <input data-input="li3InputMedia" data-name="linkedInAcct" type="text" id="linkedin" name="linkedin" class="form-control ui-wizard-content li3Input" placeholder="Enter your Linkedin profile URL"  value="<?php	echo	$linkedInAcct;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label" for="pinterest"><img class="socialicon" src="/static/img/socialicons/pinterest.png"></label>
            <div class="col-md-5">
              <input type="text" data-input="li3InputMedia" data-name="pinterestAcct" id="pinterest" name="pinterest" class="form-control ui-wizard-content li3Input" placeholder="Enter your Pinterest username"  value="<?php	echo	$pinterestAcct;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
          <div class="form-group" style="padding-bottom: 40px;">
            <label class="col-md-4 control-label" for="vimeo"><img class="socialicon" src="/static/img/socialicons/vimeo.png"></label>
            <div class="col-md-5">
              <input type="text" data-input="li3InputMedia" data-name="vimeoAcct" id="vimeo" name="vimeo" class="form-control ui-wizard-content li3Input" placeholder="Enter your Vimeo username"  value="<?php	echo	$vimeoAcct;	?>">
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
        </div>
        <!-- END sixth Step --> 
        <!-- Seventh Step -->
        <div id="clickable-seventh" class="step ui-formwizard-content" style="display: none;">
          <!-- Step Info -->
          <div class="form-group">
            <div class="col-xs-12">
              <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="clickable-first"><strong>Profile Carousel</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-second"><strong>General Info</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-third"><strong>Primary Location</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fourth"><strong>Education/Awards</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-fifth"><strong>Classification</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="clickable-sixth"><strong>Social Media</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="clickable-seventh"><strong>Profile Customization</strong></a></li>
              </ul>
            </div>
          </div>
          <!-- END Step Info -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="custom-url">Custom URL</label>
            <div class="col-md-5">				
              <input data-input="li3InputAlias" data-name="alias" type="text" id="custom-url" name="custom-url" class="form-control ui-wizard-content li3Input" placeholder="Enter your custom url" value = "<?php	echo	$aliasName	?>">
            </div>
            <div class="col-md-3"><button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button></div>
          </div>
          <div class="form-group" style="padding-bottom: 40px;">
            <label class="col-md-4 control-label" for="profile-background-color">Background Color</label>
            <div class="col-md-5">
              <select data-name="backColor" data-input="li3InputProfile" class="li3Input" id="profile-background-color" name="profile-background-color" size="1">
              <?php
                $colorsArray	=	array(
                array("#353536",	"Dark Gray"),
                array("#2699E5",	"Blue"),
                array("#B6B6B6",	"Light Gray"),
                array("#FC839B",	"Pink"),
                array("#774298",	"Purple"),
                array("#81cfc1",	"Teal"),
                array("#F0bb32",	"Gold Orange"),
                array("#d41a1a",	"Red"),
                array("#531f36",	"Eggplant"),
                array("#000000",	"Black")
                );
                $Li3->SetProfileBackgroundColor($backColor,	$colorsArray);
                ?>
              </select>
            </div>
            <div class="col-md-3"> <button type="button" style="margin-left: 10px;" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button> </div>
          </div>
        </div>
        <!-- END Seventh Step --> 
        <!-- Form Buttons -->
        <center>
          <div class="form-group form-actions profile-btns">
            <div class="col-md-12">
              <button type="reset" class="btn btn-sm btn-gray ui-wizard-content ui-formwizard-button" id="back4" value="Back" disabled="disabled">Previous</button>
              <button type="submit" class="btn btn-sm btn-primary ui-wizard-content ui-formwizard-button" id="next4" value="Next" disabled="disabled">Next</button>
            </div>
          </div>
        </center>
        <!-- END Form Buttons -->
      </form>
      <!-- END Clickable Wizard Content --> 
    </div>
    <div class="col-md-4 hidden-xs hidden-sm hidden-md visible-lg">
      <div class="editprof-intro">
        <h1 class="text-center prof-welcome">WELCOME</h1>
        <h1 class="text-center sub-header prof-welcomename"><?php	$Li3->EchoUserName();	?></h1>
        <p style="text-align:center; color:#DFDFDF;">Here is where you will start creating your Reshape profile. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <h4 style="font-weight: 700; font-size: 14px;">Content Input Progress</h4>
        <ul class="prof-progress">
          <li><span id="progress-carousel" style="color:#eaeaea;"><i class="fa fa-check"></i></span> Profile Carousel</li>
          <li><span id="progress-general" style="color:#eaeaea;"><i class="fa fa-check"></i></span> General Info</li>
          <li><span id="progress-location" style="color:#eaeaea;"><i class="fa fa-check"></i></span> Primary Location</li>
          <li><span id="progress-education" style="color:#eaeaea;"><i class="fa fa-check"></i></span> Education/Awards</li>
          <li><span id="progress-classification" style="color:#eaeaea;"><i class="fa fa-check"></i></span> Classification</li>
          <li><span id="progress-social" style="color:#eaeaea;"><i class="fa fa-check"></i></span> Social Media</li>
          <li><span id="progress-custom" style="color:#eaeaea;"><i class="fa fa-check"></i></span> Profile Customization</li>
        </ul>
        </br>
        <p><a href="<?php echo $liveProfileUrl; ?>" target="_blank" class="btn btn-lg btn-primary btn-block">View Live Reshape Profile</a></p>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Create a slideshow modal form -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="createSlide">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title">Add new slide
        </h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered modal-padding" id="slideForm" action="" method="post" enctype="multipart/form-data">
          <div class="block-full"></div>
          <input type="hidden" name="ptshiddenfield" value="slideForm" />
          <div class=" form-group">
            <label class="col-md-4 control-label" for="title">
            Image Title
            </label>
            <div class=" col-md-7 ">
              <input type="hidden" id="slideId" value="" />
              <input type="text" class="form-control" id="title" placeholder="Title for your image or video" name="title" />
            </div>
            <div class="col-md-1">
              <button type="button" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button>
            </div>
          </div>
          <div class=" form-group">
            <label class="col-md-4 control-label" for="picFile">
            Upload new image
            </label>
            <div class=" col-md-8 ">
              <input type="file" class="form-control" id="picFile" style="height: auto;" name="picFile" />
            </div>
          </div>
          <div class=" form-group">
            <label class="col-md-4 control-label" for="videoId">
            Youtube Video/Image URL
            </label>
            <div class=" col-md-7 ">
              <input type="text" class="form-control" id="videoId" placeholder="http://youtu.be/yourvideo" name="videoId" />
              <!--<label class="help-block">
                To add a Youtube video to any media slide, simply click the "Share" button on Youtube beneath your video, copy the URL thats generated and paste in the Youtube video field for that slide. Your video must be public in order to play correctly.
                </label>-->
            </div>
            <div class="col-md-1">
              <button type="button" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button>
            </div>
          </div>
          <div class=" form-group">
            <label class="col-md-4 control-label" for="descr">
            Description
            </label>
            <div class=" col-md-8 ">
              <textarea class="form-control" id="descr" name="descr" placeholder="Description for your image or video" rows="4"></textarea>
            </div>
          </div>
          <div class=" form-group">
            <label class="col-md-4 control-label" for="active">
            Visibility in public profile
            </label>
            <div class=" col-md-8 ">
              <div class="radio-inline">
                <input type="radio" value="1" id="active" checked name="isActive" />
                <label for="active">Visible</label>
              </div>
              <div class="radio-inline">
                <input type="radio" value="0" id="inactive" name="isActive" />
                <label for="inactive">Invisible</label>
              </div>
            </div>
          </div>
          <div class=" form-group">
            <label class="col-md-4 control-label" for="sortOrder">
            Slide Priority
            </label>
            <div class=" col-md-7 ">
              <input type="text" class="form-control" id="sortOrder" placeholder="0" name="sortOrder" value="9" />
              <!--<label class="help-block">
                You can sort the order of the showcase slides by changing the priority number of each. You can change the order to customize the initial look of the showcase media.
                </label>-->
            </div>
            <div class="col-md-1">
              <button type="button" class="btn btn-alt btn-sm info-btn" data-toggle="popover" data-content="This is an information popover! Use it easily with the content you want!" data-placement="right" title="Image Title" data-original-title="Image Title"> <i class="fa fa-info"></i> </button>
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">
              Close
              </button>
              <button id="saveNewSlide" type="submit" class="btn btn-sm btn-primary  hidden-sm">
              Save Changes
              </button>
              <button id="editCurrentSlide" type="submit" class="btn btn-sm btn-primary  hidden-sm" style="display: none;">
              Update Slide
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- end of Create a new slideshow modal form -->
<script>
  $(document).ready(function() {      
      // Awards
      $('#awards')

          // Add button click handler
          .on('click', '.addButton', function() {
              var $template = $('#optionTemplate_aw'),
                  $clone    = $template
                                  .clone()
                                  .removeClass('hide')
                                  .removeAttr('id')
                                  .insertBefore($template),
                  $option   = $clone.find('[name="option[]"]');

              // Add new field
              $('#awards').bootstrapValidator('addField', $option);              
          })

    // Accomplishments
    $('#accomplishments')

          // Add button click handler
          .on('click', '.addButton', function() {
              var $template = $('#optionTemplate_acc'),
                  $clone    = $template
                                  .clone()
                                  .removeClass('hide')
                                  .removeAttr('id')
                                  .insertBefore($template),
                  $option   = $clone.find('[name="option[]"]');
  
              // Add new field
              $('#accomplishments').bootstrapValidator('addField', $option);
          })
  });
</script>
<script language="javascript">
  // Set pictures if defined
  $(document).ready(function () {
      var profilePicFile = '<?php	echo	$profilePicFile	?>';
      var logoPicFile = '<?php	echo	$logoPicFile	?>';
  
      $('#profile-image-editor').cropit({exportZoom: 1.25, imageBackground: false, imageBackgroundBorderWidth: 20});
      $('#personal-logo-editor').cropit({exportZoom: 1.25, imageBackground: false, imageBackgroundBorderWidth: 20, });
  
      if (profilePicFile)
          $('#profile-image-editor').cropit('imageSrc', profilePicFile);
  
      if (logoPicFile)
          $('#personal-logo-editor').cropit('imageSrc', logoPicFile);
  });
</script>
<script>
  $('.btn').popover();
  $('.btn').click(function () {
     $('.btn').not(this).popover('hide');
  });
</script>
<script src="<?php	$Li3->EchoStaticPath("js/showcase/FWDRoyal3DCoverflow.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/pages/profile-jquery.duplicate.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/ajax/ajaxCalls_ProfessionalProfile.js");	?>" type="text/javascript"></script>
<script src="<?php	$Li3->EchoStaticPath("js/growl/bootstrap-growl.min.js");	?>" type="text/javascript"></script>
<script src="<?php	$Li3->EchoStaticPath("js/imagecrop/jquery.cropit.min.js");	?>" type="text/javascript"></script>  
<script src="http://bootstrap-colorselector.flaute.com/lib/bootstrap-colorselector-0.2.0/js/bootstrap-colorselector.js" type="text/javascript"></script>