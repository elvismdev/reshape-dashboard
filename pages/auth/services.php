<?php
  /** @var Li3instance */
  global	$Li3;
  $page_title	=	"Services Offered";
  $page_description	=	"Create &amp; edit the services you will offer";
  $page_icon	=	"sidebar-nav-icon fa fa-certificate";
  include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg.php");
?>

  <div class="block">
    <div class="block-title">
      <div class="service-filter">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-2">
              <button class="btn btn-primary newservicebtn" onClick="add_service();">ADD NEW SERVICE</button>
            </div>
            <div class="col-md-10 push-right">
              <div id="filter_grid" class="filterright">
                <h4 class="filtertitle">FILTER BY:</h4>
                <ul class="filtericons">
                  <li><a href="#" class="service_filter_all_top" data-toggle="tooltip" title="Show all" onClick="filter_view('all');"></a></li>
                  <li><a href="#" class="service_filter_one" data-toggle="tooltip" title="One to One" onClick="filter_view('10');"></a></li>
                  <li><a href="#" class="service_filter_class" data-toggle="tooltip" title="Class" onClick="filter_view('20');"></a></li>
                  <li><a href="#" class="service_filter_group" data-toggle="tooltip" title="Group" onClick="filter_view('40');"></a></li>
                  <li><a href="#" class="service_filter_broadcast" data-toggle="tooltip" title="Broadcast" onClick="filter_view('50');"></a></li>
                  <li style="display:none;"><a href="#" class="service_filter_person" data-toggle="tooltip" title="In Person" onClick="filter_view('all');"></a></li>
                  <li><a href="#" class="service_filter_private" data-toggle="tooltip" title="Private" onClick="filter_view('60');"></a></li>
                </ul>
              </div>
              <div class="viewright">
                <h4 class="viewtitle">VIEW AS:</h4>
                <ul class="filtericons">
                  <li><a data-original-title="Grid" data-toggle="tooltip" class="gridicon" onClick="$('#services_list').css('display','none'); $('#services_grid').css('display','block'); $('#filter_grid').css('display','block'); $('.equalheight').responsiveEqualHeightGrid();"> <i class=" fa fa-2x fa-th gridicon"></i></a></li>
                  <li style="margin-left:-8px;"><a data-original-title="List" data-toggle="tooltip" class="listicon" onClick="$('#services_grid').css('display','none'); $('#services_list').css('display','block'); $('#filter_grid').css('display','none'); $('.equalheight').responsiveEqualHeightGrid();"> <i class="fa fa-2x fa-th-list listicon"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" id="services_grid" >

      <?php
        $token = $Li3->GetToken();
        $userId = $Li3->GetUserId();

        // Professional info
        $proResponseData = $Li3->GetAjaxReponse( "http://dash.reshape.net/api/ajax/profile/services?token=". $token. "&id=". $userId ."&isActive=null", null);

        $services = $proResponseData["services"];

        forEach( $services as $serviceItem ) {

          $ind_serviceId = $serviceItem["serviceId"];
          $ind_typeCode = $serviceItem["typeCode"];
          $ind_name = $serviceItem["name"];
          $ind_typeLabel = $serviceItem["typeLabel"];
          $ind_price = $serviceItem["currencyPrice"];
          $ind_minutes = $serviceItem["minutes"];
          $ind_description = $serviceItem["description"];
          $ind_picture_url = $serviceItem["picture_url"];

          echo "
            <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-3\" data-filtertype=\"".$ind_typeCode."\">
              <div class=\"block servicebox equalheight\">
                <div class=\"block-title block-title-light\">
                  <div class=\"pull-right servicebtn\">
                    <div class=\"btn-group\"> <a class=\"btn btn-alt btn-sm btn-danger\" onClick=\"edit_service('".$ind_serviceId."');\"><i class=\"fa fa-pencil\"></i></a></div>
                  </div>
                  <div style=\"background:url('".$ind_picture_url."') no-repeat; background-size: cover; min-height: 180px; background-position: center center;\"> </div>
                </div>
                <h2 class=\"servicename text-center\">".$ind_name."</h2>
                <p class=\"text-center serviceinfo\"> <strong>".$ind_typeLabel."</strong><br>".$ind_minutes." mins / ".$ind_price."</p>
              </div>
            </div>";

          $list_td .= "
          <tr data=\"".$ind_typeCode."\">
            <td align=\"center\"><img src=\"".$ind_picture_url."\" style=\"max-height:64px;\"></td>
            <td>".$ind_name."</td>
            <td>".$ind_typeLabel."</td>
            <td>".$ind_minutes."</td>
            <td>".$ind_price."</td>
            <td align=\"center\"><button class=\"btn btn-primary newservicebtn\" onClick=\"edit_service('".$ind_serviceId."')\">EDIT</button></td>
          </tr>";

        }
      ?>
    </div>

    <div id="services_list" style="display:none; padding:10px;">
      <table id="services_list_table" class="display" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th></th>
            <th>Service Name</th>
            <th>Service Type</th>
            <th>Service Duration</th>
            <th>Service Price</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th></th>
            <th>Service Name</th>
            <th>Service Type</th>
            <th>Service Duration</th>
            <th>Service Price</th>
            <th>Actions</th>
          </tr>
        </tfoot>
        <tbody>
          <?php echo $list_td; ?>
        </tbody>
      </table>
    </div>
  </div>

  <div id="modal-new-service" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-center modal-bg">
          <div class="form-actions"></div>
          <h2 id="AddEditModal" class="modal-title">Create new service</h2><h4 style="text-transform: uppercase; font-size: 11px;"><span style="color:#d22130;">*</span> All fields are required</h4></span>
        </div>
        <div class="modal-body">
          <form id="AddEditServiceForm" class="form-horizontal form-bordered modal-padding" id="modal-edit-service-form" action="" method="post" enctype="multipart/form-data" novalidate>
            <div class="block-full"></div>
            <input type="hidden" id="CurrentAction">
            <input type="hidden" id="serviceId">
            <fieldset>
              <div class=" form-group">
                <label class="col-md-4 control-label" for="ServiceType">Service Type </label>
                <div class=" col-md-7 ">
                  <select class="form-control" id="ServiceType" name="ServiceType" style="height:34px;" size="1">
                    <option value="10">1-on-1</option>
                    <option value="20">1-on-group</option>
                    <option value="40">1-on-class</option>
                    <option value="50">Broadcast</option>
                    <option value="60">Other</option>
                  </select>
                </div>
                <div class="col-md-1" style="margin-top: 3px; margin-left: -9px;">
                  <a href="javascript:void(0)" class="btn btn-alt btn-sm info-btn" data-toggle="tooltip" title="" data-original-title="info"> <i class="fa fa-info"></i> </a>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="ServiceTitle">Service Title </label>
                <div class=" col-md-8 ">
                  <input type="text" class="form-control" id="ServiceTitle" placeholder="Enter Service Title" name="field2">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="main-title">Service Picture</label>
                <div class="col-md-8">
                  <!--<input id="profilePic" class="form-control ui-wizard-content" type="file" style="height: auto;">-->
					  <div id="service-image-editor">
						<!-- .cropit-image-preview-container is needed for background image to work -->
						<div class="cropit-image-preview-container">
							<input id="chose-service-pic" type="file" class="cropit-image-input">
							<div data-name="pic" class="cropit-image-preview"></div>
							<br/>
							<div class="col-md-3">
								<input data-name="pic" type="range" class="cropit-image-zoom-input" style="width:118px;">
							</div>
						</div>
					</div>
                </div>
              </div>
              <div class="form-group" style="display:block;">
                <label class="col-md-4 control-label" for="ServiceDescription">Service Description </label>
                <div class=" col-md-8 ">
                  <textarea id="ServiceDescription" name="ServiceDescription" rows="6" class="form-control" placeholder="Description of your service" style="resize:none;"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="servicePrice">Service Price </label>
                <div class=" col-md-8 ">
                  <div class="input-group"> <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                    <input type="text" id="ServicePrice" name="ServicePrice" class="form-control key-numeric" placeholder="Enter total price"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="serviceDuration">Service Duration </label>
                <div class=" col-md-8 ">
                  <div class="input-group"> <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input type="text" id="ServiceDuration" name="ServiceDuration" class="form-control key-numeric" placeholder="Enter total minutes">
                    <span class="input-group-addon">minutes</span> </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-4 control-label" for="visibility">Visibility </label>
                <div class=" col-md-8" style="margin-top: 6px;">
                  <label for="visibility">
                  <input type="checkbox" id="ServiceHidden" name="ServiceHidden" value="option1">
                  &nbsp; Hide this service </label>
                </div>
              </div>
            </fieldset>
            <input type="hidden" name="formId" value="modal-new-service">
            <div class="form-group">
              <div class="col-xs-12 text-right">
                <button id="DeleteServiceButton" type="button" data-dismiss="modal" class="btn btn-sm btn-danger hidden-sm pull-left">Delete Service </button>
				<button type="button" data-dismiss="modal" class="btn btn-sm btn-default hidden-sm">Cancel </button>
                <button id="AddEditButton" type="button" class="btn btn-sm btn-primary hidden-sm" onClick="update_service($('#serviceId').val());">Save </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.3/css/jquery.dataTables.css">
  <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="/static/Li3/js/pages/equal_height_rows.js"></script>

  <script type="text/javascript">

    function filter_view(filter_option) {
      
      if ( filter_option == 'all' ) {
        $("div[data-filtertype]").each(function(){
          $(this).show('slow');
        });      
      } else {
        $("div[data-filtertype]").each(function(){
          if ( $(this).attr('data-filtertype') != filter_option ) {
            $(this).hide('slow');
          } else {
            $(this).show('slow');
          }
        });      
      }
    }

  	var baseUrl = "<?php	$Li3->EchoDomainNameURL( ); ?>";
    $(document).ready(function() {
      $('#services_list_table').DataTable();
    });

    var removeService = function(id) {
      swal({
          title: "Are you sure?",
          text: "You will not be able to recover this service.",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it",
          closeOnConfirm: false
        },
        function () {
          var token = 1234;
          var post_data = {};
          post_data["token"] = token;
          post_data["delete"] = "true";
          post_data["id"] = id;

          var request = $.ajax({
            type: "POST",
            url: baseUrl + "api/ajax/entity/service/metaInfo/push/obliterate",
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });

          // Request successful!
          request.done(function (response) {
            if (response.error == "false") {
              swal("Deleted", "The service has been deleted.", "success");
              location.reload();
            }
            else if (response.error == "true") {
              swal("Error", "There was an unexpected error.", "error");
            }
          });

          // if the request fails
          request.fail(function (jqXHR, textStatus, error) {
            console.log("search failed")
            console.log(jqXHR.statusText);
            console.log(textStatus);
            console.log(error);
          });

        });
      }

    $(document).ready(function() {
      $('.key-numeric').keypress(function(e) {
        var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9\.]/);
        if (verified) {e.preventDefault();}
      });
    });

    //Boxes of equal height
    jQuery(function($) {
      $('.equalheight').responsiveEqualHeightGrid();
    });

    var apiURL = "/static/controllers/GetJSON.php";

    function add_service() {

      $('#AddEditModal').html('Create new service');
      $('#AddEditButton').html('Add');
      $('#ServiceType').prop('disabled',false);
      $('#AddEditServiceForm')[0].reset();
      $('#DeleteServiceButton').css('display','none');
      $('#serviceId').val('ADD');
      $('#CurrentAction').val('add');
      $('#ServiceDescription').val('');
      $('#DeleteServiceButton').attr("display", "none");

      // Removes the image if exists
      //$('#service-image-editor').cropit('disable');
      $('#service-image-editor').find(".cropit-image-preview.cropit-image-loaded").css("background-image" , "");
      //$('#service-image-editor').cropit({ exportZoom: 1.25, imageBackground: false, imageBackgroundBorderWidth: 20 });

      $('#modal-new-service').modal('show');

    }

    function edit_service(service_id) {
      $('#AddEditModal').html('Edit existing service');
      $('#AddEditButton').html('Update');
      $('#CurrentAction').val('edit');
      $('#DeleteServiceButton').css('display','block');
      $('#serviceId').val(service_id);
      $('#DeleteServiceButton').css("display", "inline");


      $.post(apiURL, { url: "http://dash.reshape.net/api/ajax/profile/services", variables: "id=<?php echo $userId; ?>&types=false&serviceId=" + service_id }, function (data,status) {

        var CurServicetypeCode = data.services[0].typeCode;
        var CurServiceName = data.services[0].name;
        var CurServicePicture = data.services[0].picture_url;
        var CurServiceDescription = data.services[0].description;
        var CurServicePrice = data.services[0].price;
        var CurServiceDuration = data.services[0].minutes;
        var CurServiceVisible = data.services[0].isActive;

        $('#ServiceType').val(CurServicetypeCode);
        $('#ServiceType').prop('disabled',true);
        $('#ServiceTitle').val(CurServiceName);
        $('#ServiceDescription').text(CurServiceDescription);
        $('#ServicePrice').val(CurServicePrice);
        $('#ServiceDuration').val(CurServiceDuration);
        if ( CurServiceVisible == "true" ) {
          $('#ServiceHidden').prop("checked", false);
        } else {
          $('#ServiceHidden').prop("checked", true);
        }

        // Set the current picture in the container
        $('#service-image-editor').cropit('imageSrc', CurServicePicture);

      }, "json");


      $('#DeleteServiceButton').on("click", function(){
        removeService(service_id);
      });

      $('#modal-new-service').modal('show');

    }

    function update_service(service_id) {

      if($("#ServiceHidden").is(':checked')) {
        var ValueIsActive = false;
      } else {
        var ValueIsActive = true;
      }

      if ( $('#serviceId').val() == "ADD" ) {
        var post_data = {
          token: '1234',
          typeCode: $('#ServiceType').val(),
          name: $('#ServiceTitle').val(),
          descr: $('#ServiceDescription').val(),
          minutes: $('#ServiceDuration').val(),
          price: $('#ServicePrice').val(),
          isActive: ValueIsActive,
        };
        var ActionURL = "http://dash.reshape.net/api/ajax/auth/user/profile/services/push/add";
      } else {
        var post_data = {
          token: '1234',
          id: service_id,
          name: $('#ServiceTitle').val(),
          descr: $('#ServiceDescription').val(),
          minutes: $('#ServiceDuration').val(),
          price: $('#ServicePrice').val(),
          isActive: ValueIsActive,
        };
        var ActionURL = "http://dash.reshape.net/api/ajax/entity/service/push/editService";
      }
     
      var request = $.ajax({
        type: "POST",
        url: ActionURL,
        dataType: "json",
        xhrFields: { withCredentials: true },
        crossDomain: true,
        data: post_data
      }).done(function(msg) {


		// Save or update the service image	if no error
		if( msg.error == "false"  ){
			if(service_id == "ADD"){
				serviceId = msg.newService.id;
				// if the image is chosen, add it
				if($('#chose-service-pic').val() != ""){
					export_image(serviceId);
				// otherwise hide the modal form and reload the page
				} else {
					$('#modal-new-service').modal('hide');
					location.reload();
				}
			} else {
				if($('#chose-service-pic').val() != ""){
					export_image(service_id);
				} else {
					$('#modal-new-service').modal('hide');
				}
			}
		}

		// var jsonString = JSON.stringify(msg);
		// alert((jsonString));
      });

    }

	// Return the image from the specified cropit container and save it
	var export_image = function(id) {
		var img = $('#service-image-editor').cropit('export', {
			type: 'image/jpeg',
			quality: .9,
			originalSize: true
		});

		if(img){

			var imgData = img;
			var postFieldName = "picFile";

			var binary = atob(imgData.split(',')[1]); // Create 8-bit unsigned array
			var array  = [];

			for (var i = 0; i < binary.length; i++)
			{
				array.push(binary.charCodeAt(i));
			}

			// Return our Blob object
			var blob = new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
			var formData = new FormData();
			formData.append(postFieldName, blob);
			formData.append("id",id);

			var url_value = baseUrl + 'api/ajax/entity/service/metaInfo/push/pic?token=1234';

			$.ajax({
				url: url_value,
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				xhrFields: {
					withCredentials: true
				},
				crossDomain: true,
				type: 'POST',
				success: function(data){
					$('#modal-new-service').modal('hide');
					location.reload();
				}
			});
		}
	}

	// Create the cropit object
	$( document ).ready(function() {
		$('#service-image-editor').cropit({ exportZoom: 1.25, imageBackground: false, imageBackgroundBorderWidth: 20 });
	});
  
</script>
  <script src="<?php	$Li3->EchoStaticPath("js/imagecrop/jquery.cropit.min.js");	?>" type="text/javascript"></script>
  <?php include($_SERVER['DOCUMENT_ROOT']	."/static/inc/page_content_end.php"); ?>