<?php
/** @var Li3instance */
global	$Li3;
$page_title	=	"Sessions";
$page_description	=	"Overview of upcoming sessions";
$page_icon	=	"sidebar-nav-icon gi gi-stopwatch";
include($_SERVER['DOCUMENT_ROOT']	.	"/static/inc/page_content_beg.php");


$token  =  $Li3->GetToken();
$userId  =  $Li3->GetUserId();
$baseUrl  =  $Li3->GetDomainNameURL();

// Services list
$services = array();
$servicesResponseData  =  $Li3->GetAjaxReponse( $baseUrl . "api/ajax/profile/services?token=" . $token . "&id=" . $userId, null);
foreach ($servicesResponseData["services"]  as  $service) {
  $row["id"] = $service["serviceId"];
  $row["name"] = $service["name"];
  $services[] = $row;  
}
?>

<div class="block"> 
  
  <!-- PAGE SPECIFIC CONTENT BEG -->
  <div class="block-title">
    <div class="service-filter">
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-3">
            <button class="btn btn-primary hidden-xs hidden-sm hidden-md visible-lg" data-toggle="modal" data-target="#create-new-session" id="addNewSession" style="margin-top: 3px; margin-left: -10px;">ADD NEW SESSION</button>
            <button class="btn btn-primary visible-xs visible-sm visible-md hidden-lg"" data-toggle="modal" data-target="#create-new-session" id="addNewSession" style="margin-top: 3px; margin-left: -10px;">ADD</button>
          </div>
          <div class="col-md-9 push-right">
            <div class="filterright">
              <h4 class="filtertitle"><span class="hidden-xs hidden-sm hidden-md visible-lg">SESSION STATUS:</span><span class="visible-xs visible-sm visible-md hidden-lg">STATUS:</span></h4>
              <ul class="filtericons">
                <li><a href="#" class="service_filter_all_top" data-toggle="tooltip" title="" data-original-title="Show all"></a></li>
                <li><a href="#" class="session_filter_approved" data-toggle="tooltip" title="" data-original-title="Sessions Approved"></a></li>
                <li><a href="#" class="session_filter_pending" data-toggle="tooltip" title="" data-original-title="Pending Session Requests"></a></li>
              </ul>
              <h4 class="filtertitle"><span class="hidden-xs hidden-sm hidden-md visible-lg">FILTER BY SERVICE TYPE:</span><span class="visible-xs visible-sm visible-md hidden-lg">FILTER:</span></h4>
              <ul class="filtericons">
                <li><a href="#" class="service_filter_all_top" data-toggle="tooltip" title="" data-original-title="Show all"></a></li>
                <li><a href="#" class="service_filter_one" data-toggle="tooltip" title="" data-original-title="One to One"></a></li>
                <li><a href="#" class="service_filter_class" data-toggle="tooltip" title="" data-original-title="Class"></a></li>
                <li><a href="#" class="service_filter_group" data-toggle="tooltip" title="" data-original-title="Group"></a></li>
                <li><a href="#" class="service_filter_broadcast" data-toggle="tooltip" title="" data-original-title="Broadcast"></a></li>
                <li><a href="#" class="service_filter_person" data-toggle="tooltip" title="" data-original-title="In Person"></a></li>
                <li><a href="#" class="service_filter_private" data-toggle="tooltip" title="" data-original-title="Private"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-10">
      <div id="calendar" data-id="<?php echo $userId; ?>" style="padding-bottom: 15px;"> 
        <script id="calendar">
					var CompCalendar = function () {
						return {
							init: function () {
								$('#calendar2').fullCalendar(
									{
										firstDay: 1, 
										aspectRatio: 1.95, 
										editable: false, 
										droppable: true, 
										eventClick: function (event, jsEvent, view) {
											window.Li3calendarModal.SetActiveEvent(event.data);
										}, 
										eventRender: function (event, element) {
											element.attr('data-toggle', 'modal');
											element.attr('id', 'event_' + event.data.eventId);
										}, 
										defaultView: "month", header: { left: "", center: "prev,title,next", right: "month,agendaWeek,agendaDay,'" }, 
										events: 
											(
												[
													{ 
														title: "Personal Training#30, 1/1", 
														id: "calendar_0", 
                            start: "", 
														end: "0001-01-01T01:00:00-05:00", 
														url: "#ptsid_367_14", 
														color: "blue", 
														allDay: false, 
														editable: false, 
														filters: { 
															ec: "sess", 
															st: "10", 
															sid: "370", 
															sst: "2", 
															reqs: "2", 
															cls: "f" }, 
														data: { 
															eventId: 30, 
															serviceId: 0, 
															serviceName: "#30 Personal Training", 
															serviceImage: "http://pictures.paultesar.com/p/id_3781__b_0__ext_jpeg__w_800__hf_1/blob", 
															sessionTime: "01/01/0001", 
															sessionNumberOfClients: "1/1", 
															editServiceName: "", 
															editServiceImage: "", 
															editServiceDate: "2014-11-03T16:29:09-05:00", 
															editServiceTime: "2014-11-03T16:29:09-05:00", 
															sessionStartsOn: "00:00", 
															sessionEndsOn: "01:00", 
															clients: [
																{ 
																	clientId: 388, 
																	clientFullName: "Katie Jones", 
																	clientLocalisation: "", 
																	clientPhoneNumber: null, 
																	clientEmailAddress: "shaginas@hotmail.com", 
																	clientNote: "", 
																	clientFullProfileLink: "http://live.reshape.net/p/388", 
																	clientListImage: "http://pictures.paultesar.com/p/id_3808__b_0__ext_jpeg__w_64/blob", 
																	clientDetailImage: "http://pictures.paultesar.com/p/id_3808__b_0__ext_jpeg__w_240/blob", 
																	clientRemoveFromSessionLink: "#" 
																}
															], 
															otherEvents: [] 
														} 
													}, 									
												]
											),
										}
								);
							}
						};
					}();
                </script> 
      </div>
    </div>
    <div class="col-md-2">
      <div class="legend">
        <h4 class="text-center" style="font-weight: 500;"> LEGEND</h4>
        <div class="us-square"></div>
        <div class="legendtext">Upcoming Session</div>
        <div class="pp-square"></div>
        <div class="legendtext">Pending Payment</div>
        <div class="r-square"></div>
        <div class="legendtext">Responded Requests</div>
        <div class="rp-square"></div>
        <div class="legendtext">Response Pending</div>
        <div class="dr-square"></div>
        <div class="legendtext">Denied Requests</div>
      </div>
    </div>
  </div>
  <script>
$(function () {
    //CompCalendar.init();
});
    </script> 
  
  <!-- PAGE SPECIFIC CONTENT END --> 
</div>
<!-- Create session modal form -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="create-new-session">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title"> Create new session </h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered modal-padding" id="ptsid_228_13-form" action="/sh/home/" method="post" enctype="multipart/form-data">
          <fieldset>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newSessionService"> Select Service <span class="text-danger"> * </span> </label>
              <div class=" col-md-8 ">
                <select class="form-control" name="newSessionService" id="newSessionService" data-id="<?php echo $userId; ?>" size="1">
                  <!--<option value="0">Select one service</option> -->
                  <?php  //$Li3->SetOptions($services);  ?>
                </select>
              </div>
              <!--<label class="col-md-8 col-md-offset-4" id="serviceType" style="padding-top: 10px;">Service type: <span></span> </label>--> 
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newSessionDate"> Session date <span class="text-danger"> * </span> </label>
              <div class=" col-md-8 ">
                <input type="text" id="newSessionDate" class="form-control input-datepicker-close" name="newSessionDate" data-date-format="mm-dd-yyyy" placeholder="mm-dd-yyyy" />
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newSessionTime"> Session Time <span class="text-danger"> * </span> </label>
              <div class=" col-md-8 ">
                <div class="input-group bootstrap-timepicker">
                  <input type="text" class="form-control" name="field3" id = "newSessionTime" />
                  <span class="input-group-btn"> <a class="btn btn-primary" href="javascript:void(0)"> <i class=" fa fa-clock-o"></i> </a> </span> </div>
              </div>
              <script language='Javascript'>$(document).ready(function () { $('#newSessionTime').timepicker({ minuteStep: 5, showSeconds: false, showMeridian: true, defaultTime: "current" }); });</script> 
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newSessionDuration">Session duration</label>
              <div class=" col-md-8 ">
                <input type="text" id="newSessionDuration" class="form-control" name="newSessionDuration" placeholder="set another duration" />
              </div>
            </div>
            <div class=" form-group" id="newSessionCapacity_group" style="display: none;">
              <label class="col-md-4 control-label" for="newSessionCapacity"> Session capacity </label>
              <div class=" col-md-8 ">
                <input type="text" id="newSessionCapacity" class="form-control" name="newSessionCapacity" placeholder="another capacity" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="newSessionPrice">Session Price</label>
              <div class=" col-md-8 ">
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                  <input type="text" id="newSessionPrice" name="newSessionPrice" class="form-control key-numeric" placeholder="Enter total price">
                </div>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newSessionNotes"> Notes </label>
              <div class=" col-md-8 ">
                <textarea id="newSessionNotes" class="form-control" name="newSessionNotes" placeholder="Brief note about this session" rows="9"></textarea>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="newSessionPrivate"> Private Session </label>
              <div class=" col-md-8 ">
                <select class="form-control" name="newSessionPrivate" id="newSessionPrivate" size="1">
                  <option selected value="0">No</option>
                  <option value="1">Yes</option>
                </select>
              </div>
            </div>
          </fieldset>
          <div class="form-group">
            <div class="col-xs-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close</button>
              <button id="saveNewSession" type="submit" class="btn btn-sm btn-primary  hidden-sm">Create session</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Edit session modal form -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="false" id="ptsid_367_14" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions">
          <div class="col-xs-12 text-right" style="margin-left: 10px;"> 
            <!--<button type="submit" class="btn btn-sm btn-default  hidden-sm" data-toggle="modal" data-target="javascript:void(0)">
                            <i class=" fa fa-comment fa-fw"></i>
                        </button>-->
            <button id="editSessionButton" type="submit" class="btn btn-sm btn-default  hidden-sm" data-toggle="modal" data-target="#ptsid_380_15" style="display: none;"> <i class=" gi gi-pencil"></i> </button>
            <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm"> Close </button>
          </div>
        </div>
        <h2 class="modal-title" id="ptsid_372_19"></h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered ui-formwizard" id="ptsid_367_14-form" action="action" method="post" enctype="multipart/form-data">
          <div class="block-full"></div>
          <script></script>
          <input type="hidden" name="ptshiddenfield" value="30" id="ptsid_378_21" class="ui-wizard-content">
          <fieldset>
            <div class="step ui-formwizard-content" id="ptsid_432_27" style="display: block;">
              <div class="form-group" style="width: 102%; margin-left: -6px; border:none;">
                <div class="col-md-12" style="height: 210px; margin-top: -20px; background-image: url(http://pictures.paultesar.com/p/id_3781__b_0__ext_jpeg__w_800__hf_1/blob);" id="ptsid_434_25"> </div>
              </div>
              <div class="form-group" style="background-color:#2E2E2E; border-bottom:none;margin-top: -15px;">
                <div class="col-md-12 text-center" style="color:#FFF;font-size:15px;" id="ptsid_436_30"><i class="fa fa-calendar fa-fw"></i>&nbsp;&nbsp;<i class="fa fa-clock-o"></i></div>
              </div>
              <div class="form-group">
                <div class="col-md-12 text-center">
                  <div class="nav nav-pills nav-justified clickable-steps"> <a href="javascript:void(0)" data-gotostep="ptsid_442_27" id="ptsid_506_31"><i class="fa fa-user fa-fw"></i> View Clients (1/1)</a> </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <label class="col-md-6 control-label">Promote Session</label>
                  <div class="col-md-6" style="margin-left: -11px;"> <span> <a href="javascript:void(0)" target="blank"> <img src="http://static.reshape.net/Li3/img/assets/fb.png"> </a> </span> <span style="margin-left: 5px;"> <a href="javascript:void(0)" target="blank"> <img src="http://static.reshape.net/Li3/img/assets/twitter.png"> </a> </span> <span style="margin-left: 5px;"> <a href="javascript:void(0)" target="blank"> <img src="http://static.reshape.net/Li3/img/assets/google.png"> </a> </span> <span style="margin-left: 5px;"> <a href="javascript:void(0)" target="blank"> <img src="http://static.reshape.net/Li3/img/assets/mail.png"> </a> </span> </div>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="step ui-formwizard-content" id="ptsid_442_27" style="display: none;">
              <div class="form-group" style="background-color:#2E2E2E; border-bottom:none;margin-top: -15px;">
                <div class="col-md-12 text-center" style="color:#FFF;font-size:15px;" id="ptsid_444_30"><i class="fa fa-calendar fa-fw"></i> <i class="fa fa-clock-o"></i> </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 text-center">
                  <div class="nav nav-pills nav-justified clickable-steps"></div>
                  <i class=" fa fa-user fa-fw"></i> Clients attending </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 text-center" style="margin-top: 23px; max-height: 658px; overflow: auto;" id="ptsid_455_28" >
                  <div class="col-xs-4 col-sm-3 col-lg-2 block-section">
                    <div class="nav nav-pills nav-justified clickable-steps"><a href="javascript:void(0)" data-gotostep="ptsid_457_29"><img class="img-circle" alt="image" src="http://pictures.paultesar.com/p/id_3808__b_0__ext_jpeg__w_64/blob" data-toggle="tooltip" title="" data-original-title="Katie Jones"></a></div>
                  </div>
                </div>
              </div>
              <div class="form-group form-actions">
                <div class="col-md-12 text-center">
                  <div class="nav nav-pills nav-justified clickable-steps"> <a href="javascript:void(0)" class="btn btn-sm btn-default back_session_detail" data-gotostep="ptsid_432_27" data-goto-step="ptsid_483_28"> <i class=" fa fa-angle-left"></i> Back to Session Detail </a> </div>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="step ui-formwizard-content" id="ptsid_457_29" style="display: none;">
              <div class="form-group" style="background-color:#2E2E2E; border-bottom:none;margin-top: -15px;">
                <div class="col-md-12 text-center" style="color:#FFF;font-size:15px;" id="ptsid_459_31"><i class="fa fa-calendar fa-fw"></i> <i class="fa fa-clock-o"></i> </div>
              </div>
              <div class="col-md-12 text-center">
                <div class="col-md-12">
                  <h3 name="clientFullName" style="margin-bottom: 0;"></h3>
                  <img src="" id="ptsid_466_29"><br>
                  <a id="ptsid_468_32" target="_blank">
                  <p style="font-size:11px;">View Full Profile</p>
                  </a> </div>
              </div>
              <div class="col-md-12 text-center"> <!-- style="margin-left: 30px; -->
                <p class="clickable-steps"> <a href="javascript:void(0)" data-gotostep="ptsid_483_28" class="switch_session"> <i class=" fa fa-calendar-o fa-fw"></i> Switch to another session<br>
                  </a> 
                  <!--<a href="javascript:void(0)" data-toggle="modal" data-target="#modal-send-message">--> 
                  <a id="send_message_session" href="javascript:void(0)" data-gotostep="modal-send-message" class="send_session"> <i class=" fa fa-comments-o fa-fw"></i> Send a personal message<br>
                  </a> 
                  <!--<i style="margin-top:20px;" class=" fa fa-times fa-fw themed-color-fire fa fa-times fa-fw"></i>--> 
                  <!-- <input type="submit" class="themed-color-fire remove_from_session ui-wizard-content ui-formwizard-button" value="Next" id="remove_from_session"> --> 
                  <a href="javascript:void(0)" class="remove_from_session" id="remove_from_session" style="color: red !important;"><i class="themed-color-fire fa fa-times fa-fw" style="margin-top:20px;"></i>Remove from session</a> </p>
              </div>
              <div class="form-group form-actions">
                <div class="col-md-12 text-center">
                  <div class="nav nav-pills nav-justified clickable-steps"> <a href="javascript:void(0)" class="btn btn-sm btn-default back_clients_list " data-gotostep="ptsid_442_27"> <i class=" fa fa-angle-left"></i> Back to Clients </a> </div>
                </div>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="client_current" value="" id="ptsid_481_25" class="ui-wizard-content">
          <fieldset>
            <div class="step ui-formwizard-content" id="ptsid_483_28" style="display: none;">
              <div class=" form-group">
                <label class="col-md-4 control-label" for="ptsid_490_31">Select Session<span class="text-danger"> *</span> </label>
                <div class=" col-md-6">
                  <select class="form-control ui-wizard-content" name="field2" size="1" id="ptsid_490_31">
                  </select>
                </div>
              </div>
              <div class="form-group form-actions text-center clickable-steps" style="padding-top: 14px">
                <div class="form-group form-actions">
                  <div class="col-md-12 text-center">
                    <div class="nav nav-pills nav-justified clickable-steps"> <a href="javascript:void(0)" class="btn btn-sm btn-default back_client_detail" data-goto-step="ptsid_457_29"><i class=" fa fa-angle-left"></i> Back to Client Detail</a>
                      <button id="switch_session" class="btn btn-sm btn-primary save_session_changes  hidden-sm ui-wizard-content ui-formwizard-button" value="Next">Switch Session</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="client_switch_session" value="" id="ptsid_491_27" class="ui-wizard-content">
          <input type="hidden" name="client_remove_from_session" value="" id="ptsid_502_28" class="ui-wizard-content">
        </form>
      </div>
    </div>
  </div>
  <script></script> 
</div>
<!-- Edit session modal form -->
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="ptsid_380_15" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title" id="ptsid_385_20"></h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered" id="ptsid_380_15-form" action="anotherAction" method="post" enctype="multipart/form-data">
          <div class="block-full"></div>
          <input type="hidden" id="editSessionId" name="editSessionId">
          <fieldset>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="editSessionDate"> Session Date <span class="text-danger"> * </span> </label>
              <div class=" col-md-6 ">
                <input type="text" id="editSessionDate" class="form-control" name="editSessionDate" placeholder="mm/dd/yyyy">
                <script> $(document).ready(function () { $('#editSessionDate').datepicker({ format: 'mm/dd/yyyy', autoclose: true }); });</script> 
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="editSessionTime"> Session Time <span class="text-danger"> * </span> </label>
              <div class=" col-md-6 ">
                <div class="input-group bootstrap-timepicker">
                  <input type="text" class="form-control" name="editSessionTime" id="editSessionTime">
                  <span class="input-group-btn"> <a class="btn btn-primary" href="javascript:void(0)"> <i class=" fa fa-clock-o"></i> </a> </span> </div>
              </div>
              <script language="Javascript">$(document).ready(function () { $('#editSessionTime').timepicker({ minuteStep: 5, showSeconds: false, showMeridian: true }); });</script> 
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="editSessionDuration">Session duration</label>
              <div class=" col-md-6 ">
                <input type="text" id="editSessionDuration" class="form-control" name="editSessionDuration" placeholder="set another duration" />
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="editSessionCapacity">Session capacity</label>
              <div class=" col-md-6 ">
                <input type="text" id="editSessionCapacity" class="form-control" name="editSessionCapacity" placeholder="set another capacity" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label" for="editSessionPrice">Session Price</label>
              <div class=" col-md-6 ">
                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                  <input type="text" id="editSessionPrice" name="editSessionPrice" class="form-control key-numeric" placeholder="Enter total price">
                </div>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="editSessionNotes">Notes</label>
              <div class=" col-md-6 ">
                <textarea id="editSessionNotes" class="form-control" name="editSessionNotes" placeholder="Brief note about this session" rows="4"></textarea>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-4 control-label" for="editSessionPrivate">Private Session</label>
              <div class=" col-md-6 ">
                <select class="form-control" name="editSessionPrivate" id="editSessionPrivate" size="1">
                  <option selected value="0">No</option>
                  <option value="1">Yes</option>
                </select>
              </div>
            </div>
          </fieldset>
          <div class="form-group form-actions">
            <div class="col-xs-12 text-right">
              <button id="deleteSession" type="button" class="btn btn-sm btn-primary hidden-sm pull-left"> Delete session </button>
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm"> Close </button>
              <button id="editSessionSubmit" type="submit" class="btn btn-sm btn-primary  hidden-sm"> Save Session Changes </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="modal-send-message" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header text-center modal-bg">
        <div class="form-group form-actions"></div>
        <h2 class="modal-title"> Send message</h2>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-bordered modal-padding" id="modal-new-message" action="" method="post" enctype="multipart/form-data" novalidate>
          <div class="block-full"></div>
          <input type="hidden" name="ptshiddenfield" value="modal-new-message">
          <fieldset>
            <div class=" form-group">
              <label class="col-md-4 control-label" style="line-height: 65px;">Recipient</label>
              <div class="col-md-8">
                <div class="col-md-3 recipient-img-container" style="margin-left: -23px;"> 
                  <!--<img class="img-circle" alt="image" src="http://pics.reshape.net/p/id_9845__b_0__ext_jpeg__w_150/1df849d.jpg" data-toggle="tooltip" title="Cedric Pommepuy" style="width: 75px;">																													--> 
                </div>
                <div class="col-md-9">
                  <h4 style="padding-left: 14px;" name="clientFullName" class="form-control-static"></h4>
                  <!--<p style="padding-left: 14px; font-size: 14px; color: #5E5E5E;">Miami, FL</p>--> 
                </div>
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-2 control-label" for="messageSubject">Subject</label>
              <div class=" col-md-10">
                <input type="text" class="form-control" id="messageSubject" placeholder="Subject" name="messageSubject">
              </div>
            </div>
            <div class=" form-group">
              <label class="col-md-2 control-label" for="messageText">Message</label>
              <div class=" col-md-10">
                <textarea class="form-control" id="messageText" placeholder="Write your message" rows="10" name="messageText"></textarea>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="formId" value="modal-new-service">
          <div class="form-group">
            <div class="col-xs-12 text-right">
              <button type="button" data-dismiss="modal" class="btn btn-sm btn-default  hidden-sm">Close </button>
              <button id="sendMessageBtn" class="btn btn-sm btn-primary  hidden-sm">Send Message </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
var clickableWizard = $('#ptsid_367_14-form');
    clickableWizard.formwizard({ disableUIStyles: true, inDuration: 0, outDuration: 0 });
    $('.clickable-steps a').on('click', function () {
        var gotostep = $(this).data('gotostep');
        clickableWizard.formwizard('show', gotostep);
    });
</script> 
<script src="<?php	$Li3->EchoStaticPath("js/ajax/ajaxCalls_Session.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/growl/bootstrap-growl.min.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/jquery.ddslick.min.js");	?>" type="text/javascript"></script> 
<script src="<?php	$Li3->EchoStaticPath("js/pages/Li3calendarHostEditModal.js");	?>" type="text/javascript"></script> 
