<?php
  /** @var Li3instance */
  global $Li3;

  if( $_POST[ "u" ] != "" ) {

    // RETRIEVE VARIABLES FROM URL =========================================
    $u = $_POST['u'];
    $p = $_POST['p'];
    // =====================================================================

    // DEFINE ADDITIONAL VARIABLES =========================================
    $token = $Li3->GetToken();
    $url = "http://dash.reshape.net/api/ajax/auth/login?token=".$token."&u=".$u."&p=".$p."&typeCodes=true";
    $postData = array('token' => $token, 'u' => $u, 'p' => $p);
    // =====================================================================

    // PERFORM ACTION & LOGIN ==============================================
    $ProcessLogin = $Li3->GetAjaxReponse($url, $postData);
    $ProfileID = $ProcessLogin['login']['loginUser']['id'];
    $UserTypesCodes = $ProcessLogin['login']['typeCodes'];
    // =====================================================================

    // GET ADDITIONAL PROFILE INFORMATION ==================================
    $url = "http://dash.reshape.net/api/ajax/profile/info?token=".$token."&id=".$ProfileID;
    $ProcessProfileBasic = $Li3->GetAjaxReponse($url, $postData);

    $ProfileName = $ProcessProfileBasic['name'];
    $ProfilePictureURL = $ProcessProfileBasic['picture_url'];

    $Li3->SetUserId( $ProfileID );
    $Li3->SetUserName( $ProfileName );
    $Li3->SetProfilePictureURL( $ProfilePictureURL );

    $Li3->Redirect( "/Home" );
    // =====================================================================

  }

  $page_title = $Li3->TitleEchoStaticPath();
  $company_name = $Li3->TitleEchoCompanyName();
  $this_page = $page_title." :: ".$company_name;

?>
<!DOCTYPE html>
<html>
  <!-- __PTS_HEAD__ -->
  <head>
    <!-- __PTS_META__ -->
    <title><?php echo $this_page; ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <!-- __PTS_META__ -->
    <!-- __PTS_CSS__ -->
    <link href="<?php $Li3->EchoStaticPath( "back/css/bootstrap.min.css" ); ?>" type="text/css" rel="stylesheet" />
    <link href="<?php $Li3->EchoStaticPath( "back/css/plugins.css" ); ?>" type="text/css" rel="stylesheet" />
    <link href="<?php $Li3->EchoStaticPath( "back/css/main.css" ); ?>" type="text/css" rel="stylesheet" />
    <link href="<?php $Li3->EchoStaticPath( "back/css/themes.css" ); ?>" type="text/css" rel="stylesheet" />
    <link href="<?php $Li3->EchoStaticPath( "back/css/themes/night.css" ); ?>" type="text/css" rel="stylesheet" />
    <link rel="icon" type="image/x-icon" href="<?php $Li3->EchoStaticPath( "img/favicon.ico" ); ?>" />
    <!-- __PTS_CSS__ -->
    <!-- __PTS_JS__ -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <script src="<?php $Li3->EchoStaticPath( "back/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js" ); ?>" type="text/javascript"></script>
    <script src="<?php $Li3->EchoStaticPath( "back/js/vendor/bootstrap.min.js" ); ?>" type="text/javascript"></script>
    <script src="<?php $Li3->EchoStaticPath( "back/js/plugins.js" ); ?>" type="text/javascript"></script>
    <script src="<?php $Li3->EchoStaticPath( "back/js/app.js" ); ?>" type="text/javascript"></script>
    <!-- __PTS_JS__ -->
  </head>
  <!-- __PTS_HEAD__ -->

  <!-- __PTS_BODY__ -->
  <body>
    <!-- __PTS_MAINCONTENT__ -->
    <div>
      <div id="page-container" class="">
        <div id="login-container" class="animation-fadeIn">
          <div class="login-title  text-center">
            <img src="<?php $Li3->EchoStaticPath( "back/img/ReShape-Logo.png" ); ?>" />
          </div>
          <div class="block push-bit">
            <form id="form_process_login" class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
              <div class="col-md-12"></div>
              <input type="hidden" name="ptshiddenfield" />
              <div class=" form-group">
                <div class=" col-xs-12 ">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class=" gi gi-envelope"></i>
                    </span>
                    <input type="text" class="form-control input-lg" placeholder="Email" name="u" id="u" autocomplete="off" value="marc@reshape.net" onKeyPress="hide_error()" />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class=" col-xs-12 ">
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class=" gi gi-asterisk"></i>
                    </span>
                    <input type="password" class="form-control input-lg" placeholder="Password" name="p" id="p" autocomplete="off" value="qwerty1234" onKeyPress="hide_error()" />
                  </div>
                </div>
              </div>

              <!-- /login-fields -->
              <div class="form-group">
                <div class=" col-xs-12 ">
                  <div id='error_div' class="alert alert-danger alert-dismissable" style="display:none;">
                    <b><span id='error_message'></span></b>
                  </div>
                </div>
              </div>

              <div class="form-group form-actions">
                <div class="col-xs-6 text-left">
                  <button class="btn btn-sm btn-primary" type="button" onClick="window.location.href = 'http://reshape.net';">
                    <i class=" fa fa-angle-left fa-fw"></i> ReShape.net
                  </button>
                </div>
                <div class="col-xs-6 text-right">
                  <button id="login_button" class="btn btn-sm btn-primary" type="button" onClick="process_login();">
                    Login <i class=" fa fa-angle-right fa-fw"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <img src="http://static.reshape.net/RESH/Images/backgrounds/loginbg.jpg" class="full-bg animation-pulseSlow" />
      </div>
    </div>
    <!-- __PTS_MAINCONTENT__ -->
    <!-- __PTS_PAGESCRIPT__ -->
    <script src="<?php $Li3->EchoStaticPath( "js/pages/login.js" ); ?>" type="text/javascript"></script>
    <!-- __PTS_PAGESCRIPT__ -->
  </body>
  <!-- __PTS_BODY__ -->
</html>