(function ()
{
    if (!window.Li3Ajax)
    {
        var Li3AjaxClass = function( )
        {
            var public  = this;
            var private = {};
 
            private.ProxyUserId = 0;
            private.ApiRootPath = "";     // http://dash.reshape.net/
            private.ApiToken    = "";     // 1234
            private.IsSetUp     = false;
 
            private.Construct = function( )
            {
            };
 
            public.SetUp = function( apiRootPath, apiToken, proxyUserId )
            {
                if( !private.IsSetUp )
                {
                    private.ApiRootPath = apiRootPath;
                    private.ApiToken    = apiToken;
                    private.ProxyUserId = proxyUserId;
                    private.IsSetUp     = true;
                }
                else
                {
                    console.log( "Li3Ajax Error: Class was already set up" );
                }
            };
 
            public.Call = function( uri, postData, method )
            {
                var request = null;
 
                if( uri )
                {
                    if (!postData)
                        postData = { };
 
                    if( !method )
                        method = "POST";
 
                    if( !uri.substring( 0, 4 ) == "http" ) // Check for absolute API path
                        uri = private.ApiRootPath + uri;
 
                    postData["token"]       = private.ApiToken;
                    postData["useAsUserId"] = private.ProxyUserId;
 
                    request = $.ajax(
                    {
                        type:        method,
                        url:         uri,
                        dataType:    "json",
                        xhrFields:   { withCredentials: true },
                        crossDomain: true,
                        data:        postData
                    });
                }
                else
                {
                    console.log( "Li3Ajax Error: missing uri for api call" );
                }
 
                return request;
 
            };
 
            private.Construct( );
        };
 
 
        window.Li3Ajax = new Li3AjaxClass( );
    }
})( );