(function()

{



var newSelectPointer = null;

var calendarDay = null;

var finalDaysTimesArray = [];

var numberOfSelect = 0;

var selectArray = [];



// Set request for current day

window.setRequestsForDay = function( date, times, id ){

	// First case : at least one item selected 

	if(newSelectPointer.val()){

		var nbEvents = 0;

		var newDaysTimesArray = [];

		times = newSelectPointer.val();

		var position;

		var i = 0;

		

		// Add each time in a temporary array  composed by [date][time]

		$.each( times, function( key, value ) {				

			// Add date+times in new array

			newDaysTimesArray.push([date, value]);

		});

		

		// In the final array, if we find the same date, we purge arrays with same date

		for( i = 0 ; i < finalDaysTimesArray.length; i++){

			position = jQuery.inArray( date, finalDaysTimesArray[i] );

			if(position != -1)

			{

				// Remove the date+times from the array

				finalDaysTimesArray.splice(i,1);

				// if the element is removed from the array, we need to keep the same position for the next element

				i--;

			}			

		}

		

		// For each new date-time, add new values in the actual array

		$.each( newDaysTimesArray, function( key, value ) {

			finalDaysTimesArray.push(value);

			nbEvents++;			

		});

		

		// Set the background in the calendar day view

		calendarDay.css('background-color', 'red');		

		// Set the text in the the calendar day view

		calendarDay.find('.fc-day-content div').text("");

		var requestsWord = nbEvents > 1 ? " requests" : " request";

		calendarDay.find('.fc-day-content div').text(nbEvents + requestsWord);

	// Second case : last item is removed (there is no requests for the current date)

	} else {

		// Put the calendar day style as init (no background, no request number text)

		calendarDay.css('background-color', 'transparent');		

		calendarDay.find('.fc-day-content div').text("");	

		// Remove the item from the list

		var timeToRemove = $('#'+ newSelectPointer.attr('id') + '_chosen').find('.search-choice span').text();

		

		// Remove the last item from the final array

		for( i = 0 ; i < finalDaysTimesArray.length; i++){

			if( finalDaysTimesArray[i][0] == date && finalDaysTimesArray[i][1] == timeToRemove){

				finalDaysTimesArray.splice(i,1);

				break;

			}			

		}		

	}	

}



// Cancel all requests

window.cancelRequests = function (){

	// Remove the array content

	finalDaysTimesArray = [];

	// Remove all select clones

	selectArray = [];

	numberOfSelect = 0;

	// Reset background color for days

	$('.fc-day').css('background-color','transparent');

	// Remove request numbers for days

	$('.fc-day-content div').text("");

}



// Save requests and submit the form

window.saveRequests = function (id, formId){

	var $elem = $( "#" + id );

	// Create hidden fields within the form (each hidden field is a clone with no id but same name)

	for( i = 0 ; i < finalDaysTimesArray.length; i++){

		var $clone = $elem.clone( false );		

		$clone.attr('id', '');

		$clone.val(finalDaysTimesArray[i][0] + ":" + finalDaysTimesArray[i][1]);

		$clone.prependTo($elem.parent());

	}

	// Submit the form

	$('#' + formId).submit();

}



// Define the day chosen

window.setCalendarDay = function( theDay )

{

	calendarDay = theDay;

}



// Define the select for the modal

window.setTimeSelect = function(date, id){

	var selectAlreadyExists = false;

	var i = 0;



	// Hide the original select

	$(".chosen-container.chosen-container-multi").hide();	

	// Hide the original div select

	$( "#" + id + "_chosen" ).hide();

	

	for( i = 0 ; i < selectArray.length; i++){

		position = jQuery.inArray( date, selectArray[i] );

		if(position != -1)

		{

			// if the select already exists, we don't create a new element

			selectAlreadyExists = true;

			break;

		}			

	}

	

	if(selectAlreadyExists)

	{

		// Display the existing select 

		var idExistingSelect = "#" + selectArray[i][1];

		// Show the select (it's the div from the plugin and not the select element)

		$(idExistingSelect + "_chosen").show();

		newSelectPointer = $(idExistingSelect);		

	}

	else

	{

		// Create a new select (clone from orignal one)

		numberOfSelect++;

		var $elem = $( "#" + id );

		var $newSelect = $elem.clone( false ).appendTo($elem.parent());

		$newSelect.attr("id", id + "_" + numberOfSelect);

		$newSelect.chosen({ width: '100%'});



		newSelectPointer = $newSelect;

		// Put the new select in an array

		selectArray.push([date, $newSelect.attr('id')]);

	}	

}



// Display request in the calendar

window.displayRequests = function(){

	var j = 0;

	var nbRequests = 1;

	var previousDate = "";

	var requestsWord = "";

	

	for( j = 0 ; j < finalDaysTimesArray.length; j++){

		// current date (must be YYYY-MM-DD)

		currentDate = finalDaysTimesArray[j][0];

		$('.fc-day[data-date= "'+ currentDate+'"]').css('background-color', 'red');

		

		// if same date as the previous one, we increment the number of requests, remove the current text and add the new one

		if(currentDate == previousDate){

			nbRequests++;

			$('.fc-day[data-date= "'+ currentDate+'"] .fc-day-content div').text("");

			requestsWord = nbRequests > 1 ? " requests" : " request";

			$('.fc-day[data-date= "'+ currentDate+'"] .fc-day-content div').text(nbRequests + requestsWord);

		} else {

			nbRequests = "1";

			requestsWord = "request";

			$('.fc-day[data-date= "'+ currentDate+'"] .fc-day-content div').text(nbRequests + requestsWord);

			previousDate = currentDate;

		}		

	}

}



})();