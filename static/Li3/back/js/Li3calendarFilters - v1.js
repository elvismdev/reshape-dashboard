(function( )
{
    var Li3calendarFiltersClass = function( )
    {
        // Facke accessibility scopes
        var private = { };
        var public  = this;
        
        // Initialize class variables
        private.Events  = [ ];
        private.Filters = [ ]; // { Key: "", Input: HTMLnode }
        
        // Fake constructor  
        private.Construct = function( )
        {
            
        };
        
        public.AddFilterInput = function( filterKey, nodeId ) 
        {
            var element = window.document.getElementById( nodeId );
            
            if( element )
            {    
                var filter   = { };
                filter.Key   = filterKey;
                filter.Input = element;
                
                private.Filters.push( filter );
            }          
        };

        public.RegisterEvents = function( events )
        {
            private.Events = []; // Copy array to isolate input list 
            
            for( var x = 0; x < events.length; x++ )
                private.Events.push( events[ x ] );    
            
            return events;      
        }; 
        
        public.GetEvents = function( )
        {
            var events = []; // Copy array to isolate internal list       
            
            for( var x = 0; x < private.Events.length; x++ )
                events.push( private.Events[ x ] );    

            return events;        
        }; 
        
        public.EventPassesFilter = function( event )
        {
            var output = false;
            
            if( event.filters )
            {
                output = true;
                
                for( var x = 0; x < private.Filters.length; x++ )
                {
                    var filter = private.Filters[ x ];
                    var key    = filter.Key;
                    
                    if( event.filters[ key ] ) 
                    {
                        var filterValue = event.filters[ key ]; 
                        var inputValue  = private.GetFieldValue( filter.Input );  

                        inputValue  = inputValue.trim( );
                        filterValue = filterValue.trim( );  
                        
                        if( inputValue && inputValue.length != 0 && inputValue != "" )
                        {
                            if( filterValue.toString( ) != inputValue.toString( ) ) // Validate only if input value is set  
                                output = false;     
                        }
                    }

                    if( !output )
                        break;   // Break loop on first false   
                } 
            } 
            
            return output;         
        };
        
        public.EventFailsFilter = function( event )
        {
            return !public.EventPassesFilter( event );    
        };
        
        public.GetFilterActiveValue = function( filterKey )
        {
            var output = null;
            
            for( var x = 0; x < private.Filters.length; x++ )
            {
                var filter = private.Filters[ x ];
                
                if( filterKey == filter.Key )
                {
                    output = private.GetFieldValue( filter.Input );
                    break;
                } 
            }  
            
            return output;
        };
        
        public.Test = function( )
        {
            alert( "Li3calendarFilters is defined" );    
        };
        
        private.GetFieldValue = function( input )
        {
            var tagName    = input.tagName.toLowerCase( ); 
            var inputValue = null;  
            
            if( tagName == "select" )
            {
                var index  = input.selectedIndex;
                inputValue = input.options[index].value
            }
            else
            {
                var inputType  = input.type.toLowerCase( );
                
                        
                if( inputType == "checkbox" || inputType == "radio" )
                {
                    if( input.checked )
                        inputValue = input.Value;     
                }
                else
                {
                    inputValue = input.Value;
                }
            }
            
            return inputValue;
        };
        
        public.ProcessFilterSetEvent = function( calendar, input )
        {
            var removeDelegate = function( event )
            { 
                return public.EventFailsFilter( event ); 
            };
            
            var events = public.GetEvents( );
            
            calendar.fullCalendar( 'removeEvents' ); 
            calendar.fullCalendar( 'addEventSource', events ); 
            calendar.fullCalendar( 'removeEvents', function( ev ){ return removeDelegate( ev ); } );    
        };
        
        // Must be called here 
        private.Construct( );
    };
  
    
    var Li3calendarModalWrapperClass = function( )
    {
        // Facke accessibility scopes
        var private = { };
        var public  = this;
        
        // Initialize class variables
        private.Nodes  = { };  // { Id: "", Node: HTMLnode }
        private.Nodes.EvenIdField;
        private.Nodes.ModalTitleNode;
        private.Nodes.MainPictureNode;  
        private.Nodes.ViewClientsLinkNode;
        private.Nodes.ClientListNode;
        private.Nodes.ClientInfoSectionNode;
        private.Nodes.ClientNameNode;
        private.Nodes.ClientPicNode;  
        
        private.Nodes.ClientSwitchLinkNode;  
        private.Nodes.ClientSwitchSectionNode;  
        private.Nodes.ClientSwitchIdField;  
        private.Nodes.ClientSwitchSelect;  
        
        private.Listeners = { };
        private.Listeners.ClientSwitchLink;
        
        //To-Do: add other elements 
        
        // Fake constructor  
        private.Construct = function( )
        {
                
        };
        
        public.RegisterNodes = function( nodeData )
        {
            var sampleData = 
            {
                EvenId: "",
                ModalTitle: "",
                MainPictureNode: "",
                ViewClientsLinkNode: ""
            };  
            
            for( var key in sampleData )
            {
                var nodeId  = sampleData[ key ];   
                var element = window.document.getElementById( nodeId );
                
                if( element )
                    private.Nodes[ key ] = element; 
            }
        };
        
        public.SetActiveEvent = function( eventData ) 
        {
            var pointer = this;
            
            private.Nodes.EvenIdField.value                    = eventData[ "eventId" ];
            private.Nodes.ModalTitleNode.innerHTML             = eventData[ "eventTitle" ];
            private.Nodes.MainPictureNode.style.backroundImage = "url('" + eventData[ "eventTitle" ] + ")";
            private.Nodes.ViewClientsLinkNode.innerHTML        = eventData[ "viewClientsText" ];
            
            this.ClientListNode.innerHTML = "";
            
            var clientData = eventData[ "clients" ];
            
             for( var x = 0; x < clientData.length; x++ ) 
             {
                 var clientInfo = clientData[x];
                 
                 var clientNode = document.createElement( "a" );
                 var imageNode  = document.createElement( "img" );
                 imageNode.src  = clientData["smallPicUrl"];

                 clientNode.setAttribute( "data-gotostep", private.Nodes.ClientInfoSectionNode.Id );
                 clientNode.addEventListener( "click", function( ev ){ pointer.SetClientSubPageValues( clientInfo ); return false; }, true );
                 
                 clientNode.appendChild( imageNode );
                 private.Nodes.ClientListNode.appendChild( clientNode );
             }
             
             
             //To-do: Remove existing options if any
             private.Nodes.ClientSwitchSelect.innerHTML = "";
             
             var otherEvents = eventData[ "otherEvents" ];
             
             for( var x = 0; x < otherEvents.length; x++ ) 
             {
                 var event = otherEvents[ x ];
                 
                 var option       = document.createElement("option");
                     option.text  = event[ "name" ];         
                     option.value = event[ "id" ];         
                 
                private.Nodes.ClientSwitchSelect.add( option );    
             }
        };
        
        public.SetClientSubPageValues = function( clientInfo )
        {
             private.Nodes.ClientNameNode.innerHTML = clientInfo["name"];
             private.Nodes.ClientPicNode.src        = clientInfo["picUrl"];
             
             if( private.Listeners.ClientSwitchLink )
                private.Nodes.ClientSwitchLinkNode.removeEventListener( "click", private.Listeners.ClientSwitchLink, true );    
             
             private.Listeners.ClientSwitchLink     = function( ev )
             { 
                 pointer.SetClientSubPageEventSelectValues( clientInfo );
                 return false;
             };

             private.Nodes.ClientSwitchLinkNode.setAttribute( "data-gotostep", private.Nodes.ClientSwitchSectionNode.Id );
             private.Nodes.ClientSwitchLinkNode.addEventListener( "click", private.Listeners.ClientSwitchLink, true );
        };
        
        public.SetClientSubPageEventSelectValues = function( eventData, clientInfo )
        {
             private.Nodes.ClientSwitchIdField.value = clientInfo[ "clienId" ];                                            
             private.Nodes.ClientNameNode.innerHTML  = clientInfo["name"];
             private.Nodes.ClientPicNode.src         = clientInfo["picUrl"];
        };
        
        // Must be called here 
        private.Construct( );
    };
  
  
  
    window.Li3calendarFilters = new Li3calendarFiltersClass( ); 
    window.Li3calendarModal   = new Li3calendarModalWrapperClass( ); 
    
    //  event function( event ) { window.Li3calendarModal.SetActiveEvent( event.data ); };
    
    /*
    $('#calendar').fullCalendar({
    eventClick: function(calEvent, jsEvent, view) {
        window.Li3calendarModal.SetActiveEvent( event.data );

        // change the border color just for fun
        $(this).css('border-color', 'red');
    }
    }); 
    */

})( );  