(function () {

	/************************************************************************/

	/* Li3calendarHostEditModal 										    */

	/************************************************************************/

	var Li3calendarHostEditModal = function( )

	{

		// Fake accessibility scopes

		var private = { };

		var public  = this;

		

		// Initialize class variables

		private.Nodes  = { };  // { Id: "", Node: HTMLnode } 

		private.Nodes.ServiceNameNode; // serviceName		

		private.Nodes.ServiceImageNode;  // serviceImage

		private.Nodes.SessionTimeNode; // sessionTime

		private.Nodes.ClientsListSessionTimeNode; // clientsListSessionTime

		private.Nodes.ClientDetailSessionTimeNode; // clientDetailTimeNode

		private.Nodes.ClientFullNameNode;

		private.Nodes.ClientDetailImageNode;

		private.Nodes.ClientPhoneNumberNode; // clientPhoneNumber

		private.Nodes.ClientEmailAddressNode; // clientEmailAddress		

		private.Nodes.ClientNoteNode; // clientNote

		private.Nodes.ClientFullProfileLinkNode; // clientFullProfileLinkNode

		private.Nodes.ClientRemoveFromSessionNode; // clientRemoveFromSessionLinkNode

		private.Nodes.SelectOtherSessionNode; // otherEvents (data row)



		// Edit session fields nodes

		private.Nodes.EditServiceNameNode;

		private.Nodes.EditServiceTitleNode;

		private.Nodes.EditServiceDateNode;

		private.Nodes.EditServiceImageNode;		

		private.Nodes.EditServiceTimeNode;



		// Hidden fields nodes

		private.Nodes.SessionHiddenFieldNode;

		private.Nodes.ClientDetailHiddenFieldNode;

		private.Nodes.ClientRemoveFromSessionHiddenFieldNode;

		private.Nodes.ClientSwitchSessionHiddenFieldNode;

		

		// Steps Nodes

		private.Nodes.ServiceStepNode;

		private.Nodes.ClientsListStepNode;

		private.Nodes.ClientDetailStepNode;

		private.Nodes.SwitchSessionStepNode;

		private.Nodes.ClientConfirmRemoveFromSessionNode;



		

		private.Listeners = { };

		private.Listeners.ClientSwitchLink;

				

		// Fake constructor  

		private.Construct = function( )

		{

			

		};

		

		// RegisterNodes( nodeData ) : 

		public.RegisterNodes = function( nodeData )

		{		    

			

			for (var key in nodeData ) //sampleData

			{

				

				var nodeId = nodeData[key]; // sampleData[ key ]			    

				var element = window.document.getElementById( nodeId );

				

				if( element )

					private.Nodes[ key ] = element; 

			}

		};

		

		public.SetActiveEvent = function( eventData ) 

		{

			var pointer = this;

			// Service Name

			if ( private.Nodes.ServiceNameNode )

				private.Nodes.ServiceNameNode.innerHTML = private.ExtractData(eventData, "serviceName");

			// Evend Id

			if (private.Nodes.SessionHiddenFieldNode)

				private.Nodes.SessionHiddenFieldNode.value = private.ExtractData(eventData, "eventId");

			// Service Image

			if (private.Nodes.ServiceImageNode)

				private.Nodes.ServiceImageNode.style.backgroundImage = "url('" + private.ExtractData(eventData, "serviceImage") + "')";

			// Session Time

			if (private.Nodes.SessionTimeNode)

			{

				private.Nodes.SessionTimeNode.innerHTML = "<i class='fa fa-calendar fa-fw'></i> " + private.ExtractData(eventData, "sessionTime") + " <i class='fa fa-clock-o'></i> " + private.ExtractData(eventData, "sessionStartsOn") + "-" + private.ExtractData(eventData, "sessionEndsOn"); // sessionTime

				private.Nodes.ClientsListSessionTimeNode.innerHTML = "<i class='fa fa-calendar fa-fw'></i> " + private.ExtractData(eventData, "sessionTime") + " <i class='fa fa-clock-o'></i> " + private.ExtractData(eventData, "sessionStartsOn") + "-" + private.ExtractData(eventData, "sessionEndsOn"); // sessionTime

				private.Nodes.ClientDetailSessionTimeNode.innerHTML = "<i class='fa fa-calendar fa-fw'></i> " + private.ExtractData(eventData, "sessionTime") + " <i class='fa fa-clock-o'></i> " + private.ExtractData(eventData, "sessionStartsOn") + "-" + private.ExtractData(eventData, "sessionEndsOn"); // sessionTime

			}

			

			// Set the data for update session modal form

			private.Nodes.EditServiceTitleNode.innerHTML = "Edit " + private.ExtractData(eventData, "serviceName");			

			private.Nodes.EditServiceDateNode.value = private.ExtractData(eventData, "sessionTime");

			private.Nodes.EditServiceImageNode.style.backgroundImage = "url('" + private.ExtractData(eventData, "serviceImage") + "')";

			private.Nodes.EditServiceTimeNode.value = private.ExtractData(eventData, "sessionStartsOn");



				

			// Go to Client list link

			if (private.Nodes.SessionNumberOfClients)

				private.Nodes.SessionNumberOfClients.innerHTML = "<i class='fa fa-user fa-fw'></i> View Clients (" + private.ExtractData(eventData, "sessionNumberOfClients") + ")";   

			

			if (private.Nodes.ClientsListContainerNode)

			{

				private.Nodes.ClientsListContainerNode.innerHTML = "";

			

				var clientData = private.ExtractData( eventData, "clients" );

			

				for( var x = 0; x < clientData.length; x++ ) 

				{

					var clientInfo      = clientData[x];

					var clientFullName  = private.ExtractData(clientInfo, "clientFullName"); // Full name

					var clientListImage = private.ExtractData(clientInfo, "clientListImage"); // Small image



					// First container node

					var clientContainerNode = document.createElement("div");

					clientContainerNode.className = "col-xs-4 col-sm-3 col-lg-2 block-section";



					// Second container node

					var clientSecondaryContainerNode = document.createElement("div");

					clientSecondaryContainerNode.className = "nav nav-pills nav-justified clickable-steps";                     

					clientContainerNode.appendChild(clientSecondaryContainerNode);



					// Link node                     

					var clientLinkNode = document.createElement("a");

					clientLinkNode.href = "javascript:void(0)";

					clientLinkNode.setAttribute("data-gotostep", private.Nodes.ClientDetailStepNode.id);

					clientLinkNode["eventData"]  = eventData;

					clientLinkNode["clientData"] = clientInfo;



					clientLinkNode.addEventListener("click", function (ev)

					{

						pointer.SetClientSubPageValues(this["clientData"],this["eventData"]);

					},

					false);



					clientSecondaryContainerNode.appendChild(clientLinkNode);



					// Image node

					var imageNode = document.createElement("img");

					imageNode.className = "img-circle";

					imageNode.alt = "image";			         

					imageNode.src = clientListImage;

					imageNode.setAttribute("data-toggle", "tooltip");

					imageNode.title = clientFullName;

					 

					clientLinkNode.appendChild(imageNode);

					 

					private.Nodes.ClientsListContainerNode.appendChild(clientContainerNode);

				}			 			     			     

			}

			// Force to affect element auto-style (classes for tooltip, ...)

			if(App)

				App.init();



			//From Individual client TO Clients list            

			$("#" + private.Nodes.ClientDetailStepNode.id).find(".back_clients_list").attr("data-gotostep", private.Nodes.ClientsListStepNode.id).on("click", function () {

				$("#" + private.Nodes.ServiceStepNode.id).css("display", "none"); // View session

				$("#" + private.Nodes.ClientDetailStepNode.id).css("display", "none"); // Client detail

				$("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "none"); // Switch session

				//$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session			    

				$("#" + private.Nodes.ClientsListStepNode.id).css("display", "block"); // Clients list



				// Affect the client id in the hidden field

				private.Nodes.ClientDetailHiddenFieldNode.value = "";

				private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";

				private.Nodes.ClientSwitchSessionHiddenFieldNode.value = "";

			});



			// From Client Detail TO Switch session

			$("#" + private.Nodes.ClientDetailStepNode.id).find(".switch_session").attr("data-gotostep", private.Nodes.SwitchSessionStepNode.id).on("click", function () {

				$("#" + private.Nodes.ServiceStepNode.id).css("display", "none"); // View session			    

				$("#" + private.Nodes.ClientDetailStepNode.id).css("display", "none"); // Client detail

				$("#" + private.Nodes.ClientsListStepNode.id).css("display", "none"); // Clients list

				$("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "block"); // Switch session

				//$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session



				// Affect the client id in the hidden field

				private.Nodes.ClientDetailHiddenFieldNode.value = private.ExtractData(clientInfo, "clientId");

				private.Nodes.ClientSwitchSessionHiddenFieldNode.value = private.ExtractData(clientInfo, "clientId");

				private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";

				// Affect the button value

				private.Nodes.ClientRemoveFromSessionNode.value = "Remove from session";

			});



			// From Clients list TO View Session        

			$("#" + private.Nodes.ClientsListStepNode.id).find(".back_session_detail").attr("data-goto-step", private.Nodes.SwitchSessionStepNode.id).on("click", function () {			    			    

				$("#" + private.Nodes.ClientsListStepNode.id).css("display", "none"); // Clients list

				$("#" + private.Nodes.ClientDetailStepNode.id).css("display", "none"); // Client detail

				$("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "none"); // Switch session

				//$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session			    

				$("#" + private.Nodes.ServiceStepNode.id).css("display", "block"); // View session

			});



			// From Remove from session TO Client Detail

			$("#" + private.Nodes.SwitchSessionStepNode.id).find(".back_client_detail").attr("data-goto-step", private.Nodes.ClientDetailStepNode.id).on("click", function () {

				$("#" + private.Nodes.ServiceStepNode.id).css("display", "none"); // View session

				$("#" + private.Nodes.ClientsListStepNode.id).css("display", "none"); // Clients list

				$("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "none"); // Switch session

				//$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session			    

				$("#" + private.Nodes.ClientDetailStepNode.id).css("display", "block"); // Client detail



				// Affect the client id in the hidden field

				private.Nodes.ClientDetailHiddenFieldNode.value = private.ExtractData(clientInfo, "clientId");

				private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";

				private.Nodes.ClientSwitchSessionHiddenFieldNode.value = "";

				// Set the remove button value (because ovveriden by default "step" component

				private.Nodes.ClientRemoveFromSessionNode.value = "Remove from session";

			});

		};

		

		private.ExtractData = function( data, fieldName )

		{

			var output = "";



			if( data[fieldName] )

				output = data[fieldName];



			return output;

		};

		

		// Affect the user values

		public.SetClientSubPageValues = function( clientInfo, eventData )

		{

			// Hide the father step and display the current one

			private.Nodes.ClientsListStepNode.style.display = "none";

			private.Nodes.ClientDetailStepNode.style.display = "block";

			

			var clientFullName = private.ExtractData(clientInfo, "clientFullName"); // Full name			         

			var clientDetailImage = private.ExtractData(clientInfo, "clientDetailImage"); // Detail image			         

			var clientEmailAddress = private.ExtractData(clientInfo, "clientEmailAddress"); // Email address			         

			var clientPhoneNumber = private.ExtractData(clientInfo, "clientPhoneNumber"); // Phone number			         

			var clientFullProfileLink = private.ExtractData(clientInfo, "clientFullProfileLink"); // Full profile link			         

			var clientId = private.ExtractData(clientInfo, "clientId"); // Client Id			         		    

			var clientLocalisation = private.ExtractData(clientInfo, "clientLocalisation"); // Localisation			         

			var clientNote = private.ExtractData(clientInfo, "clientNote"); // Note			         

			var clientRemoveFromSessionLink = private.ExtractData(clientInfo, "clientRemoveFromSessionLink"); // Remove from session link



			// Affect the client id in the hidden field

			private.Nodes.ClientDetailHiddenFieldNode.value = clientId;

			private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";

			private.Nodes.ClientSwitchSessionHiddenFieldNode.value = "";



			// Full name

			private.Nodes.ClientFullNameNode.innerHTML = clientFullName;

			// Image

			private.Nodes.ClientDetailImageNode.src = clientDetailImage;

			// Phone number

			private.Nodes.ClientPhoneNumberNode.innerHTML = "<i class='fa fa-phone fa-fw'></i> " + clientPhoneNumber;

			// Email address

			private.Nodes.ClientEmailAddressNode.innerHTML = "<i class='fa fa-envelope  fa-fw'></i> " + clientEmailAddress;

			// Full profile link		    

			private.Nodes.ClientFullProfileLinkNode.href = clientFullProfileLink;

			// Remove from session link

			private.Nodes.ClientRemoveFromSessionNode.value = "Remove from session";

			//private.Nodes.ClientRemoveFromSessionNode.href = clientRemoveFromSessionLink;

			//private.Nodes.ClientRemoveFromSessionNode.href = "javascript:void(0)";

			//private.Nodes.ClientRemoveFromSessionNode.setAttribute("data-gotostep", private.Nodes.ClientConfirmRemoveFromSessionNode.id);



			var RemoveFromSessionId = private.Nodes.ClientRemoveFromSessionNode.id;

			document.getElementById(RemoveFromSessionId).addEventListener("click", function(){

				private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = private.ExtractData(clientInfo, "clientId");

			});



			// Note

			//private.Nodes.ClientNoteNode.value = clientNote;

			// Remove the disabled attribute

			$("#" + private.Nodes.ClientDetailStepNode.id).find("textarea").removeAttr('disabled');



			$(".remove_from_session ").unbind();

			$(".save_note").unbind();

			$(".save_session_changes").unbind();

			

			 

			if( private.Listeners.ClientSwitchLink )

				//private.Nodes.ClientSwitchLinkNode.removeEventListener( "click", private.Listeners.ClientSwitchLink, true );    

			 

				private.Listeners.ClientSwitchLink     = function( ev )

				{ 

					pointer.SetClientSubPageEventSelectValues( clientInfo );

					return false;

				};



			// Remove existing options if any

			private.Nodes.SelectOtherSessionNode.options.length = 0;



			var otherEvents = window.Li3calendarFilters.GetEvents( );

			 

			for( var x = 0; x < otherEvents.length; x++ ) 

			{

				var otherEventData = otherEvents[x].data;



				if (otherEventData.serviceId == eventData.serviceId)

				{

					var addToOption = true;

					var optionTitle = "#" + private.ExtractData(otherEventData, "eventId") + " on " + private.ExtractData(otherEventData, "sessionTime") + " @ " + private.ExtractData(otherEventData, "sessionStartsOn");

					if (private.ExtractData(otherEventData, "eventId") == "")

						addToOption = false;



					var option = document.createElement("option");

					option.text  = optionTitle;

					option.value = private.ExtractData(otherEventData, "eventId");



					if (window.Li3calendarFilters.IsClientInEventData(otherEventData, clientId))

						option.disabled = "disabled";



					if (addToOption)

						private.Nodes.SelectOtherSessionNode.add(option);

				}

				 

			}

		};

		

		public.SetClientSubPageEventSelectValues = function( eventData, clientInfo )

		{

			private.Nodes.ClientSwitchIdField.value = clientInfo[ "clientId" ];                                            

			private.Nodes.ClientNameNode.innerHTML  = clientInfo["name"];

			private.Nodes.ClientPicNode.src         = clientInfo["picUrl"];

		};

		

		// Must be called here 

		private.Construct( );

	};

  

	window.Li3calendarModal   = new Li3calendarHostEditModal( ); 

})( );