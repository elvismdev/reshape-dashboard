(function(){

	

	window.do_search_messages_list = function( chatId, lastMsgId ) {

		

		var post_data = {

			"chatId": chatId,

			"lastMsgId" : lastMsgId,

		};

		

		api_key = 12345;



		var request = $.ajax({

			type: "POST",

			url: "/api/ajax/auth/user/msg/messages?token=12345",//+api_key,

			dataType: "json",

			data: post_data

		});

		

		// if the request succeeds

		request.done(function(response) {

			results_obj = response.data;

			console.log("results: "+JSON.stringify(results_obj));

			var i = 0;

			var text = "";

			var avatarUrl = "";			

			var senderId = "";

			var userID = response.userId;

			var messageClass = "";

			

			for(i = 0 ; i < response.messages.length ; i++)

			{

				// Get the text

				text = response.messages[i].text;

				// Get the user avatar

				avatarUrl = response.messages[i].sender.picture_url; //[2][4];

				senderId = response.messages[i].sender.id;

				lastMsgId = response.messages[i].messageId;

 

				// keep this "animation-expandUp" for animation when putting a message

				

				// define on which side the message must be displayed

				if(userID == senderId){

					messageClass = "chatui-talk-msg chatui-talk-msg-highlight";

				} else {

					messageClass = "chatui-talk-msg";

				}

				// Insert the message in the conversation

				$('.li3chat_container').append('<li id="'+ lastMsgId +'" class="' + messageClass + '">'

                    + '<img src=' + avatarUrl + ' alt="Avatar" class="img-circle chatui-talk-msg-avatar">'

                    + $('<div />').text(text).html()

                    + '</li>');

			}

			return lastMsgId;

		});



		// if the request fails

		request.fail(function(jqXHR, textStatus, error) {

			console.log("search failed")

			console.log(jqXHR.statusText);

			console.log(textStatus);

			console.log(error);

		});

	}

})();