(function () {
  
  var AjaxCallsMessaging = function( )
	{
    // Fake accessibility scopes
		var private = { };
		var public  = this;
    
    var conversationsContainer = $("#conversations_container");
    var messagesContainer      = $("#messages_container");
    var messageInput           = $("#input_message");
    var sendMessageForm        = $("#form_send_message");    
    var pushNewMessageBtn      = $("#pushNewMessage");
    
    public.conversation_participants = $("#conversation_participants");
    public.conversation_subject      = $("#conversation_subject");
    public.conversation_started_on = $("#conversation_started_on");
    
    var scrollableMessages     = $(".scrollable-messages");
    
    var newConversationMessageText = $("#newConversationMessageText");
    var newConversationSubject     = $("#newConversationSubject");
    var recipientsListJsId         = $("#recipientsListJsId");
    
    var modalNewMessage                = $("#modal-new-message");
    var openModalCreateConversationBtn = $("#openModalCreateConversation");
    var createConversationBtn         = $("#create_conversation_btn");
    
    // Array containing chat ids and number of messages
    var unread_conversations_a = [ ];
    var sync_conversations_a = [ ];
    var sync_conversations_last_post_a = [ ];
    
    
    // Intervals
    public.messagesInterval      = "";
    public.conversationsInterval = "";
    var intervalError = "";
    
    var baseUrl = "http://dash.reshape.net/";
    var token = 1234;
    
    // Fake constructor  
		private.Construct = function( )
		{
			
		};
    
    $(document).ready( function(){
      public.displayConversations();      
      public.send_message();
      public.create_conversation_action();
      public.enable_interval_conversations();
      
      //public.test_new_api_new_conv();
      //public.test_new_api_users_list();
      //public.test_new_api_create_conv();
      //public.test_new_api_get_messages();
      //public.test_new_api_post_message();
      //public.test_get_nb_unread_conversations();
      //public.test_search_word();
      
      $("#example-input1-group2").on("keyup", function(){
        public.displayConversations();
      });
    
    });
    
    public.test_get_nb_unread_conversations = function( ){
      var url="/static/controllers/get_number_unread_conversations.php";
      
      var post_data = { };
      
      var request = $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      request.done( function( response ) {
        console.log(response);
      });
      
    }
    
    // Depending of the response (success/error), display notifications 
    public.define_response_output = function( response ){
      // Response is successful
      var i = 0;				
      // No error, this is great!
      if(response.error == "false") {
        // Display each notice in a message
        //var message = "";
        // if notices are defined, display them
        if(response.post_data.notices.length > 0){
          var type = "";
          for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
            type =  response.post_data.notices[i].type;
            message =  response.post_data.notices[i].message;
            // Trigger a notification
            $.growl({
              title: '<strong>UPDATE</strong> ',
              message: message,
              delay: 8000,
            },{
              type: 'success'
            });
          }
        // Otherwise, display standard notice
        } else { 
          $.growl({
            title: '<strong>UPDATE</strong> ',
            message: "Information saved",
            delay: 8000,
          },{
            type: 'success'
          });
        }						
      // Oops!
      } else if (response.error == "true") {
        //Post data validation error
        for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
          type =  response.post_data.notices[i].type;
          message =  response.post_data.notices[i].message;
          // Trigger a notification
          $.growl({
            title: '<strong>ERROR</strong> ',
            message: message,
            delay: 8000,
          },{
            type: 'danger'
          });
        }
      
      }
    }
    
    // Display conversations
    public.displayConversations = function( conversationToDisplay ){
          
      var url="/static/controllers/get_conversations_list.php";
      var post_data = { };
      post_data["search"] = $("#example-input1-group2").val();
      var request = $.ajax({
        type: "POST",
        url: url,        
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
       });
          
      request.done( function( response ) {        
        var i = 0;
        
        var conversationContainer  = "";
        var statusContainer        = "";
        var currentUserId = response.userId; // Current user id
        var lastMessagePersonImage = "";
        var lastSenderId    = "";
        var lastMessagePersonNameContainer  = "";
        var lastMessagePersonName = "";
        var lastMessageDateContainer = "";
        var lastMessageDate = "";
        
        var chat = "";
        var member = "";
        conversationsContainer.empty();
        
        // If there is no conversations, disable submit form new message        
        if( response.chats && response.chats.length == 0 ){
          messageInput.attr("disabled", "disabled");
          pushNewMessageBtn.attr("disabled", "disabled");
        } else {
          messageInput.removeAttr("disabled");
          pushNewMessageBtn.removeAttr("disabled");
        }
        
        // Display conversations if exist
        if(response.chats !== undefined){
        
          for( i = 0 ; i < response.chats.length ; i++) {
            
            // Current chat item
            chat = response.chats[i];
            
            var member_nb = chat.members.length;
            var is_unread = chat.isUnread == "true" ? true : false;
            
            // Main container
            conversationContainer = $("<a>").attr("href", "#").addClass("list-group-item message").appendTo(conversationsContainer);
            conversationContainer.attr("data-id", chat.chatId);
            
            messagesContainer.attr("data-conversation", "");
            
            // Tag the current conversation as unread
            if( is_unread ){
              conversationContainer.addClass("conv_unread");
            }
            
            // Image
            lastMessagePersonImage = $("<img>").addClass("pull-left").appendTo(conversationContainer);
            lastSenderId = chat.lastSenderId;
            
            lastMessageDate = moment(chat.lastActiveOn).format("hh:mm A");
            var displayNextMember = false;
            
            var membersName_str = "";
            
            var snippet = chat.snippet.replace(/\n/g, "<br/>");
            var is_assigned = false;
            var is_last_sender_in_conv = false;
            for ( var j = 0 ; j < chat.members.length ; j++ ){
              // Current member
              member = chat.members[j];
              
              if( j == 0 ){
                membersName_str = member.name;
              } else if( j < chat.members.length - 1 ){
                membersName_str += ", " + member.name;
              } else {
                membersName_str += " & " + member.name;
              }
                if( lastSenderId == member.id ){
                  is_last_sender_in_conv = true;
                }
                // Copy of original if( ( lastSenderId == member.id && member.id != currentUserId ) || j == chat.members.length - 1 )
                if( (lastSenderId == member.id || member_nb == 1) && !is_assigned ) {
                  lastMessagePersonName = member.name;
                  lastMessagePersonImage.attr( "src", member.picture_url );
                  lastMessagePersonImage.attr( "alt", lastMessagePersonName );
                  is_assigned = true;
                } else if ( lastSenderId != member.id && !is_assigned ) {
                  lastMessagePersonName = member.name;
                  lastMessagePersonImage.attr( "src", member.picture_url );
                  lastMessagePersonImage.attr( "alt", lastMessagePersonName );             
                }
                
              var last_item_pic = member.picture_url;
              var last_item_name = member.name;
                
            }
            
            // if the last sender has quit the conversation, display current user image and no snippet
            if( !is_last_sender_in_conv ) {
              lastMessagePersonImage.attr( "src", last_item_pic );
              lastMessagePersonImage.attr( "alt", last_item_name );          
            }
            
            // Assign membersName_str in an attribute
            conversationContainer.attr("data-participants", membersName_str); 
            // Assign the subject in attribute
            var subject_str = chat.title != "" ? chat.title : "no subject";
            conversationContainer.attr("data-subject", subject_str);
            // Assign conversation beginning time in an attribute
            var conv_start_on_str = moment(chat.startedOn).format("MM/DD/YYYY hh:mm A");
            conversationContainer.attr("data-started-on", conv_start_on_str);
            // Assign the number of members          
            conversationContainer.attr("data-members", member_nb);
            
                    
            lastMessagePersonNameContainer = $("<span>").addClass("contacts-title").appendTo(conversationContainer);
            lastMessagePersonNameContainer.html( lastMessagePersonName );
            
            $("<i>").addClass("fa fa-times pull-right leaveicon").appendTo(conversationContainer);
            
            
            lastMessageDateContainer = $("<span>").addClass("msgdate pull-right").appendTo(conversationContainer);
            lastMessageDateContainer.html(lastMessageDate);
            
            lastMessageContainer = $("<p>").appendTo(conversationContainer);
            
            if( member_nb > 1 && is_last_sender_in_conv ) {
              lastMessageContainer.html( snippet );
            } else if( !is_last_sender_in_conv ) {
              lastMessageContainer.html( "" );
            }
            
          }
        } else {
          // Remove all messages from other conversation
          $(".panel.panel-default.push-up-10").css( "display", "none" );
          public.conversation_started_on.html("");
          // Reset placeholders (subject, participants list and created date)                
          public.conversation_participants.html( "" );                          
          public.conversation_subject.html( "" );                
          public.conversation_started_on.html( "" );
          messageInput.val("");
          messagesContainer.empty();
          public.disable_interval_messages();
          // If succeed, set the current chat id to null
          messagesContainer.attr("data-conversation", "");
        }

        public.setChoseConversationAction();
        
        // Display the new conversation if defined
        if( conversationToDisplay ) {
            // Display the new conversation
            conversationsContainer.find(".list-group-item.message[data-id='" + conversationToDisplay + "']").click();
            messagesContainer.attr("data-conversation", conversationToDisplay);
        }
        
        // If no conversation is opened, open the first one
        if( messagesContainer.attr("data-conversation") == "" ){
          conversationsContainer.find(".list-group-item.message").first().click();
        }
        
        var chatIdSelected = messagesContainer.attr("data-conversation");
        
        //$(".list-group-item.message").css("background-color", "white");
        //$(".list-group-item.message[data-id= '"+chatIdSelected + "']").css("background-color", "#F7F7F7");
        $(".list-group-item.message[data-id= '" + chatIdSelected + "']").addClass("conv_selected");     
      
        // Define leave conversation
        public.leave_conversation_action();
        
        //public.sync_conversations( response.chats );
      });      
    }
    
    public.sync_conversations = function ( chats ){
      var new_chats_a = [ ];
      var new_chats_time_a = [ ];
      
      var userId = $("#conversations_container").attr("data-id");
      
      if( sync_conversations_a.length == 0 ) {
        sync_conversations_a = new_chats_a;
      }
      
      if( sync_conversations_last_post_a.length == 0 ){
        sync_conversations_last_post_a = new_chats_time_a;
      }
      
      // For each chat
      for( var i = 0 ; i < chats.length ; i++ ){
        // push chat id
        var chat_id = chats[i].chatId;
        new_chats_a[i] = chat_id;
        // Last user id
        var last_user_id = chats[i].lastSenderId;       
      }
      
      // If there are more conversations than before
      if( new_chats_a.length > sync_conversations_a.length ){
        
        // Look for the new conversation(s) id
        for ( var j = 0 ; j < new_chats_a.length ; j++){
          var isFound = false;
          
          var newId = new_chats_a[j];
          
          for ( var k = 0 ; k < sync_conversations_a.length ; k++ ){            
            var sync_id = sync_conversations_a[k];
            
            if( sync_id == newId ){
              isFound = true;
              continue;
            }            
          }
          if( !isFound ){
            unread_conversations_a.push(newId);
            $(".list-group-item.message[data-id='"+ newId + "']").css("background-color", "#D2D2D2");
          }          
        }      
      }      
      sync_conversations_a = new_chats_a;
    }
    
    // Open the chat when a conversation is clicked
    public.setChoseConversationAction = function( ){
      $(".list-group-item.message").click(function(e) {
        e.preventDefault();
        
        //$(".list-group-item.message").css("background-color", "white");
        //$(this).css("background-color", "#F7F7F7");
        
        
        
        
        
        var chatId = $(this).data("id");        
        messagesContainer.attr("data-conversation", chatId);

        // API call to push unread tag as read.
        if( $(this).hasClass("conv_unread") ) {          
          public.set_conversation_as_read( chatId );
          
        }
        
        // All conversations are not selected
        $(".list-group-item.message").removeClass("conv_selected");
        
        // The selected conversation is not unread any more, it's selected
        $(this).removeClass("conv_unread").addClass("conv_selected");
        
        // Remove all messages from other conversation
        messagesContainer.empty();
        
        // Display all messages        
        public.display_messages( chatId, 0, false, true );        
      });    
    };
    
    // Send a message and refresh the conversation
    public.send_message = function(){               
      sendMessageForm.on("submit", function( e ){        
        e.preventDefault();
        
        var message_to_send = messageInput.val();
        var chatId = messagesContainer.attr("data-conversation");
                
        // Send the message if not empty
        if(message_to_send){              
          var url="/static/controllers/add_message.php";
          var post_data = { };			
          post_data["chatId"]   = chatId;
          post_data["text"] = messageInput.val();
          
          var request = $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });
          
          request.done( function( response ) {
            messageInput.val("");
            // get the last message id
            var lastMessageId = String(response.newMessage.messageId);
            
            // Add only lasts messages in the conversation
            public.display_messages( chatId, lastMessageId, true, true );
            public.displayConversations();                        
          });
          
          request.fail(function(jqXHR, textStatus, error) {
            console.log("search failed")
            console.log(jqXHR.statusText);
            console.log(textStatus);
            console.log(error);
          });        
        }
      });      
    }
    
    public.enable_interval_conversations = function(){
      public.disable_interval_conversations();
      public.conversationsInterval = setInterval( function( ev ) { public.displayConversations(); }, 15000);
    }
    
    public.disable_interval_conversations = function(){
      clearInterval(public.conversationsInterval);
    }
        
    // Enable an interval for display messages and refresh conversations list.
    // Destroy himself and create himself again to update values (as the last message id).
    public.enable_interval_messages = function (chatId, theLastMessageId, intervalTime){
      public.disable_interval_messages();
      public.messagesInterval = setInterval( function( ev ) { public.display_messages( chatId, theLastMessageId, true, false ); }, intervalTime);
    }
    
    // Disable the interval (displaying messages)
    public.disable_interval_messages = function(){
      clearInterval(public.messagesInterval);
    }    

    public.display_messages = function( chatId, messageId, isNewMessagePosted, scroll ){      
          
      public.disable_interval_messages();
      
      // Get the number of members and display a message if only one member
      var members_nb = parseInt( $(".list-group-item.message[data-id = '" + chatId + "']").attr("data-members") );
      
      if( members_nb < 2 ){
        $(".panel.panel-default.push-up-10").css( "display", "none" );        
      } else {
        $(".panel.panel-default.push-up-10").css( "display", "block" );        
      }
           
      var url="/static/controllers/get_messages.php";
      var post_data = { };			
      post_data["chatId"]   = chatId;
      post_data["lastMsgId"] = messageId;
      
      var request = $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      
      request.done( function( response ) {
        // If we switch conversation before the callback triggers, we don't display the new message.
        if( messagesContainer.attr("data-conversation") == chatId ){
          
          // Display the participants
          var participants_str = $(".list-group-item.message[data-id = '" + chatId + "']").attr("data-participants");
          public.conversation_participants.html( participants_str );          
          //Display the subject 
          var subject_str = $(".list-group-item.message[data-id = '" + chatId + "']").attr("data-subject");
          public.conversation_subject.html( subject_str );
          // Display started time
          var started_time_str = "Conversation started on " +  $(".list-group-item.message[data-id = '" + chatId + "']").attr("data-started-on");
          public.conversation_started_on.html(started_time_str);
          
          
          // Display each message
          if( response.messages ){
            for( var i = 0 ; i < response.messages.length ; i++ ) {
              var message = response.messages[i];
              var messageText         = message.text;
              messageText = messageText.replace(/\n/g, "<br/>");
              var messageSenderName   = message.sender.name;
              var messageSenderPicSrc = message.sender.picture_url;
              var messageSentOn = message.postedOn;
              var messageSenderId = message.sender.id;
              var messageSenderUrl = message.sender.profile_url;
              var userId = conversationsContainer.data("id");
              var newMessageId = message.messageId;
              
              // Define the side of the message (left/right)
              var sideClass = messageSenderId == userId ? "item in item-visible" : "item item-visible";
              // Define the item color
              var itemColor = messageSenderId == userId ? "#f6f6f6" : "#FFFFFF";
              // Add the effect if it's a new message
              sideClass += isNewMessagePosted ? " animation-expandUp" : "";

              // Main container
              var mainContainer = $("<div>").addClass(sideClass).appendTo(messagesContainer); // or use "item item-visible" to set different side
              // Image container
              var imageContainer = $("<div>").addClass("image").appendTo(mainContainer);
              // Image
              var image = $("<img>").attr("src", messageSenderPicSrc).attr("alt", messageSenderName).appendTo(imageContainer);
              // Text container
              var textContainer = $("<div>").addClass("text").attr("data-id", newMessageId).css("background-color", itemColor).appendTo(mainContainer); ////background: none repeat scroll 0 0 #f6f6f6;          
              // Heading
              var heading = $("<div>").addClass("heading").appendTo(textContainer);
              // Sender name
              var senderName = $("<a>").attr("href", messageSenderUrl).attr("target", "_blank").html(messageSenderName).appendTo(heading);
              // Date
              var date = moment(messageSentOn).format("MM/DD/YYYY");
              var time = moment(messageSentOn).format("hh:mm A");
              var dateNode = $("<span>").addClass("date").html( date + " " + time).appendTo(heading);            

              // Append the message
              textContainer.append( messageText );                                
            }
          
          }
          
          
          // Add message if ony one member
          if( members_nb < 2 ){            
            $("<div>").addClass("col-md-12 text-center").css("padding-top", "10px").css("font-weight", "700").css("font-size", "100%").css("color", "black").html("There are no more users left in this conversation.").appendTo(mainContainer);                        
          }
        }
                  
        // Get the last message id
        var theLastMessageId = messagesContainer.find(".item").last().find('.text').data("id");
        
        if( scroll || response.messages ){
          // Scroll to the bottom
          scrollableMessages.animate({ scrollTop: messagesContainer[0].scrollHeight}, 0); //500
        }
        
        // Redefine the interval with new values
        public.enable_interval_messages( chatId, theLastMessageId, 2000 );
      });
      

      // if the request fails
      request.fail(function(jqXHR, textStatus, error) {
        console.log("search failed")
        console.log(jqXHR.statusText);
        console.log(textStatus);
        console.log(error);
        var theLastMessageId = messagesContainer.find(".item").last().find('.text').data("id");
        public.enable_interval_messages(chatId, theLastMessageId, 8000);
      });
    }
    
    // Leave the current conversation
    public.leave_conversation_action = function( ){
      
      $(".leaveicon").each( function( ) {
        $(this).on( "click", function ( event ){
          event.stopPropagation();
          var pointer = $(this);
          //event.preventDefault();
          
          swal({
            title: "Are you sure?",
            text: "You will no longer be able to see messages from this conversation if you leave it.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d22130",
            confirmButtonText: "Leave",
            closeOnConfirm: false
          },
          function () {
          
            var chatId = pointer.closest(".list-group-item.message").attr("data-id");
                      
            var url="/static/controllers/leave_conversation.php";
      
            var post_data = { };			
            post_data["chatId"]   = chatId;
            
            var request = $.ajax({
              type: "POST",
              url: url,
              dataType: "json",
              xhrFields: {
                withCredentials: true
              },
              crossDomain: true,
              data: post_data
            });
                        
            request.done( function( response ) {
              //public.define_response_output( response );
              if(response.error == "false"){
                
                // Reset placeholders (subject, participants list and created date)                
                public.conversation_participants.html( "" );                          
                public.conversation_subject.html( "" );                
                public.conversation_started_on.html( "" );
              
                public.displayConversations();
                messageInput.val("");
                messagesContainer.empty();
                public.disable_interval_messages();
                // If succeed, set the current chat id to null
                messagesContainer.attr("data-conversation", "");                
              }            
            });
            
            request.fail(function(jqXHR, textStatus, error) {
              console.log("search failed")
              console.log(jqXHR.statusText);
              console.log(textStatus);
              console.log(error);
            });                  
          });        
        });      
      });    
    }
    
    // Create a conversation actions
    public.create_conversation_action = function(){
      openModalCreateConversationBtn.unbind("click");
      openModalCreateConversationBtn.on("click", function(){
        
          // Reset the form values
          newConversationSubject.val("");
          newConversationMessageText.val("");
          recipientsListJsId.empty();
          recipientsListJsId.val('').trigger('chosen:updated');
          
          // Reset the recipients
          var post_data = { };
          post_data["token"]  = token;
          //post_data["type[]"] = [1,2,3,5,7];
          
          var request = $.ajax({
            type: "POST",
            url: baseUrl + "api/ajax/auth/user/reshNetwork/list",
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });
          
          var url="/static/controllers/get_network_list.php";
          $.getJSON(url,function(response){
            var networkLinks = response.networkLinks;           
            for( var i = 0 ; i < networkLinks.length ; i++){
              var new_id = networkLinks[i].profile.id;
              // Create option
              var is_found = false;
              $.each(recipientsListJsId.find("option"), function( index, value ) {
                var option_id = $(this).val();
                if( option_id == new_id) {
                  is_found = true;                  
                }                
              });
              if( !is_found ){
                $("<option>").attr( "value", networkLinks[i].profile.id ).html( networkLinks[i].profile.name ).appendTo(recipientsListJsId);
              }
                
            }
            recipientsListJsId.chosen({width: '293px', placeholder_text_multiple: 'Enter one or more person.'});
            recipientsListJsId.val('').trigger('chosen:updated');
            
            // Reset selected options
            var selectedOptions = $("#recipientsListJsId_chosen .chosen-choices");            
            $.each( selectedOptions, function( i, val ) {
              $(this).find(".search-choice-close").click();
            });
            
            // Display the modal form
            modalNewMessage.modal("show");          
          });     
      });
      
      createConversationBtn.unbind( "click" );
      createConversationBtn.on("click", function( event ){
        event.preventDefault();
        
        if( newConversationSubject.val() != "" && newConversationMessageText.val() != "" && recipientsListJsId.val() != "" ) {
          var url="/static/controllers/add_conversation.php";
      
          var post_data = { };			
          post_data["topic"]   = newConversationSubject.val();
          post_data["message"] = newConversationMessageText.val();
          post_data["to"]      = recipientsListJsId.val();
          
           
          
          var request = $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });
          
          request.done( function( response ) {
            if( response.error == "false") {            
              //public.define_response_output( response );
              // Hide the modal form
              modalNewMessage.modal("hide");
              
              var newChatId = response.chat.chatId; 
              // Refresh conversations list
              public.displayConversations( newChatId );
              
              // Remove the current conversation if defined
              messagesContainer.empty();
              
                         
              messagesContainer.data( "conversation", newChatId );            
              // Don't display messages because when this is the only conversation in the list the first message appears twice 
              //public.display_messages( newChatId, 0, false, false );
              
            } else {
              // For each notice, display error message
              for ( var i = 0 ; i < response.post_data.notices.length ; i++ ){
                //response.post_data.notices
                public.put_growl_error_message( response.post_data.notices[i].message );
              }            
            }
            
          });

          // if the request fails
          request.fail(function(jqXHR, textStatus, error) {
            console.log("search failed")
            console.log(jqXHR.statusText);
            console.log(textStatus);
            console.log(error);                    
          });
        
        
        } else {
          public.put_growl_error_message( "You must specify all fields to create a conversation");
        }
        
      });
    }
    
    public.put_growl_error_message = function( aMessage ){
    $.growl({
      title: '<strong>ERROR</strong> ',
      message: aMessage,
      delay: 8000,      
      },{
        type: 'danger',
        z_index: 1051
      });
    }
    
    public.put_growl_success_message = function( aMessage ){
      $.growl({
        title: '<strong>UPDATE</strong> ',
        message: aMessage,
        delay: 8000,      
      },{
        type: 'success',
        z_index: 1051
      });
    }
      
    // API call to push a conversation state to "read" 
    public.set_conversation_as_read = function( chatId ){
      var url="/static/controllers/set_conversation_read.php";
        
      var post_data = { };
      post_data['chatId'] = chatId;
      
      var request = $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      request.done( function( response ) {
        //console.log(response);
      });
    }
    
    // Must be called here 
		private.Construct( );
	};
  
  

  window.ajaxCallsMessaging   = new AjaxCallsMessaging( );
})( );