(function () {
  
  var AjaxCallsNetwork = function( )
	{
    // Fake accessibility scopes
		var private = { };
		var public  = this;
    
    var baseUrl = "http://dash.reshape.net/";
    var token = 1234;
    
    // User id
    var user_id = $(".feed-box.text-center").data("id");
    
    // Filter
    public.link_type = $("#link_type");
    
    // Search by name
    public.search_network_by_name = $("#search_network_by_name");
    
    // Buttons
    public.add_new_contact_btn = $("#add_new_contact_btn");
    public.save_contact_btn    = $("#save_contact_btn");
    
    // Network item
    public.network_item_container = $(".feed-box.text-center");    
    public.open_message_modal_btn = $(".open_message_modal_btn");
    public.edit_network_user      = $(".edit_network_user");
    
    // Add contact modal form and fields
    public.modal_new_network_contact = $("#modal_new_network_contact");
    public.contact_name              = $("#contact_name");
    public.contact_link_select       = $("#network_link");
    public.select_contact_name       = $("#select_contact_name");
    
    // Edit contact modal form and fields
    public.modal_edit_network_contact = $("#modal_edit_network_contact");
    public.update_network_contact_btn = $("#update_network_contact_btn");
    public.edit_network_image = $("#edit_network_image");
    public.edit_network_name = $("#edit_network_name");
    public.remove_network_contact_btn = $("#remove_network_contact_btn");
    
    
    // Modal for sending a message
    public.modal_send_message = $("#modal_send_message");
    public.message_subject    = $("#message_subject");
    public.message_text       = $("#message_text");  
    public.send_message_btn   = $("#send_message_btn");
    

    // Fake constructor  
		private.Construct = function( )
		{
			
		};    
    
    $(document).ready( function(){
      public.reset_modal_add_person();
      public.set_save_contact();
      public.set_open_messaging_modal();
      public.set_user_list();
      public.set_edit_network_contact();
      public.set_filter_actions();
      public.set_search_by_name();
    });
    
    // Enable the search by name
    public.set_search_by_name = function( ){
      public.search_network_by_name.on("keyup", function(){
        public.process_filters();
      });    
    }
    
    // Set actions when selecting filters
    public.set_filter_actions = function( ){
      public.link_type.find("li a").on( "click", function( ){
        var filter_id = $(this).data("filter");
        public.link_type.attr("data-active", filter_id);
        public.process_filters();
      });    
    }
    
    // Process multi-filters (name and type)
    public.process_filters = function( ){
      var filter_id = parseInt( public.link_type.attr("data-active") );
      var search_val = public.search_network_by_name.val();
      
      // Look if the item name and type matches with filters
      $.each( $(".network_item"), function( key, value ) {
        var item_name = $(this).find(".network-name").html();
        var item_filter_a = JSON.parse("[" + $(this).data("filters") + "]");
        
        var is_name_matches = item_name.toLowerCase().indexOf( search_val.toLowerCase() ) >= 0;
        var is_type_matches = $.inArray( filter_id , item_filter_a ) != -1 || filter_id == -1;
        
        // If the item matches with filters, display it, otherwise hide it!
        if( is_name_matches && is_type_matches){
          $(this).show("slow");
        } else {
          $(this).hide("slow");
        }        
      });      
    }
    
    // List all users in the select
    public.set_user_list = function( ){
      // Disable the add button until user list is loaded entirely
      public.add_new_contact_btn.attr("disabled", "disabled");
    
      // Make API call to list all users
      var post_data = { };
      post_data["token"]    = token;
      
      var request = $.ajax({
        type: "POST",
        url: baseUrl + "api/ajax/entities/users/list",
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });  
      
      request.done( function( response ) {        
        if(response.error == "false"){                       
          var users_a = response.users;
          for( var i = 0 ; i < users_a.length ; i++ ) {
            var user_a = users_a[i];
            // Add all users except current one 
            if(user_a.id != public.user_id){
              var user_name = user_a.name;
              var user_id = user_a.id;              
              var new_option = $("<option>").val(user_id).html(user_name).appendTo(public.select_contact_name);                
            }              
          }
          // Sort alphabetical of options
          var options = public.select_contact_name.find('option');
          var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
          arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
          options.each(function(i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
          });
          
          public.select_contact_name.chosen({width: '293px'});         
          public.contact_link_select.chosen({width: '293px'});
          // Enable the button          
          public.add_new_contact_btn.removeAttr("disabled");
        }        
      });      
    }
    
    // Define edit network contact actions within the modal form
    public.set_edit_network_contact = function( ) {
      public.edit_network_user.on( "click", function( event ){
        var pointer = $(this);
        
        public.remove_network_contact_btn.unbind("click");
        public.remove_network_contact_btn.on( "click", function( event ){
          event.preventDefault();
          var userId = pointer.closest(public.network_item_container).attr("data-id");
          
          swal({
          title: "Are you sure?",
          text: "You won't see this network contact within your network any more.",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#d22130",
          confirmButtonText: "Remove",
          closeOnConfirm: false
          },
          function () {
          
            
            
            // Make the API call to leave the current conversation
            var post_data = { };			
            post_data["token"]  = token;
            post_data["targetId"] = userId;
            post_data["removeAll"]   = true;
            
            var request = $.ajax({
              type: "POST",
              url: baseUrl + "api/ajax/auth/user/reshNetwork/unlink",
              dataType: "json",
              xhrFields: {
                withCredentials: true
              },
              crossDomain: true,
              data: post_data
            });
            
            request.done( function( response ) {
              public.define_response_output( response );
              if(response.error == "false"){
                public.modal_edit_network_contact.modal( "hide" );
                // Reload the page
                location.reload();
              }            
            });
            
            request.fail(function(jqXHR, textStatus, error) {
              console.log("search failed")
              console.log(jqXHR.statusText);
              console.log(textStatus);
              console.log(error);
            });       
          });      
        });    
      });
    }
    
    // Send a message to the network contact 
    public.set_open_messaging_modal = function( ){
      public.open_message_modal_btn.on( "click", function( event ){
        var pointer = $(this);
        
        // Reset fields values
        public.message_subject.val("");
        public.message_text.val("");
        
        public.send_message_btn.unbind("click");
        public.send_message_btn.on( "click", function( event ){
          event.preventDefault();          
          
          // Get data from form
          var userId = pointer.closest(public.network_item_container).attr("data-id");
          var subject = public.message_subject.val();
          var messageText = public.message_text.val();
          
          // Make the api call to send a message
          var post_data = { };
          post_data["token"]  = token;
          post_data["topic"] = subject;
          post_data["message"] = messageText;          
          post_data["to[]"] = userId;
          
          var request = $.ajax({
            type: "POST",
            url: baseUrl + "api/ajax/auth/user/msg/newChat",
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });
          
          request.done( function( response ) {            
            if(response.error == "false") {
              // Hide the modal form
              message =  response.post_data.notices[3].message;
              public.put_growl_success_message( message );
              public.modal_send_message.modal("hide");              
            } else {
              for( var i = 0 ; i < response.post_data.notices.length ; i++ ){
                if(response.post_data.notices[i].type == "error"){
                  public.put_growl_error_message( response.post_data.notices[i].message );
                }              
              }
            }            
          });        
        });        
      });      
    }
    
    // When clicking "Add new contact", reset the form
    public.reset_modal_add_person = function( ){
      public.add_new_contact_btn.unbind("click");      
      public.add_new_contact_btn.on("click", function(){
        
        // Reset user list selection
        public.select_contact_name.val('').trigger('chosen:updated');        
        // Reset links selection        
        public.contact_link_select.val('').trigger('chosen:updated');
        
        public.modal_new_network_contact.modal( "show" );
      });      
    }
    
    // Action when save contact button is clicked
    public.set_save_contact = function( ){
      public.save_contact_btn.on( "click", function( event ){
        event.preventDefault();
        
        // Get values from the form        
        // Person id
        var personId = public.select_contact_name.val();
        // Link types
        var linkTypes = public.contact_link_select.val();
        
        // Make the API call to add the person within the network
        var post_data = { };
        post_data["token"]    = token;
        post_data["targetId"] = personId;
        post_data["type[]"]   = linkTypes;
        
        var request = $.ajax({
          type: "POST",
          url: baseUrl + "api/ajax/auth/user/reshNetwork/connect",
          dataType: "json",
          xhrFields: {
            withCredentials: true
          },
          crossDomain: true,
          data: post_data
        });
        
        request.done( function( response ) {
          public.define_response_output( response );
        
          if( response.error == "false" ){
            public.modal_new_network_contact.modal( "hide" );
            // Reload the page
            location.reload();
           }
        });        
      });    
    }
    
    // Depending of the response (success/error), display notifications 
    public.define_response_output = function( response ){
      // Response is successful
      var i = 0;				
      // No error, this is great!
      if(response.error == "false") {        
        // if notices are defined, display them
        if(response.post_data.notices.length > 0){
          var type = "";
          for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
            type =  response.post_data.notices[i].type;
            message =  response.post_data.notices[i].message;
            // Trigger a notification
            $.growl({
              title: '<strong>UPDATE</strong> ',
              message: message,
              delay: 8000,
            },{
              type: 'success'
            });
          }
        // Otherwise, display standard notice
        } else { 
          $.growl({
            title: '<strong>UPDATE</strong> ',
            message: "Information saved",
            delay: 8000,
          },{
            type: 'success'
          });
        }						
      // Oops!
      } else if (response.error == "true") {
        //Post data validation error
        for( i = 0 ; i < response.post_data.notices.length ; i++ ){					
          type =  response.post_data.notices[i].type;
          message =  response.post_data.notices[i].message;
          // Trigger a notification
          $.growl({
            title: '<strong>ERROR</strong> ',
            message: message,
            delay: 8000,
          },{
            type: 'danger'
          });
        }     
      }
    }
    
    public.put_growl_error_message = function( aMessage ){
    $.growl({
      title: '<strong>ERROR</strong> ',
      message: aMessage,
      delay: 8000,      
      },{
        type: 'danger',
        z_index: 1051
      });
    }
    
    public.put_growl_success_message = function( aMessage ){
      $.growl({
        title: '<strong>UPDATE</strong> ',
        message: aMessage,
        delay: 8000,      
      },{
        type: 'success',
        z_index: 1051
      });
    }
    
    // Must be called here 
		private.Construct( );
	};

  window.ajaxCallsNetwork   = new AjaxCallsNetwork( );
})( );