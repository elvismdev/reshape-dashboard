(function() {
	
	var token = "1234";
	var baseUrl = "http://dash.reshape.net/";
  var accomplishedColor = "#aad178";
  var unAccomplishedColor  = "#eaeaea;";
  
  // Fields reference
  var f_city = $('#city');
  var f_region = $('#region');
  var f_degree = $('#degree');
  var f_institution = $('#institution');
  var f_award = $('input[name="Award"]');
  var f_accomplishment = $('input[name="Accomplishment"]');
  var f_department = $('#id0');
  var f_specialties = $('#id1');
  var f_focuses = $('#id2');
  var f_activities = $('#id3');
  var f_certifications = $('#id4'); 
  var f_facebook = $('#facebook');
  var f_twitter = $('#twitter');
  var f_instagram = $('#instagram');
  var f_spotify = $('#spotify');
  var f_youtube = $('#youtube');
  var f_googleplus = $('#googleplus');
  var f_linkedin = $('#linkedin');
  var f_pinterest = $('#pinterest');
  var f_vimeo = $('#vimeo');
  var f_custom_url = $('#custom-url');  
  var f_public_name = $('#public-name');
  var f_main_title = $('#main-title');
  var f_tagline = $('#tagline');
  var f_bio = $('#bio');
  var f_personal_website = $('#personal-website');
  var numberOfSlides = 0;
    
	
	// When document is ready, put values in the fields
	$( document ).ready(function() {
		//Set the trigger after adding a new item in the list		
		set_trigger_add_item();
		//set_disabled_picture_uploader_components();
    // Set the wizard 
    set_wizard();
    // Define colors for progress data
    define_all_progress();
    
		$(function () {
			$('#profile-background-color').colorselector();
		});		
	});	
	
	// Save values in the system
	var save_values = function($obj){
		if($obj.val() != "" || $obj.val() == ""){
			var selector = $obj.attr("data-input");
			var apiUrl = "";
			
			// Define the API call to execute
			switch(selector){
				case "li3InputProfile" : apiUrl = baseUrl + "api/ajax/auth/user/profile/push/page"		   ; break;
				case "li3InputClassif" : apiUrl = baseUrl + "api/ajax/auth/user/profile/attributes/push/set" ; break;
				case "li3InputMedia"   : apiUrl = baseUrl + "api/ajax/auth/user/profile/push/socialHub"	   ; break;
				case "li3InputAlias"   : apiUrl = baseUrl + "api/ajax/auth/user/profile/metaInfo/push/alias" ; break;
			}
			// Identifier
			var identifier = $obj.attr("data-name");
			// Get the value
			var newValue = $obj.val();
			
			// if the identifier is defined, we call the correct API to update the value
			if(identifier){
				// Post data
				var post_data = { };			
				post_data["token"]    = token;
				
				// Specific post data for multiple items (award, accomplishment, ...)
				var post_data_award = [];
				var post_data_acomp = [];
				
				// For awards and accomplishments, we create an array
				if(identifier == "award"){					
					// Return all similar fields
					$(".li3Input[data-name='award']").each(function( index ) {
						var value = $(this).val();
						if( value != "" || value == ""){ // possible to set a null value (ie for if we don't want to display awards in the live profile)
							post_data_award.push( value );
						}
					});		
					post_data[identifier] = post_data_award;				
				}			
				else if(identifier == "accomp"){
					// Return all similar fields
					$(".li3Input[data-name='accomp']").each(function( index ) {
						var value = $(this).val();
						if( value != "" || value == ""){ // possible to set a null value (ie for if we don't want to display awards in the live profile)
							post_data_acomp.push( value );
						}					
					});
					post_data[identifier] = post_data_acomp;
				}
				// Update the gender
				else if(identifier == "gender"){					
					post_data["a[]"] = newValue;
					post_data["groupId"] = $obj.attr("data-group");
				}				
				// Update classifications (multi select)
				else if(identifier == "multi"){
					post_data["a"] = newValue;
					post_data["groupId"] = $obj.attr("data-group");
				}
				else { 
					// If the identifier is a standard one, just set the value in the post data
					post_data[identifier] = newValue;
				}
				
				// Launch the call with post_data
				var request = $.ajax({
					type: "POST",
					url: apiUrl,
					dataType: "json",
					xhrFields: {
						withCredentials: true
					},
					crossDomain: true,
					data: post_data
				});	
				
				// Request successful!
				request.done(function(response) {
					define_response_output(response);
				});
							
				// if the request fails
				request.fail(function(jqXHR, textStatus, error) {
					console.log("search failed")
					console.log(jqXHR.statusText);
					console.log(textStatus);
					console.log(error);
				});		
			}
		}		
	}
		
	// Save field value
	$(".li3Input").change(function() {
		save_values($(this));
    // Updates the process span
    define_progress($(this));
	});
	
	// Depending of the response (success/error), display notifications 
	var define_response_output = function(response){
		// Response is successful
		var i = 0;				
		// No error, this is great!
		if(response.error == "false") {
			// Display each notice in a message
			//var message = "";
			// if notices are defined, display them
			if(response.post_data.notices.length > 0){
				var type = "";
				for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
					type =  response.post_data.notices[i].type;
					//message =  response.post_data.notices[i].message;
					// Trigger a notification
					$.growl({
						title: '<strong>UPDATE</strong> ',
						message: "Information saved",
						delay: 8000,
					},{
						type: 'success'
					});
				}
			// Otherwise, display standard notice
			} else { 
				$.growl({
					title: '<strong>UPDATE</strong> ',
					message: "Information saved",
					delay: 8000,
				},{
					type: 'success'
				});
			}						
		// Oops!
		} else if (response.error == "true") {
			//Post data validation error
			for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
				type =  response.post_data.notices[i].type;
				message =  response.post_data.notices[i].message;
				// Trigger a notification
				$.growl({
					title: '<strong>ERROR</strong> ',
					message: message,
					delay: 8000,
				},{
					type: 'danger'
				});
			}
		
		}
	}
	
	// Trigger the event when removing an item from the multi input
	var set_trigger_remove_item = function(){		
		$("button[data-name]").click(function(event){			
			var identifier = $(this).attr("data-name");
			var obj = "";
			
			if(identifier == "award"){
				var selector = "[data-name='award']";
				var target = $(this).closest(selector);				
				obj = $('.li3Input[data-name="award"]').first();				
			}
			else if(identifier == "accomp"){
				obj = $('.li3Input[data-name="accomp"]').first();				
			}			
			save_values(obj);
		});
	}
	
	// Trigger the event when adding an item from the multi input
	var set_trigger_add_item = function(){
		var body = $('body');
		body.off( 'click', '.addButton');
		body.on( 'click', '.addButton', function(){
			set_action_change();			
      set_bind_remove_item();      
    });
    set_bind_remove_item();
	};
  
  var set_bind_remove_item = function( ){
    var body = $('body');
    body.off('click', '.removeButton');
    body.on('click', '.removeButton', function(){
      var $row    = $(this).parent().parent();//closest('.item');
      var $option = $row.find('[name="option[]"]');
      
      // Get the type 
      var identifier = $(this).data("name");
      var selector = "";
      switch( identifier ){
        case "award"  : selector = "#awards"          ; break;
        case "accomp" : selector = "#accomplishments" ; break;
      }
      
      // Remove element containing the option
      $row.remove();
    
      var obj = $(this).parent().prev().find("input");
      set_action_remove(obj);      
    });  
  }
		
	// When an item from the multi input is deleted, call the function to save
	var set_action_remove = function(obj){		
		save_values(obj);
	}
		
	// Call the save method when a value is changed from an input
	var set_action_change = function(){
		$(".li3Input").on("change", function() {
			save_values($(this));
		});	
	}
	
	// Called when the button for saving a picture is clicked 
	$(".savePic").click(function(event){
		// Get the croppit container id
		$containerId = $(this).parent().parent().parent();
		var type = $(this).attr("data-name");
		export_image($containerId, type);
	});
	
	// Return the image from the specified cropit container
	var export_image = function($id, type) {
		var img = $id.cropit('export', {
			type: 'image/jpeg',
			quality: .9,
			originalSize: true
		});
		
		var imgData = img;
		var postFieldName = "picFile";
		 
		var binary = atob(imgData.split(',')[1]); // Create 8-bit unsigned array
		var array  = [];
		 
		for (var i = 0; i < binary.length; i++)
		{
			array.push(binary.charCodeAt(i));
		}
		 
		// Return our Blob object
		var blob = new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
		var formData = new FormData();
		formData.append(postFieldName, blob);
		
		
		var url_value = "";
		if(type == "pic"){
			url_value = baseUrl + 'api/ajax/auth/user/profile/metaInfo/push/pic?token=1234';
		} else if (type=="logo"){
			url_value = baseUrl + 'api/ajax/auth/user/profile/metaInfo/push/logo?token=1234';
		}
		
		$.ajax({
			url: url_value,
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			type: 'POST',
			success: function(data){
				$.growl({
					title: '<strong>UPDATE</strong> ',
					message: data.post_data.notices[0].message,
					delay: 8000,
				},{
					type: 'success'
				});
			}
		});
	}
	
	// When the button "add new slide" is clicked, open the modal and reset all values
	$("#addSlide").click(function(event){
		$("#editCurrentSlide").css("display", "none");
		$("#saveNewSlide").css("display", "inline");
		$("#saveNewSlide").removeAttr("disabled");
		// Set the modal title
		$("#createSlide .modal-title").html("Add new slide");
		// Title
		var titlePointer = $('#title');
		var title = titlePointer.val("");
		// Image		
		var picFilePointer = $('#newSlidePicFile');
		var picFile = picFilePointer .val("");
		// Description
		var descrPointer = $('#descr');
		var descr = descrPointer.val("");		
		// Video
		var videoIdPointer = $('#videoId');
		var videoId = videoIdPointer.val("");
		// Status
		var isActive = $('#active')[0].checked;
		// Order
		var sortOrderPointer = $('#sortOrder');
		var sortOrder = sortOrderPointer.val("");
		// Slide Id
		$('#slideId').val("");
		
	});	
	
		
	// Make the API call to add a new slide	
	$( "#saveNewSlide" ).click(function( event ) {
		event.preventDefault();	
		
		$(this).attr("disabled", "disabled");
		
		// Pic size
		var picSize = "650";		

		// Form data with additional values		
		var formData = new FormData($("#slideForm")[0]);
		formData.append("token", token);
		formData.append("picSize", picSize);
		
		var request = $.ajax({
			type: "POST",
			cache: false,
			contentType: false,
			processData: false,
			url: baseUrl + "api/ajax/auth/user/profile/push/addShowcaseSlide", 
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data: formData
		});	
		
		// if the request succeeds
		request.done(function(response) {
			if(response.error == "false"){
				$("#createSlide").modal('hide');
				location.reload();
			} else if(response.error == "true"){
				$( "#saveNewSlide" ).removeAttr("disabled");
			}				
			define_response_output(response);			
		});
					
		// if the request fails
		request.fail(function(jqXHR, textStatus, error) {
			console.log("search failed")
			console.log(jqXHR.statusText);
			console.log(textStatus);
			console.log(error);
		});
	});
	
	// Make the API call to save modifications on chosen slide
	$("#editCurrentSlide").click(function( event ){
		event.preventDefault();	
		$(this).attr("disabled", "disabled");
		
		var slideId = $('#slideId').val();
		// Form data with additional values		
		var token = 1234;
		var formData = new FormData($("#slideForm")[0]);
		formData.append("token", token);
		formData.append("slideId", slideId);
		
		var request = $.ajax({
			type: "POST",
			cache: false,
			contentType: false,
			processData: false,
			url: baseUrl + "api/ajax/auth/user/profile/push/editShowcaseSlide", 
			dataType: "json",
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data: formData		
		});	
		
		// if the request succeeds
		request.done(function(response) {
			if(response.error == "false"){
				$("#createSlide").modal('hide');
				location.reload();				
			} else if(response.error == "true"){
				$("#editCurrentSlide").removeAttr("disabled");
			}
			
			define_response_output(response);			
		});
					
		// if the request fails
		request.fail(function(jqXHR, textStatus, error) {
			console.log("search failed")
			console.log(jqXHR.statusText);
			console.log(textStatus);
			console.log(error);
		});	
	});
	/*
	var set_disabled_picture_uploader_components = function(){
		$("button[data-name='pic']").attr("disabled", "disabled");
		$("input[data-name='pic']").attr("disabled", "disabled");
		$("div[data-name='pic']").css("cursor", "default");
		
		$("button[data-name='logo']").attr("disabled", "disabled");
		$("input[data-name='logo']").attr("disabled", "disabled");
		$("div[data-name='logo']").css("cursor", "default");
				
		$("#chose-profile-pic").on("change", function() {
			$("button[data-name='pic']").removeAttr("disabled");
			$("input[data-name='pic']").removeAttr("disabled");
			$("div[data-name='pic']").css("cursor", "move");
		});
		
		$("#chose-personal-pic").on("change", function() {
			$("button[data-name='logo']").removeAttr("disabled");
			$("input[data-name='logo']").removeAttr("disabled");
			$("div[data-name='logo']").css("cursor", "move");
		});
	}
	*/
  var set_wizard = function(){
  
    /* Initialize Clickable Wizard */
    var clickableWizard = $('#clickable-wizard');
    
    clickableWizard.formwizard({disableUIStyles: true, inDuration: 0, outDuration: 0 });    

    $('.clickable-steps a').on('click', function(){
        var gotostep = $(this).data('gotostep');
        clickableWizard.formwizard('show', gotostep);

        // Hide submit button (Next) if on the last step
        var state = clickableWizard.formwizard("state");
        if(state.isLastStep){
          $('#next4').attr("disabled", "disabled");
        } else{
          $('#next4').removeAttr("disabled");
        }
          
        
    });
    
    $('#next4').on('click', function(){         
        // Hide submit button (Next) if on the last step
        var state = clickableWizard.formwizard("state");
        if(state.isLastStep){
          $('#next4').attr("disabled", "disabled");
        } else{
          $('#next4').removeAttr("disabled");
        }          
    });
  }
  
  var define_all_progress = function(){
    define_progress_location();
    define_progress_education();
    define_progress_classification();
    define_progress_social();
    define_progress_custom();
    define_progress_general();
    define_progress_carousel();
  }
  
  var define_progress = function( $obj ){    
    var id = $obj.attr("id");
    var name = $obj.attr("name");
    switch(id){
      case "city"             : define_progress_location() ; break;
      case "region"           : define_progress_location() ; break;
      case "degree"           : define_progress_education() ; break;
      case "institution"      : define_progress_education() ; break;
      case "id0"              : define_progress_classification(); break;
      case "id1"              : define_progress_classification(); break;
      case "id2"              : define_progress_classification(); break;
      case "id3"              : define_progress_classification(); break;
      case "id4"              : define_progress_classification(); break;
      case "facebook"         : define_progress_social() ; break;
      case "twitter"          : define_progress_social() ; break;
      case "instagram"        : define_progress_social() ; break;
      case "spotify"          : define_progress_social() ; break;
      case "youtube"          : define_progress_social() ; break;
      case "googleplus"       : define_progress_social() ; break;
      case "linkedin"         : define_progress_social() ; break;
      case "pinterest"        : define_progress_social() ; break;
      case "vimeo"            : define_progress_social() ; break;
      case "custom-url"       : define_progress_custom() ; break;
      case "public-name"      : define_progress_general(); break;
      case "main-title"       : define_progress_general(); break;
      case "tagline"          : define_progress_general(); break;
      case "bio"              : define_progress_general(); break;
      case "personal-website" : define_progress_general(); break;
    }
    
    switch(name){
      case "Award" : define_progress_education() ; break;
      case "Accomplishment" : define_progress_education() ; break;
    }        
  }
  
  var define_progress_carousel = function(){
    // Get the number of slides
    numberOfSlides = $('#coverflowData').find('.slide').length;;   
    $('#progress-carousel').removeAttr( 'style' );
    if(numberOfSlides > 0){ 
      $('#progress-carousel').css("color" , accomplishedColor );
    } else {
      $('#progress-carousel').css("color" , unAccomplishedColor );
    }
  
  }
  
  var define_progress_general = function(){
    $('#progress-general').removeAttr( 'style' );
    if( f_public_name.val() != "" && f_main_title.val() != "" && f_tagline.val() != "" && f_bio.val() != "" && f_personal_website.val() != ""  ){
      $('#progress-general').css("color" , accomplishedColor );
    } else {
      $('#progress-general').css("color" , unAccomplishedColor );
    }     
  }
  
  var define_progress_custom = function(){
    $('#progress-custom').removeAttr( 'style' );
    if( f_custom_url.val() != "" ) {
      $('#progress-custom').css("color" , accomplishedColor );
    } else {
      $('#progress-custom').css("color" , unAccomplishedColor );
    }    
  }
  
  var define_progress_social = function(){
    $('#progress-social').removeAttr( 'style' );
    if( f_facebook.val() != "" && f_twitter.val() != "" 
      && f_instagram.val() != "" && f_spotify.val() != "" 
        && f_youtube.val() != "" && f_googleplus.val() != "" 
          && f_linkedin.val() != "" && f_pinterest.val() != "" 
            && f_vimeo.val() != "" ){
      $('#progress-social').css("color" , accomplishedColor );
    } else {
      $('#progress-social').css("color" , unAccomplishedColor );
    }    
  }
  
  var define_progress_classification = function(){
    $('#progress-classification').removeAttr( 'style' );
    if( f_department.val() != "" && f_specialties.val() != "" && f_focuses.val() != "" && f_activities.val() != "" && f_certifications.val() != "" ) {
      $('#progress-classification').css("color" , accomplishedColor );
    } else {
      $('#progress-classification').css("color" , unAccomplishedColor );
    }    
  }
  
  var define_progress_education = function(){
    $('#progress-education').removeAttr( 'style' );
    if(f_degree.val() != "" && f_institution.val() != "" && f_award.val() != "" && f_accomplishment.val() != "" ){
      $('#progress-education ').css("color" , accomplishedColor );
    } else {
      $('#progress-education ').css("color" , unAccomplishedColor );
    }
  }
  
  var define_progress_location = function(){
    $('#progress-location').removeAttr( 'style' );
    if(f_city.val() != "" && f_region.val() != "" ){
      $('#progress-location').css("color" , accomplishedColor );
    } else {
      $('#progress-location').css("color" , unAccomplishedColor );
    }
  }
  
})();