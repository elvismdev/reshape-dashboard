var baseUrl = "http://dash.reshape.net/";
var token   = "1234";

$(document).ready(function(){
  var sessionsObj = new sessions_obj();  
  
  // Make an API call to display events in the calendar
  sessionsObj.display_sessions();

  // Define the action when the "save new session" button is clicked
  sessionsObj.set_save_new_session_click();    
  set_edit_session_submit_action();  
  sessionsObj.set_delete_session_action();  
  // Define the select for chosing a service
  set_select_service();  
  new_session_empty_form();
  
  // Register nodes from the edit modal form to the calendar
  window.Li3calendarModal.RegisterNodes({
    ServiceStepNode:"ptsid_432_27",
    ServiceNameNode:"ptsid_372_19",
    ServiceImageNode:"ptsid_434_25",
    SessionTimeNode:"ptsid_436_30",
    SessionNumberOfClients:"ptsid_506_31",
    ClientsListStepNode:"ptsid_442_27",
    ClientsListContainerNode:"ptsid_455_28",
    ClientsListSessionTimeNode:"ptsid_444_30",
    ClientDetailStepNode:"ptsid_457_29",
    ClientDetailSessionTimeNode:"ptsid_459_31",
    ClientDetailImageNode:"ptsid_466_29",
    ClientFullProfileLinkNode:"ptsid_468_32",
    ClientFullNameNode:"clientFullName",
    ClientLocalisationNode:"ptsid_474_33",
    ClientPhoneNumberNode:"ptsid_475_33",
    ClientEmailAddressNode:"ptsid_476_34",
    ClientRemoveFromSessionNode:"remove_from_session",
    SwitchSessionStepNode:"ptsid_483_28",
    SelectOtherSessionNode:"ptsid_490_31",
    EditServiceTitleNode:"ptsid_385_20",
    EditServiceNode:"ptsid_380_15",
    EditServiceImageNode:"ptsid_395_25",
    EditServiceDateNode:"editSessionDate",
    EditServiceTimeNode:"editSessionTime",
    SessionHiddenFieldNode:"ptsid_378_21",
    ClientDetailHiddenFieldNode:"ptsid_481_25",
    ClientRemoveFromSessionHiddenFieldNode:"ptsid_502_28",
    ClientSwitchSessionHiddenFieldNode:"ptsid_491_27"
  });
  
});

var sessions_obj = function(){    
  
  this.display_sessions = function( ){

    var post_data = { };			
    post_data["token"] = token;
    post_data["showPrivate"] = true;
    post_data["hostId"] = $("#calendar").data("id");
    
    
    // Launch the call with post_data
    var request = $.ajax({
      type: "POST",
      url: baseUrl + "api/ajax/entities/services/sessions/upcoming",
      dataType: "json",
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      data: post_data
    });	
    
    // Request successful!
    request.done(function(response) {
      set_sessions(response);      
    });
          
    // if the request fails
    request.fail(function(jqXHR, textStatus, error) {
      console.log("search failed")
      console.log(jqXHR.statusText);
      console.log(textStatus);
      console.log(error);
    });
  
  }
   
  this.set_save_new_session_click = function(){
    $("#saveNewSession").on("click", function( event ){
      event.preventDefault();
      if( !$("#newSessionService").data("ddslick").selectedData){
        put_growl_error_message("You must select a service");
        $('#newSessionService').focus();
      } else {
        var service         = $("#newSessionService").data("ddslick").selectedData.value;
        var sessionDate     = $('#newSessionDate').val();
        var sessionTime     = $('#newSessionTime').val();
        var sessionNotes    = $('#newSessionNotes').val();
        var sessionPrivate  = $('#newSessionPrivate').val() == "0" ? "false" : "true";
        var sessionCapacity = $('#newSessionCapacity').val();
        var sessionPrice    = $('#newSessionPrice').val();
        var sessionDuration = $('#newSessionDuration').val();
        // Get the full date
        var fullDate = get_date_time_to_full_date(sessionDate, sessionTime);          
        
        if(sessionDate == "") { 
          // Trigger a notification
          put_growl_error_message("You must select a date");
          $('#newSessionDate').focus();             
        } else {    
          var post_data = { };			
          post_data["token"]       = token;    
          post_data["serviceId"]   = service;
          post_data["startsOn"]    = fullDate;
          post_data["isPrivate"]   = sessionPrivate;    
          post_data["note"]        = sessionNotes;
          post_data["memberLimit"] = sessionCapacity;
          post_data["price"]       = sessionPrice;
          post_data["minutes"]     = sessionDuration;
          
          // Execute the call with post_data
          var request = $.ajax({
            type: "POST",
            url: baseUrl + "api/ajax/entities/services/sessions/add",
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });
          
          // Request successful!
          request.done(function(response) {
            define_response_output(response);
            var sessionsObj = new sessions_obj();  
            sessionsObj.display_sessions();
            $('#create-new-session').modal("hide");
          });
                
          // if the request fails
          request.fail(function(jqXHR, textStatus, error) {
            console.log("search failed")
            console.log(jqXHR.statusText);
            console.log(textStatus);
            console.log(error);
          });	
        }     
      }      
    });
  }
    
  put_growl_error_message = function( aMessage ){
    $.growl({
      title: '<strong>ERROR</strong> ',
      message: aMessage,
      delay: 8000,      
    },{
      type: 'danger',
      z_index: 1051
    });
  }
  
  put_growl_success_message = function( aMessage ){
    $.growl({
      title: '<strong>UPDATE</strong> ',
      message: aMessage,
      delay: 8000,      
    },{
      type: 'success',
      z_index: 1051
    });
  }
    
  set_select_service = function(){
    
    var post_data = { };			
    post_data["token"]       = token;    
    post_data["id"] = $("#newSessionService").data("id");
    
    // Execute the call with post_data
    var request = $.ajax({
      type: "POST",
      url: baseUrl + "api/ajax/profile/services",
      dataType: "json",
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      data: post_data
    });
    
    // Request successful!
    request.done(function( response ) {
      var ddData = [ ];           
      
      for( var i = 0 ; i < response.services.length ; i++ ){
        var data = { };
        data["value"] = response.services[i].serviceId;
        data["text"] = response.services[i].name;
        data["description"] = response.services[i].typeLabel;
        
        var iconSrc = "";
        switch(response.services[i].typeLabel){
          case "1-on-1"     : iconSrc = "/static/img/filtericons/onetoone_min.png"  ; break;
          case "Broadcast"  : iconSrc = "/static/img/filtericons/broadcast_min.png" ; break;
          case "1-on-group" : iconSrc = "/static/img/filtericons/group_min.png"     ; break;
          case "1-on-class" : iconSrc = "/static/img/filtericons/broadcast_min.png" ; break;
          case "Other"      : iconSrc = "/static/img/filtericons/person_min.png"    ; break;   
        }       
        data["imageSrc"] = iconSrc;

        ddData.push(data);
      }
      // Sort the array by text ascending
      ddData.sort(function(a, b){
        var a1= a.text.toUpperCase(), b1= b.text.toUpperCase();
        if(a1 == b1) return 0;
        return a1> b1? 1: -1;
      });
      
      $("#newSessionService").ddslick({
        data: ddData,
        width: 295,
        imagePosition: "left",
        selectText: "Select one service",
        onSelected: function (data) {
          define_create_session_values(data.selectedData.value);
        }
      });
          
    // if the request fails
    request.fail(function(jqXHR, textStatus, error) {
      console.log("search failed")
      console.log(jqXHR.statusText);
      console.log(textStatus);
      console.log(error);
    });   
});
  
  }
  
  new_session_empty_form = function() {
    
    $("#addNewSession").on( "click", function(){      
      // Delete the select and call it again
      $("#newSessionService").ddslick('destroy');
      set_select_service();
      $("#newSessionDate").val("");
      $('#newSessionTime').timepicker('setTime', new Date());
      $("#newSessionCapacity_group").css("display", "none");
      $("#newSessionDuration").val("");
      $("#newSessionCapacity").val("");
      $("#newSessionPrice").val("");
      $("#newSessionNotes").val("");
      $("#newSessionPrivate").val(0);
    
    });
  }
    
  define_create_session_values = function(serviceId) {     
    // If a service is chosen, make an api call to retrieve the capacity
    if(serviceId != 0){
    
      var post_data = { };			
      post_data["token"] = token;    
      post_data["id"] = serviceId;
      
      // Execute the call with post_data
      var request = $.ajax({
        type: "POST",
        url: baseUrl + "api/ajax/entity/service/pull/info",
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      // Request successful!
      request.done(function(response) {
        var capacity = parseInt(response.service.memberLimit);
        var duration = parseInt(response.service.minutes);
        var type = response.service.typeLabel;
        var price    = parseInt(response.service.price);
        //labelCapacity.html("Session capacity");
        
        if( capacity > 1)
          $("#newSessionCapacity_group").css("display", "block");
        else
          $("#newSessionCapacity_group").css("display", "none");
        
        $("#serviceType span").html( type );
        $("#newSessionCapacity").val( capacity );
        $("#newSessionPrice").val( price );
        $("#newSessionDuration").val( duration );
        define_service_capacity( capacity );
        
      });
            
      // if the request fails
      request.fail(function(jqXHR, textStatus, error) {
        console.log("search failed")
        console.log(jqXHR.statusText);
        console.log(textStatus);
        console.log(error);
      });
      
    }    
  }

  define_service_capacity = function( maxCapacity ){    
    $("#newSessionCapacity").unbind( "change" );
    $("#newSessionCapacity").on("change", function( event ){            
      if( $(this).val() != "" ) {
        var capacity = parseInt($(this).val());
        if( capacity > maxCapacity ) {
          put_growl_error_message("The session capacity cannot be superior than the maximum capacity (" + maxCapacity + ").");        
          $('#newSessionCapacity').val(maxCapacity);
          $('#newSessionCapacity').focus(); //does not focus, why?
        } else if( isNaN(capacity) ) {
          put_growl_error_message("The session capacity must be an integer.");        
          $('#newSessionCapacity').val(maxCapacity);
          $('#newSessionCapacity').focus(); //does not focus, why?
        }      
      }     
    });
  }  
  
  get_date_time_to_full_date = function( theDate, theTime ){
    var dateMoment = moment(theDate, "MM-DD-YYYY");
    var dateMomentDate = dateMoment.format("YYYY-MM-DD");
    // Get the time and format it
    var timeMoment = moment(theTime, "hh:mm A").format();
    var timeMomentTime = timeMoment.substring(10, timeMoment.length);
    
    // Return the final date/time string    
    return dateMomentDate + timeMomentTime;    
  }
  
  // Depending of the response (success/error), display notifications 
	define_response_output = function(response){
		// Response is successful
		var i = 0;				
		// No error, this is great!
		if(response.error == "false") {
			// Display each notice in a message
			//var message = "";
			// if notices are defined, display them
			if(response.post_data.notices.length > 0){
				var type = "";
				for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
					type =  response.post_data.notices[i].type;
					//message =  response.post_data.notices[i].message;
					// Trigger a notification
          put_growl_success_message("Information saved");
				}
			// Otherwise, display standard notice
			} else { 
				put_growl_success_message("Information saved");
			}						
		// Oops!
		} else if (response.error == "true") {
			//Post data validation error
			for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
				type =  response.post_data.notices[i].type;
				message =  response.post_data.notices[i].message;
				// Trigger a notification
        put_growl_error_message(message);				
			}		
		}
	}
   
  // Set sessions in the calendar
  set_sessions = function( eventsJson ){
    var sessions = eventsJson.sessions;
    var events_arr = [];
    var i = 0;

    for( i = 0 ; i < sessions.length ; i++ ){
      var event =  { };
      event["title"] = sessions[i].sessionName;
      event["start"] = sessions[i].sessionDate;
      event["id"]    = sessions[i].sessionId;
      event["memberCount"] = sessions[i].memberCount;
      event["memberLimit"] = sessions[i].memberLimit;
      event["color"] = "#8cc34b";
       
      
      var sessionType = sessions[i].sessionType;
      var iconClass = "";
      switch(sessionType){
        case "1-on-1"     : iconClass = "service_filter_one"       ; break;
        case "Broadcast"  : iconClass = "service_filter_broadcast" ; break;
        case "1-on-group" : iconClass = "service_filter_group"     ; break;
        case "1-on-class" : iconClass = "service_filter_class"     ; break;
        case "Other"      : iconClass = "service_filter_person"    ; break;      
      }
      
      event["iconClass"] = iconClass;
      
      
      events_arr.push(event);  
    }    
    
    $('#calendar').fullCalendar(
    {
      firstDay: 1, 
      aspectRatio: 1.95, 
      editable: false, 
      droppable: true,
      eventClick: function( event, jsEvent, view ) {
        
        window.Li3calendarModal.SetActiveEvent(event);        
      },
      eventRender: function (event, element) {
        element.attr('id', 'event_' + event.id);
        element.css("cursor", "pointer");
        element.prepend("<div>").css("padding", "5px");        
        element.find(".fc-event-time").css("line-height", "25px");
        element.find(".fc-event-time").css("font-weight","normal");
        element.find(".fc-event-time").html( event["title"] );
        element.find(".fc-event-time").before("<a class=" + event["iconClass"] + " style='background-size: cover; display: block; height: 25px; width: 25px; float: left;'></a>");
        element.find(".fc-event-title").before("<br/>");
        
        var left_padding = "";        
        var view = $('#calendar').fullCalendar('getView').name;       
        switch( view ){
          case  "month" : left_padding = "30px" ; break;
          case "agendaWeek" : left_padding = "25px" ; break;
          case "agendaDay"  : left_padding = "25px" ; break;
        }
                
        element.find(".fc-event-title").css("padding-left", left_padding).html( event['start'].format('hh:mm A') + "<span class='event-occupation' style='float: right;'>" + event.memberCount + "/" + event.memberLimit + "<i style='padding-left:5px;' class='fa fa-user fa-users'></i></span>" );
      },
      defaultView: "month", header: { left: "", center: "prev,title,next", right: "month,agendaWeek,agendaDay,'" },       
    });  
    // Remove events
    $('#calendar').fullCalendar( 'removeEvents' );
    // Add them again
    $('#calendar').fullCalendar( 'addEventSource', events_arr );
  }
  
  set_edit_session_submit_action = function(){
    $('#editSessionSubmit').click( function( event ){
      event.preventDefault();
      
      var sessionId       = $('#editSessionId').val();
      var sessionDate     = $('#editSessionDate').val();
      var sessionTime     = $('#editSessionTime').val();
      var sessionDuration = $('#editSessionDuration').val();
      var sessionCapacity = $('#editSessionCapacity').val();
      var sessionNotes    = $('#editSessionNotes').val();
      var sessionPrivate  = $('#editSessionPrivate').val();
      var sessionPrice    = $('#editSessionPrice').val();
      var fullDate = get_date_time_to_full_date(sessionDate, sessionTime);      
      var isSessionPrivate = sessionPrivate == "1";
      
      var post_data = { };
      post_data["token"]       = token;      
      post_data["id"]          = sessionId;           
      post_data["startsOn"]    = fullDate;
      post_data["isPrivate"]   = isSessionPrivate;
      post_data["note"]        = sessionNotes;
      post_data["minutes"]     = sessionDuration;
      post_data["memberLimit"] = sessionCapacity;
      post_data["price"]       = sessionPrice;
      
            
      // Execute the call with post_data
      var request = $.ajax({
        type: "POST",
        url: baseUrl + "api/ajax/entity/rtcSession/push/info",
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      // Request successful!
      request.done(function(response) {
          define_response_output(response);
          var sessionsObj = new sessions_obj();
          
          if(response.error == "false") {
          // update session date and time in the modal view
          var updatedSessionDate = response.updatedSession.sessionDate;
          var updatedSessionDuration = response.updatedSession.duration;                
          
          // New session date start
          var mo_sessionDate = moment(updatedSessionDate).format("MM/DD/YYYY");
          // New Session time start
          var mo_sessionTimeStart = moment(updatedSessionDate).format("hh:mm A");       
          // New Session time end
          var mo_sessionTimeEnd = moment(updatedSessionDate).add(updatedSessionDuration, 'm').format("hh:mm A");               

          // Set values in the DOM
          $("#ptsid_436_30").html("<i class='fa fa-calendar fa-fw'></i> " + mo_sessionDate + " <i class='fa fa-clock-o'></i> " + mo_sessionTimeStart + "-" + mo_sessionTimeEnd);
          $("#ptsid_444_30").html("<i class='fa fa-calendar fa-fw'></i> " + mo_sessionDate + " <i class='fa fa-clock-o'></i> " + mo_sessionTimeStart + "-" + mo_sessionTimeEnd);
          $("#ptsid_459_31").html("<i class='fa fa-calendar fa-fw'></i> " + mo_sessionDate + " <i class='fa fa-clock-o'></i> " + mo_sessionTimeStart + "-" + mo_sessionTimeEnd);       
          // Update the calendar view
          sessionsObj.display_sessions();        
        }
        
      });
            
      // if the request fails
      request.fail(function(jqXHR, textStatus, error) {
        console.log("search failed")
        console.log(jqXHR.statusText);
        console.log(textStatus);
        console.log(error);
      });      
    });
  }
  
  define_options_switch_session = function() {
    //var sessionId = $('#editSessionId').val();
    var serviceId = ""; 
    
    var token = 1234;
    var post_data = {};
    post_data["token"]     = token;
    post_data["serviceId"] = serviceId;

    var request = $.ajax({
      type: "POST",
      url: baseUrl + "api/ajax/entities/services/sessions/upcoming", 
      dataType: "json",
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      data: post_data
    });

    // Request successful!
    request.done(function (response) {
      if (response.error == "false") {
        var otherSessionId   = "";
        var otherSessionDate = "";
        var otherSessionTime = "";
        
      }
      else if (response.error == "true") {
        swal("Error", "There was an unexpected error.", "error");
      }
    });

    // if the request fails
    request.fail(function (jqXHR, textStatus, error) {
      console.log("search failed")
      console.log(jqXHR.statusText);
      console.log(textStatus);
      console.log(error);
    });
      
  
  }
    
  this.set_delete_session_action = function(){
    $("#deleteSession").unbind( "click");
    $("#deleteSession").on("click", function( event ){
      event.preventDefault();
      var sessionId = $('#editSessionId').val();
      
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this session.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d22130",
        confirmButtonText: "Delete",
        closeOnConfirm: false
      },
      function () {
        var token = 1234;
        var post_data = {};
        post_data["token"] = token;
        post_data["delete"] = "true";
        post_data["id"] = sessionId;

        var request = $.ajax({
          type: "POST",
          url: baseUrl + "api/ajax/entity/rtcSession/push/info",
          dataType: "json",
          xhrFields: {
            withCredentials: true
          },
          crossDomain: true,
          data: post_data
        });

        // Request successful!
        request.done(function (response) {
          if (response.error == "false") {
            $('#calendar').fullCalendar( 'removeEvents' , sessionId );
            $('#ptsid_380_15').modal('hide');
            $('#ptsid_367_14').modal('hide');            
            swal("Deleted", "The session has been deleted.", "success");            
          }
          else if (response.error == "true") {
            swal("Error", "There was an unexpected error.", "error");
          }
        });

        // if the request fails
        request.fail(function (jqXHR, textStatus, error) {
          console.log("search failed")
          console.log(jqXHR.statusText);
          console.log(textStatus);
          console.log(error);
        });

      });         
    });      
  }
  
 }