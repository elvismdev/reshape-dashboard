$( document ).ready(function() {

	var scrollHeight = $('.col-sm-8.col-lg-9.messaging_right_column').height() - $('.chatui-input').height() - $('.block-title.clearfix').height(); 

	$('.chatui-talk').slimScroll({ height: scrollHeight+'px', scrollTo: $('.li3chat_container').css('height') });	

	$('.nav.nav-pills.nav-stacked').slimScroll({ height: $('.tab-content messaging_tab').height()  });

	

	// On resize window

	$( window ).resize(function() {  

		//$('.chatui-talk').slimScroll({ height: $('.tab-content.messaging_tab').css('height'), scrollTo: $('.li3chat_container').height() });

		$('.chatui-talk').slimScroll({ scrollTo: $('.li3chat_container').height() });

		var scrollHeight = $('.col-sm-8.col-lg-9.messaging_right_column').height() - $('.chatui-input').height() - $('.block-title.clearfix').height(); 			

		$('.chatui-container .slimScrollDiv').css('height', scrollHeight+'px');

		$('.chatui-talk').css('height', scrollHeight+'px');

	});

});