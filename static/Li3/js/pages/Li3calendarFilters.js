(function( )
{
	var Li3calendarFiltersClass = function( )
	{
		// Facke accessibility scopes
		var private = { };
		var public  = this;
		
		// Initialize class variables
		private.Events  = [ ];
		private.Filters = [ ]; // { Key: "", Input: HTMLnode }
		
		// Fake constructor  
		private.Construct = function( )
		{
			
		};
		
		public.AddFilterInput = function( filterKey, nodeId ) 
		{
			var element = window.document.getElementById( nodeId );
			
			if( element )
			{    
				var filter   = { };
				filter.Key   = filterKey;
				filter.Input = element;
				
				private.Filters.push( filter );
			}          
		};

		public.RegisterEvents = function( events )
		{
			private.Events = []; // Copy array to isolate input list 
			
			for( var x = 0; x < events.length; x++ )
				private.Events.push( events[ x ] );    
			
			return events;      
		}; 
		
		public.GetEvents = function( )
		{
			var events = []; // Copy array to isolate internal list       
			
			for( var x = 0; x < private.Events.length; x++ )
				events.push( private.Events[ x ] );    

			return events;        
		};

		public.IsClientInEventData = function (eventData, clientId)
		{
		    var output = false;

            if (eventData.clients)
		    {
		        for (var y = 0; y < eventData.clients.length; y++)
		        {
		            var client = eventData.clients[y];

		            if (client.clientId == clientId)
		            {
		                output = true;
		                break;
		            }
		        }
            }

            return output;
		};

		public.IsClientInEvent = function( eventId, clientId )
		{
		    var  events = this.GetEvents();
		    var  output = false; 

		    for( var x = 0; x < events; x++ )
		    {
		        var eventData = events[x].data;

		        if ( eventData.eventId == eventId )
		        {
		            output = public.IsClientInEventData(eventData, clientId);
		            break;
		        }
		    }

		    return output;
		};
        		
		public.EventPassesFilter = function( event )
		{
			var output = false;
			
			if( event.filters )
			{
				output = true;
				
				for( var x = 0; x < private.Filters.length; x++ )
				{
					var filter = private.Filters[ x ];
					var key    = filter.Key;
					
					if( event.filters[ key ] ) 
					{
						var filterValue = event.filters[ key ]; 
						var inputValue  = private.GetFieldValue( filter.Input );  

						inputValue  = inputValue.trim( );
						filterValue = filterValue.trim( );  
						
						if( inputValue && inputValue.length != 0 && inputValue != "" )
						{
							if( filterValue.toString( ) != inputValue.toString( ) ) // Validate only if input value is set  
								output = false;     
						}
					}

					if( !output )
						break;   // Break loop on first false   
				} 
			} 
			
			return output;         
		};
		
		public.EventFailsFilter = function( event )
		{
			return !public.EventPassesFilter( event );    
		};
		
		public.GetFilterActiveValue = function( filterKey )
		{
			var output = null;
			
			for( var x = 0; x < private.Filters.length; x++ )
			{
				var filter = private.Filters[ x ];
				
				if( filterKey == filter.Key )
				{
					output = private.GetFieldValue( filter.Input );
					break;
				} 
			}  
			
			return output;
		};
		
		public.Test = function( )
		{
			alert( "Li3calendarFilters is defined" );    
		};
		
		private.GetFieldValue = function( input )
		{
			var tagName    = input.tagName.toLowerCase( ); 
			var inputValue = null;  
			
			if( tagName == "select" )
			{
				var index  = input.selectedIndex;
				inputValue = input.options[index].value
			}
			else
			{
				var inputType  = input.type.toLowerCase( );
				
						
				if( inputType == "checkbox" || inputType == "radio" )
				{
					if( input.checked )
						inputValue = input.Value;     
				}
				else
				{
					inputValue = input.Value;
				}
			}
			
			return inputValue;
		};
		
		public.ProcessFilterSetEvent = function( calendar, input )
		{
			var removeDelegate = function( event )
			{ 
				return public.EventFailsFilter( event ); 
			};
			
			var events = public.GetEvents( );
			
			calendar.fullCalendar( 'removeEvents' ); 
			calendar.fullCalendar( 'addEventSource', events ); 
			calendar.fullCalendar( 'removeEvents', function( ev ){ return removeDelegate( ev ); } );    
		};
		
		// Must be called here 
		private.Construct( );
	};
	window.Li3calendarFilters = new Li3calendarFiltersClass( ); 
})( );  