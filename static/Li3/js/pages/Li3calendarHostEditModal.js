(function () {
	/************************************************************************/
	/* Li3calendarHostEditModal 										    */
	/************************************************************************/
	var Li3calendarHostEditModal = function( )
	{
		// Fake accessibility scopes
		var private = { };
		var public  = this;
    
    var token = 1234;
		
		// Initialize class variables
		private.Nodes  = { };  // { Id: "", Node: HTMLnode } 
		private.Nodes.ServiceNameNode; // serviceName		
		private.Nodes.ServiceImageNode;  // serviceImage
		private.Nodes.SessionTimeNode; // sessionTime
		private.Nodes.ClientsListSessionTimeNode; // clientsListSessionTime
		private.Nodes.ClientDetailSessionTimeNode; // clientDetailTimeNode
		private.Nodes.ClientFullNameNode;
		private.Nodes.ClientDetailImageNode;
		private.Nodes.ClientPhoneNumberNode; // clientPhoneNumber
		private.Nodes.ClientEmailAddressNode; // clientEmailAddress		
		private.Nodes.ClientNoteNode; // clientNote
		private.Nodes.ClientFullProfileLinkNode; // clientFullProfileLinkNode
		private.Nodes.ClientRemoveFromSessionNode; // clientRemoveFromSessionLinkNode
		private.Nodes.SelectOtherSessionNode; // otherEvents (data row)

		// Edit session fields nodes
		private.Nodes.EditServiceNameNode;
		private.Nodes.EditServiceTitleNode;
		private.Nodes.EditServiceDateNode;
		private.Nodes.EditServiceImageNode;		
		private.Nodes.EditServiceTimeNode;

		// Hidden fields nodes
		private.Nodes.SessionHiddenFieldNode;
		private.Nodes.ClientDetailHiddenFieldNode;
		private.Nodes.ClientRemoveFromSessionHiddenFieldNode;
		private.Nodes.ClientSwitchSessionHiddenFieldNode;
		
		// Steps Nodes
		private.Nodes.ServiceStepNode;
		private.Nodes.ClientsListStepNode;
		private.Nodes.ClientDetailStepNode;
		private.Nodes.SwitchSessionStepNode;
		private.Nodes.ClientConfirmRemoveFromSessionNode;

		
		private.Listeners = { };
		private.Listeners.ClientSwitchLink;
				
		// Fake constructor  
		private.Construct = function( )
		{
			
		};
		
		// RegisterNodes( nodeData ) : 
		public.RegisterNodes = function( nodeData )
		{		    
			
			for (var key in nodeData ) //sampleData
			{
				
				var nodeId = nodeData[key]; // sampleData[ key ]			    
				var element = window.document.getElementById( nodeId );
				
				if( element )
					private.Nodes[ key ] = element; 
			}
		};
    
    define_session_edit_capacity = function( maxCapacity, actualCapacity ){
    $("#editSessionCapacity").unbind( "change" );
    $("#editSessionCapacity").on("change", function( event ){            
      if( $(this).val() != "" ) {
        var capacity = parseInt($(this).val());
        if( capacity > maxCapacity ) {
          put_growl_error_message("The session capacity cannot be superior than the maximum capacity (" + maxCapacity + ").");        
          $('#editSessionCapacity').val(actualCapacity);
          // $('#newSessionCapacity').focus(); does not focus, why?
        } else if( isNaN(capacity) ) {
          put_growl_error_message("The session capacity must be an integer.");        
          $('#editSessionCapacity').val(actualCapacity);
          // $('#newSessionCapacity').focus(); does not focus, why?
        }      
      }     
    });    
  }
    
    
		
		public.SetActiveEvent = function( eventData )
		{
      // Make API call to retrieve all session info
      var post_data = { };			
      post_data["token"] = token;
      
      var pointer = this;
      
      var sessionId = eventData.id;      
      post_data["id"] = eventData.id;
      post_data["picSize"] = 600;
      post_data["picSquare"] = false;
      
     
      // Launch the call with post_data to retrieve service session info
      var request = $.ajax({
        type: "POST",
        url: baseUrl + "api/ajax/entity/rtcSession/pull/info",        
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      // Request successful!
      request.done( function( response ) {
        // Retrieve the data
        var sessionDateFull  = response.session.sessionDate;
        var sessionName      = response.session.sessionName;
        var serviceImage     = response.session.service.picture_url;
        var sessionDuration  = parseInt(response.session.duration);
        var serviceCapacity  = parseInt(response.session.service.memberLimit);
        var sessionPrice     = parseInt(response.session.price);
        var sessionCapacity  = parseInt(response.session.memberLimit);
        var sessionNote      = response.session.note;   
        var sessionIsPrivate = response.session.isPrivate;
        var sessionId        = response.session.sessionId;
        var serviceId        = response.session.service.serviceId;
        var serviceType      = response.session.service.typeLabel;
        
        // Make a copy of the time to add the duration (in minutes)
        var mo_sessionTime = moment(sessionDateFull);
        
        // Session date
        var mo_sessionDate = moment(sessionDateFull).format("MM/DD/YYYY");
        // Session time start
        var mo_sessionTimeStart = moment(sessionDateFull).format("hh:mm A");
        // Session time end
        var mo_sessionTimeEnd = mo_sessionTime.add(sessionDuration, 'm').format("hh:mm A");
        
        define_session_edit_capacity( serviceCapacity, sessionCapacity );
        
        // Define the icon depending on the service type
        var iconClass = "";
        switch(serviceType){
          case "1-on-1"     : iconClass = "service_filter_one"       ; break;
          case "Broadcast"  : iconClass = "service_filter_broadcast" ; break;
          case "1-on-group" : iconClass = "service_filter_group"     ; break;
          case "1-on-class" : iconClass = "service_filter_class"     ; break;
          case "Other"      : iconClass = "service_filter_person"    ; break;   
        }
        
        
        // Set the session id
        $('#editSessionId').val(sessionId);      
        
        // Service Name
        if ( private.Nodes.ServiceNameNode )
          private.Nodes.ServiceNameNode.innerHTML = "<span>" + sessionName + " #" + sessionId + "</span><br/>" + "<a class='" +iconClass+ "' style='width: 42px; height: 42px; display: inline-block; margin-right: 5px; margin-bottom: -12px; background-size: cover;'></a>" +serviceType;
        // Service Image
        if (private.Nodes.ServiceImageNode)
          private.Nodes.ServiceImageNode.style.backgroundImage = "url('" + serviceImage + "')";
        // Session Time
        if (private.Nodes.SessionTimeNode)
        {
          private.Nodes.SessionTimeNode.innerHTML = "<i class='fa fa-calendar fa-fw'></i> " + mo_sessionDate + " <i class='fa fa-clock-o'></i> " + mo_sessionTimeStart + "-" + mo_sessionTimeEnd;
          private.Nodes.ClientsListSessionTimeNode.innerHTML = "<i class='fa fa-calendar fa-fw'></i> " + mo_sessionDate + " <i class='fa fa-clock-o'></i> " + mo_sessionTimeStart + "-" + mo_sessionTimeEnd;
          private.Nodes.ClientDetailSessionTimeNode.innerHTML = "<i class='fa fa-calendar fa-fw'></i> " + mo_sessionDate + " <i class='fa fa-clock-o'></i> " + mo_sessionTimeStart + "-" + mo_sessionTimeEnd;
        }
        
        // Set the data for update session modal form
        private.Nodes.EditServiceTitleNode.innerHTML = "Edit " + sessionName + " #" + sessionId + "<br/>" + "<a class='" +iconClass+ "' style='width: 42px; height: 42px; display: inline-block; margin-right: 5px; margin-bottom: -12px; background-size: cover;'></a>" +serviceType;
        
        //private.Nodes.EditServiceDateNode.value = mo_sessionDate;
        //private.Nodes.EditServiceImageNode.style.backgroundImage = "url('" + serviceImage + "')";
        private.Nodes.EditServiceTimeNode.value = mo_sessionTimeStart;
        
        // use #editSessionDate and call datePicker to set the date
        $('#editSessionDate').datepicker('update', mo_sessionDate);        
        $('#editSessionTime').timepicker('setTime', mo_sessionTimeStart);
        $('#editSessionCapacity').val(sessionCapacity);
        $('#editSessionDuration').val(sessionDuration);
        $('#editSessionNotes').val(sessionNote);
        $('#editSessionPrice').val(sessionPrice);
        
        if(sessionIsPrivate == "true")
          $('#editSessionPrivate').val("1");
        else 
          $('#editSessionPrivate').val("0");
          
          
        
        if (private.Nodes.ClientsListContainerNode) {
          private.Nodes.ClientsListContainerNode.innerHTML = "";
        
          var clientData = private.ExtractData( eventData, "clients" );
          
          
          // Retrieve clients
          var post_data = { };
          post_data["token"] = token;
          post_data["sessionId"] = sessionId;
          post_data["bookingStatus"] = "2";
          post_data["paymentStatus"] = "2";     
          post_data["sessionLinkStatus"] = "2";
          post_data["picSize"] = 150;
          
          var numberOfBookings = 0;
          
          // Launch the call with post_data to retrieve service session info
          var request = $.ajax({
            type: "POST",
            url: baseUrl + "api/ajax/entities/rtcBookings/list",
            dataType: "json",
            xhrFields: {
              withCredentials: true
            },
            crossDomain: true,
            data: post_data
          });
          
          request.done( function( response ) {
          
            for( var x = 0; x < response.bookings.length ; x++ ) {
            
              numberOfBookings++;
              
              var theBooking      = response.bookings[x];
              var clientInfo      = theBooking.member;
              var clientFullName  = theBooking.member.name; // Full name 
              var clientListImage = theBooking.member.picture_url; // Small image
              var clientId        = theBooking.memberId; // Client id
              var bookingId       = theBooking.bookingId; // Booking id
              var clientCity      = theBooking.member.pageData.city; // Client city
              var clientRegion    = theBooking.member.pageData.region; // Client region

              // First container node
              var clientContainerNode = document.createElement("div");
              clientContainerNode.className = "col-xs-4 col-sm-3 col-lg-4 block-section"; //lg-2

              // Second container node
              var clientSecondaryContainerNode = document.createElement("div");
              clientSecondaryContainerNode.className = "nav nav-pills nav-justified clickable-steps";                     
              clientContainerNode.appendChild(clientSecondaryContainerNode);

              // Link node              
              var clientLinkNode = document.createElement("a");
              clientLinkNode.href = "javascript:void(0)";
              clientLinkNode.setAttribute("data-gotostep", private.Nodes.ClientDetailStepNode.id);
              clientLinkNode["clientInfo"] = clientInfo;
              clientLinkNode["bookingId"] = bookingId;
              
              // Set the data for selected client
              clientLinkNode.addEventListener("click", function (ev) {
                pointer.SetClientSubPageValues( this["clientInfo"], this["bookingId"], serviceId, sessionId  );
                },
                false);

              clientSecondaryContainerNode.appendChild(clientLinkNode);

              // Image node
              var imageNode = document.createElement("img");
              imageNode.className = "img-circle";
              imageNode.alt = "image";			         
              imageNode.src = clientListImage;
              imageNode.setAttribute("data-toggle", "tooltip");
              imageNode.title = clientFullName;
              //imageNode.style.width = "75px";
              imageNode.style.width = "75%";
              
              // Name node underneath the image
              var nameNode = document.createElement("p");
              nameNode.innerHTML = clientFullName + "<br>" + clientCity + "<br/>" + clientRegion;
              //nameNode.style.width = "75px";
              nameNode.style.width = "100%";
              
              clientLinkNode.appendChild(imageNode);
              clientLinkNode.appendChild(nameNode);
              private.Nodes.ClientsListContainerNode.appendChild(clientContainerNode);              
            }	   
            
            // Go to Client list link
            if (private.Nodes.SessionNumberOfClients)
              private.Nodes.SessionNumberOfClients.innerHTML = "<i class='fa fa-user fa-fw'></i> View Clients ("+ numberOfBookings + "/" + sessionCapacity + ")";
            
          });     
        }
        // Force to affect element auto-style (classes for tooltip, ...)
        if(App)
          App.init();
          
        // From View session to clients list
        $("#ptsid_506_31").on( "click", function(){
          $("#editSessionButton").css("display", "none");
        });

        //From Individual client TO Clients list
        $("#" + private.Nodes.ClientDetailStepNode.id).find(".back_clients_list").attr("data-gotostep", private.Nodes.ClientsListStepNode.id).on("click", function () {
          $("#" + private.Nodes.ServiceStepNode.id).css("display", "none"); // View session
          $("#" + private.Nodes.ClientDetailStepNode.id).css("display", "none"); // Client detail
          $("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "none"); // Switch session
          //$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session			    
          $("#" + private.Nodes.ClientsListStepNode.id).css("display", "block"); // Clients list

          // Affect the client id in the hidden field
          private.Nodes.ClientDetailHiddenFieldNode.value = "";
          private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";
          private.Nodes.ClientSwitchSessionHiddenFieldNode.value = "";
          $("#editSessionButton").css("display", "none");
        });

        // From Client Detail TO Switch session
        $("#" + private.Nodes.ClientDetailStepNode.id).find(".switch_session").attr("data-gotostep", private.Nodes.SwitchSessionStepNode.id).on("click", function () {
          $("#" + private.Nodes.ServiceStepNode.id).css("display", "none"); // View session			    
          $("#" + private.Nodes.ClientDetailStepNode.id).css("display", "none"); // Client detail
          $("#" + private.Nodes.ClientsListStepNode.id).css("display", "none"); // Clients list
          $("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "block"); // Switch session
          //$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session
          $("#editSessionButton").css("display", "none");
          // Affect the button value
          private.Nodes.ClientRemoveFromSessionNode.value = "Remove from session";
        });
        
        // From Client Detail TO Write Message
        $("#" + private.Nodes.ClientDetailStepNode.id).find(".send_session").attr("data-gotostep", "modal-send-message").on("click", function () {          
          $("#modal-send-message").modal("show");
          $("#editSessionButton").css("display", "none");
        });
        
        // From Clients list TO View Session        
        $("#" + private.Nodes.ClientsListStepNode.id).find(".back_session_detail").attr("data-goto-step", private.Nodes.SwitchSessionStepNode.id).on("click", function () {			    			    
          $("#" + private.Nodes.ClientsListStepNode.id).css("display", "none"); // Clients list
          $("#" + private.Nodes.ClientDetailStepNode.id).css("display", "none"); // Client detail
          $("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "none"); // Switch session
          //$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session			    
          $("#" + private.Nodes.ServiceStepNode.id).css("display", "block"); // View session
          $("#editSessionButton").css("display", "inline-block");
        });

        // From Remove from session TO Client Detail
        $("#" + private.Nodes.SwitchSessionStepNode.id).find(".back_client_detail").attr("data-goto-step", private.Nodes.ClientDetailStepNode.id).on("click", function () {
          $("#" + private.Nodes.ServiceStepNode.id).css("display", "none"); // View session
          $("#" + private.Nodes.ClientsListStepNode.id).css("display", "none"); // Clients list
          $("#" + private.Nodes.SwitchSessionStepNode.id).css("display", "none"); // Switch session
          //$("#" + private.Nodes.ClientConfirmRemoveFromSessionNode.id).css("display", "none"); // Remove from session			    
          $("#" + private.Nodes.ClientDetailStepNode.id).css("display", "block"); // Client detail
          $("#editSessionButton").css("display", "none");

          // Affect the client id in the hidden field
          //private.Nodes.ClientDetailHiddenFieldNode.value = private.ExtractData(clientInfo, "clientId");
          private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";
          private.Nodes.ClientSwitchSessionHiddenFieldNode.value = "";
          // Set the remove button value (because overridden by default "step" component
          private.Nodes.ClientRemoveFromSessionNode.value = "Remove from session";
        });
                        
        // Show the modal form, first step only is visible         
        $('#ptsid_367_14').modal("show");
        $('#ptsid_432_27').css("display", "block");
        $('#ptsid_442_27').css("display", "none");
        $('#ptsid_457_29').css("display", "none");
        $('#ptsid_367_14-form').formwizard("show", 'ptsid_432_27');
        $("#editSessionButton").css("display", "inline-block");
        
      });
      
		};
    
    public.put_growl_success_message = function( aMessage ){
      $.growl({
        title: '<strong>UPDATE</strong> ',
        message: aMessage,
        delay: 8000,      
      },{
        type: 'success',
        z_index: 1051
      });
  }
    
    // Depending of the response (success/error), display notifications 
    var define_response_output = function(response){
      // Response is successful
      var i = 0;				
      // No error, this is great!
      if(response.error == "false") {
        // Display each notice in a message
        //var message = "";
        // if notices are defined, display them
        if(response.post_data.notices.length > 0){
          var type = "";
          for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
            type =  response.post_data.notices[i].type;
            message =  response.post_data.notices[i].message;
            // Trigger a notification
            $.growl({
              title: '<strong>UPDATE</strong> ',
              message: message,
              delay: 8000,
            },{
              type: 'success'
            });
          }
        // Otherwise, display standard notice
        } else { 
          $.growl({
            title: '<strong>UPDATE</strong> ',
            message: "Information saved",
            delay: 8000,
          },{
            type: 'success'
          });
        }						
      // Oops!
      } else if (response.error == "true") {
        //Post data validation error
        for( i = 0 ; i < response.post_data.notices.length ; i++ ){						
          type =  response.post_data.notices[i].type;
          message =  response.post_data.notices[i].message;
          // Trigger a notification
          $.growl({
            title: '<strong>ERROR</strong> ',
            message: message,
            delay: 8000,
          },{
            type: 'danger'
          });
        }
      
      }
    }
		
		private.ExtractData = function( data, fieldName )
		{
      return data[fieldName] ? data[fieldName] : "";
		};
		
		// Affect the user values
		public.SetClientSubPageValues = function( clientInfo, bookingId, serviceId, actualSessionId )
		{    
      clientCity = clientInfo.pageData.city;
      clientRegion = clientInfo.pageData.region;
      clientRegion = clientRegion ? ", " + clientRegion : "";
      
      $('h3[name="clientFullName"]').html(clientInfo.name + "<p style='font-size: 14px;'>" + clientCity +  clientRegion + "</p>");
      $('h4[name="clientFullName"]').html(clientInfo.name + "<p style='font-size: 14px;'>" + clientCity +  clientRegion + "</p>");
      
      // Set the image for send a message modal popup
      var imageNode = document.createElement("img");
      imageNode.className = "img-circle";
      imageNode.alt = "image";			         
      imageNode.src = clientInfo.picture_url;
      imageNode.setAttribute("data-toggle", "tooltip");
      imageNode.title = clientInfo.name;
      imageNode.style.width = "75px";
      
      $('.recipient-img-container').html(imageNode);
      
			// Hide the father step and display the current one
			private.Nodes.ClientsListStepNode.style.display = "none";
			private.Nodes.ClientDetailStepNode.style.display = "block";
			         
			var clientDetailImage = clientInfo.picture_url; // Detail image			         			
			var clientFullProfileLink = clientInfo.profile_url;
			var clientId = clientInfo.id; // Client Id			         		    
			
			// Affect the client id in the hidden field
			private.Nodes.ClientDetailHiddenFieldNode.value = clientId;
			private.Nodes.ClientRemoveFromSessionHiddenFieldNode.value = "";
			private.Nodes.ClientSwitchSessionHiddenFieldNode.value = "";
      
			// Image
			private.Nodes.ClientDetailImageNode.src = clientDetailImage;			
			// Full profile link		    
			private.Nodes.ClientFullProfileLinkNode.href = clientFullProfileLink;
			// Remove from session link
			private.Nodes.ClientRemoveFromSessionNode.value = "Remove from session";

			var RemoveFromSessionId = private.Nodes.ClientRemoveFromSessionNode.id;
			
			$("#remove_from_session").unbind();
			$(".save_note").unbind();
			$(".save_session_changes").unbind();
      $("#switch_session").unbind();
      
      // Switch a booking from the current session to another
      $("#switch_session").on("click", function( event ){
        event.preventDefault();               
        var post_data = { };
        post_data["token"] = token;
        post_data["sessionId"] = $('#ptsid_490_31').val();
        post_data["id"] = bookingId;
        
        var request = $.ajax({
          type: "POST",
          url: baseUrl + "api/ajax/entity/rtcBooking/push/session",
          dataType: "json",
          xhrFields: {
            withCredentials: true
          },
          crossDomain: true,
          data: post_data
        });
                
        request.done( function( response ) {
          define_response_output(response);
          if(response.error == "false"){
            $("#ptsid_367_14").modal("hide");
            var sessionsObj = new sessions_obj();  
            // Make an API call to display events in the calendar
            sessionsObj.display_sessions();          
          }          
        });        
      });
      
      // Remove a booking from a session
      $('#remove_from_session').on("click", function( event ){
        event.preventDefault();
        
        swal({
          title:'Are you sure to remove the client from this session?', 
          text:'Pease confirm or cancel', 
          type: 'warning', 
          showCancelButton: true, 
          }, 
          function( ){ 
          
            var post_data = { };
            post_data["token"] = token;
            post_data["id"] = bookingId;
            post_data["unset"] = true;
            
            var request = $.ajax({
              type: "POST",
              url: baseUrl + "api/ajax/entity/rtcBooking/push/session",
              dataType: "json",
              xhrFields: {
                withCredentials: true
              },
              crossDomain: true,
              data: post_data
            });
                    
            request.done( function( response ) {
              define_response_output(response);
              if(response.error == "false"){
                $("#ptsid_367_14").modal("hide");
                var sessionsObj = new sessions_obj();  
                // Make an API call to display events in the calendar
                sessionsObj.display_sessions();          
              }          
            });           
          });        
      });
            
      // Send message            
      $("#send_message_session").on("click", function(){
      
        // Reset the fields
        $("#messageSubject").val("");
        $("#messageText").val("");
        
        // Set the subject
        $("#messageSubject").val( $("#ptsid_372_19 span").html() + " message from " + $(".sidebar-user-name").html() );        
        
        $("#sendMessageBtn").unbind("click");
        $("#sendMessageBtn").on("click", function( event ){
          event.preventDefault();
          
          if( $("#messageSubject").val() == "" || $("#messageText").val() == ""){
            // Trigger a notification
            $.growl({
              title: '<strong>ERROR</strong> ',
              message: "The subject and or the message cannot not be empty",
              delay: 8000,
            },{
              type: 'danger'
            });              
          
          } else {
            // Process the message sending
            var post_data = { };
            post_data["token"]   = token;
            post_data["topic"]   = $("#messageSubject").val();
            post_data["message"] = $("#messageText").val();
            post_data["to[]"]    = clientId;
            
            var request = $.ajax({
              type: "POST",
              url: baseUrl + "api/ajax/auth/user/msg/newChat",
              dataType: "json",
              xhrFields: {
                withCredentials: true
              },
              crossDomain: true,
              data: post_data
            });
                    
            request.done( function( response ) {                           
              if(response.error == "false"){
                message =  response.post_data.notices[3].message;
                public.put_growl_success_message( message );
                $("#modal-send-message").modal("hide");                 
              }          
            });                    
          }          
        });
      
        
      
      });
			
				private.Listeners.ClientSwitchLink     = function( ev )
				{ 
					pointer.SetClientSubPageEventSelectValues( clientInfo );
					return false;
				};

			// Remove existing options if any
			private.Nodes.SelectOtherSessionNode.options.length = 0;
      
      var post_data = { };
      post_data["token"]     = token;
      post_data["serviceId"] = serviceId; // Sessions with same service offered
      post_data["status"]    = ["1"]; // only "Open" sessions
      
      // Launch the call with post_data to retrieve service session info
      var request = $.ajax({
        type: "POST",
        url: baseUrl + "api/ajax/entities/services/sessions/upcoming",
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
        data: post_data
      });
      
      request.done( function( response ) {
        var sessions = response.sessions;
        var y = 0;
        var sessionDate = "";
        var sessionId = "";
        var sessionDateMoment = "";
        var sessionDateStart = "";
        var sessionTime = "";
        var statusText = "";
        var sessionPrivacy = "";
        for( y = 0 ; y < sessions.length ; y++ ){
          sessionDate = sessions[y].sessionDate;
          sessionId = sessions[y].sessionId;
          statusText = sessions[y].statusText;
          sessionPrivacy = sessions[y].isPrivate == "true" ? "PRIVATE" : "PUBLIC";
          
          sessionDateMoment = moment(sessionDate);
          sessionDateStart = sessionDateMoment.format("MM/DD/YYYY");
          sessionTime = sessionDateMoment.format("hh:mm A");
          
          
          var optionTitle = "#" + sessionId + " on " + sessionDateStart + " @ " + sessionTime + " - " + sessionPrivacy;
          
          var option = document.createElement("option");
					option.text  = optionTitle;
					option.value = sessionId;

          // Session must be opened
					if( actualSessionId == sessionId /*|| statusText != "Open"*/ )
						option.disabled = "disabled";

          private.Nodes.SelectOtherSessionNode.add(option);

        }        
      
      });
		};
		
		public.SetClientSubPageEventSelectValues = function( eventData, clientInfo )
		{
			private.Nodes.ClientSwitchIdField.value = clientInfo[ "clientId" ];                                            
			private.Nodes.ClientNameNode.innerHTML  = clientInfo["name"];
			private.Nodes.ClientPicNode.src         = clientInfo["picUrl"];
		};
		
		// Must be called here 
		private.Construct( );
	};
  
	window.Li3calendarModal   = new Li3calendarHostEditModal( );
})( );