
  function process_login() {
    
    var dNow = new Date();
    var localdate= (dNow.getMonth()+1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes();
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

    if ( $('#u').val() == '') {
      $('#error_message').text('Valid Email Address is Required');
      display_error();
      $('#u').focus();
      return false;
//    } else if ( $('#u').val().search(emailRegEx) == -1) {
//      $('#error_message').text('Valid Email Address is Required');
//      display_error();
//      $('#u').focus();
//      return false;
    } else if ( $('#p').val() == '') {
      $('#error_message').text('Password is Required');
      display_error();
      $('#p').focus();
      return false;
    } else {
      hide_error();
    }

    var post_data = {	token: '1234', u: $('#u').val(), p: $('#p').val(), setSession: 's' };
      
    var request = $.ajax({
      type: "POST",
      url: "http://dash.reshape.net/api/ajax/auth/login",
      dataType: "json",
      xhrFields: { withCredentials: true },
      crossDomain: true,
      data: post_data
    });	

    request.done(function(response) {
      
      if(response.error != "true"){
        $('#form_process_login').submit();
//        var ProfileID = response.login.loginUser;
//        var ProfileName = response.login.name;
//       var ProfilePicture = response.login.picture_url;
//        var ProfileURL = response.login.profile_url;
      } else {
        var ValidationError = response.post_data.notices[0].message;  
        $('#error_message').text(ValidationError);
        $('#u').val('');
        $('#p').val('');
        display_error();             
      }
    });
        
  }
  
  function display_error() {
    $('#error_div').slideDown('fast');
  }
  
  function hide_error() {
    $('#error_div').slideUp('fast');
  }
  
  $('#u').keypress(function(e){
    if (e.which == 13){
      $("#login_button").click();
    }
  });  

  $('#p').keypress(function(e){
    if (e.which == 13){
      $("#login_button").click();
    }
  });  

