<?php
  
  function GetAjaxReponseJSON( $url, $postData ){
    $ch = curl_init($url);		
    curl_setopt_array($ch, array(
      CURLOPT_POST => TRUE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_HTTPHEADER => array( 'Content-Type: application/json' ),
      CURLOPT_POSTFIELDS => $postData
    ));
    
    // Send the request
    $response = curl_exec($ch);
  
    // Check for errors
    if($response === FALSE){
      die(curl_error($ch));
    }
  
    // Decode the response
    // $responseData = json_decode($response, TRUE);
    return $response;		
  }
  
  $token = "?token=499a3c3d198a41795ced7e2d7691648b375803c2d13c83a4f7d5af78e5ec1899";	 
  $variables = $_POST['variables'];
  $url = $_POST['url']."?token=499a3c3d198a41795ced7e2d7691648b375803c2d13c83a4f7d5af78e5ec1899&".$variables;	
  
  echo GetAjaxReponseJSON($url, $postData);    

?>