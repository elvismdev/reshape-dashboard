<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  // Subject
  $subject = $_POST['topic'];
  // Message
  $message = $_POST['message'];
  // Recipients
  $recipients = $_POST['to'];
  
  //$q = intval($_GET['q']);

  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
    
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  
  // Force user id because getUserId return old id
  $userId = 273;
  // Add the current user in the recipients
  $recipients[] = $userId; 
  $sql_add_conv = "INSERT INTO conversation ( subject, lastSenderId ) VALUES (?, ? )";
  
  $statement_new_conv = $link->prepare($sql_add_conv);
  
  $statement_new_conv->bind_param("si", $subject, $userId );
  
  if ( $statement_new_conv->execute() ) {
    $new_conv_id = $statement_new_conv->insert_id;
    
    foreach( $recipients as $recipient ){
      // Link the members
      $sql_add_member = "INSERT INTO conversation_member ( chatId, userId ) VALUES (?, ? )";
      $statement_new_member = $link->prepare($sql_add_member);
      $statement_new_member->bind_param("ii", $new_conv_id, $recipient );
      
      if($statement_new_member->execute() ) {            
      } else {
        die('Error : (' . $mysqli->errno .') '. $mysqli->error);      
      }
      // Close the statement
      $statement_new_member->close();
    }
    
    // Add the message
    $sql_add_message = "INSERT INTO conversation_message ( chatId, senderId, text ) VALUES (?, ?, ?)";
    $statement_new_message = $link->prepare($sql_add_message);
    $statement_new_message->bind_param("iis", $new_conv_id, $userId, $message );
    
    if($statement_new_message->execute() ) {
      // New message id
      $new_message_id = $statement_new_message->insert_id;
      
      // Get message and postedOn
      $sql_get_new_message_info ="SELECT text, senderId, sendDate FROM conversation_message where id = " . $new_message_id;
      if ( $result_get_new_message_info = $link->query( $sql_get_new_message_info) ) {
        while( $row_new_message = $result_get_new_message_info->fetch_assoc() ) {
          $message = $row_new_message['text'];
          $sendDate = $row_new_message['sendDate'];
          $senderId = $row_new_message['senderId'];
          
          // Get the poster name and picture url
          $sql_poster ="SELECT id, first_name, last_name, logo_url FROM professional_user where id = " . $senderId;
          if ( $result_user = $link->query( $sql_poster) ) {
          
            while( $row_user = $result_user->fetch_assoc() ) {
            
              // User info
              $row_user_profile_a = array();
              
              $row_user_profile_a['id'] = $row_user['id'];
              $row_user_profile_a['name'] = $row_user['first_name'] . ' ' . $row_user['last_name'];
              $row_user_profile_a['picture_url'] = $row_user['logo_url'];
            
              // New message text
              $new_message_id = $statement_new_message->insert_id;
          
              // Build the JSON response
              $rows = array();
              $rows["userId"] = $userId;
              $rows["error"] = "false";
              $rows["chat"]["chatId"] = $new_conv_id;
              $row_new_message_a = array();
              $row_new_message_a['messageId'] = $new_message_id; // message id
              $row_new_message_a['text'] = $message; // message
              $row_new_message_a['postedOn'] = $sendDate; // posted on
              
              $row_new_message_a['sender'] = $row_user_profile_a;
              $rows['newMessage'] = $row_new_message_a;
              echo json_encode( $rows ) ;
            } 
          } else {
            printf("error: %s\n", mysqli_error($link));
          }  
          /* free result_users set */
          mysqli_free_result($result_user);
        }
      } else {
        printf("error: %s\n", mysqli_error($link));
      }
      /* free result_users set */
      mysqli_free_result($result_get_new_message_info);
    } else {
      die('Error : (' . $mysqli->errno .') '. $mysqli->error);
    }
    // Close the statement
    $statement_new_message->close();    
  } else {
    die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
  }
  // Close the statement
  $statement_new_conv->close();
   
  $db_obj->db_close();
?>