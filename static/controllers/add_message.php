<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  // Posted parameters
  $text = $_POST['text']; // Text 
  $chatId    = $_POST['chatId']; // Chat id
    
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
  
  $sql_add_mess = "INSERT INTO conversation_message ( chatId, senderId, text ) VALUES (?, ?, ?)";
  $statement_new_mess = $link->prepare($sql_add_mess);
  $statement_new_mess->bind_param("iis", $chatId, $userId, $text );
 
  if ( $statement_new_mess->execute() ) {
    $new_message_id = $statement_new_mess->insert_id;
    
    // Get the send time from the new message
    $sql_new_message ="SELECT sendDate FROM conversation_message WHERE  id = " . $new_message_id;
    
    if ( $result_message = $link->query( $sql_new_message) ) {       
      $rows = array();
      $rows["userId"] = $userId;
      $rows["newMessage"]["messageId"] = $new_message_id;
      
      while( $row_message = $result_message->fetch_assoc() ) {
        $lastActiveOn = $row_message['sendDate'];
        // Update the conversation fields (lastActiveOn, lastSenderId)
        $sql_update_conv = "UPDATE conversation SET lastActiveOn = ?, lastSenderId= ? WHERE id = ?";
        $statement_update_conv = $link->prepare($sql_update_conv);
        $statement_update_conv->bind_param("sii", $lastActiveOn, $userId, $chatId );
         
        if ( $statement_update_conv->execute() ) {
          // Update #1 : member fields (set isUnread to 1 - everyone has not seen the new message)
          $sql_update_memb_all = "UPDATE conversation_member SET isUnread = 1 WHERE chatId = ?";
          $statement_update_memb_all = $link->prepare($sql_update_memb_all);
          $statement_update_memb_all->bind_param("i", $chatId );
          
          if ( $statement_update_memb_all->execute() ) {
            // Update #2 : member fields (set isUnread to 0 - current user has seen his message)
            $sql_update_memb_user = "UPDATE conversation_member SET isUnread = 0 WHERE chatId = ? AND userId = ?";
            $statement_update_memb_user = $link->prepare($sql_update_memb_user);
            $statement_update_memb_user->bind_param("ii", $chatId, $userId );
            
            if ( $statement_update_memb_user->execute() ) {
              // Update complete!
              $rows["error"] = "false";
              echo json_encode( $rows );
            
            } else {
              die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
            }  
            // Close the statement
          $statement_update_memb_user->close();
          } else {
            die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
          }
          // Close the statement
          $statement_update_memb_all->close();
        } else {
          die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
        }
        // Close the statement
        $statement_update_conv->close();        
      }
       
    } else {
      printf("error: %s\n", mysqli_error($link));
    }
    /* free result_message set */
    mysqli_free_result($result_message);
    
  } else {
    die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
  }
  // Close the statement
  $statement_new_mess->close();
   
  $db_obj->db_close();
?>