<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  //$q = intval($_GET['q']);
  
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 384;
  $sql_conv="SELECT conv.id, conv.subject, conv.startedOn, conv.lastActiveOn, conv.lastSenderId, conv.isUnread, conv.isArchived FROM conversation conv, conversation_member memb where memb.userId = " . $userId . " AND conv.id = memb.chatId";
  
  
  if ( $result_conv = $link->query( $sql_conv) ) {       
    $rows = array();
    $rows["userId"] = $userId;
    
    while( $row_conv = $result_conv->fetch_assoc() ) {      
      
      $row_conv_a = array();
      $row_conv_a['chatId']       = $row_conv['id'];
      $row_conv_a['title']        = $row_conv['subject'];
      $row_conv_a['isUnread']     = $row_conv['isUnread'] == 0 ? "false" : "true" ;
      $row_conv_a['lastSenderId'] = $row_conv['lastSenderId'];
      $row_conv_a['lastActiveOn'] = $row_conv['lastActiveOn'];
      $row_conv_a['startedOn']    = $row_conv['startedOn'];
    
      // Retrieves all users linked to the conversation
      $convId = $row_conv['id'];      
      $sql_users = "SELECT user.id, user.first_name, user.last_name, user.logo_url from professional_user user, conversation_member memb where user.id = memb.userId AND memb.chatId = " . $convId;
      
      if( $result_users = $link->query( $sql_users ) ){                
        $row_users_a = array();
                
        while( $row_users = $result_users->fetch_assoc() ) {
        
          $row_users_a['id']          = $row_users['id'];
          $row_users_a['name']        = $row_users['first_name'] . ' ' . $row_users['last_name'];
          $row_users_a['picture_url'] = $row_users['logo_url'];
          
          $row_conv_a['members'][]    = $row_users_a;
                    
        }
        $rows['chats'][] = $row_conv_a;
      }
    }
    
    //echo json_encode( $rows ) ;

    /* free result_conv set */
    mysqli_free_result($result_conv);
  } else {
    printf("error: %s\n", mysqli_error($link));
  }
  
  $db_obj->db_close();
?>