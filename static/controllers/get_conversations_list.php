<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  // Searched word
  $word = "%{$_POST['search']}%";
  
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
 
  $rows = array();
  $rows["userId"] = $userId;
  $rows["search"] = $_POST['search']; 
  
  $sql_conv="SELECT distinct conv.id, conv.subject, conv.lastSenderId, conv.lastActiveOn, conv.startedOn,  memb.userId, memb.isUnread, memb.isArchived FROM conversation conv, conversation_member memb, conversation_message mess, professional_user usr WHERE ( conv.subject like ? OR mess.text like ? OR usr.first_name 
  like ? OR usr.last_name like ? ) AND memb.isArchived = 0 AND memb.chatId = conv.id AND mess.chatId = conv.id AND memb.userId = usr.id AND memb.userId = ? order by lastActiveOn DESC";
 
  if($stmt = $link->prepare( $sql_conv )) {
    $stmt->bind_param("ssssi", $word, $word, $word, $word, $userId); 
    $stmt->execute(); 
    $stmt->bind_result( $r_chatId, $r_subject, $r_lastSenderId, $r_lastActiveOn, $r_startedOn, $r_userId, $r_isUnread, $r_isArchived );
    $row_conv_a = array();
    
    while ($stmt->fetch()) {
      $row_conv_a['chatId']       = $r_chatId;
      $row_conv_a['title']        = $r_subject;
      $row_conv_a['isUnread']     = $r_isUnread == 0 ? "false" : "true" ;
      $row_conv_a['lastSenderId'] = $r_lastSenderId;
      $row_conv_a['lastActiveOn'] = $r_lastActiveOn;
      $row_conv_a['startedOn']    = $r_startedOn;
      $row_conv_a['snippet']      = "";
      $row_conv_a['members']      = "";
      
      $rows['chats'][] = $row_conv_a;      
    }
    
    if( sizeof( $rows['chats']) > 0 ) {
      // Get the last message
      $sql_last_mess ="SELECT text FROM conversation_message where chatId = ? ORDER BY id DESC LIMIT 1";
      
      // Set the snippet for each chat
      foreach( $rows['chats'] as &$row_chat ){
        $chatId = $row_chat['chatId'];
        
        if( $stmt2 = $link->prepare( $sql_last_mess ) ) {        
          $stmt2->bind_param("i", $chatId);
          $stmt2->execute(); 
          $stmt2->bind_result( $r_last_mess_text  );
          
          while( $stmt2->fetch() ) {
            $last_message = $r_last_mess_text;
            $last_snippet = (strlen($last_message) > 23) ? substr($last_message,0,20).'...' : $last_message;
            
            $row_chat['snippet'] = $last_snippet;
            
          }
        } else {
          die('Error 2 : (' . $link->errno .') '. $link->error);
        }
        // Close the statement
        $stmt2->close();
      } 
      
      // Retrieves all users linked to the conversation
      $sql_users = "SELECT user.id, user.first_name, user.last_name, user.logo_url from professional_user user, conversation_member memb where user.id = memb.userId AND memb.isArchived = 0 AND memb.chatId = ?";
      
      // Set users relating to the conversation
      foreach( $rows['chats'] as &$row_chat ){
        $chatId = $row_chat['chatId'];
        
        if( $stmt3 = $link->prepare( $sql_users ) ) {
          $stmt3->bind_param("i", $chatId); 
          $stmt3->execute(); 
          $stmt3->bind_result( $r_usr_id, $r_usr_first_name, $r_usr_last_name, $r_usr_logo_url  );
          
          $row_users_members = array();
                  
          while ( $stmt3->fetch() ) {
            $row_users_a = array();
            $row_users_a['id']          = $r_usr_id;
            $row_users_a['name']        = $r_usr_first_name . ' ' . $r_usr_last_name;
            $row_users_a['picture_url'] = $r_usr_logo_url;
            
            $row_users_members[]    = $row_users_a;
            $row_conv_a['members'] = $row_users_members;  
            
          }
          $row_chat['members'] = $row_users_members;
        
        } else {
          die('Error 3 : (' . $link->errno .') '. $link->error);
        }
        // Close the statement
        $stmt3->close();
      }
    }
    
    $rows["error"] = "false";
    // Close the statement
    $stmt->close();
    
    echo json_encode( $rows ) ;

    
  } else {
    die('Error 3 : (' . $link->errno .') '. $link->error);
    $rows["error"] = "true";
  }
  
  $db_obj->db_close();
?>