<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  // Last message id
  $lastMsgId = $_POST['lastMsgId'];
  // Chat id
  $chatId    = $_POST['chatId'];
    
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
  
  // Get all messages
  $sql_messages="SELECT id, senderId, text, sendDate FROM conversation_message WHERE chatId = " . $chatId. " AND id > " . $lastMsgId . " order by id ASC";
  
    
  if ( $result_messages = $link->query( $sql_messages) ) {       
    $rows = array();
    $rows["userId"] = $userId;
    
    
    while( $row_message = $result_messages->fetch_assoc() ) {
      
      $row_message_a = array();
      $row_message_a['messageId']       = $row_message['id'];
      $row_message_a['text']        = $row_message['text'];
      $row_message_a['postedOn']     = $row_message['sendDate'];
      // Last sender id
      $lastSenderId = $row_message['senderId'];
      
      
      // Get the last sender info
      $sql_get_last_sender ="SELECT id, first_name, last_name, logo_url FROM professional_user where id = " . $lastSenderId;
      
      if ( $result_get_last_sender = $link->query( $sql_get_last_sender) ) {
        while( $row_last_sender = $result_get_last_sender->fetch_assoc() ) {
          $row_sender = array();
          $row_sender['id'] = $row_last_sender['id'];
          $row_sender['name'] = $row_last_sender['first_name'] . ' ' . $row_last_sender['last_name'];
          $row_sender['picture_url'] = $row_last_sender['logo_url'];
          
          $row_message_a['sender'] = $row_sender;
        }
        $rows['messages'][] = $row_message_a;
      } else {
        printf("error: %s\n", mysqli_error($link));
      }
    }
    
    $rows["error"] = "false";
    
    echo json_encode( $rows ) ;

    /* free result_messages set */
    mysqli_free_result($result_messages);
  } else {
    printf("error: %s\n", mysqli_error($link));
  }
  
  $db_obj->db_close();
?>