<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  //$q = intval($_GET['q']);
  
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
  $sql_users="SELECT id, first_name,last_name, logo_url FROM professional_user where id <> " . $userId;
  
  
  if ( $result_users = $link->query( $sql_users) ) {       
    $rows = array();
    $rows["userId"] = $userId;
    
    while( $row_user = $result_users->fetch_assoc() ) {      
      
      $row_user_profile_a = array();
      
      $row_user_profile_a['profile']['id'] = $row_user['id'];
      $row_user_profile_a['profile']['name'] = $row_user['first_name'] . ' ' . $row_user['last_name'];
      $row_user_profile_a['profile']['picture_url'] = $row_user['logo_url'];
      
      $rows['networkLinks'][] = $row_user_profile_a;      
    }
    
    echo json_encode( $rows ) ;

    /* free result_users set */
    mysqli_free_result($result_users);
  } else {
    printf("error: %s\n", mysqli_error($link));
  }
  
  $db_obj->db_close();
?>