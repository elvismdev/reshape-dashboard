<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  // Last message id
  $lastMsgId = $_POST['lastMsgId'];
  // Chat id
  $chatId    = $_POST['chatId'];
    
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
  
  // Get the number of unread conversations
  $sql_unread_conv="SELECT chatId FROM conversation_member WHERE userId = ? and isUnread = 1";
  $stmt_unread_conv = $link->prepare($sql_unread_conv);
  $rows = array();
  $rows["userId"] = $userId;
  $rows["sql"] = $sql_unread_conv;
  if( $stmt_unread_conv ){
    $stmt_unread_conv->bind_param("i", $userId);
    
    if( true ){
      //$stmt_unread_conv -> bind_result($test);
      $stmt_unread_conv->fetch();  
      $rows["unread"] = $stmt_unread_conv->num_rows;
      $rows["error"] = "false";
    } else {
      die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
      $rows["error"] = "true";
    }
  }
  // Close the statement
  $stmt_unread_conv->close();
  
  echo json_encode( $rows );
  
  $db_obj->db_close();
?>