<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  // Chat id
  $chatId    = $_POST['chatId'];
    
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
  
  // Update the conversation member fields (isArchived is set to 1)
  $sql_update_member = "UPDATE conversation_member SET isArchived = 1  WHERE chatId = ? AND userId = ?";
  $statement_update_member = $link->prepare($sql_update_member);
  $statement_update_member->bind_param("ii", $chatId, $userId );
  
  $rows = array();
  $rows["userId"] = $userId;  
   
  if ( $statement_update_member->execute() ) {
    $rows["error"] = "false"; 
  
  } else {
    die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
    $rows["error"] = "true";
  }
  // Close the statement
  $statement_update_member->close();
  
  
  echo json_encode( $rows );
  
  $db_obj->db_close();
?>