<?php
  include($_SERVER['DOCUMENT_ROOT']	.	"/classes/Li3dbConnector.php");
  
  //global	$Li3;
  //$userId  =  $Li3->GetUserId();
  
  // Searched word
  $chatId = $_POST['chatId'];
     
  $db_obj = new Li3dbConnector();
  $db_obj->db_connect();  
  $link = $db_obj->link;
  
  if (!$link) {
    die('Could not connect: ' . mysqli_error($link));
  }
  // Force user id because getUserId return old id
  $userId = 273;
  
  // Update the conversation member fields (isUnread is set to 0)
  $sql = "UPDATE conversation_member SET isUnread = 0  WHERE chatId = ? AND userId = ?";
  $stmt = $link->prepare($sql);
  $stmt->bind_param("ii", $chatId, $userId );
  
  $rows = array();
  $rows["userId"] = $userId;  
   
  if ( $stmt->execute() ) {
    $rows["error"] = "false"; 
  
  } else {
    die('Error : (' . $mysqli->errno .') '. $mysqli->error); 
    $rows["error"] = "true";
  }
  
  // Close the statement
  $stmt->close();
  
  echo json_encode( $rows );
  
  $db_obj->db_close();
?>