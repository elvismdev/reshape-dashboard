<?php

  require_once $_SERVER['DOCUMENT_ROOT']."/classes/Li3instance.php";
  require_once $_SERVER['DOCUMENT_ROOT']."/classes/Li3pageController.php";

  global $Li3;

  // RETRIEVE VARIABLES FROM URL =========================================
  $u = $_POST['u'];
  $p = $_POST['p'];
  // =====================================================================

  // DEFINE ADDITIONAL VARIABLES =========================================
  $token = $Li3->GetToken();
  $url = "http://dash.reshape.net/api/ajax/auth/login?token=".$token."&u=".$u."&p=".$p;
  $postData = array('token' => $token, 'u' => $u, 'p' => $p);
  // =====================================================================

  // PERFORM ACTION & LOGIN ============================================== 
  $ProcessLogin = $Li3->GetAjaxReponse($url, $postData);  
  $ProfileID = $ProcessLogin['login']['loginUser']['id'];
  // =====================================================================
  
  // GET ADDITIONAL PROFILE INFORMATION ==================================    
  $url = "http://dash.reshape.net/api/ajax/profile/info?token=".$token."&id=".$ProfileID;    
  $ProcessProfileBasic = $Li3->GetAjaxReponse($url, $postData);  
  
  $ProfileName = $ProcessLogin['name'];
  $ProfilePictureURL = $ProcessLogin['picture_url'];

  echo "@".$ProfileName."@";

  $Li3->SetUserId( $ProfileID );
  $Li3->SetUserName( $ProfileName );
  
  
  die;
  
  $Li3->Redirect( "/Home" );
  // =====================================================================

?>