<?php
  
  if ( $UserAdmin != "" ) {
    $first_item = "<li class=\"bcfirst-li\"><button type=\"button\" class=\"btn btn-xs btn-warning\" onclick=\"window.location='/logoutadmin?id=".$UserAdmin."'\">LOGOUT ADMIN</button></li><li><a href=\"/\"><i class=\"sidebar-nav-icon fa fa-home\"></i></a> <a href=\"/\">Home</a></li>";
  } else {
    $first_item = "<li class=\"bcfirst-li\"><a href=\"/\"><i class=\"sidebar-nav-icon fa fa-home\"></i></a> <a href=\"/\">Home</a></li>";
  }

  $apiRootPath    = $Li3->GetDomainNameURL();
  $apiPublicToken = $Li3->GetToken();
  $proxyUserId    = $Li3->GetUserId();

?>


  <link href="<?php	$Li3->EchoStaticPath("css/showcase/skin_modern_silver.css");	?>" type="text/css" rel="stylesheet" />
  <link href="<?php	$Li3->EchoStaticPath("css/showcase/html_content.css");	?>" type="text/css" rel="stylesheet" />
  
  <div id="page-content">
  <div class="content-header">
    <ul class="breadcrumb breadcrumb-top">
      <?php echo $first_item; ?>
      <li><?php echo $page_title; ?></li>
    </ul>
    <div class="header-section">
      <h1> <i class="<?php echo $page_icon; ?>" style="margin-right: 23px; color:#d3202c;"></i> <?php echo $page_title; ?> <small style="margin-top: -14px;"><?php echo $page_description; ?></small> </h1>
    </div>
  </div>
    