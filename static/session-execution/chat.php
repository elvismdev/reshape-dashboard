<div class="chat-widget pull-right">
  <!-- Chat Open Handle -->
  <div class="chat-widget-handle text-center toggle-chat">
    <span class="chat-icon">
      <img src="img/components/openchaticon.png">
    </span>
    <span class="chat-open-icon">
      <img src="img/components/lefticon.png">
    </span>
  </div> <!-- Chat open handle -->

  <div class="chat-widget-screen chat-screen">
    <!-- Chat Header -->
    <div class="chat-header">
        <img src="img/components/openchaticon.png" class="pull-left">
        <span class="chat-title">
          Chat
        </span>
        <img src="img/components/hidechaticon.png" class="pull-right toggle-chat">
    </div> <!-- End .chat-header -->
    <div class="chat-content">
      <!-- Messages get printed within here. -->
      <div id="msgsSink" class="messages-sink"></div>

      <!-- This is the message input form -->
      <div class="messages-input">
        <textarea id="msgInput" class="form-control" placeholder="Type your message here..."></textarea>
          <!-- <select id="targetSelect">
            <option value="all">ALL</option>
          </select> -->
          <a id="sendBtn" class="btn btn-sm btn-grey btn-block send-btn disabled"
             href="javascript://nop">
            Send
          </a>
      </div>
    </div>
  </div>
</div>