<?php
// Test the features with the following url:  
  $theme = $_GET['theme'] ?: 'dark';
  $scopeId = $_GET['scpId'];
  $session_count = $_GET['session_count'];
  $userName = $_GET['user'] ?: 'Local User';
  $viewType = $_GET['view'] ?: 'client';

  $settings = 

  if ( $theme == "dark" ) { 
    $css_theme = "reshape-dark";
    $img_path = "dark";
    $header_color = "FFFFFF";
    $person_name_color = "FFFFFF";
    $bg_right_area = "000000";
    $borderright = "2C2C2D";
  } else if ( $theme == "light" ) {
    $css_theme = "reshape-light";
    $img_path = "light";
    $header_color = "39393B";
    $person_name_color = "39393B";
    $bg_right_area = "A1A0A0";
    $borderright = "E0E0E0";
  }
?>