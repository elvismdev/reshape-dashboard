<!-- ##################################################### -->
<!-- Scope Form -->
<!-- ##################################################### -->
<!-- Hidden Fields for script values -->
<form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <select id='viewType' class="form-control">
      <option value="client">Client</option>
      <option value="trainer">Trainer</option>
    </select>
    <input id="scope_id" type="text" class="form-control" placeholder="Scope Id" value="12321">
    <input id='user_name' type='text' class="form-control" value="User<?=substr(time(), -4,4)?>">
  </div>
</form>

<ul class="nav navbar-nav navbar-right">
  <!-- ##################################################### -->
  <!-- Session Controls -->
  <!-- ##################################################### -->
  <!-- Start Session -->
  <li id="li-playsession">
    <a id="a-playsession" class="nav_a" href="javascript://nop">
      <img id="icon-playsession" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-playsession-off.png">
      Start Session
    </a>
  </li>

  <!-- Stop Session -->
  <li id="li-stopsession" style="display:none;">
    <a id="a-stopsession" class="nav_a" href="javascript://nop">
      <img id="icon-stopsession" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-stopsession-off.png">
      Stop Session
    </a>
  </li>

  

  <!-- ##################################################### -->
  <!-- Timer Controls -->
  <!-- ##################################################### -->
  <!-- 
  <li id="li-timer">
    <a id="a-timer" class="nav_a" href="#">
      <img id="icon-timer" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-timer-off.png">
      00:00
    </a>
  </li> 
  -->

  <!-- ##################################################### -->
  <!-- Camera Settings -->
  <!-- ##################################################### -->
  <!-- hidden for now...
  <li class="dropdown">
      <a id="a-settings" class="dropdown-toggle" data-toggle="dropdown" href="#">
        <img id="icon-settings" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-settings-off.png">
        Settings
      </a>
      <ul class="dropdown-menu" role="menu">
        <li>
          <a href="javascript:void(0)">
            Video Capture Device<br/>
            <select id="camSelect" class="ctrl">
              <option value="-1">Loading...</option>
            </select>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            Audio Capture Device<br/>
            <select id="camSelect" class="ctrl">
              <option value="-1">Loading...</option>
            </select>
          </a>
        </li>
        <li>
          <a href="javascript:void(0)">
            Audio Output Device<br/>
            <select id="camSelect" class="ctrl">
              <option value="-1">Loading...</option>
            </select>
          </a>
        </li>
      </ul>
  </li>
  -->

  <!-- ##################################################### -->
  <!-- Sound Controls -->
  <!-- ##################################################### -->
  <!-- Mute -->
  <li id="li-mute">
    <a id="a-mute" class="nav_a" href="#">
      <img id="icon-mute" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-mute-off.png">
      Mute
    </a>
  </li>

  <!-- Un-mute (hidden) -->
  <li id="li-unmute" style="display:none;">
    <a id="a-unmute" class="nav_a" href="#">
      <img id="icon-unmute" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-unmute-off.png">
      Unmute
    </a>
  </li>

  <!-- ##################################################### -->
  <!-- Video Controls -->
  <!-- ##################################################### -->
  <!-- Video Show -->
  <li id="li-hide-off">
    <a id="a-hide-off" class="nav_a_last" href="#">
      <img id="icon-hide-off" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-hide-off.png">
      Hide
    </a>
  </li>
  <!-- Video Hide (hidden) -->
  <li id="li-hide-on" style="display:none">
    <a id="a-hide-on" class="nav_a_last" href="#">
      <img id="icon-hide-on" class="navbar-icon" src="img/<?php echo $img_path; ?>/icon-hide-off.png">
      Show
    </a>
  </li> 
 
</ul>