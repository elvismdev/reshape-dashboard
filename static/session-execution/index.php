<?php
// Test the features with the following url:
// http://{base_url}?scpId=123&view=trainer&user=Trainer%20Name
// http://{base_url}?scpId=123&view=client&user=Client%20Name
  $theme = $_GET['theme'] ?: 'dark';
  $scopeId = $_GET['scpId'];
  $session_count = $_GET['session_count'];
  $userName = $_GET['user'] ?: 'Local User';
  $viewType = $_GET['view'] ?: 'client';

  if ( $theme == "dark" ) { 
    $css_theme = "reshape-dark";
    $img_path = "dark";
    $header_color = "FFFFFF";
    $person_name_color = "FFFFFF";
    $bg_right_area = "000000";
    $borderright = "2C2C2D";
  } else if ( $theme == "light" ) {
    $css_theme = "reshape-light";
    $img_path = "light";
    $header_color = "39393B";
    $person_name_color = "39393B";
    $bg_right_area = "A1A0A0";
    $borderright = "E0E0E0";
  }

?>

<!DOCTYPE html>
<html>
  <head>
    <title>ReShape</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/reshape.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://getbootstrap.com/assets/js/html5shiv.js"></script>
    <script src="http://getbootstrap.com/assets/js/respond.min.js"></script>
    <![endif]-->    
  </head>

  <body>
    <!-- Hidden Fields for script values -->
    <input type='hidden' id='theme_name' value='<?php echo $img_path; ?>'>
    
    <!--
     Beginning of the templates section. There is only one template -  remote
     video feed renderer
    -->
    <div id="templatesContainer" style="display:none;">
    <!-- Message Template -->
      <span id="msgTmpl" class="msg-itm">
        <span class="msg-header">
          User <span class="userid-wrapper"></span>
          <span class="direct-indicator hidden">(direct message)</span>
        </span>
        <span class="msg-content"></span>
        <br/>
      </span>
      <!-- Renderer Templates -->
      <div id="rendererTmpl" class="render-widget remote-renderer">
        <div class="user-info text-center">
            <span class="user-name-wrapper"></span>
            <span class="user-id-wrapper hide"></span>
            <span class="pull-right feed-controls"> 
                <span class="mute-user">
                  <img src="img/components/muteicon.png">
                </span>
                <!-- @todo add unmute icon -->
                <span class="unmute-user" style="display: none;">
                  <img src="img/components/muteicon.png">
                </span>
                <span class="kick-user">
                  <img src="img/components/kickicon.png">
                </span>
            </span>
        </div>
        <div class="render-wrapper"></div>
      </div>
    </div>

    <!-- End of the templates section. -->


    <!-- Navigation -->
    <?php include("navbar-top/base.php");?>
    <!-- End .navbar.navbar-inverse.navbar-fixed-top -->
    <!-- Start Layout -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">

          <!-- Joined User Screens -->
          <div id="clientRenderingWrapper"></div>

            <!-- Local Feed Renderer -->
            <?php include("local-feed.php");?>

          <!-- Chat Widget -->
          <?php include("chat.php");?>
           <!-- End .chat-widget -->


        </div> <!-- End .col-md-12 -->
    </div> <!-- End .row -->
    </div> <!-- End .container-fluid -->
    <div id="loader" class="loading">Loading&#8230;</div>
  </body> 

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="//code.jquery.com/jquery.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/sha256.js"></script>
  
  <!-- Development, non-minimised SDK -->
  <script src="//d36pfzlm4aixmv.cloudfront.net/stable_v3/addlive-sdk.js"></script>
  <script src="js/reshape.js"></script>
  <script src="js/reshape.addlive-v3.js"></script>

  <!-- Production, minimised SDK -->
  <!--<script type="text/javascript"-->
  <!--src="//d36pfzlm4aixmv.cloudfront.net/stable_v3/addlive-sdk.min.js"></script>-->



  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>

  


</html>