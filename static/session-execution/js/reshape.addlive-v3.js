  /**
   * @fileoverview
   * @TODO file description
   *
   * @author Tadeusz Kozak
   * @date 26-06-2012 10:37
   */
  (function(w) {
      'use strict';
      // Scope constants
      // To set your own APP_ID (check shared-assets/scripts.js)
      var APPLICATION_ID = ADLT.APP_ID,
          APP_SHARED_SECRET = ADLT.API_KEY,
          /**
           * Configuration of the streams to publish upon connection established
           * @type {Object}
           */
          CONNECTION_CONFIGURATION = {
              videoStream: {
                  maxWidth: 2000,
                  maxHeight: 2000,
                  maxFps: 24,
                  useAdaptation: true
              },
              autopublishVideo: true,
              autopublishAudio: true
          },
          mediaConnType2Label = {};
      mediaConnType2Label[ADL.ConnectionType.NOT_CONNECTED] = 'not connected';
      mediaConnType2Label[ADL.ConnectionType.TCP_RELAY] = 'RTP/TCP relayed';
      mediaConnType2Label[ADL.ConnectionType.UDP_RELAY] = 'RTP/UDP relayed';
      mediaConnType2Label[ADL.ConnectionType.UDP_P2P] = 'RTP/UDP in P2P';
      // Scope variables
      var scopeId, 
          userId, 
          viewType, 
          trainer,
          greeted = [],
          localVideoStarted = false,
          rendererTmplId = 'rendererTmpl',
          clientRenderer = 'userRenderer',
          clientRenderContainer = '#clientRenderingWrapper',
          /**
           * @type {ADL.MediaConnection}
           */
          mediaConnection;
      /**
       * Document ready callback - starts the AddLive platform initialization.
       */
      function onDomReady() {
          console.log('DOM loaded');
          // assuming the initAddLiveLogging is exposed via ADLT namespace.
          // (check shared-assets/scripts.js)
          initUI();
          var initOptions = {
              applicationId: APPLICATION_ID,
              enableReconnects: true,
              useWebRTC:true
          };
          // Initializes the AddLive SDK. Please refer to ../shared-assets/scripts.js
          ADLT.initializeAddLiveQuick(onPlatformReady, initOptions);
      }

      function initUI() {
          console.log('Initializing the UI');
          RUI.bindAudioEvents();
          RUI.bindVideoEvents();

          //set the view type
          $('#a-mute, #a-unmute').click(onPublishAudioChanged);
          $('#a-hide-off, #a-hide-on').click(onPublishVideoChanged);

          $('#camSelect').change(function() {
              var selectedDev = $(this).val();
              ADL.getService().setVideoCaptureDevice(ADL.r(startLocalVideoMaybe), selectedDev);
          });
          $('#micSelect').change(ADLT.getDevChangedHandler('AudioCapture'));
          $('#spkSelect').change(ADLT.getDevChangedHandler('AudioOutput'));
          console.log('UI initialized');
      }

      function onPlatformReady() {
          console.log('==============================================================');
          console.log('==============================================================');
          console.log('AddLive SDK ready - setting up the application');
          // assuming the populateDevicesQuick is exposed via ADLT namespace.
          // (check shared-assets/scripts.js)
          startLocalVideoMaybe()
          if(!ADL.usesWebRTC()){
            ADLT.populateDevicesQuick();  
          }
          
          initServiceListener();

          //hide the loader now that everything is ready.
          console.log('AddLive Platform ready.');
          console.log('==============================================================');
          console.log('==============================================================');
          
          ADLT.loader().hide();
        
      }
      /**
       * ==========================================================================
       * Beginning of the AddLive service events handling code
       * ==========================================================================
       */
      function initServiceListener() {
          console.log('Initializing the AddLive Service Listener');
          // 1. Instantiate the listener
          var listener = new ADL.AddLiveServiceListener();
          // 2. Define the handler for the user event
          listener.onUserEvent = function(e) {
              console.log('Got new user event: ' + e.userId);
              if (e.isConnected) {
                  onUserJoined(e);
              } else {
                  onUserDisconnect(e);
              }
          };

          // 3. Define the handler for streaming status changed event
          listener.onMediaStreamEvent = function(e) {
              console.log('Got new media streaming status changed event');
              switch (e.mediaType) {
                  case ADL.MediaType.AUDIO:
                      onRemoteAudioStreamStatusChanged(e);
                      break;
                  case ADL.MediaType.VIDEO:
                      onRemoteVideoStreamStatusChanged(e);
                      break;
                  default:
                      console.warn('Got unsupported media type in media stream event: ' + e.mediaType);
              }
          };
          // 4. Define the handler for the media connection type changed event
          listener.onMediaConnTypeChanged = function(e) {
              console.log('Got new media connection type: ' + e.connectionType);
              $('#connTypeLbl').html(mediaConnType2Label[e.connectionType]);
          };
          // 5. Define the handler for the connection lost event
          listener.onConnectionLost = function(e) {
              console.warn('Got connection lost notification: ' + JSON.stringify(e));
              disconnectHandler();              
          };
          // 6. Define the handler for the reconnection event
          listener.onSessionReconnected = function(e) {
              console.log('Connection successfully reestablished!');
              postConnectHandler();
          };

          //6.1 listener for sent messages
          listener.onMessage = function(e){
              console.log('Got message from user');
              receiveMessage(e);
          }

          // 7. Prepare the success handler
          var onSucc = function() {
              console.log('AddLive service listener registered');
              $('#a-playsession').click(connect);
          };
          // 8. Finally register the AddLive Service Listener
          ADL.getService().addServiceListener(ADL.r(onSucc), listener);
      }

      function onUserJoined(e) {
          console.log('Got new user with id: ' + e.userId);
          console.log('User: ', e);
          // 1. Prepare a rendering widget for the user.
          var renderer = $('#rendererTmpl').clone().hide();
          renderer.attr('id', clientRenderer + e.userId);
          renderer.find('.render-wrapper').attr('id', 'renderer' + e.userId);
          renderer.find('.user-id-wrapper').html(e.userId);

          // 2. Append it to the rendering area.
          $(clientRenderContainer).prepend(renderer);
          console.log("Sink added to DOM for user: ", e.userId);
          if (e.videoPublished) {
              // 3a. Render the sink if the video stream is being published.
              ADL.renderSink({
                  sinkId: e.videoSinkId,
                  containerId: 'renderer' + e.userId
              });
          } else {
              // 3b. Just show the no video stream published indicator.
              renderer.find('.no-video-text').show();
              renderer.find('.allowReceiveVideoChckbx').hide();
          }
          // 4. Show the 'audio muted' indicator if user does not publish audio stream
          if (!e.audioPublished) {
              renderer.find('.muted-indicator').show();
              renderer.find('.allowReceiveAudioChckbx').hide();
          }

          renderer.find('.mute-user').click(function(){
            triggerEvent({'action':'MUTE_REQUEST'}, e.userId);
            $(this).hide();
            renderer.find('.unmute-user').show();
          });

          renderer.find('.unmute-user').click(function(){
            triggerEvent({'action':'UNMUTE_REQUEST'}, e.userId);
            $(this).hide();
            renderer.find('.mute-user').show();
          });

          renderer.find('.kick-user').click(function(){
            triggerEvent({'action':'KICK'}, e.userId);
            renderer.remove();
          });

          //if trainers, add all users to direct message!
          if(viewType == 'trainer'){
            $('<option id="user' + e.userId + '" value="' + e.userId + '">User ' +
                e.userId + '</option>').appendTo($('#targetSelect'));
            appendChatMessage(e.userId, 'User with id ' + e.userId + ' just joined ' +
                'the chat');
          }
          introduceMyself(e.userId);
      }

      function onUserDisconnect(e){
        console.log('User with id: ' + e.userId + ' left the media scope');

        //remove the direct message option
        $('#user' + e.userId).remove();
        appendChatMessage(e.userId, 'User with id ' + e.userId + ' just left ' +
            'the chat');
        //remove the rendering widget
        $('#' + clientRenderer + e.userId).html('').remove();

        //removed the user from greeted in case they join again
        greeted.splice(greeted.indexOf(e.userId),1);
        // adjust the remaining panes
        adjustClientPanes();
      }

      function onRemoteVideoStreamStatusChanged(e) {
          console.log('Got change in video streaming for user with id: ' + e.userId + ' user just ' + (e.videoPublished ? 'published' : 'stopped publishing') + ' the stream');
          // 1. Grab the rendering widget corresponding to the user
          var renderingWidget = $('#' + clientRenderer + e.userId);
          if (e.videoPublished) {
              // 2a. If video was just published - render it and hide the
              // 'No video from user' indicator
              ADL.renderSink({
                  sinkId: e.videoSinkId,
                  containerId: 'renderer' + e.userId
              });
              renderingWidget.find('.no-video-text').hide();
          } else {
              // 2b. If video was just unpublished - clear the renderer and show the
              // 'No video from user' indicator
              renderingWidget.find('.render-wrapper').empty();
              renderingWidget.find('.no-video-text').show();
          }
      }

      function onRemoteAudioStreamStatusChanged(e) {
          console.log('Got change in audio streaming for user with id: ' + e.userId + ' user just ' + (e.audioPublished ? 'published' : 'stopped publishing') + ' the stream');
          // 1. Find the 'Audio is muted' indicator corresponding to the user
          var muteIndicator = $('#' + clientRenderer + e.userId).find('.muted-indicator');
          if (e.audioPublished) {
              // 2a. Hide it if audio stream was just published
              muteIndicator.hide();
          } else {
              // 2.b Show it if audio was just unpublished
              muteIndicator.show();
          }
      }
      /**
       * ==========================================================================
       * End of the AddLive service events handling code
       * ==========================================================================
       */
      function startLocalVideoMaybe() {
          if (localVideoStarted) {
              return;
          }
          console.log('Starting local preview of current user');
          // 1. Define the result handler
          var resultHandler = function(sinkId) {
              console.log('Local preview started. Rendering the sink with id: ' + sinkId);
              ADL.renderSink({
                  sinkId: sinkId,
                  containerId: 'renderLocalPreview',
                  mirror: true,
                  filterType: ADL.VideoScalingFilter.BICUBIC,
              });
              localVideoStarted = true;
          };
          // 2. Request the SDK to start capturing local user's preview
          return ADL.getService().startLocalVideo(ADL.r(resultHandler));
      }

      /**
       * ==========================================================================
       * Beginning of the user's events handling code
       * ==========================================================================
       */
      /**
       * Handles the change of the 'Publish Audio' checkbox
       */
      function onPublishAudioChanged() {
          if (!scopeId) {
              // If the scope id is not defined, it means that we're not connected and
              // thus there is nothing to do here.
              return;
          }
          // Since we're connected we need to either start or stop publishing the
          // audio stream, depending on the new state of the checkbox
          if ($('#a-mute').is(':visible')) {
              mediaConnection.unpublishAudio(ADL.r());
              console.log('Audio is muted');
          } else {
              mediaConnection.publishAudio(ADL.r());
              console.log('Audio is un-muted');
          }
      }
      /**
       * Handles the change of the 'Publish Audio' checkbox
       */
      function onPublishVideoChanged() {
          if (!scopeId) {
              // If the scope id is not defined, it means that we're not connected and
              // thus there is nothing to do here.
              return;
          }
          // Since we're connected we need to either start or stop publishing the
          // audio stream, depending on the new state of the checkbox
          // @todo replace with unhide element class
          if ($('#a-hide-on').is(':visible')) {
              console.log('showing video');
              mediaConnection.publishVideo(ADL.r());
          } else {
              console.log('hiding video');
              mediaConnection.unpublishVideo(ADL.r());
          }
      }
      /**
       * ==========================================================================
       * End of the user's events handling code
       * ==========================================================================
       */
      

      /**
       * ==========================================================================
       * Beginning of the connection management code
       * ==========================================================================
       */
      function connect() {
          //get the viewType at the time of connection
          viewType = $('#viewType').val();
          if(viewType == 'trainer'){
            $('.feed-controls').show();
          } else {
            $('.feed-controls').hide();
          }
          console.log('Establishing a connection to the AddLive Streaming Server as a '+viewType);
          // 1. Disable the connect button to avoid a cascade of connect requests
          $('#a-playsession').unbind('click');

          // 2. Get the scope id and generate the user id.
          scopeId = $('#scope_id').val();

          // assuming the genRandomUserId is exposed via ADLT namespace.
          // (check shared-assets/scripts.js)
          userId = ADLT.genRandomUserId();
          // userId = $('#user_id').val();

          // 3. Define the result handler - delegates the processing to
          // the postConnectHandler
          var connDescriptor = genConnectionDescriptor(scopeId, userId);
          /**
           *
           * @param {ADL.MediaConnection} mConn
           */
          var onSucc = function(mConn) {
              console.log('Connection established');
              mediaConnection = mConn;
              postConnectHandler();
              ADLT.loader().hide();
          };
          // 4. Define the error handler - enabled the connect button again
          var onErr = function() {
              console.log("Connection Failed.");
              $('#a-playsession').click(connect);
              $('#li-playsession').show();
              $('#a-stopsession').unbind('click');
              $('#li-stopsession').hide();
              ADLT.loader().hide();
          };
          // 5. Request the SDK to establish a connection
          ADLT.loader().show();
          console.log('Requesting to establish a connection');

          ADL.getService().connect(ADL.r(onSucc, onErr), connDescriptor);
          
      }

      function disconnect() {
          console.log('Terminating a connection to the AddLive Streaming Server');
          // 1. Define the result handler
          var succHandler = function() {
              scopeId = undefined;
              userId = undefined;
              mediaConnection = undefined;
              disconnectHandler();
          };
          mediaConnection.disconnect(ADL.r(succHandler));
      }
      /**
       * Common post disconnect handler - used when user explicitly terminates the
       * connection or if the connection gets terminated due to the networking issues.
       *
       * It just resets the UI to the default state.
       */
      function disconnectHandler() {

          // 1. Toggle the active state of the Connect/Disconnect buttons
          $('#a-playsession').click(connect);
          $("#li-playsession").css('display', 'block');
          // 1.1 Disable the stop session button
          $('#a-stopsession').unbind('click');
          $("#li-stopsession").css('display', 'none');
          // 2. Reset the connection type label
          $('#connTypeLbl').html('none');
          // 3. Clear the remote user renderers
          $(clientRenderContainer).find('.remote-renderer').html('').remove();
          // 4. Clear the local user id label
          $('#localUserIdLbl').html('Not Connected');

          //reset the greeting counter for next time we connect.
          greeted = [];
      }
      /**
       * Common post connect handler - used when user manually establishes the
       * connection or connection is being reestablished after being lost due to the
       * Internet connectivity issues.
       *
       */
      function postConnectHandler() {
          console.log('Connected. Disabling connect button and enabling the disconnect');
          // 1. Enable the disconnect button
          $('#a-stopsession').click(disconnect);
          $("#li-stopsession").css('display', 'block');
          // 2. Disable the connect button
          $('#a-playsession').unbind('click')
          $("#li-playsession").css('display', 'none');
          // 3. Update the local user id label
          $('#localUserIdLbl').html("User Id: (" + userId + ")");

          var welcomeMessage = 'You\'ve just joined the text chat.' +
              'You\' re personal id: ' + userId;

          appendChatMessage(userId, welcomeMessage);
          $('#sendBtn').removeClass('disabled').click(sendMsg);
      }

      function genConnectionDescriptor(scopeId, userId) {
          // Prepare the connection descriptor by cloning the configuration and
          // updating the URL and the token.
          var connDescriptor = $.extend({}, CONNECTION_CONFIGURATION);
          connDescriptor.scopeId = scopeId;
          connDescriptor.authDetails = ADLT.genAuth(scopeId, userId);
          connDescriptor.autopublishAudio = false;//$('#a-mute').is(':visible');
          connDescriptor.autopublishVideo = true;//$('#a-hide-off').is(':visible');
          return connDescriptor;
      }

      /**
       * ==================================================================
       * Sudo Event Handler using internal messaging system
       * ==================================================================
       */
      function triggerEvent(e, user){
        if(user){
          console.log('sending message to ' + user);
        }
        ADL.getService().sendMessage(
          ADL.r(),
          scopeId,
          JSON.stringify(e),
          parseInt(user)
        );
      }

      function receiveMessage(e){
        //recieve ADL.MessageEvent Object
        console.log('message:', e);
        //parse sent message to JSON
        var msg
        try {
          msg = JSON.parse(e.data);
        } catch(e){
          msg = {
            action:'MESSAGE',
            msg: e.data
          }
        }
        console.log('parsed:', msg);
        //instructions based on what they want us to do.
        switch(msg.action){
          case 'INTRODUCTION':
              greetUser(msg.data);
          case 'MESSAGE':
              appendChatMessage(e.srcUserId, msg.text, msg.direct)
            break;
          case 'KICK':
              disconnect();
            break;
          case 'MUTE_REQUEST':
              //mute user
              $('#a-mute').click();
              $('#a-unmute').unbind('click');
            break;
          case 'UNMUTE_REQUEST':
              RUI.bindAudioEvents();
              $('#a-unmute').click(onPublishAudioChanged).click();
  
            break;
        }
      }

      /**
       * ==================================================================
       * End of Sudo Event Handler using internal messaging system
       * ==================================================================
       */

      function introduceMyself(to) {
        //posts user details into the room and reads other users information
        var message = {
          action: 'INTRODUCTION',
          data: {
            userId: userId,
            user_name: $('#user_name').val(),
            view: viewType
          }
        };
        console.log('Introducing myself to '+to, message);
        //send a message with our description
        triggerEvent(message, to);
      }

      function greetUser(user){
        var isGreeted = Boolean(greeted.indexOf(user.userId) > -1),
            isInRoom = Boolean($('#' + clientRenderer + user.userId).length > 0);

        //do I know this user? and its not myself connecting
        console.log('Have i greeted '+user.userId+'?', isGreeted);
        console.log('Is his feed rendered?', isInRoom);

        // if we have him in the greeted and his video feed has been rendered, we'll introduce ourselves
        if(!isGreeted && isInRoom){
          // hey how are you, my name is
          console.log("Greeting user ", user.userId);
          introduceMyself(user.userId);

          //set his name tag.
          $('#' + clientRenderer + user.userId).find('.user-name-wrapper').html(user.user_name + ' ID:('+user.userId+')');  

          //both of us are clients, he's going down....
          if(viewType == 'client' && user.view == viewType){
            console.log('Im a '+viewType);
            console.log('User '+user.userId+'is a '+user.view+', removing feed');
            $('#' + clientRenderer + user.userId).remove();
          }
          greeted.push(user.userId);
          console.log('Guest List: ',greeted);
        }
        adjustClientPanes();
      }

      function adjustClientPanes() {
          if($(clientRenderContainer).children().length == 0){
            return;
          }        
          // 1. Initialize defaults
          var wrapper = {
                width : $(clientRenderContainer).width(),
                height : $(clientRenderContainer).height()
              },
              panes = $('.remote-renderer:not(#'+ rendererTmplId +')'),
              totalPanes = panes.length,
              columns = 1,
              rows = 1,
              aspect = (16 / 9), // (width / heigth) ratio.
              padding = 5,
              width, height;
              
          // 2. Define the number of columns we would need
          columns = Math.ceil( totalPanes/ 2 );
            
          // 2.1 We can never have less columns than panes
          if(columns < 2 && totalPanes > 1){
            columns = 2;
          }

          // 3. define the number of rows we would need
          if(totalPanes > 2){
            rows = 2;
          }

          wrapper.height = wrapper.height - (rows * $('.user-info').height());
          
          // 4. Calculate the given height and make the widgth relative by aspect.
          console.log('Calculating feed sizes.');
          console.log('wrapper h: ', wrapper.height, 'w: ', wrapper.width);

          width = (wrapper.width / columns);
          height = (width / aspect);
          //if it turns out the height aspect is more than the total height
          if((height * rows) > wrapper.height){
            height = (wrapper.height / rows);
            width = ((wrapper.height / rows) * aspect);
          }
          
          console.log('totalPanes: ', totalPanes, 'columns', columns, 'rows', rows, 'height: ', height , 'width: ', width);
          console.log('Adjusting feed sizes.');

          // 5. Adjust the feed sizes
          panes.width(width).height(height);//.css('margin-right', padding + 'px');
          console.log("Showing rendered feeds");
          panes.show();
      }
      /**
       * ==========================================================================
       * End of the connection management code
       * ==========================================================================
       */
      
      
       /**
        * =========================================================================
        * Chat Code
        * =========================================================================
        */
       function sendMsg() {
          var $msgInput = $('#msgInput');
          var msgContent = $msgInput.val();
          $msgInput.val('');
          var msgRecipient = parseInt($('#targetSelect').val());
          console.log('Sending new message to: ' + msgRecipient);
          if (msgRecipient === 'all') {
            msgRecipient = undefined;
          }
          var msg = JSON.stringify({
            action:'MESSAGE',
            text:msgContent,
            direct:!!msgRecipient
          });
          console.log(msg);
          ADL.getService().sendMessage(ADL.r(),scopeId, msg, msgRecipient);
          appendChatMessage(userId, msgContent);
        }

        function appendChatMessage(sender, content, direct) {
          var msgClone = $('#msgTmpl').clone();
          msgClone.find('.userid-wrapper').html(sender);
          msgClone.find('.msg-content').html(content);
          if (direct) {
            msgClone.find('.direct-indicator').removeClass('hidden');
          }
          msgClone.appendTo('#msgsSink');
        }
      /**
        * =========================================================================
        * End of Chat Code
        * =========================================================================
        */


      /**
       * Register the document ready handler.
       */
      $(onDomReady);
  })(window);