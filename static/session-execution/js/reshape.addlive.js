  /**
   * @fileoverview
   * @TODO file description
   *
   * @author Tadeusz Kozak
   * @date 26-06-2012 10:37
   */
  (function(w) {
      'use strict';
      // Scope constants
      // To set your own APP_ID (check shared-assets/scripts.js)
      var APPLICATION_ID = ADLT.APP_ID,
          APP_SHARED_SECRET = ADLT.API_KEY,
          /**
           * Configuration of the streams to publish upon connection established
           * @type {Object}
           */
          CONNECTION_CONFIGURATION = {
              videoStream: {
                  maxWidth: 1280,
                  maxHeight: 720,
                  maxFps: 24,
                  useAdaptation: true
              },
              /**
               * Flags defining that both streams should be automatically published upon
               * connection.
               */
              autopublishVideo: true,
              autopublishAudio: true
          },
          mediaConnType2Label = {};
      mediaConnType2Label[ADL.ConnectionType.NOT_CONNECTED] = 'not connected';
      mediaConnType2Label[ADL.ConnectionType.TCP_RELAY] = 'RTP/TCP relayed';
      mediaConnType2Label[ADL.ConnectionType.UDP_RELAY] = 'RTP/UDP relayed';
      mediaConnType2Label[ADL.ConnectionType.UDP_P2P] = 'RTP/UDP in P2P';
      // Scope variables
      var scopeId, 
          userId, 
          viewType, 
          trainer,
          localVideoStarted = false,
          rendererTmplId = 'rendererTmpl',
          /**
           * @type {ADL.MediaConnection}
           */
          mediaConnection;
      /**
       * Document ready callback - starts the AddLive platform initialization.
       */
      function onDomReady() {
          console.log('DOM loaded');
          // assuming the initAddLiveLogging is exposed via ADLT namespace.
          // (check shared-assets/scripts.js)
          ADLT.initAddLiveLogging();
          initUI();
          var initOptions = {
              applicationId: APPLICATION_ID,
              enableReconnects: true,
              useWebRTC : true
          };
          // Initializes the AddLive SDK. Please refer to ../shared-assets/scripts.js
          ADLT.initializeAddLiveQuick(onPlatformReady, initOptions);
      }

      function initUI() {
          console.log('Initializing the UI');
          //set the view type
          viewType = $('#viewType').val()
          $('#a-mute, #a-unmute').click(onPublishAudioChanged);
          $('#a-hide-off, #a-hide-on').click(onPublishVideoChanged);

          $('#camSelect').change(function() {
              var selectedDev = $(this).val();
              ADL.getService().setVideoCaptureDevice(ADL.r(startLocalVideoMaybe), selectedDev);
          });
          $('#micSelect').change(ADLT.getDevChangedHandler('AudioCapture'));
          $('#spkSelect').change(ADLT.getDevChangedHandler('AudioOutput'));
          console.log('UI initialized');
      }

      function onPlatformReady() {
          console.log('==============================================================');
          console.log('==============================================================');
          console.log('AddLive SDK ready - setting up the application');
          // assuming the populateDevicesQuick is exposed via ADLT namespace.
          // (check shared-assets/scripts.js)
          ADLT.populateDevicesQuick();
          startLocalVideoMaybe();
          initServiceListener();
      }
      /**
       * ==========================================================================
       * Beginning of the AddLive service events handling code
       * ==========================================================================
       */
      function initServiceListener() {
          console.log('Initializing the AddLive Service Listener');
          // 1. Instantiate the listener
          var listener = new ADL.AddLiveServiceListener();
          // 2. Define the handler for the user event
          listener.onUserEvent = function(e) {
              console.log('Got new user event: ' + e.userId);
              if (e.isConnected) {
                  onUserJoined(e);
                  
              } else {
                  console.log('User with id: ' + e.userId + ' left the media scope');

                  //remove the direct message option
                  $('#user' + e.userId).remove();
                  appendMessage(e.userId, 'User with id ' + e.userId + ' just left ' +
                      'the chat');
                  $('#clientRenderingWidget' + e.userId).html('').remove();
              }

              if (e.isConnected) {
                
              } else {
                
              }
          };
          // 3. Define the handler for streaming status changed event
          listener.onMediaStreamEvent = function(e) {
              console.log('Got new media streaming status changed event');
              switch (e.mediaType) {
                  case ADL.MediaType.AUDIO:
                      onRemoteAudioStreamStatusChanged(e);
                      break;
                  case ADL.MediaType.VIDEO:
                      onRemoteVideoStreamStatusChanged(e);
                      break;
                  default:
                      console.warn('Got unsupported media type in media stream event: ' + e.mediaType);
              }
          };
          // 4. Define the handler for the media connection type changed event
          listener.onMediaConnTypeChanged = function(e) {
              console.log('Got new media connection type: ' + e.connectionType);
              $('#connTypeLbl').html(mediaConnType2Label[e.connectionType]);
          };
          // 5. Define the handler for the connection lost event
          listener.onConnectionLost = function(e) {
              console.warn('Got connection lost notification: ' + JSON.stringify(e));
              disconnectHandler();
          };
          // 6. Define the handler for the reconnection event
          listener.onSessionReconnected = function(e) {
              console.log('Connection successfully reestablished!');
              postConnectHandler();
          };

          //6.1 listener for sent messages
          listener.onMessage = function(e){
              console.log('Got message from user');
              receiveMessage(e);
          }

          // 7. Prepare the success handler
          var onSucc = function() {
              console.log('AddLive service listener registered');
              $('#a-playsession').click(connect);
          };
          // 8. Finally register the AddLive Service Listener
          ADL.getService().addServiceListener(ADL.r(onSucc), listener);
      }

      function onUserJoined(e) {
          console.log('Got new user with id: ' + e.userId);
          console.log('User: ', e);
          // 1. Prepare a rendering widget for the user.
          var renderer = $('#rendererTmpl').clone().hide();
          renderer.attr('id', 'clientRenderingWidget' + e.userId);
          renderer.find('.render-wrapper').attr('id', 'renderer' + e.userId);
          renderer.find('.user-id-wrapper').html(e.userId);
          // 2. Append it to the rendering area.
          $('#clientRenderingWrapper').prepend(renderer);
          //tell them who is in the room
          sendRoomInformation();
          
          if (e.videoPublished) {
              // 3a. Render the sink if the video stream is being published.
              ADL.renderSink({
                  sinkId: e.videoSinkId,
                  containerId: 'renderer' + e.userId
              });
          } else {
              // 3b. Just show the no video stream published indicator.
              renderer.find('.no-video-text').show();
              renderer.find('.allowReceiveVideoChckbx').hide();
          }
          // 4. Show the 'audio muted' indicator if user does not publish audio stream
          if (!e.audioPublished) {
              renderer.find('.muted-indicator').show();
              renderer.find('.allowReceiveAudioChckbx').hide();
          }

          renderer.find('.mute-user').click(function(){
            sendMessage({'action':'MUTE_REQUEST'}, e.userId);
            $(this).hide();
            renderer.find('.unmute-user').show();
          });

          renderer.find('.unmute-user').click(function(){
            sendMessage({'action':'UNMUTE_REQUEST'}, e.userId);
            $(this).hide();
            renderer.find('.mute-user').show();
          });

          renderer.find('.kick-user').click(function(){
            sendMessage({'action':'KICK'}, e.userId);
            renderer.remove();
          });

          //add trainers to direct message!
          if(viewType == 'trainer'){
            $('<option id="user' + e.userId + '" value="' + e.userId + '">User ' +
                e.userId + '</option>').appendTo($('#targetSelect'));
            appendMessage(e.userId, 'User with id ' + e.userId + ' just joined ' +
                'the chat');
          }
      }

      function onRemoteVideoStreamStatusChanged(e) {
          console.log('Got change in video streaming for user with id: ' + e.userId + ' user just ' + (e.videoPublished ? 'published' : 'stopped publishing') + ' the stream');
          // 1. Grab the rendering widget corresponding to the user
          var renderingWidget = $('#clientRenderingWidget' + e.userId);
          if (e.videoPublished) {
              // 2a. If video was just published - render it and hide the
              // 'No video from user' indicator
              ADL.renderSink({
                  sinkId: e.videoSinkId,
                  containerId: 'renderer' + e.userId
              });
              renderingWidget.find('.no-video-text').hide();
          } else {
              // 2b. If video was just unpublished - clear the renderer and show the
              // 'No video from user' indicator
              renderingWidget.find('.render-wrapper').empty();
              renderingWidget.find('.no-video-text').show();
          }
      }

      function onRemoteAudioStreamStatusChanged(e) {
          console.log('Got change in audio streaming for user with id: ' + e.userId + ' user just ' + (e.audioPublished ? 'published' : 'stopped publishing') + ' the stream');
          // 1. Find the 'Audio is muted' indicator corresponding to the user
          var muteIndicator = $('#clientRenderingWidget' + e.userId).find('.muted-indicator');
          if (e.audioPublished) {
              // 2a. Hide it if audio stream was just published
              muteIndicator.hide();
          } else {
              // 2.b Show it if audio was just unpublished
              muteIndicator.show();
          }
      }
      /**
       * ==========================================================================
       * End of the AddLive service events handling code
       * ==========================================================================
       */
      function startLocalVideoMaybe() {
          if (localVideoStarted) {
              return;
          }
          console.log('Starting local preview of current user');
          // 1. Define the result handler
          var resultHandler = function(sinkId) {
              console.log('Local preview started. Rendering the sink with id: ' + sinkId);
              ADL.renderSink({
                  sinkId: sinkId,
                  containerId: 'renderLocalPreview',
                  mirror: true
              });
              localVideoStarted = true;
          };
          // 2. Request the SDK to start capturing local user's preview
          ADL.getService().startLocalVideo(ADL.r(resultHandler));
      }
      /**
       * ==========================================================================
       * Beginning of the connection management code
       * ==========================================================================
       */
      function connect() {
          console.log('Establishing a connection to the AddLive Streaming Server');
          // 1. Disable the connect button to avoid a cascade of connect requests
          $('#a-playsession').unbind('click');
          // 2. Get the scope id and generate the user id.
          scopeId = $('#scopeIdTxtField').val();
          // assuming the genRandomUserId is exposed via ADLT namespace.
          // (check shared-assets/scripts.js)
          userId = ADLT.genRandomUserId();
          // 3. Define the result handler - delegates the processing to
          // the postConnectHandler
          var connDescriptor = genConnectionDescriptor(scopeId, userId);
          /**
           *
           * @param {ADL.MediaConnection} mConn
           */
          var onSucc = function(mConn) {
              mediaConnection = mConn;
              postConnectHandler();
              sendUserInfo();
          };
          // 4. Define the error handler - enabled the connect button again
          var onErr = function() {
              $('#a-playsession').click(connect);
              $('#li-playsession').show();
              $('#a-stopsession').unbind('click');
              $('#li-stopsession').hide();
          };
          // 5. Request the SDK to establish a connection
          ADL.getService().connect(ADL.r(onSucc, onErr), connDescriptor);
      }

      function disconnect() {
          console.log('Terminating a connection to the AddLive Streaming Server');
          // 1. Define the result handler
          var succHandler = function() {
              scopeId = undefined;
              userId = undefined;
              mediaConnection = undefined;
              disconnectHandler();
          };
          mediaConnection.disconnect(ADL.r(succHandler));
      }
      /**
       * Common post disconnect handler - used when user explicitly terminates the
       * connection or if the connection gets terminated due to the networking issues.
       *
       * It just resets the UI to the default state.
       */
      function disconnectHandler() {
          // 1. Toggle the active state of the Connect/Disconnect buttons
          $('#a-playsession').click(connect);
          $("#li-playsession").css('display', 'block');
          // 1.1 Disable the stop session button
          $('#a-stopsession').unbind('click');
          $("#li-stopsession").css('display', 'none');
          // 2. Reset the connection type label
          $('#connTypeLbl').html('none');
          // 3. Clear the remote user renderers
          $('#clientRenderingWrapper').find('.remote-renderer').html('').remove();
          // 4. Clear the local user id label
          $('#localUserIdLbl').html('Not Connected');
      }
      /**
       * Common post connect handler - used when user manually establishes the
       * connection or connection is being reestablished after being lost due to the
       * Internet connectivity issues.
       *
       */
      function postConnectHandler() {
          console.log('Connected. Disabling connect button and enabling the disconnect');
          // 1. Enable the disconnect button
          $('#a-stopsession').click(disconnect);
          $("#li-stopsession").css('display', 'block');
          // 2. Disable the connect button
          $('#a-playsession').unbind('click')
          $("#li-playsession").css('display', 'none');
          // 3. Update the local user id label
          $('#localUserIdLbl').html("User Id: (" + userId + ")");

          var welcomeMessage = 'You\'ve just joined the text chat.' +
              'You\' re personal id: ' + userId;

          appendMessage(userId, welcomeMessage);
          $('#sendBtn').removeClass('disabled').click(sendMsg);
      }

      function genConnectionDescriptor(scopeId, userId) {
          // Prepare the connection descriptor by cloning the configuration and
          // updating the URL and the token.
          var connDescriptor = $.extend({}, CONNECTION_CONFIGURATION);
          connDescriptor.scopeId = scopeId;
          connDescriptor.authDetails = ADLT.genAuth(scopeId, userId);
          connDescriptor.autopublishAudio = $('#a-mute').is(':visible');
          connDescriptor.autopublishVideo = $('#a-hide-off').is(':visible');
          return connDescriptor;
      }

      function sendMessage(msg, user){
        if(user){
          console.log('sending message to ' + user);
        }
        ADL.getService().sendMessage(
          ADL.r(),
          scopeId,
          JSON.stringify(msg),
          parseInt(user)
        );
      }

      function receiveMessage(e){
        //recieve ADL.MessageEvent Object
        console.log('message:', e);
        //parse sent message to JSON
        var msg
        try {
          msg = JSON.parse(e.data);
        } catch(e){
          msg = {
            action:'MESSAGE',
            msg: e.data
          }
        }
        console.log('parsed:', msg);
        //instructions based on what they want us to do.
        switch(msg.action){
          case 'USER_JOINED':
              //a new user has just connected.
              setupConnectedUser(msg);
          case 'CONNECTED':
              //we just connected, setup the other screens.
              setupRoom(msg);
            break;
          case 'MESSAGE':
              appendMessage(e.srcUserId, msg.text, msg.direct)
            break;
          case 'KICK':
              disconnect();
              //clear all feeds
              //$('#clientRenderingWrapper').html('');
            break;
          case 'MUTE_REQUEST':
              //mute user
              $('#a-mute').click();
            break;
          case 'UNMUTE_REQUEST':
            $('#a-unmute').click();
            break;
        }
        adjustClientPanes();
      }

      function sendUserInfo() {
        //posts user details into the room and reads other users information
        var message = {
          'action': 'USER_JOINED',
          'userId': userId,
          'user_name':$('#userName').val(),
          'view': viewType,
        };
        if(viewType == 'trainer'){
          trainer = userId;
        }
        console.log('action message: ', message);
        //send a message with our description
        sendMessage(message);        
      }

      function sendRoomInformation(){
        // tell the person that just connected who the trainer is
        if(viewType == 'trainer'){
          var message = {
            'action': 'CONNECTED',
            'trainer': trainer,
            'trainer_name' : $('#userName').val(),
          };
          sendMessage(message);
          adjustClientPanes();
        }
      }

      function setupConnectedUser(user){
        switch(user.view){
          case 'client':
              console.log('client connected with userId: ' + user.userId);
              $('#clientRenderingWidget' + user.userId).find('.user-name-wrapper').html(user.user_name);
              //what do we do when we're connected?
            break;
          case 'trainer':
              console.log('trainer connected with userId: ' + user.userId);
              $('#clientRenderingWidget' + user.userId).find('.user-name-wrapper').html(user.user_name);
              //what do we do when we're connected?
            break;
        }
      }

      function setupRoom(msg){
        console.log('setting up my room');
        switch(viewType){
          case 'client':
              var trainerId = msg.trainer,
              allowedSenders = [];
              console.log('trainerId ', trainerId);
              // foreach client connected
              $('.remote-renderer:not('+rendererTmplId+') .user-id-wrapper').each(function(){
                  //connected user id parsed as an int
                  var user = parseInt($(this).html());
                  
                  //to the valid user intger and non-trainer we bid thee farewell
                  if(user === trainerId && !isNaN(user)){
                    //set the trainer as the only allowed
                    allowedSenders.push(user);
                    $('#clientRenderingWidget'+user).find('.user-name-wrapper').html(msg.trainer_name);
                    //add him as the sole value for messaging
                    $('#targetSelect').html($('<option id="user' + user + '" value="' + user + '">User ' +
                      user + '</option>'));
                    
                    appendMessage(userId, 'User with id ' + userId + ' just joined ' +
                      'the chat');
                  } else if(!isNaN(user) && trainerId !== undefined){
                    console.log(trainerId);
                    console.log(user + ' is not a trainer');
                    $('#clientRenderingWidget'+user).remove();
                  }
              });
              //show the remaining users
              if(allowedSenders.length > 0){
                ADL.getService().setAllowedSenders(ADL.r(), scopeId,
                                          ADL.MediaType.AUDIO, allowedSenders);
                ADL.getService().setAllowedSenders(ADL.r(), scopeId,
                                          ADL.MediaType.VIDEO, allowedSenders);
              }
            break;
          case 'trainer':
            //everything goes!
            break;
        }
      }

      function adjustClientPanes() {
          var panes = $('.remote-renderer:not(#'+ rendererTmplId +')'),
              totalPanes = panes.length,
              columns = 1,
              rows = 1,
              aspect = (4 / 3),
              padding = 5,
              width, height;
          // Define the number of columns we would need
          if (2 <= totalPanes && totalPanes <= 4) {
              columns = 2;
          } else if (5 <= totalPanes && totalPanes <= 6) {
              columns = 3;
          } else if (7 <= totalPanes && totalPanes <= 8) {
              columns = 4;
          } else if (9 <= totalPanes && totalPanes <= 10) {
              columns = 5;
          } else if (11 <= totalPanes && totalPanes <= 12) {
              columns = 6;
          }
          //define the number of rows we would need
          if(totalPanes > 2){
            rows = 2;
          }
          // if (3 <= totalPanes && totalPanes <= 6) {
          //     rows = 2;
          // } else if (7 <= totalPanes && totalPanes <= 12) {
          //     rows = 3;
          // }
          //the total amount of space available by the number of columns
          width = (($('#clientRenderingWrapper').width() / columns) - (padding * 2));
          //keep an aspect ratio here.
          height = (($(window).height() - $('#video-controls').height() - ($('.trainer-wrapper').height() + 10)) / rows) /// (width / aspect);
          panes.width(width).height(height).css('float', 'left').css('margin-right', padding + 'px').css('position','relative');
          $('.remote-renderer:not('+rendererTmplId+')').show();
      }
      /**
       * ==========================================================================
       * End of the connection management code
       * ==========================================================================
       */
      /**
       * ==========================================================================
       * Beginning of the user's events handling code
       * ==========================================================================
       */
      /**
       * Handles the change of the 'Publish Audio' checkbox
       */
      function onPublishAudioChanged() {
          if (!scopeId) {
              // If the scope id is not defined, it means that we're not connected and
              // thus there is nothing to do here.
              return;
          }
          // Since we're connected we need to either start or stop publishing the
          // audio stream, depending on the new state of the checkbox
          if ($('#a-unmute').is(':visible')) {
              console.log('Audio is muted');
              mediaConnection.publishAudio(ADL.r());
          } else {
              console.log('Audio is un-muted');
              mediaConnection.unpublishAudio(ADL.r());
          }
      }
      /**
       * Handles the change of the 'Publish Audio' checkbox
       */
      function onPublishVideoChanged() {
          if (!scopeId) {
              // If the scope id is not defined, it means that we're not connected and
              // thus there is nothing to do here.
              return;
          }
          // Since we're connected we need to either start or stop publishing the
          // audio stream, depending on the new state of the checkbox
          // @todo replace with unhide element class
          if ($('#a-hide-on').is(':visible')) {
              console.log('showing video');
              mediaConnection.publishVideo(ADL.r());
          } else {
              console.log('hiding video');
              mediaConnection.unpublishVideo(ADL.r());
          }
      }
      /**
       * ==========================================================================
       * End of the user's events handling code
       * ==========================================================================
       */
      
       /**
        * =========================================================================
        * Chat Code
        * =========================================================================
        */
       function sendMsg() {
          var $msgInput = $('#msgInput');
          var msgContent = $msgInput.val();
          $msgInput.val('');
          var msgRecipient = parseInt($('#targetSelect').val());
          console.log('Sending new message to: ' + msgRecipient);
          if (msgRecipient === 'all') {
            msgRecipient = undefined;
          }
          var msg = JSON.stringify({
            action:'MESSAGE',
            text:msgContent,
            direct:!!msgRecipient
          });
          console.log(msg);
          ADL.getService().sendMessage(ADL.r(),scopeId, msg, msgRecipient);
          appendMessage(userId, msgContent);
        }

        function appendMessage(sender, content, direct) {
          var msgClone = $('#msgTmpl').clone();
          msgClone.find('.userid-wrapper').html(sender);
          msgClone.find('.msg-content').html(content);
          if (direct) {
            msgClone.find('.direct-indicator').removeClass('hidden');
          }
          msgClone.appendTo('#msgsSink');
        }
      /**
        * =========================================================================
        * End of Chat Code
        * =========================================================================
        */


      /**
       * Register the document ready handler.
       */
      $(onDomReady);
      $(window).resize(function(){
        adjustClientPanes();
      });
  })(window);