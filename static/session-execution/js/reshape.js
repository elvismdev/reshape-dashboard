// JavaScript Document

  (function(w){
    
    'use strict';
    var RUI = w.RUI || {};
    /**
     * Mute un mute bingings.
     * @return {[type]} [description]
    */
    RUI.bindAudioEvents = function(){

      // Mute bindings
      $('#a-mute').mouseover(function() {
        $("#icon-mute").attr('src','img/' + $('#theme_name').val() + '/icon-mute-on.png');
      });

      $('#a-mute').mouseout(function() {
        $("#icon-mute").attr('src','img/' + $('#theme_name').val() + '/icon-mute-off.png');
      });

      $('#a-mute').click(function() {
        $("#li-mute").css('display','none');
        $("#li-unmute").css('display','block');
      });

      // Un-mute bindings
      $('#a-unmute').mouseover(function() {
        $("#icon-unmute").attr('src','img/' + $('#theme_name').val() + '/icon-unmute-on.png');
      });

      $('#a-unmute').mouseout(function() {
        $("#icon-unmute").attr('src','img/' + $('#theme_name').val() + '/icon-unmute-off.png');
      });

      $('#a-unmute').click(function() {
        $("#li-unmute").css('display','none');
        $("#li-mute").css('display','block');
        //handler_sound_unmute();
      });
    };

    /**
     * Video Icon Bindings
     * @return {[type]} [description]
     */
    RUI.bindVideoEvents = function(){
      $('#a-hide-off').mouseover(function() {
        $("#icon-hide-off").attr('src','img/' + $('#theme_name').val() + '/icon-hide-on.png');
      } );
      $('#a-hide-off').mouseout(function() {
        $("#icon-hide-off").attr('src','img/' + $('#theme_name').val() + '/icon-hide-off.png');
      } );

      $('#a-hide-off').click(function(){
        $("#li-hide-off").hide();
        $('#li-hide-on').show();
      });

      $('#a-hide-on').mouseover(function() {
        $("#icon-hide-on").attr('src','img/' + $('#theme_name').val() + '/icon-hide-on.png');
      } );
      $('#a-hide-on').mouseout(function() {
        $("#icon-hide-on").attr('src','img/' + $('#theme_name').val() + '/icon-hide-off.png');
      } );

      $('#a-hide-on').click(function(){
        $("#li-hide-on").hide();
        $('#li-hide-off').show();
      });
    };
    w.RUI = RUI;
  })(window);

  // TIMER FUNCTIONS ===========================================================
  function timer_startstop() {
    alert('timer');
  }
  // ===========================================================================
  // Toggle Chat
  function toggleChat(){
    var chat = $('.chat-screen');
    if(chat.is(':visible')){
      chat.unbind('mouseover');
      chat.unbind('mouseout');
      chat.parent().find('.chat-widget-handle').show();
      chat.hide();
    } else {
      chat.parent().find('.chat-widget-handle').hide();
      chat.show();

      chat.mouseover(function(){
        $(this).parent().css('opacity',1);
      });
      chat.mouseout(function(){
        $(this).parent().css('opacity',$(this).css('opacity'));
      })
    }
  }
  
  // SCRIPT STYLE BINDING =====================================================
  $(document).ready(function(){
    $("form").submit(function(){return false;});
    $('.toggle-chat').click(toggleChat);

    $('#a-playsession').mouseover(function() {
      $("#icon-playsession").attr('src','img/' + $('#theme_name').val() + '/icon-playsession-on.png');
    } );
    $('#a-playsession').mouseout(function() {
      $("#icon-playsession").attr('src','img/' + $('#theme_name').val() + '/icon-playsession-off.png');
    } );
    $('#a-playsession').click(function() {
      return;
      $("#li-playsession").css('display','none');
      $("#li-stopsession").css('display','block');
      handler_sessions_playsession();
    } );

    $('#a-stopsession').mouseover(function() {
      $("#icon-stopsession").attr('src','img/' + $('#theme_name').val() + '/icon-stopsession-on.png');
    } );
    $('#a-stopsession').mouseout(function() {
      $("#icon-stopsession").attr('src','img/' + $('#theme_name').val() + '/icon-stopsession-off.png');
    } );
    $('#a-stopsession').click(function() {
      return;
      $("#li-stopsession").css('display','none');
      $("#li-playsession").css('display','block');
      handler_sessions_stopsession();
    } );

    $('#a-timer').mouseover(function() {
      $("#icon-timer").attr('src','img/' + $('#theme_name').val() + '/icon-timer-on.png');
    } );
    $('#a-timer').mouseout(function() {
      $("#icon-timer").attr('src','img/' + $('#theme_name').val() + '/icon-timer-off.png');
    } );
    $('#a-timer').click(function() {
      timer_startstop();
    } );

    $('#a-settings').mouseover(function() {
      $("#icon-settings").attr('src','img/' + $('#theme_name').val() + '/icon-settings-on.png');
    } );
    $('#a-settings').mouseout(function() {
      $("#icon-settings").attr('src','img/' + $('#theme_name').val() + '/icon-settings-off.png');
    } );
    $('#a-settings').click(function() {
		  handler_settings();
    });
  });
  // ===========================================================================
