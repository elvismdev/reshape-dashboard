<!-- Local Feed Renderer -->
<div id="renderingWrapper" class="trainer-wrapper">
  <div class="render-widget">
    <div id="renderLocalPreview" class="render-wrapper"></div>
    <span id="localUserIdLbl" class="person-name"><?php echo $user_name; ?></span>
  </div>
</div>