<!-- Top bar navigation menu. contains branch sction and controls -->
<nav id="video-controls" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand">
        <img src="img/dark/ReShape-Logo.png" class="logo image-responsive">
      </a>
    </div>
    <!-- Brand logo and heading -->
    <?php include("navbar-top/brand.php");?>
    <!-- Video Controls -->
    <?php include("controls.php");?>
</nav>